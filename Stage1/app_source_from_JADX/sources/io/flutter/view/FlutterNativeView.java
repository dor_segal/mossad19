package io.flutter.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;
import io.flutter.app.FlutterPluginRegistry;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.BinaryMessenger.BinaryMessageHandler;
import io.flutter.plugin.common.BinaryMessenger.BinaryReply;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

public class FlutterNativeView implements BinaryMessenger {
    private static final String TAG = "FlutterNativeView";
    private boolean applicationIsRunning;
    private final Context mContext;
    private FlutterView mFlutterView;
    private final Map<String, BinaryMessageHandler> mMessageHandlers;
    private long mNativePlatformView;
    private int mNextReplyId;
    private final Map<Integer, BinaryReply> mPendingReplies;
    private final FlutterPluginRegistry mPluginRegistry;

    private static native long nativeAttach(FlutterNativeView flutterNativeView, boolean z);

    private static native void nativeDestroy(long j);

    private static native void nativeDetach(long j);

    private static native void nativeDispatchEmptyPlatformMessage(long j, String str, int i);

    private static native void nativeDispatchPlatformMessage(long j, String str, ByteBuffer byteBuffer, int i, int i2);

    private static native String nativeGetObservatoryUri();

    private static native void nativeInvokePlatformMessageEmptyResponseCallback(long j, int i);

    private static native void nativeInvokePlatformMessageResponseCallback(long j, int i, ByteBuffer byteBuffer, int i2);

    private static native void nativeRunBundleAndSnapshotFromLibrary(long j, String str, String str2, String str3, String str4, AssetManager assetManager);

    public FlutterNativeView(Context context) {
        this(context, false);
    }

    public FlutterNativeView(Context context, boolean isBackgroundView) {
        this.mNextReplyId = 1;
        this.mPendingReplies = new HashMap();
        this.mContext = context;
        this.mPluginRegistry = new FlutterPluginRegistry(this, context);
        attach(this, isBackgroundView);
        assertAttached();
        this.mMessageHandlers = new HashMap();
    }

    public void detach() {
        this.mPluginRegistry.detach();
        this.mFlutterView = null;
        nativeDetach(this.mNativePlatformView);
    }

    public void destroy() {
        this.mPluginRegistry.destroy();
        this.mFlutterView = null;
        nativeDestroy(this.mNativePlatformView);
        this.mNativePlatformView = 0;
        this.applicationIsRunning = false;
    }

    public FlutterPluginRegistry getPluginRegistry() {
        return this.mPluginRegistry;
    }

    public void attachViewAndActivity(FlutterView flutterView, Activity activity) {
        this.mFlutterView = flutterView;
        this.mPluginRegistry.attach(flutterView, activity);
    }

    public boolean isAttached() {
        return this.mNativePlatformView != 0;
    }

    public long get() {
        return this.mNativePlatformView;
    }

    public void assertAttached() {
        if (!isAttached()) {
            throw new AssertionError("Platform view is not attached");
        }
    }

    public void runFromBundle(FlutterRunArguments args) {
        if (args.bundlePath == null) {
            throw new AssertionError("A bundlePath must be specified");
        } else if (args.entrypoint != null) {
            runFromBundleInternal(args.bundlePath, args.entrypoint, args.libraryPath, args.defaultPath);
        } else {
            throw new AssertionError("An entrypoint must be specified");
        }
    }

    @Deprecated
    public void runFromBundle(String bundlePath, String defaultPath, String entrypoint, boolean reuseRuntimeController) {
        runFromBundleInternal(bundlePath, entrypoint, null, defaultPath);
    }

    private void runFromBundleInternal(String bundlePath, String entrypoint, String libraryPath, String defaultPath) {
        assertAttached();
        if (this.applicationIsRunning) {
            throw new AssertionError("This Flutter engine instance is already running an application");
        }
        nativeRunBundleAndSnapshotFromLibrary(this.mNativePlatformView, bundlePath, defaultPath, entrypoint, libraryPath, this.mContext.getResources().getAssets());
        this.applicationIsRunning = true;
    }

    public boolean isApplicationRunning() {
        return this.applicationIsRunning;
    }

    public static String getObservatoryUri() {
        return nativeGetObservatoryUri();
    }

    public void send(String channel, ByteBuffer message) {
        send(channel, message, null);
    }

    public void send(String channel, ByteBuffer message, BinaryReply callback) {
        if (isAttached()) {
            int replyId = 0;
            if (callback != null) {
                int i = this.mNextReplyId;
                this.mNextReplyId = i + 1;
                replyId = i;
                this.mPendingReplies.put(Integer.valueOf(replyId), callback);
            }
            if (message == null) {
                nativeDispatchEmptyPlatformMessage(this.mNativePlatformView, channel, replyId);
            } else {
                nativeDispatchPlatformMessage(this.mNativePlatformView, channel, message, message.position(), replyId);
            }
            return;
        }
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("FlutterView.send called on a detached view, channel=");
        stringBuilder.append(channel);
        Log.d(str, stringBuilder.toString());
    }

    public void setMessageHandler(String channel, BinaryMessageHandler handler) {
        if (handler == null) {
            this.mMessageHandlers.remove(channel);
        } else {
            this.mMessageHandlers.put(channel, handler);
        }
    }

    private void attach(FlutterNativeView view, boolean isBackgroundView) {
        this.mNativePlatformView = nativeAttach(view, isBackgroundView);
    }

    private void handlePlatformMessage(final String channel, byte[] message, final int replyId) {
        assertAttached();
        BinaryMessageHandler handler = (BinaryMessageHandler) this.mMessageHandlers.get(channel);
        if (handler != null) {
            ByteBuffer buffer;
            if (message == null) {
                buffer = null;
            } else {
                try {
                    buffer = ByteBuffer.wrap(message);
                } catch (Exception ex) {
                    Log.e(TAG, "Uncaught exception in binary message listener", ex);
                    nativeInvokePlatformMessageEmptyResponseCallback(this.mNativePlatformView, replyId);
                }
            }
            handler.onMessage(buffer, new BinaryReply() {
                private final AtomicBoolean done = new AtomicBoolean(0);

                public void reply(ByteBuffer reply) {
                    if (!FlutterNativeView.this.isAttached()) {
                        String str = FlutterNativeView.TAG;
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("handlePlatformMessage replying to a detached view, channel=");
                        stringBuilder.append(channel);
                        Log.d(str, stringBuilder.toString());
                    } else if (this.done.getAndSet(true)) {
                        throw new IllegalStateException("Reply already submitted");
                    } else {
                        if (reply == null) {
                            FlutterNativeView.nativeInvokePlatformMessageEmptyResponseCallback(FlutterNativeView.this.mNativePlatformView, replyId);
                        } else {
                            FlutterNativeView.nativeInvokePlatformMessageResponseCallback(FlutterNativeView.this.mNativePlatformView, replyId, reply, reply.position());
                        }
                    }
                }
            });
            return;
        }
        nativeInvokePlatformMessageEmptyResponseCallback(this.mNativePlatformView, replyId);
    }

    private void handlePlatformMessageResponse(int replyId, byte[] reply) {
        BinaryReply callback = (BinaryReply) this.mPendingReplies.remove(Integer.valueOf(replyId));
        if (callback != null) {
            ByteBuffer byteBuffer;
            if (reply == null) {
                byteBuffer = null;
            } else {
                try {
                    byteBuffer = ByteBuffer.wrap(reply);
                } catch (Exception ex) {
                    Log.e(TAG, "Uncaught exception in binary message reply handler", ex);
                    return;
                }
            }
            callback.reply(byteBuffer);
        }
    }

    private void updateSemantics(ByteBuffer buffer, String[] strings) {
        if (this.mFlutterView != null) {
            this.mFlutterView.updateSemantics(buffer, strings);
        }
    }

    private void updateCustomAccessibilityActions(ByteBuffer buffer, String[] strings) {
        if (this.mFlutterView != null) {
            this.mFlutterView.updateCustomAccessibilityActions(buffer, strings);
        }
    }

    private void onFirstFrame() {
        if (this.mFlutterView != null) {
            this.mFlutterView.onFirstFrame();
        }
    }

    private void onPreEngineRestart() {
        if (this.mFlutterView != null) {
            this.mFlutterView.resetAccessibilityTree();
        }
        if (this.mPluginRegistry != null) {
            this.mPluginRegistry.onPreEngineRestart();
        }
    }
}
