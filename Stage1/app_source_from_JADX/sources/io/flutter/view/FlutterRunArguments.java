package io.flutter.view;

public class FlutterRunArguments {
    public String bundlePath;
    public String defaultPath;
    public String entrypoint;
    public String libraryPath;
}
