package io.flutter.view;

public final class FlutterCallbackInformation {
    public final String callbackClassName;
    public final String callbackLibraryPath;
    public final String callbackName;

    private static native FlutterCallbackInformation nativeLookupCallbackInformation(long j);

    public static FlutterCallbackInformation lookupCallbackInformation(long handle) {
        return nativeLookupCallbackInformation(handle);
    }

    private FlutterCallbackInformation(String callbackName, String callbackClassName, String callbackLibraryPath) {
        this.callbackName = callbackName;
        this.callbackClassName = callbackClassName;
        this.callbackLibraryPath = callbackLibraryPath;
    }
}
