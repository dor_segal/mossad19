package io.flutter.view;

import android.view.Choreographer;
import android.view.Choreographer.FrameCallback;

public class VsyncWaiter {
    public static long refreshPeriodNanos = 16666666;

    private static native void nativeOnVsync(long j, long j2, long j3);

    public static void asyncWaitForVsync(final long cookie) {
        Choreographer.getInstance().postFrameCallback(new FrameCallback() {
            public void doFrame(long frameTimeNanos) {
                VsyncWaiter.nativeOnVsync(frameTimeNanos, frameTimeNanos + VsyncWaiter.refreshPeriodNanos, cookie);
            }
        });
    }
}
