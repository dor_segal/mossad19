package io.flutter.view;

import android.app.Activity;
import android.graphics.Rect;
import android.opengl.Matrix;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeInfo.AccessibilityAction;
import android.view.accessibility.AccessibilityNodeProvider;
import com.iwalk.locksmither.BuildConfig;
import io.flutter.plugin.common.BasicMessageChannel;
import io.flutter.plugin.common.BasicMessageChannel.MessageHandler;
import io.flutter.plugin.common.BasicMessageChannel.Reply;
import io.flutter.plugin.common.StandardMessageCodec;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

class AccessibilityBridge extends AccessibilityNodeProvider implements MessageHandler<Object> {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final int ACTION_SHOW_ON_SCREEN = 16908342;
    private static final int ROOT_NODE_ID = 0;
    private static final float SCROLL_EXTENT_FOR_INFINITY = 100000.0f;
    private static final float SCROLL_POSITION_CAP_FOR_INFINITY = 70000.0f;
    private static final String TAG = "FlutterView";
    static int firstResourceId = 267386881;
    private SemanticsObject mA11yFocusedObject;
    private boolean mAccessibilityEnabled = false;
    private Map<Integer, CustomAccessibilityAction> mCustomAccessibilityActions;
    private final View mDecorView;
    private final BasicMessageChannel<Object> mFlutterAccessibilityChannel;
    private SemanticsObject mHoveredObject;
    private SemanticsObject mInputFocusedObject;
    private Integer mLastLeftFrameInset = Integer.valueOf(ROOT_NODE_ID);
    private Map<Integer, SemanticsObject> mObjects;
    private final FlutterView mOwner;
    private int previousRouteId = ROOT_NODE_ID;
    private List<Integer> previousRoutes;

    enum Action {
        TAP(1),
        LONG_PRESS(2),
        SCROLL_LEFT(4),
        SCROLL_RIGHT(8),
        SCROLL_UP(16),
        SCROLL_DOWN(32),
        INCREASE(64),
        DECREASE(128),
        SHOW_ON_SCREEN(256),
        MOVE_CURSOR_FORWARD_BY_CHARACTER(512),
        MOVE_CURSOR_BACKWARD_BY_CHARACTER(1024),
        SET_SELECTION(2048),
        COPY(4096),
        CUT(8192),
        PASTE(16384),
        DID_GAIN_ACCESSIBILITY_FOCUS(32768),
        DID_LOSE_ACCESSIBILITY_FOCUS(65536),
        CUSTOM_ACTION(131072),
        DISMISS(262144),
        MOVE_CURSOR_FORWARD_BY_WORD(524288),
        MOVE_CURSOR_BACKWARD_BY_WORD(1048576);
        
        final int value;

        private Action(int value) {
            this.value = value;
        }
    }

    private class CustomAccessibilityAction {
        String hint;
        int id = -1;
        String label;
        int overrideId = -1;
        int resourceId = -1;

        CustomAccessibilityAction() {
        }

        boolean isStandardAction() {
            return this.overrideId != -1;
        }
    }

    enum Flag {
        HAS_CHECKED_STATE(1),
        IS_CHECKED(2),
        IS_SELECTED(4),
        IS_BUTTON(8),
        IS_TEXT_FIELD(16),
        IS_FOCUSED(32),
        HAS_ENABLED_STATE(64),
        IS_ENABLED(128),
        IS_IN_MUTUALLY_EXCLUSIVE_GROUP(256),
        IS_HEADER(512),
        IS_OBSCURED(1024),
        SCOPES_ROUTE(2048),
        NAMES_ROUTE(4096),
        IS_HIDDEN(8192),
        IS_IMAGE(16384),
        IS_LIVE_REGION(32768),
        HAS_TOGGLED_STATE(65536),
        IS_TOGGLED(131072),
        HAS_IMPLICIT_SCROLLING(262144);
        
        final int value;

        private Flag(int value) {
            this.value = value;
        }
    }

    private class SemanticsObject {
        static final /* synthetic */ boolean $assertionsDisabled = false;
        int actions;
        private float bottom;
        List<SemanticsObject> childrenInHitTestOrder;
        List<SemanticsObject> childrenInTraversalOrder;
        List<CustomAccessibilityAction> customAccessibilityActions;
        String decreasedValue;
        int flags;
        private boolean globalGeometryDirty = true;
        private Rect globalRect;
        private float[] globalTransform;
        boolean hadPreviousConfig = null;
        String hint;
        int id = -1;
        String increasedValue;
        private float[] inverseTransform;
        private boolean inverseTransformDirty = true;
        String label;
        private float left;
        CustomAccessibilityAction onLongPressOverride;
        CustomAccessibilityAction onTapOverride;
        SemanticsObject parent;
        int previousActions;
        int previousFlags;
        String previousLabel;
        float previousScrollExtentMax;
        float previousScrollExtentMin;
        float previousScrollPosition;
        int previousTextSelectionBase;
        int previousTextSelectionExtent;
        String previousValue;
        private float right;
        int scrollChildren;
        float scrollExtentMax;
        float scrollExtentMin;
        int scrollIndex;
        float scrollPosition;
        TextDirection textDirection;
        int textSelectionBase;
        int textSelectionExtent;
        private float top;
        private float[] transform;
        String value;

        static {
            Class cls = AccessibilityBridge.class;
        }

        SemanticsObject() {
        }

        boolean hasAction(Action action) {
            return (this.actions & action.value) != 0;
        }

        boolean hadAction(Action action) {
            return (this.previousActions & action.value) != 0;
        }

        boolean hasFlag(Flag flag) {
            return (this.flags & flag.value) != 0;
        }

        boolean hadFlag(Flag flag) {
            return (this.previousFlags & flag.value) != 0;
        }

        boolean didScroll() {
            return (Float.isNaN(this.scrollPosition) || Float.isNaN(this.previousScrollPosition) || this.previousScrollPosition == this.scrollPosition) ? false : true;
        }

        boolean didChangeLabel() {
            boolean z = false;
            if (this.label == null && this.previousLabel == null) {
                return false;
            }
            if (!(this.label == null || this.previousLabel == null)) {
                if (this.label.equals(this.previousLabel)) {
                    return z;
                }
            }
            z = true;
            return z;
        }

        void log(String indent, boolean recursive) {
            String str = AccessibilityBridge.TAG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(indent);
            stringBuilder.append("SemanticsObject id=");
            stringBuilder.append(this.id);
            stringBuilder.append(" label=");
            stringBuilder.append(this.label);
            stringBuilder.append(" actions=");
            stringBuilder.append(this.actions);
            stringBuilder.append(" flags=");
            stringBuilder.append(this.flags);
            stringBuilder.append("\n");
            stringBuilder.append(indent);
            stringBuilder.append("  +-- textDirection=");
            stringBuilder.append(this.textDirection);
            stringBuilder.append("\n");
            stringBuilder.append(indent);
            stringBuilder.append("  +-- rect.ltrb=(");
            stringBuilder.append(this.left);
            stringBuilder.append(", ");
            stringBuilder.append(this.top);
            stringBuilder.append(", ");
            stringBuilder.append(this.right);
            stringBuilder.append(", ");
            stringBuilder.append(this.bottom);
            stringBuilder.append(")\n");
            stringBuilder.append(indent);
            stringBuilder.append("  +-- transform=");
            stringBuilder.append(Arrays.toString(this.transform));
            stringBuilder.append("\n");
            Log.i(str, stringBuilder.toString());
            if (this.childrenInTraversalOrder != null && recursive) {
                str = new StringBuilder();
                str.append(indent);
                str.append("  ");
                str = str.toString();
                for (SemanticsObject child : this.childrenInTraversalOrder) {
                    child.log(str, recursive);
                }
            }
        }

        void updateWith(ByteBuffer buffer, String[] strings) {
            int i;
            this.hadPreviousConfig = true;
            this.previousValue = this.value;
            this.previousLabel = this.label;
            this.previousFlags = this.flags;
            this.previousActions = this.actions;
            this.previousTextSelectionBase = this.textSelectionBase;
            this.previousTextSelectionExtent = this.textSelectionExtent;
            this.previousScrollPosition = this.scrollPosition;
            this.previousScrollExtentMax = this.scrollExtentMax;
            this.previousScrollExtentMin = this.scrollExtentMin;
            this.flags = buffer.getInt();
            this.actions = buffer.getInt();
            this.textSelectionBase = buffer.getInt();
            this.textSelectionExtent = buffer.getInt();
            this.scrollChildren = buffer.getInt();
            this.scrollIndex = buffer.getInt();
            this.scrollPosition = buffer.getFloat();
            this.scrollExtentMax = buffer.getFloat();
            this.scrollExtentMin = buffer.getFloat();
            int stringIndex = buffer.getInt();
            this.label = stringIndex == -1 ? null : strings[stringIndex];
            stringIndex = buffer.getInt();
            this.value = stringIndex == -1 ? null : strings[stringIndex];
            stringIndex = buffer.getInt();
            this.increasedValue = stringIndex == -1 ? null : strings[stringIndex];
            stringIndex = buffer.getInt();
            this.decreasedValue = stringIndex == -1 ? null : strings[stringIndex];
            stringIndex = buffer.getInt();
            this.hint = stringIndex == -1 ? null : strings[stringIndex];
            this.textDirection = TextDirection.fromInt(buffer.getInt());
            this.left = buffer.getFloat();
            this.top = buffer.getFloat();
            this.right = buffer.getFloat();
            this.bottom = buffer.getFloat();
            if (this.transform == null) {
                this.transform = new float[16];
            }
            for (int i2 = AccessibilityBridge.ROOT_NODE_ID; i2 < 16; i2++) {
                this.transform[i2] = buffer.getFloat();
            }
            this.inverseTransformDirty = true;
            this.globalGeometryDirty = true;
            int childCount = buffer.getInt();
            if (childCount == 0) {
                this.childrenInTraversalOrder = null;
                this.childrenInHitTestOrder = null;
            } else {
                SemanticsObject child;
                if (this.childrenInTraversalOrder == null) {
                    this.childrenInTraversalOrder = new ArrayList(childCount);
                } else {
                    this.childrenInTraversalOrder.clear();
                }
                for (i = AccessibilityBridge.ROOT_NODE_ID; i < childCount; i++) {
                    child = AccessibilityBridge.this.getOrCreateObject(buffer.getInt());
                    child.parent = this;
                    this.childrenInTraversalOrder.add(child);
                }
                if (this.childrenInHitTestOrder == null) {
                    this.childrenInHitTestOrder = new ArrayList(childCount);
                } else {
                    this.childrenInHitTestOrder.clear();
                }
                for (i = AccessibilityBridge.ROOT_NODE_ID; i < childCount; i++) {
                    child = AccessibilityBridge.this.getOrCreateObject(buffer.getInt());
                    child.parent = this;
                    this.childrenInHitTestOrder.add(child);
                }
            }
            i = buffer.getInt();
            if (i == 0) {
                this.customAccessibilityActions = null;
                return;
            }
            if (this.customAccessibilityActions == null) {
                this.customAccessibilityActions = new ArrayList(i);
            } else {
                this.customAccessibilityActions.clear();
            }
            for (int i3 = AccessibilityBridge.ROOT_NODE_ID; i3 < i; i3++) {
                CustomAccessibilityAction action = AccessibilityBridge.this.getOrCreateAction(buffer.getInt());
                if (action.overrideId == Action.TAP.value) {
                    this.onTapOverride = action;
                } else if (action.overrideId == Action.LONG_PRESS.value) {
                    this.onLongPressOverride = action;
                } else {
                    this.customAccessibilityActions.add(action);
                }
                this.customAccessibilityActions.add(action);
            }
        }

        private void ensureInverseTransform() {
            if (this.inverseTransformDirty) {
                this.inverseTransformDirty = false;
                if (this.inverseTransform == null) {
                    this.inverseTransform = new float[16];
                }
                if (!Matrix.invertM(this.inverseTransform, AccessibilityBridge.ROOT_NODE_ID, this.transform, AccessibilityBridge.ROOT_NODE_ID)) {
                    Arrays.fill(this.inverseTransform, 0.0f);
                }
            }
        }

        Rect getGlobalRect() {
            return this.globalRect;
        }

        SemanticsObject hitTest(float[] point) {
            float w = point[3];
            int i = AccessibilityBridge.ROOT_NODE_ID;
            float x = point[AccessibilityBridge.ROOT_NODE_ID] / w;
            float y = point[1] / w;
            if (x >= this.left && x < this.right && y >= this.top) {
                if (y < this.bottom) {
                    if (this.childrenInHitTestOrder != null) {
                        float[] transformedPoint = new float[4];
                        while (i < this.childrenInHitTestOrder.size()) {
                            SemanticsObject child = (SemanticsObject) this.childrenInHitTestOrder.get(i);
                            if (!child.hasFlag(Flag.IS_HIDDEN)) {
                                child.ensureInverseTransform();
                                Matrix.multiplyMV(transformedPoint, AccessibilityBridge.ROOT_NODE_ID, child.inverseTransform, AccessibilityBridge.ROOT_NODE_ID, point, AccessibilityBridge.ROOT_NODE_ID);
                                SemanticsObject result = child.hitTest(transformedPoint);
                                if (result != null) {
                                    return result;
                                }
                            }
                            i++;
                        }
                    }
                    return this;
                }
            }
            return null;
        }

        boolean isFocusable() {
            boolean z = false;
            if (hasFlag(Flag.SCOPES_ROUTE)) {
                return false;
            }
            if ((this.actions & ((((Action.SCROLL_RIGHT.value | Action.SCROLL_LEFT.value) | Action.SCROLL_UP.value) | Action.SCROLL_DOWN.value) ^ -1)) == 0 && this.flags == 0 && ((this.label == null || this.label.isEmpty()) && (this.value == null || this.value.isEmpty()))) {
                if (this.hint == null || this.hint.isEmpty()) {
                    return z;
                }
            }
            z = true;
            return z;
        }

        void collectRoutes(List<SemanticsObject> edges) {
            if (hasFlag(Flag.SCOPES_ROUTE)) {
                edges.add(this);
            }
            if (this.childrenInTraversalOrder != null) {
                for (int i = AccessibilityBridge.ROOT_NODE_ID; i < this.childrenInTraversalOrder.size(); i++) {
                    ((SemanticsObject) this.childrenInTraversalOrder.get(i)).collectRoutes(edges);
                }
            }
        }

        String getRouteName() {
            if (hasFlag(Flag.NAMES_ROUTE) && this.label != null && !this.label.isEmpty()) {
                return this.label;
            }
            if (this.childrenInTraversalOrder != null) {
                for (int i = AccessibilityBridge.ROOT_NODE_ID; i < this.childrenInTraversalOrder.size(); i++) {
                    String newName = ((SemanticsObject) this.childrenInTraversalOrder.get(i)).getRouteName();
                    if (newName != null && !newName.isEmpty()) {
                        return newName;
                    }
                }
            }
            return null;
        }

        void updateRecursively(float[] ancestorTransform, Set<SemanticsObject> visitedObjects, boolean forceUpdate) {
            boolean forceUpdate2;
            Set<SemanticsObject> set = visitedObjects;
            set.add(this);
            if (this.globalGeometryDirty) {
                forceUpdate2 = true;
            } else {
                forceUpdate2 = forceUpdate;
            }
            int i = AccessibilityBridge.ROOT_NODE_ID;
            if (forceUpdate2) {
                if (r0.globalTransform == null) {
                    r0.globalTransform = new float[16];
                }
                Matrix.multiplyMM(r0.globalTransform, AccessibilityBridge.ROOT_NODE_ID, ancestorTransform, AccessibilityBridge.ROOT_NODE_ID, r0.transform, AccessibilityBridge.ROOT_NODE_ID);
                float[] sample = new float[4];
                sample[2] = 0.0f;
                sample[3] = 1.0f;
                point1 = new float[4];
                point2 = new float[4];
                float[] point3 = new float[4];
                point4 = new float[4];
                transformPoint(point1, r0.globalTransform, sample);
                sample[AccessibilityBridge.ROOT_NODE_ID] = r0.right;
                sample[1] = r0.top;
                transformPoint(point2, r0.globalTransform, sample);
                sample[AccessibilityBridge.ROOT_NODE_ID] = r0.right;
                sample[1] = r0.bottom;
                transformPoint(point3, r0.globalTransform, sample);
                sample[AccessibilityBridge.ROOT_NODE_ID] = r0.left;
                sample[1] = r0.bottom;
                transformPoint(point4, r0.globalTransform, sample);
                if (r0.globalRect == null) {
                    r0.globalRect = new Rect();
                }
                r0.globalRect.set(Math.round(min(point1[AccessibilityBridge.ROOT_NODE_ID], point2[AccessibilityBridge.ROOT_NODE_ID], point3[AccessibilityBridge.ROOT_NODE_ID], point4[AccessibilityBridge.ROOT_NODE_ID])), Math.round(min(point1[1], point2[1], point3[1], point4[1])), Math.round(max(point1[AccessibilityBridge.ROOT_NODE_ID], point2[AccessibilityBridge.ROOT_NODE_ID], point3[AccessibilityBridge.ROOT_NODE_ID], point4[AccessibilityBridge.ROOT_NODE_ID])), Math.round(max(point1[1], point2[1], point3[1], point4[1])));
                i = AccessibilityBridge.ROOT_NODE_ID;
                r0.globalGeometryDirty = false;
            }
            if (r0.childrenInTraversalOrder != null) {
                for (i = 
/*
Method generation error in method: io.flutter.view.AccessibilityBridge.SemanticsObject.updateRecursively(float[], java.util.Set, boolean):void, dex: classes.dex
jadx.core.utils.exceptions.CodegenException: Error generate insn: PHI: (r3_5 'i' int) = (r3_0 'i' int), (r3_4 'i' int) binds: {(r3_0 'i' int)=B:5:0x0010, (r3_4 'i' int)=B:12:0x007c} in method: io.flutter.view.AccessibilityBridge.SemanticsObject.updateRecursively(float[], java.util.Set, boolean):void, dex: classes.dex
	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:226)
	at jadx.core.codegen.RegionGen.makeLoop(RegionGen.java:184)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:61)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.RegionGen.makeRegionIndent(RegionGen.java:93)
	at jadx.core.codegen.RegionGen.makeIf(RegionGen.java:118)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:57)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.RegionGen.makeSimpleRegion(RegionGen.java:87)
	at jadx.core.codegen.RegionGen.makeRegion(RegionGen.java:53)
	at jadx.core.codegen.MethodGen.addInstructions(MethodGen.java:187)
	at jadx.core.codegen.ClassGen.addMethod(ClassGen.java:320)
	at jadx.core.codegen.ClassGen.addMethods(ClassGen.java:257)
	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:220)
	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
	at jadx.core.codegen.ClassGen.addInnerClasses(ClassGen.java:233)
	at jadx.core.codegen.ClassGen.addClassBody(ClassGen.java:219)
	at jadx.core.codegen.ClassGen.addClassCode(ClassGen.java:110)
	at jadx.core.codegen.ClassGen.makeClass(ClassGen.java:75)
	at jadx.core.codegen.CodeGen.visit(CodeGen.java:12)
	at jadx.core.ProcessClass.process(ProcessClass.java:40)
	at jadx.api.JadxDecompiler.processClass(JadxDecompiler.java:282)
	at jadx.api.JavaClass.decompile(JavaClass.java:62)
	at jadx.api.JadxDecompiler.lambda$appendSourcesSave$0(JadxDecompiler.java:200)
	at jadx.api.JadxDecompiler$$Lambda$8/662822946.run(Unknown Source)
Caused by: jadx.core.utils.exceptions.CodegenException: PHI can be used only in fallback mode
	at jadx.core.codegen.InsnGen.fallbackOnlyInsn(InsnGen.java:537)
	at jadx.core.codegen.InsnGen.makeInsnBody(InsnGen.java:509)
	at jadx.core.codegen.InsnGen.makeInsn(InsnGen.java:220)
	... 26 more

*/

                private void transformPoint(float[] result, float[] transform, float[] point) {
                    Matrix.multiplyMV(result, AccessibilityBridge.ROOT_NODE_ID, transform, AccessibilityBridge.ROOT_NODE_ID, point, AccessibilityBridge.ROOT_NODE_ID);
                    float w = result[3];
                    result[AccessibilityBridge.ROOT_NODE_ID] = result[AccessibilityBridge.ROOT_NODE_ID] / w;
                    result[1] = result[1] / w;
                    result[2] = result[2] / w;
                    result[3] = 0.0f;
                }

                private float min(float a, float b, float c, float d) {
                    return Math.min(a, Math.min(b, Math.min(c, d)));
                }

                private float max(float a, float b, float c, float d) {
                    return Math.max(a, Math.max(b, Math.max(c, d)));
                }

                private String getValueLabelHint() {
                    StringBuilder sb = new StringBuilder();
                    array = new String[3];
                    String str = this.value;
                    int i = AccessibilityBridge.ROOT_NODE_ID;
                    array[AccessibilityBridge.ROOT_NODE_ID] = str;
                    array[1] = this.label;
                    array[2] = this.hint;
                    int length = array.length;
                    while (i < length) {
                        String word = array[i];
                        if (word != null && word.length() > 0) {
                            if (sb.length() > 0) {
                                sb.append(", ");
                            }
                            sb.append(word);
                        }
                        i++;
                    }
                    return sb.length() > 0 ? sb.toString() : null;
                }
            }

            private enum TextDirection {
                UNKNOWN,
                LTR,
                RTL;

                public static TextDirection fromInt(int value) {
                    switch (value) {
                        case BuildConfig.VERSION_CODE /*1*/:
                            return RTL;
                        case 2:
                            return LTR;
                        default:
                            return UNKNOWN;
                    }
                }
            }

            AccessibilityBridge(FlutterView owner) {
                this.mOwner = owner;
                this.mObjects = new HashMap();
                this.mCustomAccessibilityActions = new HashMap();
                this.previousRoutes = new ArrayList();
                this.mFlutterAccessibilityChannel = new BasicMessageChannel(owner, "flutter/accessibility", StandardMessageCodec.INSTANCE);
                this.mDecorView = ((Activity) owner.getContext()).getWindow().getDecorView();
            }

            void setAccessibilityEnabled(boolean accessibilityEnabled) {
                this.mAccessibilityEnabled = accessibilityEnabled;
                if (accessibilityEnabled) {
                    this.mFlutterAccessibilityChannel.setMessageHandler(this);
                } else {
                    this.mFlutterAccessibilityChannel.setMessageHandler(null);
                }
            }

            public AccessibilityNodeInfo createAccessibilityNodeInfo(int virtualViewId) {
                boolean z = false;
                if (virtualViewId == -1) {
                    AccessibilityNodeInfo result = AccessibilityNodeInfo.obtain(this.mOwner);
                    this.mOwner.onInitializeAccessibilityNodeInfo(result);
                    if (this.mObjects.containsKey(Integer.valueOf(ROOT_NODE_ID))) {
                        result.addChild(this.mOwner, ROOT_NODE_ID);
                    }
                    return result;
                }
                SemanticsObject object = (SemanticsObject) this.mObjects.get(Integer.valueOf(virtualViewId));
                if (object == null) {
                    return null;
                }
                boolean z2;
                boolean hasToggledState;
                AccessibilityNodeInfo result2 = AccessibilityNodeInfo.obtain(this.mOwner, virtualViewId);
                if (VERSION.SDK_INT >= 18) {
                    result2.setViewIdResourceName(BuildConfig.FLAVOR);
                }
                result2.setPackageName(this.mOwner.getContext().getPackageName());
                result2.setClassName("android.view.View");
                result2.setSource(this.mOwner, virtualViewId);
                result2.setFocusable(object.isFocusable());
                if (this.mInputFocusedObject != null) {
                    result2.setFocused(this.mInputFocusedObject.id == virtualViewId);
                }
                if (this.mA11yFocusedObject != null) {
                    result2.setAccessibilityFocused(this.mA11yFocusedObject.id == virtualViewId);
                }
                if (object.hasFlag(Flag.IS_TEXT_FIELD)) {
                    result2.setPassword(object.hasFlag(Flag.IS_OBSCURED));
                    result2.setClassName("android.widget.EditText");
                    if (VERSION.SDK_INT >= 18) {
                        result2.setEditable(true);
                        if (!(object.textSelectionBase == -1 || object.textSelectionExtent == -1)) {
                            result2.setTextSelection(object.textSelectionBase, object.textSelectionExtent);
                        }
                        if (VERSION.SDK_INT > 18 && this.mA11yFocusedObject != null && this.mA11yFocusedObject.id == virtualViewId) {
                            result2.setLiveRegion(1);
                        }
                    }
                    int granularities = ROOT_NODE_ID;
                    if (object.hasAction(Action.MOVE_CURSOR_FORWARD_BY_CHARACTER)) {
                        result2.addAction(256);
                        granularities = ROOT_NODE_ID | 1;
                    }
                    if (object.hasAction(Action.MOVE_CURSOR_BACKWARD_BY_CHARACTER)) {
                        result2.addAction(512);
                        granularities |= 1;
                    }
                    if (object.hasAction(Action.MOVE_CURSOR_FORWARD_BY_WORD)) {
                        result2.addAction(256);
                        granularities |= 2;
                    }
                    if (object.hasAction(Action.MOVE_CURSOR_BACKWARD_BY_WORD)) {
                        result2.addAction(512);
                        granularities |= 2;
                    }
                    result2.setMovementGranularities(granularities);
                }
                if (object.hasAction(Action.SET_SELECTION)) {
                    result2.addAction(131072);
                }
                if (object.hasAction(Action.COPY)) {
                    result2.addAction(16384);
                }
                if (object.hasAction(Action.CUT)) {
                    result2.addAction(65536);
                }
                if (object.hasAction(Action.PASTE)) {
                    result2.addAction(32768);
                }
                if (object.hasFlag(Flag.IS_BUTTON)) {
                    result2.setClassName("android.widget.Button");
                }
                if (object.hasFlag(Flag.IS_IMAGE)) {
                    result2.setClassName("android.widget.ImageView");
                }
                if (VERSION.SDK_INT > 18 && object.hasAction(Action.DISMISS)) {
                    result2.setDismissable(true);
                    result2.addAction(1048576);
                }
                if (object.parent != null) {
                    result2.setParent(this.mOwner, object.parent.id);
                } else {
                    result2.setParent(this.mOwner);
                }
                Rect bounds = object.getGlobalRect();
                if (object.parent != null) {
                    Rect parentBounds = object.parent.getGlobalRect();
                    Rect boundsInParent = new Rect(bounds);
                    boundsInParent.offset(-parentBounds.left, -parentBounds.top);
                    result2.setBoundsInParent(boundsInParent);
                } else {
                    result2.setBoundsInParent(bounds);
                }
                result2.setBoundsInScreen(bounds);
                result2.setVisibleToUser(true);
                if (object.hasFlag(Flag.HAS_ENABLED_STATE)) {
                    if (!object.hasFlag(Flag.IS_ENABLED)) {
                        z2 = false;
                        result2.setEnabled(z2);
                        if (object.hasAction(Action.TAP)) {
                            if (VERSION.SDK_INT >= 21 || object.onTapOverride == null) {
                                result2.addAction(16);
                                result2.setClickable(true);
                            } else {
                                result2.addAction(new AccessibilityAction(16, object.onTapOverride.hint));
                                result2.setClickable(true);
                            }
                        }
                        if (object.hasAction(Action.LONG_PRESS)) {
                            if (VERSION.SDK_INT >= 21 || object.onLongPressOverride == null) {
                                result2.addAction(32);
                                result2.setLongClickable(true);
                            } else {
                                result2.addAction(new AccessibilityAction(32, object.onLongPressOverride.hint));
                                result2.setLongClickable(true);
                            }
                        }
                        if (object.hasAction(Action.SCROLL_LEFT) || object.hasAction(Action.SCROLL_UP) || object.hasAction(Action.SCROLL_RIGHT) || object.hasAction(Action.SCROLL_DOWN)) {
                            result2.setScrollable(true);
                            if (object.hasFlag(Flag.HAS_IMPLICIT_SCROLLING)) {
                                if (!object.hasAction(Action.SCROLL_LEFT)) {
                                    if (object.hasAction(Action.SCROLL_RIGHT)) {
                                        result2.setClassName("android.widget.ScrollView");
                                    }
                                }
                                result2.setClassName("android.widget.HorizontalScrollView");
                            }
                            if (object.hasAction(Action.SCROLL_LEFT) || object.hasAction(Action.SCROLL_UP)) {
                                result2.addAction(4096);
                            }
                            if (object.hasAction(Action.SCROLL_RIGHT) || object.hasAction(Action.SCROLL_DOWN)) {
                                result2.addAction(8192);
                            }
                        }
                        if (object.hasAction(Action.INCREASE) || object.hasAction(Action.DECREASE)) {
                            result2.setClassName("android.widget.SeekBar");
                            if (object.hasAction(Action.INCREASE)) {
                                result2.addAction(4096);
                            }
                            if (object.hasAction(Action.DECREASE)) {
                                result2.addAction(8192);
                            }
                        }
                        if (object.hasFlag(Flag.IS_LIVE_REGION) && VERSION.SDK_INT > 18) {
                            result2.setLiveRegion(1);
                        }
                        z2 = object.hasFlag(Flag.HAS_CHECKED_STATE);
                        hasToggledState = object.hasFlag(Flag.HAS_TOGGLED_STATE);
                        if (!z2) {
                            if (hasToggledState) {
                                result2.setCheckable(z);
                                if (!z2) {
                                    result2.setChecked(object.hasFlag(Flag.IS_CHECKED));
                                    result2.setContentDescription(object.getValueLabelHint());
                                    if (object.hasFlag(Flag.IS_IN_MUTUALLY_EXCLUSIVE_GROUP)) {
                                        result2.setClassName("android.widget.CheckBox");
                                    } else {
                                        result2.setClassName("android.widget.RadioButton");
                                    }
                                } else if (hasToggledState) {
                                    result2.setText(object.getValueLabelHint());
                                } else {
                                    result2.setChecked(object.hasFlag(Flag.IS_TOGGLED));
                                    result2.setClassName("android.widget.Switch");
                                    result2.setContentDescription(object.getValueLabelHint());
                                }
                                result2.setSelected(object.hasFlag(Flag.IS_SELECTED));
                                if (this.mA11yFocusedObject == null && this.mA11yFocusedObject.id == virtualViewId) {
                                    result2.addAction(128);
                                } else {
                                    result2.addAction(64);
                                }
                                if (VERSION.SDK_INT >= 21 && object.customAccessibilityActions != null) {
                                    for (CustomAccessibilityAction action : object.customAccessibilityActions) {
                                        result2.addAction(new AccessibilityAction(action.resourceId, action.label));
                                    }
                                }
                                if (object.childrenInTraversalOrder != null) {
                                    for (SemanticsObject child : object.childrenInTraversalOrder) {
                                        if (child.hasFlag(Flag.IS_HIDDEN)) {
                                            result2.addChild(this.mOwner, child.id);
                                        }
                                    }
                                }
                                return result2;
                            }
                        }
                        z = true;
                        result2.setCheckable(z);
                        if (!z2) {
                            result2.setChecked(object.hasFlag(Flag.IS_CHECKED));
                            result2.setContentDescription(object.getValueLabelHint());
                            if (object.hasFlag(Flag.IS_IN_MUTUALLY_EXCLUSIVE_GROUP)) {
                                result2.setClassName("android.widget.CheckBox");
                            } else {
                                result2.setClassName("android.widget.RadioButton");
                            }
                        } else if (hasToggledState) {
                            result2.setText(object.getValueLabelHint());
                        } else {
                            result2.setChecked(object.hasFlag(Flag.IS_TOGGLED));
                            result2.setClassName("android.widget.Switch");
                            result2.setContentDescription(object.getValueLabelHint());
                        }
                        result2.setSelected(object.hasFlag(Flag.IS_SELECTED));
                        if (this.mA11yFocusedObject == null) {
                        }
                        result2.addAction(64);
                        for (CustomAccessibilityAction action2 : object.customAccessibilityActions) {
                            result2.addAction(new AccessibilityAction(action2.resourceId, action2.label));
                        }
                        if (object.childrenInTraversalOrder != null) {
                            for (SemanticsObject child2 : object.childrenInTraversalOrder) {
                                if (child2.hasFlag(Flag.IS_HIDDEN)) {
                                    result2.addChild(this.mOwner, child2.id);
                                }
                            }
                        }
                        return result2;
                    }
                }
                z2 = true;
                result2.setEnabled(z2);
                if (object.hasAction(Action.TAP)) {
                    if (VERSION.SDK_INT >= 21) {
                    }
                    result2.addAction(16);
                    result2.setClickable(true);
                }
                if (object.hasAction(Action.LONG_PRESS)) {
                    if (VERSION.SDK_INT >= 21) {
                    }
                    result2.addAction(32);
                    result2.setLongClickable(true);
                }
                result2.setScrollable(true);
                if (object.hasFlag(Flag.HAS_IMPLICIT_SCROLLING)) {
                    if (object.hasAction(Action.SCROLL_LEFT)) {
                        if (object.hasAction(Action.SCROLL_RIGHT)) {
                            result2.setClassName("android.widget.ScrollView");
                        }
                    }
                    result2.setClassName("android.widget.HorizontalScrollView");
                }
                result2.addAction(4096);
                result2.addAction(8192);
                result2.setClassName("android.widget.SeekBar");
                if (object.hasAction(Action.INCREASE)) {
                    result2.addAction(4096);
                }
                if (object.hasAction(Action.DECREASE)) {
                    result2.addAction(8192);
                }
                result2.setLiveRegion(1);
                z2 = object.hasFlag(Flag.HAS_CHECKED_STATE);
                hasToggledState = object.hasFlag(Flag.HAS_TOGGLED_STATE);
                if (z2) {
                    if (hasToggledState) {
                        result2.setCheckable(z);
                        if (!z2) {
                            result2.setChecked(object.hasFlag(Flag.IS_CHECKED));
                            result2.setContentDescription(object.getValueLabelHint());
                            if (object.hasFlag(Flag.IS_IN_MUTUALLY_EXCLUSIVE_GROUP)) {
                                result2.setClassName("android.widget.RadioButton");
                            } else {
                                result2.setClassName("android.widget.CheckBox");
                            }
                        } else if (hasToggledState) {
                            result2.setChecked(object.hasFlag(Flag.IS_TOGGLED));
                            result2.setClassName("android.widget.Switch");
                            result2.setContentDescription(object.getValueLabelHint());
                        } else {
                            result2.setText(object.getValueLabelHint());
                        }
                        result2.setSelected(object.hasFlag(Flag.IS_SELECTED));
                        if (this.mA11yFocusedObject == null) {
                        }
                        result2.addAction(64);
                        for (CustomAccessibilityAction action22 : object.customAccessibilityActions) {
                            result2.addAction(new AccessibilityAction(action22.resourceId, action22.label));
                        }
                        if (object.childrenInTraversalOrder != null) {
                            for (SemanticsObject child22 : object.childrenInTraversalOrder) {
                                if (child22.hasFlag(Flag.IS_HIDDEN)) {
                                    result2.addChild(this.mOwner, child22.id);
                                }
                            }
                        }
                        return result2;
                    }
                }
                z = true;
                result2.setCheckable(z);
                if (!z2) {
                    result2.setChecked(object.hasFlag(Flag.IS_CHECKED));
                    result2.setContentDescription(object.getValueLabelHint());
                    if (object.hasFlag(Flag.IS_IN_MUTUALLY_EXCLUSIVE_GROUP)) {
                        result2.setClassName("android.widget.CheckBox");
                    } else {
                        result2.setClassName("android.widget.RadioButton");
                    }
                } else if (hasToggledState) {
                    result2.setText(object.getValueLabelHint());
                } else {
                    result2.setChecked(object.hasFlag(Flag.IS_TOGGLED));
                    result2.setClassName("android.widget.Switch");
                    result2.setContentDescription(object.getValueLabelHint());
                }
                result2.setSelected(object.hasFlag(Flag.IS_SELECTED));
                if (this.mA11yFocusedObject == null) {
                }
                result2.addAction(64);
                for (CustomAccessibilityAction action222 : object.customAccessibilityActions) {
                    result2.addAction(new AccessibilityAction(action222.resourceId, action222.label));
                }
                if (object.childrenInTraversalOrder != null) {
                    for (SemanticsObject child222 : object.childrenInTraversalOrder) {
                        if (child222.hasFlag(Flag.IS_HIDDEN)) {
                            result2.addChild(this.mOwner, child222.id);
                        }
                    }
                }
                return result2;
            }

            public boolean performAction(int virtualViewId, int action, Bundle arguments) {
                SemanticsObject object = (SemanticsObject) this.mObjects.get(Integer.valueOf(virtualViewId));
                boolean hasSelection = false;
                if (object == null) {
                    return false;
                }
                switch (action) {
                    case 16:
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.TAP);
                        return true;
                    case 32:
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.LONG_PRESS);
                        return true;
                    case 64:
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.DID_GAIN_ACCESSIBILITY_FOCUS);
                        sendAccessibilityEvent(virtualViewId, 32768);
                        if (this.mA11yFocusedObject == null) {
                            this.mOwner.invalidate();
                        }
                        this.mA11yFocusedObject = object;
                        if (object.hasAction(Action.INCREASE) || object.hasAction(Action.DECREASE)) {
                            sendAccessibilityEvent(virtualViewId, 4);
                        }
                        return true;
                    case 128:
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.DID_LOSE_ACCESSIBILITY_FOCUS);
                        sendAccessibilityEvent(virtualViewId, 65536);
                        this.mA11yFocusedObject = null;
                        return true;
                    case 256:
                        return performCursorMoveAction(object, virtualViewId, arguments, true);
                    case 512:
                        return performCursorMoveAction(object, virtualViewId, arguments, false);
                    case 4096:
                        if (object.hasAction(Action.SCROLL_UP)) {
                            this.mOwner.dispatchSemanticsAction(virtualViewId, Action.SCROLL_UP);
                        } else if (object.hasAction(Action.SCROLL_LEFT)) {
                            this.mOwner.dispatchSemanticsAction(virtualViewId, Action.SCROLL_LEFT);
                        } else if (!object.hasAction(Action.INCREASE)) {
                            return false;
                        } else {
                            object.value = object.increasedValue;
                            sendAccessibilityEvent(virtualViewId, 4);
                            this.mOwner.dispatchSemanticsAction(virtualViewId, Action.INCREASE);
                        }
                        return true;
                    case 8192:
                        if (object.hasAction(Action.SCROLL_DOWN)) {
                            this.mOwner.dispatchSemanticsAction(virtualViewId, Action.SCROLL_DOWN);
                        } else if (object.hasAction(Action.SCROLL_RIGHT)) {
                            this.mOwner.dispatchSemanticsAction(virtualViewId, Action.SCROLL_RIGHT);
                        } else if (!object.hasAction(Action.DECREASE)) {
                            return false;
                        } else {
                            object.value = object.decreasedValue;
                            sendAccessibilityEvent(virtualViewId, 4);
                            this.mOwner.dispatchSemanticsAction(virtualViewId, Action.DECREASE);
                        }
                        return true;
                    case 16384:
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.COPY);
                        return true;
                    case 32768:
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.PASTE);
                        return true;
                    case 65536:
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.CUT);
                        return true;
                    case 131072:
                        Map<String, Integer> selection = new HashMap();
                        if (arguments != null && arguments.containsKey("ACTION_ARGUMENT_SELECTION_START_INT") && arguments.containsKey("ACTION_ARGUMENT_SELECTION_END_INT")) {
                            hasSelection = true;
                        }
                        if (hasSelection) {
                            selection.put("base", Integer.valueOf(arguments.getInt("ACTION_ARGUMENT_SELECTION_START_INT")));
                            selection.put("extent", Integer.valueOf(arguments.getInt("ACTION_ARGUMENT_SELECTION_END_INT")));
                        } else {
                            selection.put("base", Integer.valueOf(object.textSelectionExtent));
                            selection.put("extent", Integer.valueOf(object.textSelectionExtent));
                        }
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.SET_SELECTION, selection);
                        return true;
                    case 1048576:
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.DISMISS);
                        return true;
                    case ACTION_SHOW_ON_SCREEN /*16908342*/:
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.SHOW_ON_SCREEN);
                        return true;
                    default:
                        CustomAccessibilityAction contextAction = (CustomAccessibilityAction) this.mCustomAccessibilityActions.get(Integer.valueOf(action - firstResourceId));
                        if (contextAction == null) {
                            return false;
                        }
                        this.mOwner.dispatchSemanticsAction(virtualViewId, Action.CUSTOM_ACTION, Integer.valueOf(contextAction.id));
                        return true;
                }
            }

            boolean performCursorMoveAction(SemanticsObject object, int virtualViewId, Bundle arguments, boolean forward) {
                int granularity = arguments.getInt("ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT");
                boolean extendSelection = arguments.getBoolean("ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN");
                switch (granularity) {
                    case BuildConfig.VERSION_CODE /*1*/:
                        if (forward && object.hasAction(Action.MOVE_CURSOR_FORWARD_BY_CHARACTER)) {
                            this.mOwner.dispatchSemanticsAction(virtualViewId, Action.MOVE_CURSOR_FORWARD_BY_CHARACTER, Boolean.valueOf(extendSelection));
                            return true;
                        } else if (!forward && object.hasAction(Action.MOVE_CURSOR_BACKWARD_BY_CHARACTER)) {
                            this.mOwner.dispatchSemanticsAction(virtualViewId, Action.MOVE_CURSOR_BACKWARD_BY_CHARACTER, Boolean.valueOf(extendSelection));
                            return true;
                        }
                        break;
                    case 2:
                        if (forward && object.hasAction(Action.MOVE_CURSOR_FORWARD_BY_WORD)) {
                            this.mOwner.dispatchSemanticsAction(virtualViewId, Action.MOVE_CURSOR_FORWARD_BY_WORD, Boolean.valueOf(extendSelection));
                            return true;
                        } else if (!forward && object.hasAction(Action.MOVE_CURSOR_BACKWARD_BY_WORD)) {
                            this.mOwner.dispatchSemanticsAction(virtualViewId, Action.MOVE_CURSOR_BACKWARD_BY_WORD, Boolean.valueOf(extendSelection));
                            return true;
                        }
                        break;
                    default:
                        break;
                }
                return false;
            }

            public AccessibilityNodeInfo findFocus(int focus) {
                switch (focus) {
                    case BuildConfig.VERSION_CODE /*1*/:
                        if (this.mInputFocusedObject != null) {
                            return createAccessibilityNodeInfo(this.mInputFocusedObject.id);
                        }
                        break;
                    case 2:
                        break;
                    default:
                        break;
                }
                if (this.mA11yFocusedObject != null) {
                    return createAccessibilityNodeInfo(this.mA11yFocusedObject.id);
                }
                return null;
            }

            private SemanticsObject getRootObject() {
                return (SemanticsObject) this.mObjects.get(Integer.valueOf(ROOT_NODE_ID));
            }

            private SemanticsObject getOrCreateObject(int id) {
                SemanticsObject object = (SemanticsObject) this.mObjects.get(Integer.valueOf(id));
                if (object != null) {
                    return object;
                }
                object = new SemanticsObject();
                object.id = id;
                this.mObjects.put(Integer.valueOf(id), object);
                return object;
            }

            private CustomAccessibilityAction getOrCreateAction(int id) {
                CustomAccessibilityAction action = (CustomAccessibilityAction) this.mCustomAccessibilityActions.get(Integer.valueOf(id));
                if (action != null) {
                    return action;
                }
                action = new CustomAccessibilityAction();
                action.id = id;
                action.resourceId = firstResourceId + id;
                this.mCustomAccessibilityActions.put(Integer.valueOf(id), action);
                return action;
            }

            void handleTouchExplorationExit() {
                if (this.mHoveredObject != null) {
                    sendAccessibilityEvent(this.mHoveredObject.id, 256);
                    this.mHoveredObject = null;
                }
            }

            void handleTouchExploration(float x, float y) {
                if (!this.mObjects.isEmpty()) {
                    SemanticsObject newObject = getRootObject().hitTest(new float[]{x, y, 0.0f, 1.0f});
                    if (newObject != this.mHoveredObject) {
                        if (newObject != null) {
                            sendAccessibilityEvent(newObject.id, 128);
                        }
                        if (this.mHoveredObject != null) {
                            sendAccessibilityEvent(this.mHoveredObject.id, 256);
                        }
                        this.mHoveredObject = newObject;
                    }
                }
            }

            void updateCustomAccessibilityActions(ByteBuffer buffer, String[] strings) {
                while (buffer.hasRemaining()) {
                    CustomAccessibilityAction action = getOrCreateAction(buffer.getInt());
                    action.overrideId = buffer.getInt();
                    int stringIndex = buffer.getInt();
                    String str = null;
                    action.label = stringIndex == -1 ? null : strings[stringIndex];
                    stringIndex = buffer.getInt();
                    if (stringIndex != -1) {
                        str = strings[stringIndex];
                    }
                    action.hint = str;
                }
            }

            void updateSemantics(ByteBuffer buffer, String[] strings) {
                SemanticsObject object;
                Set<SemanticsObject> visitedObjects;
                AccessibilityBridge accessibilityBridge = this;
                ArrayList<SemanticsObject> updated = new ArrayList();
                while (buffer.hasRemaining()) {
                    object = getOrCreateObject(buffer.getInt());
                    object.updateWith(buffer, strings);
                    if (!object.hasFlag(Flag.IS_HIDDEN)) {
                        if (object.hasFlag(Flag.IS_FOCUSED)) {
                            accessibilityBridge.mInputFocusedObject = object;
                        }
                        if (object.hadPreviousConfig) {
                            updated.add(object);
                        }
                    }
                }
                ByteBuffer byteBuffer = buffer;
                String[] strArr = strings;
                Set<SemanticsObject> visitedObjects2 = new HashSet();
                object = getRootObject();
                List<SemanticsObject> newRoutes = new ArrayList();
                if (object != null) {
                    float[] identity = new float[16];
                    Matrix.setIdentityM(identity, ROOT_NODE_ID);
                    if (VERSION.SDK_INT >= 23) {
                        Rect visibleFrame = new Rect();
                        accessibilityBridge.mDecorView.getWindowVisibleDisplayFrame(visibleFrame);
                        if (!accessibilityBridge.mLastLeftFrameInset.equals(Integer.valueOf(visibleFrame.left))) {
                            object.globalGeometryDirty = true;
                            object.inverseTransformDirty = true;
                        }
                        accessibilityBridge.mLastLeftFrameInset = Integer.valueOf(visibleFrame.left);
                        Matrix.translateM(identity, ROOT_NODE_ID, (float) visibleFrame.left, 0.0f, 0.0f);
                    }
                    object.updateRecursively(identity, visitedObjects2, false);
                    object.collectRoutes(newRoutes);
                }
                SemanticsObject lastAdded = null;
                for (SemanticsObject semanticsObject : newRoutes) {
                    if (!accessibilityBridge.previousRoutes.contains(Integer.valueOf(semanticsObject.id))) {
                        lastAdded = semanticsObject;
                    }
                }
                if (lastAdded == null && newRoutes.size() > 0) {
                    lastAdded = (SemanticsObject) newRoutes.get(newRoutes.size() - 1);
                }
                if (!(lastAdded == null || lastAdded.id == accessibilityBridge.previousRouteId)) {
                    accessibilityBridge.previousRouteId = lastAdded.id;
                    createWindowChangeEvent(lastAdded);
                }
                accessibilityBridge.previousRoutes.clear();
                for (SemanticsObject semanticsObject2 : newRoutes) {
                    accessibilityBridge.previousRoutes.add(Integer.valueOf(semanticsObject2.id));
                }
                Iterator<Entry<Integer, SemanticsObject>> it = accessibilityBridge.mObjects.entrySet().iterator();
                while (it.hasNext()) {
                    SemanticsObject object2 = (SemanticsObject) ((Entry) it.next()).getValue();
                    if (!visitedObjects2.contains(object2)) {
                        willRemoveSemanticsObject(object2);
                        it.remove();
                    }
                }
                sendAccessibilityEvent(ROOT_NODE_ID, 2048);
                Iterator it2 = updated.iterator();
                while (it2.hasNext()) {
                    AccessibilityEvent event;
                    ArrayList<SemanticsObject> updated2;
                    String newValue;
                    AccessibilityEvent selectionEvent;
                    object2 = (SemanticsObject) it2.next();
                    if (object2.didScroll()) {
                        int visibleChildren;
                        event = obtainAccessibilityEvent(object2.id, 4096);
                        float position = object2.scrollPosition;
                        float max = object2.scrollExtentMax;
                        if (Float.isInfinite(object2.scrollExtentMax)) {
                            max = SCROLL_EXTENT_FOR_INFINITY;
                            if (position > SCROLL_POSITION_CAP_FOR_INFINITY) {
                                position = SCROLL_POSITION_CAP_FOR_INFINITY;
                            }
                        }
                        if (Float.isInfinite(object2.scrollExtentMin)) {
                            max += SCROLL_EXTENT_FOR_INFINITY;
                            if (position < -70000.0f) {
                                position = -70000.0f;
                            }
                            position += SCROLL_EXTENT_FOR_INFINITY;
                        } else {
                            max -= object2.scrollExtentMin;
                            position -= object2.scrollExtentMin;
                        }
                        if (!object2.hadAction(Action.SCROLL_UP)) {
                            if (!object2.hadAction(Action.SCROLL_DOWN)) {
                                if (object2.hadAction(Action.SCROLL_LEFT) || object2.hadAction(Action.SCROLL_RIGHT)) {
                                    event.setScrollX((int) position);
                                    event.setMaxScrollX((int) max);
                                }
                                if (object2.scrollChildren <= 0) {
                                    event.setItemCount(object2.scrollChildren);
                                    event.setFromIndex(object2.scrollIndex);
                                    visibleChildren = ROOT_NODE_ID;
                                    for (SemanticsObject updated3 : object2.childrenInHitTestOrder) {
                                        updated2 = updated;
                                        visitedObjects = visitedObjects2;
                                        if (!updated3.hasFlag(Flag.IS_HIDDEN)) {
                                            visibleChildren++;
                                        }
                                        updated = updated2;
                                        visitedObjects2 = visitedObjects;
                                    }
                                    updated2 = updated;
                                    visitedObjects = visitedObjects2;
                                    event.setToIndex((object2.scrollIndex + visibleChildren) - 1);
                                } else {
                                    updated2 = updated;
                                    visitedObjects = visitedObjects2;
                                }
                                sendAccessibilityEvent(event);
                            }
                        }
                        event.setScrollY((int) position);
                        event.setMaxScrollY((int) max);
                        if (object2.scrollChildren <= 0) {
                            updated2 = updated;
                            visitedObjects = visitedObjects2;
                        } else {
                            event.setItemCount(object2.scrollChildren);
                            event.setFromIndex(object2.scrollIndex);
                            visibleChildren = ROOT_NODE_ID;
                            while (r8.hasNext()) {
                                updated2 = updated;
                                visitedObjects = visitedObjects2;
                                if (!updated3.hasFlag(Flag.IS_HIDDEN)) {
                                    visibleChildren++;
                                }
                                updated = updated2;
                                visitedObjects2 = visitedObjects;
                            }
                            updated2 = updated;
                            visitedObjects = visitedObjects2;
                            event.setToIndex((object2.scrollIndex + visibleChildren) - 1);
                        }
                        sendAccessibilityEvent(event);
                    } else {
                        updated2 = updated;
                        visitedObjects = visitedObjects2;
                    }
                    if (object2.hasFlag(Flag.IS_LIVE_REGION) != null) {
                        if (!((object2.label == null ? BuildConfig.FLAVOR : object2.label).equals(object2.previousLabel == null ? BuildConfig.FLAVOR : object2.label) && object2.hadFlag(Flag.IS_LIVE_REGION))) {
                            sendAccessibilityEvent(object2.id, 2048);
                        }
                    } else if (!(object2.hasFlag(Flag.IS_TEXT_FIELD) == null || object2.didChangeLabel() == null || accessibilityBridge.mInputFocusedObject == null || accessibilityBridge.mInputFocusedObject.id != object2.id)) {
                        sendAccessibilityEvent(object2.id, 2048);
                        if (accessibilityBridge.mA11yFocusedObject != null && accessibilityBridge.mA11yFocusedObject.id == object2.id && object2.hadFlag(Flag.IS_SELECTED) == null && object2.hasFlag(Flag.IS_SELECTED) != null) {
                            updated = obtainAccessibilityEvent(object2.id, 4);
                            updated.getText().add(object2.label);
                            sendAccessibilityEvent(updated);
                        }
                        if (!(accessibilityBridge.mInputFocusedObject == null || accessibilityBridge.mInputFocusedObject.id != object2.id || object2.hadFlag(Flag.IS_TEXT_FIELD) == null || object2.hasFlag(Flag.IS_TEXT_FIELD) == null || (accessibilityBridge.mA11yFocusedObject != null && accessibilityBridge.mA11yFocusedObject.id != accessibilityBridge.mInputFocusedObject.id))) {
                            updated = object2.previousValue == null ? object2.previousValue : BuildConfig.FLAVOR;
                            newValue = object2.value == null ? object2.value : BuildConfig.FLAVOR;
                            event = createTextChangedEvent(object2.id, updated, newValue);
                            if (event != null) {
                                sendAccessibilityEvent(event);
                            }
                            if (object2.previousTextSelectionBase == object2.textSelectionBase || object2.previousTextSelectionExtent != object2.textSelectionExtent) {
                                selectionEvent = obtainAccessibilityEvent(object2.id, 8192);
                                selectionEvent.getText().add(newValue);
                                selectionEvent.setFromIndex(object2.textSelectionBase);
                                selectionEvent.setToIndex(object2.textSelectionExtent);
                                selectionEvent.setItemCount(newValue.length());
                                sendAccessibilityEvent(selectionEvent);
                            }
                        }
                        updated = updated2;
                        visitedObjects2 = visitedObjects;
                    }
                    updated = obtainAccessibilityEvent(object2.id, 4);
                    updated.getText().add(object2.label);
                    sendAccessibilityEvent(updated);
                    if (object2.previousValue == null) {
                    }
                    if (object2.value == null) {
                    }
                    event = createTextChangedEvent(object2.id, updated, newValue);
                    if (event != null) {
                        sendAccessibilityEvent(event);
                    }
                    if (object2.previousTextSelectionBase == object2.textSelectionBase) {
                    }
                    selectionEvent = obtainAccessibilityEvent(object2.id, 8192);
                    selectionEvent.getText().add(newValue);
                    selectionEvent.setFromIndex(object2.textSelectionBase);
                    selectionEvent.setToIndex(object2.textSelectionExtent);
                    selectionEvent.setItemCount(newValue.length());
                    sendAccessibilityEvent(selectionEvent);
                    updated = updated2;
                    visitedObjects2 = visitedObjects;
                }
                visitedObjects = visitedObjects2;
            }

            private AccessibilityEvent createTextChangedEvent(int id, String oldValue, String newValue) {
                AccessibilityEvent e = obtainAccessibilityEvent(id, 16);
                e.setBeforeText(oldValue);
                e.getText().add(newValue);
                int i = ROOT_NODE_ID;
                while (i < oldValue.length() && i < newValue.length()) {
                    if (oldValue.charAt(i) != newValue.charAt(i)) {
                        break;
                    }
                    i++;
                }
                if (i >= oldValue.length() && i >= newValue.length()) {
                    return null;
                }
                int firstDifference = i;
                e.setFromIndex(firstDifference);
                int oldIndex = oldValue.length() - 1;
                int newIndex = newValue.length() - 1;
                while (oldIndex >= firstDifference && newIndex >= firstDifference) {
                    if (oldValue.charAt(oldIndex) != newValue.charAt(newIndex)) {
                        break;
                    }
                    oldIndex--;
                    newIndex--;
                }
                e.setRemovedCount((oldIndex - firstDifference) + 1);
                e.setAddedCount((newIndex - firstDifference) + 1);
                return e;
            }

            private AccessibilityEvent obtainAccessibilityEvent(int virtualViewId, int eventType) {
                AccessibilityEvent event = AccessibilityEvent.obtain(eventType);
                event.setPackageName(this.mOwner.getContext().getPackageName());
                event.setSource(this.mOwner, virtualViewId);
                return event;
            }

            private void sendAccessibilityEvent(int virtualViewId, int eventType) {
                if (this.mAccessibilityEnabled) {
                    if (virtualViewId == 0) {
                        this.mOwner.sendAccessibilityEvent(eventType);
                    } else {
                        sendAccessibilityEvent(obtainAccessibilityEvent(virtualViewId, eventType));
                    }
                }
            }

            private void sendAccessibilityEvent(AccessibilityEvent event) {
                if (this.mAccessibilityEnabled) {
                    this.mOwner.getParent().requestSendAccessibilityEvent(this.mOwner, event);
                }
            }

            public void onMessage(Object message, Reply<Object> reply) {
                Object obj;
                Integer nodeId;
                AccessibilityEvent e;
                HashMap<String, Object> annotatedEvent = (HashMap) message;
                String type = (String) annotatedEvent.get("type");
                HashMap<String, Object> data = (HashMap) annotatedEvent.get("data");
                int hashCode = type.hashCode();
                if (hashCode != -1140076541) {
                    if (hashCode != -649620375) {
                        if (hashCode != 114595) {
                            if (hashCode == 114203431) {
                                if (type.equals("longPress")) {
                                    obj = 1;
                                    switch (obj) {
                                        case ROOT_NODE_ID /*0*/:
                                            this.mOwner.announceForAccessibility((String) data.get("message"));
                                            break;
                                        case BuildConfig.VERSION_CODE /*1*/:
                                            nodeId = (Integer) annotatedEvent.get("nodeId");
                                            if (nodeId != null) {
                                                sendAccessibilityEvent(nodeId.intValue(), 2);
                                                break;
                                            }
                                            return;
                                        case 2:
                                            nodeId = (Integer) annotatedEvent.get("nodeId");
                                            if (nodeId != null) {
                                                sendAccessibilityEvent(nodeId.intValue(), 1);
                                                break;
                                            }
                                            return;
                                        case 3:
                                            e = obtainAccessibilityEvent(ROOT_NODE_ID, 32);
                                            e.getText().add((String) data.get("message"));
                                            sendAccessibilityEvent(e);
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        } else if (type.equals("tap")) {
                            obj = 2;
                            switch (obj) {
                                case ROOT_NODE_ID /*0*/:
                                    this.mOwner.announceForAccessibility((String) data.get("message"));
                                    break;
                                case BuildConfig.VERSION_CODE /*1*/:
                                    nodeId = (Integer) annotatedEvent.get("nodeId");
                                    if (nodeId != null) {
                                        sendAccessibilityEvent(nodeId.intValue(), 2);
                                        break;
                                    }
                                    return;
                                case 2:
                                    nodeId = (Integer) annotatedEvent.get("nodeId");
                                    if (nodeId != null) {
                                        sendAccessibilityEvent(nodeId.intValue(), 1);
                                        break;
                                    }
                                    return;
                                case 3:
                                    e = obtainAccessibilityEvent(ROOT_NODE_ID, 32);
                                    e.getText().add((String) data.get("message"));
                                    sendAccessibilityEvent(e);
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else if (type.equals("announce")) {
                        obj = null;
                        switch (obj) {
                            case ROOT_NODE_ID /*0*/:
                                this.mOwner.announceForAccessibility((String) data.get("message"));
                                break;
                            case BuildConfig.VERSION_CODE /*1*/:
                                nodeId = (Integer) annotatedEvent.get("nodeId");
                                if (nodeId != null) {
                                    sendAccessibilityEvent(nodeId.intValue(), 2);
                                    break;
                                }
                                return;
                            case 2:
                                nodeId = (Integer) annotatedEvent.get("nodeId");
                                if (nodeId != null) {
                                    sendAccessibilityEvent(nodeId.intValue(), 1);
                                    break;
                                }
                                return;
                            case 3:
                                e = obtainAccessibilityEvent(ROOT_NODE_ID, 32);
                                e.getText().add((String) data.get("message"));
                                sendAccessibilityEvent(e);
                                break;
                            default:
                                break;
                        }
                    }
                } else if (type.equals("tooltip")) {
                    obj = 3;
                    switch (obj) {
                        case ROOT_NODE_ID /*0*/:
                            this.mOwner.announceForAccessibility((String) data.get("message"));
                            break;
                        case BuildConfig.VERSION_CODE /*1*/:
                            nodeId = (Integer) annotatedEvent.get("nodeId");
                            if (nodeId != null) {
                                sendAccessibilityEvent(nodeId.intValue(), 2);
                                break;
                            }
                            return;
                        case 2:
                            nodeId = (Integer) annotatedEvent.get("nodeId");
                            if (nodeId != null) {
                                sendAccessibilityEvent(nodeId.intValue(), 1);
                                break;
                            }
                            return;
                        case 3:
                            e = obtainAccessibilityEvent(ROOT_NODE_ID, 32);
                            e.getText().add((String) data.get("message"));
                            sendAccessibilityEvent(e);
                            break;
                        default:
                            break;
                    }
                }
                obj = -1;
                switch (obj) {
                    case ROOT_NODE_ID /*0*/:
                        this.mOwner.announceForAccessibility((String) data.get("message"));
                        break;
                    case BuildConfig.VERSION_CODE /*1*/:
                        nodeId = (Integer) annotatedEvent.get("nodeId");
                        if (nodeId != null) {
                            sendAccessibilityEvent(nodeId.intValue(), 2);
                            break;
                        }
                        return;
                    case 2:
                        nodeId = (Integer) annotatedEvent.get("nodeId");
                        if (nodeId != null) {
                            sendAccessibilityEvent(nodeId.intValue(), 1);
                            break;
                        }
                        return;
                    case 3:
                        e = obtainAccessibilityEvent(ROOT_NODE_ID, 32);
                        e.getText().add((String) data.get("message"));
                        sendAccessibilityEvent(e);
                        break;
                    default:
                        break;
                }
            }

            private void createWindowChangeEvent(SemanticsObject route) {
                AccessibilityEvent e = obtainAccessibilityEvent(route.id, 32);
                e.getText().add(route.getRouteName());
                sendAccessibilityEvent(e);
            }

            private void willRemoveSemanticsObject(SemanticsObject object) {
                object.parent = null;
                if (this.mA11yFocusedObject == object) {
                    sendAccessibilityEvent(this.mA11yFocusedObject.id, 65536);
                    this.mA11yFocusedObject = null;
                }
                if (this.mInputFocusedObject == object) {
                    this.mInputFocusedObject = null;
                }
                if (this.mHoveredObject == object) {
                    this.mHoveredObject = null;
                }
            }

            void reset() {
                this.mObjects.clear();
                if (this.mA11yFocusedObject != null) {
                    sendAccessibilityEvent(this.mA11yFocusedObject.id, 65536);
                }
                this.mA11yFocusedObject = null;
                this.mHoveredObject = null;
                sendAccessibilityEvent(ROOT_NODE_ID, 2048);
            }
        }
