package io.flutter.view;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import io.flutter.util.PathUtils;
import java.io.File;
import java.io.FilenameFilter;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

class ResourceExtractor {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final String TAG = "ResourceExtractor";
    private static final String TIMESTAMP_PREFIX = "res_timestamp-";
    private final Context mContext;
    private ExtractTask mExtractTask;
    private final HashSet<String> mResources = new HashSet();

    /* renamed from: io.flutter.view.ResourceExtractor$1 */
    class C00121 implements FilenameFilter {
        C00121() {
        }

        public boolean accept(File dir, String name) {
            return name.startsWith(ResourceExtractor.TIMESTAMP_PREFIX);
        }
    }

    private class ExtractTask extends AsyncTask<Void, Void, Void> {
        private static final int BUFFER_SIZE = 16384;

        ExtractTask() {
        }

        /* JADX WARNING: inconsistent code. */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        private void extractResources() {
            /*
            r16 = this;
            r1 = r16;
            r0 = new java.io.File;
            r2 = io.flutter.view.ResourceExtractor.this;
            r2 = r2.mContext;
            r2 = io.flutter.util.PathUtils.getDataDirectory(r2);
            r0.<init>(r2);
            r2 = r0;
            r3 = r1.checkTimestamp(r2);
            if (r3 == 0) goto L_0x001d;
        L_0x0018:
            r0 = io.flutter.view.ResourceExtractor.this;
            r0.deleteFiles();
        L_0x001d:
            r0 = io.flutter.view.ResourceExtractor.this;
            r0 = r0.mContext;
            r0 = r0.getResources();
            r4 = r0.getAssets();
            r0 = 0;
            r5 = io.flutter.view.ResourceExtractor.this;
            r5 = r5.mResources;
            r5 = r5.iterator();
            r6 = r0;
        L_0x0037:
            r0 = r5.hasNext();
            if (r0 == 0) goto L_0x00f4;
        L_0x003d:
            r0 = r5.next();
            r0 = (java.lang.String) r0;
            r7 = r0;
            r0 = new java.io.File;	 Catch:{ FileNotFoundException -> 0x00f1, IOException -> 0x00d0 }
            r0.<init>(r2, r7);	 Catch:{ FileNotFoundException -> 0x00f1, IOException -> 0x00d0 }
            r8 = r0;
            r0 = r8.exists();	 Catch:{ FileNotFoundException -> 0x00f1, IOException -> 0x00d0 }
            if (r0 == 0) goto L_0x0051;
        L_0x0050:
            goto L_0x0037;
        L_0x0051:
            r0 = r8.getParentFile();	 Catch:{ FileNotFoundException -> 0x00f1, IOException -> 0x00d0 }
            if (r0 == 0) goto L_0x005e;
        L_0x0057:
            r0 = r8.getParentFile();	 Catch:{ FileNotFoundException -> 0x00f1, IOException -> 0x00d0 }
            r0.mkdirs();	 Catch:{ FileNotFoundException -> 0x00f1, IOException -> 0x00d0 }
        L_0x005e:
            r0 = r4.open(r7);	 Catch:{ FileNotFoundException -> 0x00f1, IOException -> 0x00d0 }
            r9 = r0;
            r10 = 0;
            r0 = new java.io.FileOutputStream;	 Catch:{ Throwable -> 0x00b5 }
            r0.<init>(r8);	 Catch:{ Throwable -> 0x00b5 }
            r11 = r0;
            r0 = 16384; // 0x4000 float:2.2959E-41 double:8.0948E-320;
            if (r6 != 0) goto L_0x007b;
        L_0x006e:
            r12 = new byte[r0];	 Catch:{ Throwable -> 0x0077, all -> 0x0072 }
            r6 = r12;
            goto L_0x007b;
        L_0x0072:
            r0 = move-exception;
            r13 = r6;
            r12 = r10;
        L_0x0075:
            r6 = r0;
            goto L_0x009b;
        L_0x0077:
            r0 = move-exception;
            r12 = r6;
            r6 = r0;
            goto L_0x0096;
        L_0x007b:
            r12 = 0;
            r13 = 0;
        L_0x007d:
            r14 = r9.read(r6, r12, r0);	 Catch:{ Throwable -> 0x0077, all -> 0x0072 }
            r13 = r14;
            r15 = -1;
            if (r14 == r15) goto L_0x0089;
        L_0x0085:
            r11.write(r6, r12, r13);	 Catch:{ Throwable -> 0x0077, all -> 0x0072 }
            goto L_0x007d;
        L_0x0089:
            r11.flush();	 Catch:{ Throwable -> 0x0077, all -> 0x0072 }
            r11.close();	 Catch:{ Throwable -> 0x00b5 }
            if (r9 == 0) goto L_0x0094;
        L_0x0091:
            r9.close();	 Catch:{ FileNotFoundException -> 0x00f1, IOException -> 0x00d0 }
            goto L_0x0037;
        L_0x0096:
            throw r6;	 Catch:{ all -> 0x0097 }
        L_0x0097:
            r0 = move-exception;
            r13 = r12;
            r12 = r6;
            goto L_0x0075;
        L_0x009b:
            if (r12 == 0) goto L_0x00a7;
        L_0x009d:
            r11.close();	 Catch:{ Throwable -> 0x00a1, all -> 0x00ab }
            goto L_0x00aa;
        L_0x00a1:
            r0 = move-exception;
            r14 = r0;
            r12.addSuppressed(r14);	 Catch:{ Throwable -> 0x00ad, all -> 0x00ab }
            goto L_0x00aa;
        L_0x00a7:
            r11.close();	 Catch:{ Throwable -> 0x00ad, all -> 0x00ab }
        L_0x00aa:
            throw r6;	 Catch:{ Throwable -> 0x00ad, all -> 0x00ab }
        L_0x00ab:
            r0 = move-exception;
            goto L_0x00b3;
        L_0x00ad:
            r0 = move-exception;
            r10 = r0;
            r6 = r13;
            goto L_0x00b7;
        L_0x00b1:
            r0 = move-exception;
            r13 = r6;
        L_0x00b3:
            r6 = r0;
            goto L_0x00b8;
        L_0x00b5:
            r0 = move-exception;
            r10 = r0;
        L_0x00b7:
            throw r10;	 Catch:{ all -> 0x00b1 }
        L_0x00b8:
            if (r9 == 0) goto L_0x00c9;
        L_0x00ba:
            if (r10 == 0) goto L_0x00c6;
        L_0x00bc:
            r9.close();	 Catch:{ Throwable -> 0x00c0 }
            goto L_0x00c9;
        L_0x00c0:
            r0 = move-exception;
            r11 = r0;
            r10.addSuppressed(r11);	 Catch:{ FileNotFoundException -> 0x00cd, IOException -> 0x00ca }
            goto L_0x00c9;
        L_0x00c6:
            r9.close();	 Catch:{ FileNotFoundException -> 0x00cd, IOException -> 0x00ca }
        L_0x00c9:
            throw r6;	 Catch:{ FileNotFoundException -> 0x00cd, IOException -> 0x00ca }
        L_0x00ca:
            r0 = move-exception;
            r6 = r13;
            goto L_0x00d1;
        L_0x00cd:
            r0 = move-exception;
            r6 = r13;
            goto L_0x00f2;
        L_0x00d0:
            r0 = move-exception;
        L_0x00d1:
            r5 = "ResourceExtractor";
            r8 = new java.lang.StringBuilder;
            r8.<init>();
            r9 = "Exception unpacking resources: ";
            r8.append(r9);
            r9 = r0.getMessage();
            r8.append(r9);
            r8 = r8.toString();
            android.util.Log.w(r5, r8);
            r5 = io.flutter.view.ResourceExtractor.this;
            r5.deleteFiles();
            return;
        L_0x00f1:
            r0 = move-exception;
        L_0x00f2:
            goto L_0x0037;
        L_0x00f4:
            if (r3 == 0) goto L_0x0107;
        L_0x00f6:
            r0 = new java.io.File;	 Catch:{ IOException -> 0x00ff }
            r0.<init>(r2, r3);	 Catch:{ IOException -> 0x00ff }
            r0.createNewFile();	 Catch:{ IOException -> 0x00ff }
            goto L_0x0107;
        L_0x00ff:
            r0 = move-exception;
            r5 = "ResourceExtractor";
            r7 = "Failed to write resource timestamp";
            android.util.Log.w(r5, r7);
        L_0x0107:
            return;
            */
            throw new UnsupportedOperationException("Method not decompiled: io.flutter.view.ResourceExtractor.ExtractTask.extractResources():void");
        }

        private String checkTimestamp(File dataDir) {
            PackageInfo packageInfo = null;
            try {
                packageInfo = ResourceExtractor.this.mContext.getPackageManager().getPackageInfo(ResourceExtractor.this.mContext.getPackageName(), 0);
                if (packageInfo == null) {
                    return ResourceExtractor.TIMESTAMP_PREFIX;
                }
                String expectedTimestamp = new StringBuilder();
                expectedTimestamp.append(ResourceExtractor.TIMESTAMP_PREFIX);
                expectedTimestamp.append(packageInfo.versionCode);
                expectedTimestamp.append("-");
                expectedTimestamp.append(packageInfo.lastUpdateTime);
                expectedTimestamp = expectedTimestamp.toString();
                String[] existingTimestamps = ResourceExtractor.this.getExistingTimestamps(dataDir);
                if (existingTimestamps == null) {
                    return null;
                }
                if (existingTimestamps.length == 1) {
                    if (expectedTimestamp.equals(existingTimestamps[0])) {
                        return null;
                    }
                }
                return expectedTimestamp;
            } catch (NameNotFoundException e) {
                return ResourceExtractor.TIMESTAMP_PREFIX;
            }
        }

        protected Void doInBackground(Void... unused) {
            extractResources();
            return null;
        }
    }

    ResourceExtractor(Context context) {
        this.mContext = context;
    }

    ResourceExtractor addResource(String resource) {
        this.mResources.add(resource);
        return this;
    }

    ResourceExtractor addResources(Collection<String> resources) {
        this.mResources.addAll(resources);
        return this;
    }

    ResourceExtractor start() {
        this.mExtractTask = new ExtractTask();
        this.mExtractTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
        return this;
    }

    void waitForCompletion() {
        try {
            this.mExtractTask.get();
        } catch (CancellationException e) {
            deleteFiles();
        } catch (ExecutionException e2) {
            deleteFiles();
        } catch (InterruptedException e3) {
            deleteFiles();
        }
    }

    private String[] getExistingTimestamps(File dataDir) {
        return dataDir.list(new C00121());
    }

    private void deleteFiles() {
        File dataDir = new File(PathUtils.getDataDirectory(this.mContext));
        Iterator it = this.mResources.iterator();
        while (it.hasNext()) {
            File file = new File(dataDir, (String) it.next());
            if (file.exists()) {
                file.delete();
            }
        }
        String[] existingTimestamps = getExistingTimestamps(dataDir);
        if (existingTimestamps != null) {
            for (String timestamp : existingTimestamps) {
                new File(dataDir, timestamp).delete();
            }
        }
    }
}
