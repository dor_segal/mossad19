package io.flutter.view;

import android.content.Context;
import java.io.File;
import java.io.IOException;

class ResourcePaths {
    public static final String TEMPORARY_RESOURCE_PREFIX = ".org.chromium.Chromium.";

    ResourcePaths() {
    }

    public static File createTempFile(Context context, String suffix) throws IOException {
        String str = TEMPORARY_RESOURCE_PREFIX;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("_");
        stringBuilder.append(suffix);
        return File.createTempFile(str, stringBuilder.toString(), context.getCacheDir());
    }
}
