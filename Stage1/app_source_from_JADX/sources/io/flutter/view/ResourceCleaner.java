package io.flutter.view;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import java.io.File;
import java.io.FilenameFilter;

class ResourceCleaner {
    private static final long DELAY_MS = 5000;
    private static final String TAG = "ResourceCleaner";
    private final Context mContext;

    /* renamed from: io.flutter.view.ResourceCleaner$1 */
    class C00101 implements FilenameFilter {
        C00101() {
        }

        public boolean accept(File dir, String name) {
            return name.startsWith(ResourcePaths.TEMPORARY_RESOURCE_PREFIX);
        }
    }

    private class CleanTask extends AsyncTask<Void, Void, Void> {
        private final File[] mFilesToDelete;

        CleanTask(File[] filesToDelete) {
            this.mFilesToDelete = filesToDelete;
        }

        boolean hasFilesToDelete() {
            return this.mFilesToDelete.length > 0;
        }

        protected Void doInBackground(Void... unused) {
            String str = ResourceCleaner.TAG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Cleaning ");
            stringBuilder.append(this.mFilesToDelete.length);
            stringBuilder.append(" resources.");
            Log.i(str, stringBuilder.toString());
            for (File file : this.mFilesToDelete) {
                if (file.exists()) {
                    deleteRecursively(file);
                }
            }
            return null;
        }

        private void deleteRecursively(File parent) {
            if (parent.isDirectory()) {
                for (File child : parent.listFiles()) {
                    deleteRecursively(child);
                }
            }
            parent.delete();
        }
    }

    ResourceCleaner(Context context) {
        this.mContext = context;
    }

    void start() {
        File cacheDir = this.mContext.getCacheDir();
        if (cacheDir != null) {
            final CleanTask task = new CleanTask(cacheDir.listFiles(new C00101()));
            if (task.hasFilesToDelete()) {
                new Handler().postDelayed(new Runnable() {
                    public void run() {
                        task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Void[0]);
                    }
                }, DELAY_MS);
            }
        }
    }
}
