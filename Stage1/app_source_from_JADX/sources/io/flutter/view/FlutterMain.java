package io.flutter.view;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import com.iwalk.locksmither.BuildConfig;
import io.flutter.util.PathUtils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FlutterMain {
    private static final String AOT_ISOLATE_SNAPSHOT_DATA_KEY = "isolate-snapshot-data";
    private static final String AOT_ISOLATE_SNAPSHOT_INSTR_KEY = "isolate-snapshot-instr";
    private static final String AOT_SHARED_LIBRARY_PATH = "aot-shared-library-path";
    private static final String AOT_SNAPSHOT_PATH_KEY = "aot-snapshot-path";
    private static final String AOT_VM_SNAPSHOT_DATA_KEY = "vm-snapshot-data";
    private static final String AOT_VM_SNAPSHOT_INSTR_KEY = "vm-snapshot-instr";
    private static final String DEFAULT_AOT_ISOLATE_SNAPSHOT_DATA = "isolate_snapshot_data";
    private static final String DEFAULT_AOT_ISOLATE_SNAPSHOT_INSTR = "isolate_snapshot_instr";
    private static final String DEFAULT_AOT_SHARED_LIBRARY_PATH = "app.so";
    private static final String DEFAULT_AOT_VM_SNAPSHOT_DATA = "vm_snapshot_data";
    private static final String DEFAULT_AOT_VM_SNAPSHOT_INSTR = "vm_snapshot_instr";
    private static final String DEFAULT_FLUTTER_ASSETS_DIR = "flutter_assets";
    private static final String DEFAULT_FLX = "app.flx";
    private static final String DEFAULT_KERNEL_BLOB = "kernel_blob.bin";
    private static final String FLUTTER_ASSETS_DIR_KEY = "flutter-assets-dir";
    private static final String FLX_KEY = "flx";
    public static final String PUBLIC_AOT_AOT_SHARED_LIBRARY_PATH;
    public static final String PUBLIC_AOT_ISOLATE_SNAPSHOT_DATA_KEY;
    public static final String PUBLIC_AOT_ISOLATE_SNAPSHOT_INSTR_KEY;
    public static final String PUBLIC_AOT_VM_SNAPSHOT_DATA_KEY;
    public static final String PUBLIC_AOT_VM_SNAPSHOT_INSTR_KEY;
    public static final String PUBLIC_FLUTTER_ASSETS_DIR_KEY;
    public static final String PUBLIC_FLX_KEY;
    private static final String SHARED_ASSET_DIR = "flutter_shared";
    private static final String SHARED_ASSET_ICU_DATA = "icudtl.dat";
    private static final String TAG = "FlutterMain";
    private static String sAotIsolateSnapshotData = DEFAULT_AOT_ISOLATE_SNAPSHOT_DATA;
    private static String sAotIsolateSnapshotInstr = DEFAULT_AOT_ISOLATE_SNAPSHOT_INSTR;
    private static String sAotSharedLibraryPath = DEFAULT_AOT_SHARED_LIBRARY_PATH;
    private static String sAotVmSnapshotData = DEFAULT_AOT_VM_SNAPSHOT_DATA;
    private static String sAotVmSnapshotInstr = DEFAULT_AOT_VM_SNAPSHOT_INSTR;
    private static String sFlutterAssetsDir = DEFAULT_FLUTTER_ASSETS_DIR;
    private static String sFlx = DEFAULT_FLX;
    private static String sIcuDataPath;
    private static boolean sInitialized = false;
    private static boolean sIsPrecompiledAsBlobs;
    private static boolean sIsPrecompiledAsSharedLibrary;
    private static ResourceExtractor sResourceExtractor;
    private static Settings sSettings;

    private static final class ImmutableSetBuilder<T> {
        HashSet<T> set = new HashSet();

        static <T> ImmutableSetBuilder<T> newInstance() {
            return new ImmutableSetBuilder();
        }

        private ImmutableSetBuilder() {
        }

        ImmutableSetBuilder<T> add(T element) {
            this.set.add(element);
            return this;
        }

        @SafeVarargs
        final ImmutableSetBuilder<T> add(T... elements) {
            for (T element : elements) {
                this.set.add(element);
            }
            return this;
        }

        Set<T> build() {
            return Collections.unmodifiableSet(this.set);
        }
    }

    public static class Settings {
        private String logTag;

        public String getLogTag() {
            return this.logTag;
        }

        public void setLogTag(String tag) {
            this.logTag = tag;
        }
    }

    private static native void nativeInit(Context context, String[] strArr, String str, String str2, String str3);

    private static native void nativeRecordStartTimestamp(long j);

    static {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(FlutterMain.class.getName());
        stringBuilder.append('.');
        stringBuilder.append(AOT_SHARED_LIBRARY_PATH);
        PUBLIC_AOT_AOT_SHARED_LIBRARY_PATH = stringBuilder.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append(FlutterMain.class.getName());
        stringBuilder.append('.');
        stringBuilder.append(AOT_VM_SNAPSHOT_DATA_KEY);
        PUBLIC_AOT_VM_SNAPSHOT_DATA_KEY = stringBuilder.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append(FlutterMain.class.getName());
        stringBuilder.append('.');
        stringBuilder.append(AOT_VM_SNAPSHOT_INSTR_KEY);
        PUBLIC_AOT_VM_SNAPSHOT_INSTR_KEY = stringBuilder.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append(FlutterMain.class.getName());
        stringBuilder.append('.');
        stringBuilder.append(AOT_ISOLATE_SNAPSHOT_DATA_KEY);
        PUBLIC_AOT_ISOLATE_SNAPSHOT_DATA_KEY = stringBuilder.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append(FlutterMain.class.getName());
        stringBuilder.append('.');
        stringBuilder.append(AOT_ISOLATE_SNAPSHOT_INSTR_KEY);
        PUBLIC_AOT_ISOLATE_SNAPSHOT_INSTR_KEY = stringBuilder.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append(FlutterMain.class.getName());
        stringBuilder.append('.');
        stringBuilder.append(FLX_KEY);
        PUBLIC_FLX_KEY = stringBuilder.toString();
        stringBuilder = new StringBuilder();
        stringBuilder.append(FlutterMain.class.getName());
        stringBuilder.append('.');
        stringBuilder.append(FLUTTER_ASSETS_DIR_KEY);
        PUBLIC_FLUTTER_ASSETS_DIR_KEY = stringBuilder.toString();
    }

    private static String fromFlutterAssets(String filePath) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(sFlutterAssetsDir);
        stringBuilder.append(File.separator);
        stringBuilder.append(filePath);
        return stringBuilder.toString();
    }

    public static void startInitialization(Context applicationContext) {
        startInitialization(applicationContext, new Settings());
    }

    public static void startInitialization(Context applicationContext, Settings settings) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            throw new IllegalStateException("startInitialization must be called on the main thread");
        } else if (sSettings == null) {
            sSettings = settings;
            long initStartTimestampMillis = SystemClock.uptimeMillis();
            initConfig(applicationContext);
            initAot(applicationContext);
            initResources(applicationContext);
            System.loadLibrary("flutter");
            nativeRecordStartTimestamp(SystemClock.uptimeMillis() - initStartTimestampMillis);
        }
    }

    public static void ensureInitializationComplete(Context applicationContext, String[] args) {
        if (Looper.myLooper() != Looper.getMainLooper()) {
            throw new IllegalStateException("ensureInitializationComplete must be called on the main thread");
        } else if (sSettings == null) {
            throw new IllegalStateException("ensureInitializationComplete must be called after startInitialization");
        } else if (!sInitialized) {
            try {
                sResourceExtractor.waitForCompletion();
                List<String> shellArgs = new ArrayList();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("--icu-data-file-path=");
                stringBuilder.append(sIcuDataPath);
                shellArgs.add(stringBuilder.toString());
                if (args != null) {
                    Collections.addAll(shellArgs, args);
                }
                if (sIsPrecompiledAsSharedLibrary) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("--aot-shared-library-path=");
                    stringBuilder.append(new File(PathUtils.getDataDirectory(applicationContext), sAotSharedLibraryPath));
                    shellArgs.add(stringBuilder.toString());
                } else {
                    if (sIsPrecompiledAsBlobs) {
                        stringBuilder = new StringBuilder();
                        stringBuilder.append("--aot-snapshot-path=");
                        stringBuilder.append(PathUtils.getDataDirectory(applicationContext));
                        shellArgs.add(stringBuilder.toString());
                    } else {
                        stringBuilder = new StringBuilder();
                        stringBuilder.append("--cache-dir-path=");
                        stringBuilder.append(PathUtils.getCacheDirectory(applicationContext));
                        shellArgs.add(stringBuilder.toString());
                        stringBuilder = new StringBuilder();
                        stringBuilder.append("--aot-snapshot-path=");
                        stringBuilder.append(PathUtils.getDataDirectory(applicationContext));
                        stringBuilder.append("/");
                        stringBuilder.append(sFlutterAssetsDir);
                        shellArgs.add(stringBuilder.toString());
                    }
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("--vm-snapshot-data=");
                    stringBuilder.append(sAotVmSnapshotData);
                    shellArgs.add(stringBuilder.toString());
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("--vm-snapshot-instr=");
                    stringBuilder.append(sAotVmSnapshotInstr);
                    shellArgs.add(stringBuilder.toString());
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("--isolate-snapshot-data=");
                    stringBuilder.append(sAotIsolateSnapshotData);
                    shellArgs.add(stringBuilder.toString());
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("--isolate-snapshot-instr=");
                    stringBuilder.append(sAotIsolateSnapshotInstr);
                    shellArgs.add(stringBuilder.toString());
                }
                if (sSettings.getLogTag() != null) {
                    stringBuilder = new StringBuilder();
                    stringBuilder.append("--log-tag=");
                    stringBuilder.append(sSettings.getLogTag());
                    shellArgs.add(stringBuilder.toString());
                }
                nativeInit(applicationContext, (String[]) shellArgs.toArray(new String[0]), findAppBundlePath(applicationContext), PathUtils.getFilesDir(applicationContext), PathUtils.getCacheDirectory(applicationContext));
                sInitialized = true;
            } catch (Exception e) {
                Log.e(TAG, "Flutter initialization failed.", e);
                throw new RuntimeException(e);
            }
        }
    }

    private static void initConfig(Context applicationContext) {
        try {
            Bundle metadata = applicationContext.getPackageManager().getApplicationInfo(applicationContext.getPackageName(), 128).metaData;
            if (metadata != null) {
                sAotSharedLibraryPath = metadata.getString(PUBLIC_AOT_AOT_SHARED_LIBRARY_PATH, DEFAULT_AOT_SHARED_LIBRARY_PATH);
                sAotVmSnapshotData = metadata.getString(PUBLIC_AOT_VM_SNAPSHOT_DATA_KEY, DEFAULT_AOT_VM_SNAPSHOT_DATA);
                sAotVmSnapshotInstr = metadata.getString(PUBLIC_AOT_VM_SNAPSHOT_INSTR_KEY, DEFAULT_AOT_VM_SNAPSHOT_INSTR);
                sAotIsolateSnapshotData = metadata.getString(PUBLIC_AOT_ISOLATE_SNAPSHOT_DATA_KEY, DEFAULT_AOT_ISOLATE_SNAPSHOT_DATA);
                sAotIsolateSnapshotInstr = metadata.getString(PUBLIC_AOT_ISOLATE_SNAPSHOT_INSTR_KEY, DEFAULT_AOT_ISOLATE_SNAPSHOT_INSTR);
                sFlx = metadata.getString(PUBLIC_FLX_KEY, DEFAULT_FLX);
                sFlutterAssetsDir = metadata.getString(PUBLIC_FLUTTER_ASSETS_DIR_KEY, DEFAULT_FLUTTER_ASSETS_DIR);
            }
        } catch (NameNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    private static void initResources(Context applicationContext) {
        Context context = applicationContext;
        new ResourceCleaner(context).start();
        sResourceExtractor = new ResourceExtractor(context);
        String icuAssetPath = new StringBuilder();
        icuAssetPath.append(SHARED_ASSET_DIR);
        icuAssetPath.append(File.separator);
        icuAssetPath.append(SHARED_ASSET_ICU_DATA);
        icuAssetPath = icuAssetPath.toString();
        sResourceExtractor.addResource(icuAssetPath);
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(PathUtils.getDataDirectory(applicationContext));
        stringBuilder.append(File.separator);
        stringBuilder.append(icuAssetPath);
        sIcuDataPath = stringBuilder.toString();
        sResourceExtractor.addResource(fromFlutterAssets(sFlx)).addResource(fromFlutterAssets(sAotVmSnapshotData)).addResource(fromFlutterAssets(sAotVmSnapshotInstr)).addResource(fromFlutterAssets(sAotIsolateSnapshotData)).addResource(fromFlutterAssets(sAotIsolateSnapshotInstr)).addResource(fromFlutterAssets(DEFAULT_KERNEL_BLOB));
        if (sIsPrecompiledAsSharedLibrary) {
            sResourceExtractor.addResource(sAotSharedLibraryPath);
        } else {
            sResourceExtractor.addResource(sAotVmSnapshotData).addResource(sAotVmSnapshotInstr).addResource(sAotIsolateSnapshotData).addResource(sAotIsolateSnapshotInstr);
        }
        sResourceExtractor.start();
    }

    private static Set<String> listAssets(Context applicationContext, String path) {
        try {
            return ImmutableSetBuilder.newInstance().add(applicationContext.getResources().getAssets().list(path)).build();
        } catch (IOException e) {
            Log.e(TAG, "Unable to list assets", e);
            throw new RuntimeException(e);
        }
    }

    private static void initAot(Context applicationContext) {
        Set<String> assets = listAssets(applicationContext, BuildConfig.FLAVOR);
        sIsPrecompiledAsBlobs = assets.containsAll(Arrays.asList(new String[]{sAotVmSnapshotData, sAotVmSnapshotInstr, sAotIsolateSnapshotData, sAotIsolateSnapshotInstr}));
        sIsPrecompiledAsSharedLibrary = assets.contains(sAotSharedLibraryPath);
        if (!sIsPrecompiledAsBlobs) {
            return;
        }
        if (sIsPrecompiledAsSharedLibrary) {
            throw new RuntimeException("Found precompiled app as shared library and as Dart VM snapshots.");
        }
    }

    public static boolean isRunningPrecompiledCode() {
        if (!sIsPrecompiledAsBlobs) {
            if (!sIsPrecompiledAsSharedLibrary) {
                return false;
            }
        }
        return true;
    }

    public static String findAppBundlePath(Context applicationContext) {
        File appBundle = new File(PathUtils.getDataDirectory(applicationContext), sFlutterAssetsDir);
        return appBundle.exists() ? appBundle.getPath() : null;
    }

    public static String getLookupKeyForAsset(String asset) {
        return fromFlutterAssets(asset);
    }

    public static String getLookupKeyForAsset(String asset, String packageName) {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("packages");
        stringBuilder.append(File.separator);
        stringBuilder.append(packageName);
        stringBuilder.append(File.separator);
        stringBuilder.append(asset);
        return getLookupKeyForAsset(stringBuilder.toString());
    }
}
