package io.flutter.view;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.database.ContentObserver;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.graphics.SurfaceTexture;
import android.graphics.SurfaceTexture.OnFrameAvailableListener;
import android.net.Uri;
import android.os.Build.VERSION;
import android.os.Handler;
import android.provider.Settings.Global;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceHolder.Callback;
import android.view.SurfaceView;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityManager;
import android.view.accessibility.AccessibilityManager.AccessibilityStateChangeListener;
import android.view.accessibility.AccessibilityManager.TouchExplorationStateChangeListener;
import android.view.accessibility.AccessibilityNodeProvider;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import com.iwalk.locksmither.BuildConfig;
import io.flutter.app.FlutterPluginRegistry;
import io.flutter.plugin.common.ActivityLifecycleListener;
import io.flutter.plugin.common.BasicMessageChannel;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.BinaryMessenger.BinaryMessageHandler;
import io.flutter.plugin.common.BinaryMessenger.BinaryReply;
import io.flutter.plugin.common.JSONMessageCodec;
import io.flutter.plugin.common.JSONMethodCodec;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.StandardMessageCodec;
import io.flutter.plugin.common.StringCodec;
import io.flutter.plugin.editing.TextInputPlugin;
import io.flutter.plugin.platform.PlatformPlugin;
import io.flutter.view.TextureRegistry.SurfaceTextureEntry;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;
import org.json.JSONException;

public class FlutterView extends SurfaceView implements BinaryMessenger, TextureRegistry, AccessibilityStateChangeListener {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final String TAG = "FlutterView";
    private static final int kPointerChangeAdd = 1;
    private static final int kPointerChangeCancel = 0;
    private static final int kPointerChangeDown = 4;
    private static final int kPointerChangeHover = 3;
    private static final int kPointerChangeMove = 5;
    private static final int kPointerChangeRemove = 2;
    private static final int kPointerChangeUp = 6;
    private static final int kPointerDeviceKindInvertedStylus = 3;
    private static final int kPointerDeviceKindMouse = 1;
    private static final int kPointerDeviceKindStylus = 2;
    private static final int kPointerDeviceKindTouch = 0;
    private static final int kPointerDeviceKindUnknown = 4;
    private boolean mAccessibilityEnabled;
    private int mAccessibilityFeatureFlags;
    private final AccessibilityManager mAccessibilityManager;
    private AccessibilityBridge mAccessibilityNodeProvider;
    private final List<ActivityLifecycleListener> mActivityLifecycleListeners;
    private final AnimationScaleObserver mAnimationScaleObserver;
    private final List<FirstFrameListener> mFirstFrameListeners;
    private final BasicMessageChannel<Object> mFlutterKeyEventChannel;
    private final BasicMessageChannel<String> mFlutterLifecycleChannel;
    private final MethodChannel mFlutterLocalizationChannel;
    private final MethodChannel mFlutterNavigationChannel;
    private final BasicMessageChannel<Object> mFlutterSettingsChannel;
    private final BasicMessageChannel<Object> mFlutterSystemChannel;
    private final InputMethodManager mImm;
    private boolean mIsSoftwareRenderingEnabled;
    private InputConnection mLastInputConnection;
    private final ViewportMetrics mMetrics;
    private FlutterNativeView mNativeView;
    private final Callback mSurfaceCallback;
    private final TextInputPlugin mTextInputPlugin;
    private boolean mTouchExplorationEnabled;
    private TouchExplorationListener mTouchExplorationListener;
    private final AtomicLong nextTextureId;

    /* renamed from: io.flutter.view.FlutterView$1 */
    class C00081 implements Callback {
        C00081() {
        }

        public void surfaceCreated(SurfaceHolder holder) {
            FlutterView.this.assertAttached();
            FlutterView.nativeSurfaceCreated(FlutterView.this.mNativeView.get(), holder.getSurface());
        }

        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            FlutterView.this.assertAttached();
            FlutterView.nativeSurfaceChanged(FlutterView.this.mNativeView.get(), width, height);
        }

        public void surfaceDestroyed(SurfaceHolder holder) {
            FlutterView.this.assertAttached();
            FlutterView.nativeSurfaceDestroyed(FlutterView.this.mNativeView.get());
        }
    }

    private enum AccessibilityFeature {
        ACCESSIBLE_NAVIGATION(1),
        INVERT_COLORS(2),
        DISABLE_ANIMATIONS(4);
        
        final int value;

        private AccessibilityFeature(int value) {
            this.value = value;
        }
    }

    private class AnimationScaleObserver extends ContentObserver {
        public AnimationScaleObserver(Handler handler) {
            super(handler);
        }

        public void onChange(boolean selfChange) {
            onChange(selfChange, null);
        }

        public void onChange(boolean selfChange, Uri uri) {
            String value = Global.getString(FlutterView.this.getContext().getContentResolver(), "transition_animation_scale");
            if (value == null || !value.equals("0")) {
                FlutterView.this.mAccessibilityFeatureFlags = FlutterView.this.mAccessibilityFeatureFlags & (AccessibilityFeature.DISABLE_ANIMATIONS.value ^ -1);
            } else {
                FlutterView.this.mAccessibilityFeatureFlags = FlutterView.this.mAccessibilityFeatureFlags | AccessibilityFeature.DISABLE_ANIMATIONS.value;
            }
            FlutterView.nativeSetAccessibilityFeatures(FlutterView.this.mNativeView.get(), FlutterView.this.mAccessibilityFeatureFlags);
        }
    }

    public interface FirstFrameListener {
        void onFirstFrame();
    }

    public interface Provider {
        FlutterView getFlutterView();
    }

    class TouchExplorationListener implements TouchExplorationStateChangeListener {
        TouchExplorationListener() {
        }

        public void onTouchExplorationStateChanged(boolean enabled) {
            if (enabled) {
                FlutterView.this.mTouchExplorationEnabled = true;
                FlutterView.this.ensureAccessibilityEnabled();
                FlutterView.this.mAccessibilityFeatureFlags = FlutterView.this.mAccessibilityFeatureFlags | AccessibilityFeature.ACCESSIBLE_NAVIGATION.value;
                FlutterView.nativeSetAccessibilityFeatures(FlutterView.this.mNativeView.get(), FlutterView.this.mAccessibilityFeatureFlags);
            } else {
                FlutterView.this.mTouchExplorationEnabled = false;
                if (FlutterView.this.mAccessibilityNodeProvider != null) {
                    FlutterView.this.mAccessibilityNodeProvider.handleTouchExplorationExit();
                }
                FlutterView.this.mAccessibilityFeatureFlags = FlutterView.this.mAccessibilityFeatureFlags & (AccessibilityFeature.ACCESSIBLE_NAVIGATION.value ^ -1);
                FlutterView.nativeSetAccessibilityFeatures(FlutterView.this.mNativeView.get(), FlutterView.this.mAccessibilityFeatureFlags);
            }
            FlutterView.this.resetWillNotDraw();
        }
    }

    static final class ViewportMetrics {
        float devicePixelRatio = 1.0f;
        int physicalHeight = 0;
        int physicalPaddingBottom = 0;
        int physicalPaddingLeft = 0;
        int physicalPaddingRight = 0;
        int physicalPaddingTop = 0;
        int physicalViewInsetBottom = 0;
        int physicalViewInsetLeft = 0;
        int physicalViewInsetRight = 0;
        int physicalViewInsetTop = 0;
        int physicalWidth = 0;

        ViewportMetrics() {
        }
    }

    enum ZeroSides {
        NONE,
        LEFT,
        RIGHT,
        BOTH
    }

    final class SurfaceTextureRegistryEntry implements SurfaceTextureEntry {
        private final long id;
        private OnFrameAvailableListener onFrameListener = new C00091();
        private boolean released;
        private final SurfaceTexture surfaceTexture;

        /* renamed from: io.flutter.view.FlutterView$SurfaceTextureRegistryEntry$1 */
        class C00091 implements OnFrameAvailableListener {
            C00091() {
            }

            public void onFrameAvailable(SurfaceTexture texture) {
                if (!SurfaceTextureRegistryEntry.this.released) {
                    FlutterView.nativeMarkTextureFrameAvailable(FlutterView.this.mNativeView.get(), SurfaceTextureRegistryEntry.this.id);
                }
            }
        }

        SurfaceTextureRegistryEntry(long id, SurfaceTexture surfaceTexture) {
            this.id = id;
            this.surfaceTexture = surfaceTexture;
            if (VERSION.SDK_INT >= 21) {
                this.surfaceTexture.setOnFrameAvailableListener(this.onFrameListener, new Handler());
            } else {
                this.surfaceTexture.setOnFrameAvailableListener(this.onFrameListener);
            }
        }

        public SurfaceTexture surfaceTexture() {
            return this.surfaceTexture;
        }

        public long id() {
            return this.id;
        }

        public void release() {
            if (!this.released) {
                this.released = true;
                FlutterView.nativeUnregisterTexture(FlutterView.this.mNativeView.get(), this.id);
                this.surfaceTexture.setOnFrameAvailableListener(null);
                this.surfaceTexture.release();
            }
        }
    }

    private static native void nativeDispatchPointerDataPacket(long j, ByteBuffer byteBuffer, int i);

    private static native void nativeDispatchSemanticsAction(long j, int i, int i2, ByteBuffer byteBuffer, int i3);

    private static native Bitmap nativeGetBitmap(long j);

    private static native boolean nativeGetIsSoftwareRenderingEnabled();

    private static native void nativeMarkTextureFrameAvailable(long j, long j2);

    private static native void nativeRegisterTexture(long j, long j2, SurfaceTexture surfaceTexture);

    private static native void nativeSetAccessibilityFeatures(long j, int i);

    private static native void nativeSetSemanticsEnabled(long j, boolean z);

    private static native void nativeSetViewportMetrics(long j, float f, int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9, int i10);

    private static native void nativeSurfaceChanged(long j, int i, int i2);

    private static native void nativeSurfaceCreated(long j, Surface surface);

    private static native void nativeSurfaceDestroyed(long j);

    private static native void nativeUnregisterTexture(long j, long j2);

    public FlutterView(Context context) {
        this(context, null);
    }

    public FlutterView(Context context, AttributeSet attrs) {
        this(context, attrs, null);
    }

    public FlutterView(Context context, AttributeSet attrs, FlutterNativeView nativeView) {
        super(context, attrs);
        this.nextTextureId = new AtomicLong(0);
        this.mIsSoftwareRenderingEnabled = false;
        this.mAccessibilityEnabled = false;
        this.mTouchExplorationEnabled = false;
        this.mAccessibilityFeatureFlags = 0;
        this.mIsSoftwareRenderingEnabled = nativeGetIsSoftwareRenderingEnabled();
        this.mAnimationScaleObserver = new AnimationScaleObserver(new Handler());
        this.mMetrics = new ViewportMetrics();
        this.mMetrics.devicePixelRatio = context.getResources().getDisplayMetrics().density;
        setFocusable(true);
        setFocusableInTouchMode(true);
        Activity activity = (Activity) getContext();
        if (nativeView == null) {
            this.mNativeView = new FlutterNativeView(activity.getApplicationContext());
        } else {
            this.mNativeView = nativeView;
        }
        this.mNativeView.attachViewAndActivity(this, activity);
        this.mSurfaceCallback = new C00081();
        getHolder().addCallback(this.mSurfaceCallback);
        this.mAccessibilityManager = (AccessibilityManager) getContext().getSystemService("accessibility");
        this.mActivityLifecycleListeners = new ArrayList();
        this.mFirstFrameListeners = new ArrayList();
        this.mFlutterLocalizationChannel = new MethodChannel(this, "flutter/localization", JSONMethodCodec.INSTANCE);
        this.mFlutterNavigationChannel = new MethodChannel(this, "flutter/navigation", JSONMethodCodec.INSTANCE);
        this.mFlutterKeyEventChannel = new BasicMessageChannel(this, "flutter/keyevent", JSONMessageCodec.INSTANCE);
        this.mFlutterLifecycleChannel = new BasicMessageChannel(this, "flutter/lifecycle", StringCodec.INSTANCE);
        this.mFlutterSystemChannel = new BasicMessageChannel(this, "flutter/system", JSONMessageCodec.INSTANCE);
        this.mFlutterSettingsChannel = new BasicMessageChannel(this, "flutter/settings", JSONMessageCodec.INSTANCE);
        PlatformPlugin platformPlugin = new PlatformPlugin(activity);
        new MethodChannel(this, "flutter/platform", JSONMethodCodec.INSTANCE).setMethodCallHandler(platformPlugin);
        addActivityLifecycleListener(platformPlugin);
        this.mImm = (InputMethodManager) getContext().getSystemService("input_method");
        this.mTextInputPlugin = new TextInputPlugin(this);
        setLocales(getResources().getConfiguration());
        setUserSettings();
    }

    private void encodeKeyEvent(KeyEvent event, Map<String, Object> message) {
        message.put("flags", Integer.valueOf(event.getFlags()));
        message.put("codePoint", Integer.valueOf(event.getUnicodeChar()));
        message.put("keyCode", Integer.valueOf(event.getKeyCode()));
        message.put("scanCode", Integer.valueOf(event.getScanCode()));
        message.put("metaState", Integer.valueOf(event.getMetaState()));
    }

    public boolean onKeyUp(int keyCode, KeyEvent event) {
        if (!isAttached()) {
            return super.onKeyUp(keyCode, event);
        }
        Map<String, Object> message = new HashMap();
        message.put("type", "keyup");
        message.put("keymap", "android");
        encodeKeyEvent(event, message);
        this.mFlutterKeyEventChannel.send(message);
        return super.onKeyUp(keyCode, event);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (!isAttached()) {
            return super.onKeyDown(keyCode, event);
        }
        if (!(event.getDeviceId() == -1 || this.mLastInputConnection == null || !this.mImm.isAcceptingText())) {
            this.mLastInputConnection.sendKeyEvent(event);
        }
        Map<String, Object> message = new HashMap();
        message.put("type", "keydown");
        message.put("keymap", "android");
        encodeKeyEvent(event, message);
        this.mFlutterKeyEventChannel.send(message);
        return super.onKeyDown(keyCode, event);
    }

    public FlutterNativeView getFlutterNativeView() {
        return this.mNativeView;
    }

    public FlutterPluginRegistry getPluginRegistry() {
        return this.mNativeView.getPluginRegistry();
    }

    public String getLookupKeyForAsset(String asset) {
        return FlutterMain.getLookupKeyForAsset(asset);
    }

    public String getLookupKeyForAsset(String asset, String packageName) {
        return FlutterMain.getLookupKeyForAsset(asset, packageName);
    }

    public void addActivityLifecycleListener(ActivityLifecycleListener listener) {
        this.mActivityLifecycleListeners.add(listener);
    }

    public void onStart() {
        this.mFlutterLifecycleChannel.send("AppLifecycleState.inactive");
    }

    public void onPause() {
        this.mFlutterLifecycleChannel.send("AppLifecycleState.inactive");
    }

    public void onPostResume() {
        updateAccessibilityFeatures();
        for (ActivityLifecycleListener listener : this.mActivityLifecycleListeners) {
            listener.onPostResume();
        }
        this.mFlutterLifecycleChannel.send("AppLifecycleState.resumed");
    }

    public void onStop() {
        this.mFlutterLifecycleChannel.send("AppLifecycleState.paused");
    }

    public void onMemoryPressure() {
        Map<String, Object> message = new HashMap(1);
        message.put("type", "memoryPressure");
        this.mFlutterSystemChannel.send(message);
    }

    public void addFirstFrameListener(FirstFrameListener listener) {
        this.mFirstFrameListeners.add(listener);
    }

    public void removeFirstFrameListener(FirstFrameListener listener) {
        this.mFirstFrameListeners.remove(listener);
    }

    public void enableTransparentBackground() {
        setZOrderOnTop(true);
        getHolder().setFormat(-2);
    }

    public void disableTransparentBackground() {
        setZOrderOnTop(false);
        getHolder().setFormat(-1);
    }

    public void setInitialRoute(String route) {
        this.mFlutterNavigationChannel.invokeMethod("setInitialRoute", route);
    }

    public void pushRoute(String route) {
        this.mFlutterNavigationChannel.invokeMethod("pushRoute", route);
    }

    public void popRoute() {
        this.mFlutterNavigationChannel.invokeMethod("popRoute", null);
    }

    private void setUserSettings() {
        Map<String, Object> message = new HashMap();
        message.put("textScaleFactor", Float.valueOf(getResources().getConfiguration().fontScale));
        message.put("alwaysUse24HourFormat", Boolean.valueOf(DateFormat.is24HourFormat(getContext())));
        this.mFlutterSettingsChannel.send(message);
    }

    private void setLocales(Configuration config) {
        if (VERSION.SDK_INT >= 24) {
            try {
                Object localeList = config.getClass().getDeclaredMethod("getLocales", new Class[0]).invoke(config, new Object[0]);
                Method localeListGet = localeList.getClass().getDeclaredMethod("get", new Class[]{Integer.TYPE});
                int localeCount = ((Integer) localeList.getClass().getDeclaredMethod("size", new Class[0]).invoke(localeList, new Object[0])).intValue();
                List<String> data = new ArrayList();
                for (int index = 0; index < localeCount; index++) {
                    Locale locale = (Locale) localeListGet.invoke(localeList, new Object[]{Integer.valueOf(index)});
                    data.add(locale.getLanguage());
                    data.add(locale.getCountry());
                    data.add(locale.getScript());
                    data.add(locale.getVariant());
                }
                this.mFlutterLocalizationChannel.invokeMethod("setLocale", data);
                return;
            } catch (Exception e) {
            }
        }
        Locale locale2 = config.locale;
        MethodChannel methodChannel = this.mFlutterLocalizationChannel;
        String str = "setLocale";
        String[] strArr = new String[4];
        strArr[0] = locale2.getLanguage();
        strArr[1] = locale2.getCountry();
        strArr[2] = VERSION.SDK_INT >= 21 ? locale2.getScript() : BuildConfig.FLAVOR;
        strArr[3] = locale2.getVariant();
        methodChannel.invokeMethod(str, Arrays.asList(strArr));
    }

    protected void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        setLocales(newConfig);
        setUserSettings();
    }

    float getDevicePixelRatio() {
        return this.mMetrics.devicePixelRatio;
    }

    public FlutterNativeView detach() {
        if (!isAttached()) {
            return null;
        }
        getHolder().removeCallback(this.mSurfaceCallback);
        this.mNativeView.detach();
        FlutterNativeView view = this.mNativeView;
        this.mNativeView = null;
        return view;
    }

    public void destroy() {
        if (isAttached()) {
            getHolder().removeCallback(this.mSurfaceCallback);
            this.mNativeView.destroy();
            this.mNativeView = null;
        }
    }

    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        try {
            this.mLastInputConnection = this.mTextInputPlugin.createInputConnection(this, outAttrs);
            return this.mLastInputConnection;
        } catch (JSONException e) {
            Log.e(TAG, "Failed to create input connection", e);
            return null;
        }
    }

    private int getPointerChangeForAction(int maskedAction) {
        if (maskedAction == 0) {
            return 4;
        }
        if (maskedAction == 1) {
            return kPointerChangeUp;
        }
        if (maskedAction == kPointerChangeMove) {
            return 4;
        }
        if (maskedAction == kPointerChangeUp) {
            return kPointerChangeUp;
        }
        if (maskedAction == 2) {
            return kPointerChangeMove;
        }
        if (maskedAction == 3) {
            return 0;
        }
        return -1;
    }

    private int getPointerDeviceTypeForToolType(int toolType) {
        switch (toolType) {
            case BuildConfig.VERSION_CODE /*1*/:
                return 0;
            case 2:
                return 2;
            case 3:
                return 1;
            case 4:
                return 3;
            default:
                return 4;
        }
    }

    private void addPointerForIndex(MotionEvent event, int pointerIndex, int pointerChange, int pointerData, ByteBuffer packet) {
        if (pointerChange != -1) {
            int pointerKind = getPointerDeviceTypeForToolType(event.getToolType(pointerIndex));
            packet.putLong(event.getEventTime() * 1000);
            packet.putLong((long) pointerChange);
            packet.putLong((long) pointerKind);
            packet.putLong((long) event.getPointerId(pointerIndex));
            packet.putDouble((double) event.getX(pointerIndex));
            packet.putDouble((double) event.getY(pointerIndex));
            if (pointerKind == 1) {
                packet.putLong((long) (event.getButtonState() & 31));
            } else if (pointerKind == 2) {
                packet.putLong((long) ((event.getButtonState() >> 4) & 15));
            } else {
                packet.putLong(0);
            }
            packet.putLong(0);
            packet.putDouble((double) event.getPressure(pointerIndex));
            packet.putDouble(0.0d);
            packet.putDouble(1.0d);
            if (pointerKind == 2) {
                packet.putDouble((double) event.getAxisValue(24, pointerIndex));
                packet.putDouble(0.0d);
            } else {
                packet.putDouble(0.0d);
                packet.putDouble(0.0d);
            }
            packet.putDouble((double) event.getSize(pointerIndex));
            packet.putDouble((double) event.getToolMajor(pointerIndex));
            packet.putDouble((double) event.getToolMinor(pointerIndex));
            packet.putDouble(0.0d);
            packet.putDouble(0.0d);
            packet.putDouble((double) event.getAxisValue(8, pointerIndex));
            if (pointerKind == 2) {
                packet.putDouble((double) event.getAxisValue(25, pointerIndex));
            } else {
                packet.putDouble(0.0d);
            }
            packet.putLong((long) pointerData);
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        FlutterView flutterView = this;
        int p = 0;
        if (!isAttached()) {
            return false;
        }
        if (VERSION.SDK_INT >= 21) {
            requestUnbufferedDispatch(event);
        }
        int pointerCount = event.getPointerCount();
        ByteBuffer packet = ByteBuffer.allocateDirect((pointerCount * 21) * 8);
        packet.order(ByteOrder.LITTLE_ENDIAN);
        int maskedAction = event.getActionMasked();
        int pointerChange = getPointerChangeForAction(event.getActionMasked());
        if (maskedAction != 0) {
            if (maskedAction != kPointerChangeMove) {
                int p2;
                if (maskedAction != 1) {
                    if (maskedAction != kPointerChangeUp) {
                        while (true) {
                            p2 = p;
                            if (p2 >= pointerCount) {
                                break;
                            }
                            addPointerForIndex(event, p2, pointerChange, 0, packet);
                            p = p2 + 1;
                        }
                        nativeDispatchPointerDataPacket(flutterView.mNativeView.get(), packet, packet.position());
                        return true;
                    }
                }
                while (true) {
                    p2 = p;
                    if (p2 >= pointerCount) {
                        break;
                    }
                    if (p2 != event.getActionIndex() && event.getToolType(p2) == 1) {
                        addPointerForIndex(event, p2, kPointerChangeMove, 1, packet);
                    }
                    p = p2 + 1;
                }
                addPointerForIndex(event, event.getActionIndex(), pointerChange, 0, packet);
                nativeDispatchPointerDataPacket(flutterView.mNativeView.get(), packet, packet.position());
                return true;
            }
        }
        addPointerForIndex(event, event.getActionIndex(), pointerChange, 0, packet);
        nativeDispatchPointerDataPacket(flutterView.mNativeView.get(), packet, packet.position());
        return true;
    }

    public boolean onHoverEvent(MotionEvent event) {
        if (isAttached()) {
            return handleAccessibilityHoverEvent(event);
        }
        return false;
    }

    protected void onSizeChanged(int width, int height, int oldWidth, int oldHeight) {
        this.mMetrics.physicalWidth = width;
        this.mMetrics.physicalHeight = height;
        updateViewportMetrics();
        super.onSizeChanged(width, height, oldWidth, oldHeight);
    }

    ZeroSides calculateShouldZeroSides() {
        Activity activity = (Activity) getContext();
        int orientation = activity.getResources().getConfiguration().orientation;
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        if (orientation == 2) {
            if (rotation == 1) {
                return ZeroSides.RIGHT;
            }
            if (rotation == 3) {
                return VERSION.SDK_INT >= 23 ? ZeroSides.LEFT : ZeroSides.RIGHT;
            } else if (rotation == 0 || rotation == 2) {
                return ZeroSides.BOTH;
            }
        }
        return ZeroSides.NONE;
    }

    int calculateBottomKeyboardInset(WindowInsets insets) {
        double systemWindowInsetBottom = (double) insets.getSystemWindowInsetBottom();
        double height = (double) getRootView().getHeight();
        Double.isNaN(height);
        if (systemWindowInsetBottom < height * 0.18d) {
            return 0;
        }
        return insets.getSystemWindowInsetBottom();
    }

    public final WindowInsets onApplyWindowInsets(WindowInsets insets) {
        int systemWindowInsetRight;
        boolean navigationBarHidden = true;
        boolean statusBarHidden = (getWindowSystemUiVisibility() & 4) != 0;
        if ((getWindowSystemUiVisibility() & 2) == 0) {
            navigationBarHidden = false;
        }
        ZeroSides zeroSides = ZeroSides.NONE;
        if (navigationBarHidden) {
            zeroSides = calculateShouldZeroSides();
        }
        this.mMetrics.physicalPaddingTop = statusBarHidden ? 0 : insets.getSystemWindowInsetTop();
        ViewportMetrics viewportMetrics = this.mMetrics;
        if (zeroSides != ZeroSides.RIGHT) {
            if (zeroSides != ZeroSides.BOTH) {
                systemWindowInsetRight = insets.getSystemWindowInsetRight();
                viewportMetrics.physicalPaddingRight = systemWindowInsetRight;
                this.mMetrics.physicalPaddingBottom = 0;
                viewportMetrics = this.mMetrics;
                if (zeroSides != ZeroSides.LEFT) {
                    if (zeroSides == ZeroSides.BOTH) {
                        systemWindowInsetRight = insets.getSystemWindowInsetLeft();
                        viewportMetrics.physicalPaddingLeft = systemWindowInsetRight;
                        this.mMetrics.physicalViewInsetTop = 0;
                        this.mMetrics.physicalViewInsetRight = 0;
                        this.mMetrics.physicalViewInsetBottom = navigationBarHidden ? calculateBottomKeyboardInset(insets) : insets.getSystemWindowInsetBottom();
                        this.mMetrics.physicalViewInsetLeft = 0;
                        updateViewportMetrics();
                        return super.onApplyWindowInsets(insets);
                    }
                }
                systemWindowInsetRight = 0;
                viewportMetrics.physicalPaddingLeft = systemWindowInsetRight;
                this.mMetrics.physicalViewInsetTop = 0;
                this.mMetrics.physicalViewInsetRight = 0;
                if (navigationBarHidden) {
                }
                this.mMetrics.physicalViewInsetBottom = navigationBarHidden ? calculateBottomKeyboardInset(insets) : insets.getSystemWindowInsetBottom();
                this.mMetrics.physicalViewInsetLeft = 0;
                updateViewportMetrics();
                return super.onApplyWindowInsets(insets);
            }
        }
        systemWindowInsetRight = 0;
        viewportMetrics.physicalPaddingRight = systemWindowInsetRight;
        this.mMetrics.physicalPaddingBottom = 0;
        viewportMetrics = this.mMetrics;
        if (zeroSides != ZeroSides.LEFT) {
            if (zeroSides == ZeroSides.BOTH) {
                systemWindowInsetRight = insets.getSystemWindowInsetLeft();
                viewportMetrics.physicalPaddingLeft = systemWindowInsetRight;
                this.mMetrics.physicalViewInsetTop = 0;
                this.mMetrics.physicalViewInsetRight = 0;
                if (navigationBarHidden) {
                }
                this.mMetrics.physicalViewInsetBottom = navigationBarHidden ? calculateBottomKeyboardInset(insets) : insets.getSystemWindowInsetBottom();
                this.mMetrics.physicalViewInsetLeft = 0;
                updateViewportMetrics();
                return super.onApplyWindowInsets(insets);
            }
        }
        systemWindowInsetRight = 0;
        viewportMetrics.physicalPaddingLeft = systemWindowInsetRight;
        this.mMetrics.physicalViewInsetTop = 0;
        this.mMetrics.physicalViewInsetRight = 0;
        if (navigationBarHidden) {
        }
        this.mMetrics.physicalViewInsetBottom = navigationBarHidden ? calculateBottomKeyboardInset(insets) : insets.getSystemWindowInsetBottom();
        this.mMetrics.physicalViewInsetLeft = 0;
        updateViewportMetrics();
        return super.onApplyWindowInsets(insets);
    }

    protected boolean fitSystemWindows(Rect insets) {
        if (VERSION.SDK_INT > 19) {
            return super.fitSystemWindows(insets);
        }
        this.mMetrics.physicalPaddingTop = insets.top;
        this.mMetrics.physicalPaddingRight = insets.right;
        this.mMetrics.physicalPaddingBottom = 0;
        this.mMetrics.physicalPaddingLeft = insets.left;
        this.mMetrics.physicalViewInsetTop = 0;
        this.mMetrics.physicalViewInsetRight = 0;
        this.mMetrics.physicalViewInsetBottom = insets.bottom;
        this.mMetrics.physicalViewInsetLeft = 0;
        updateViewportMetrics();
        return true;
    }

    private boolean isAttached() {
        return this.mNativeView != null && this.mNativeView.isAttached();
    }

    void assertAttached() {
        if (!isAttached()) {
            throw new AssertionError("Platform view is not attached");
        }
    }

    private void preRun() {
        resetAccessibilityTree();
    }

    private void postRun() {
    }

    public void runFromBundle(FlutterRunArguments args) {
        assertAttached();
        preRun();
        this.mNativeView.runFromBundle(args);
        postRun();
    }

    @Deprecated
    public void runFromBundle(String bundlePath, String defaultPath) {
        runFromBundle(bundlePath, defaultPath, "main", false);
    }

    @Deprecated
    public void runFromBundle(String bundlePath, String defaultPath, String entrypoint) {
        runFromBundle(bundlePath, defaultPath, entrypoint, false);
    }

    @Deprecated
    public void runFromBundle(String bundlePath, String defaultPath, String entrypoint, boolean reuseRuntimeController) {
        FlutterRunArguments args = new FlutterRunArguments();
        args.bundlePath = bundlePath;
        args.entrypoint = entrypoint;
        args.defaultPath = defaultPath;
        runFromBundle(args);
    }

    public Bitmap getBitmap() {
        assertAttached();
        return nativeGetBitmap(this.mNativeView.get());
    }

    private void updateViewportMetrics() {
        if (isAttached()) {
            nativeSetViewportMetrics(this.mNativeView.get(), this.mMetrics.devicePixelRatio, this.mMetrics.physicalWidth, this.mMetrics.physicalHeight, this.mMetrics.physicalPaddingTop, this.mMetrics.physicalPaddingRight, this.mMetrics.physicalPaddingBottom, this.mMetrics.physicalPaddingLeft, this.mMetrics.physicalViewInsetTop, this.mMetrics.physicalViewInsetRight, this.mMetrics.physicalViewInsetBottom, this.mMetrics.physicalViewInsetLeft);
            double refreshRate = (double) ((WindowManager) getContext().getSystemService("window")).getDefaultDisplay().getRefreshRate();
            Double.isNaN(refreshRate);
            VsyncWaiter.refreshPeriodNanos = (long) (1.0E9d / refreshRate);
        }
    }

    public void updateSemantics(ByteBuffer buffer, String[] strings) {
        try {
            if (this.mAccessibilityNodeProvider != null) {
                buffer.order(ByteOrder.LITTLE_ENDIAN);
                this.mAccessibilityNodeProvider.updateSemantics(buffer, strings);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Uncaught exception while updating semantics", ex);
        }
    }

    public void updateCustomAccessibilityActions(ByteBuffer buffer, String[] strings) {
        try {
            if (this.mAccessibilityNodeProvider != null) {
                buffer.order(ByteOrder.LITTLE_ENDIAN);
                this.mAccessibilityNodeProvider.updateCustomAccessibilityActions(buffer, strings);
            }
        } catch (Exception ex) {
            Log.e(TAG, "Uncaught exception while updating local context actions", ex);
        }
    }

    public void onFirstFrame() {
        for (FirstFrameListener listener : new ArrayList(this.mFirstFrameListeners)) {
            listener.onFirstFrame();
        }
    }

    protected void dispatchSemanticsAction(int id, Action action) {
        dispatchSemanticsAction(id, action, null);
    }

    protected void dispatchSemanticsAction(int id, Action action, Object args) {
        if (isAttached()) {
            ByteBuffer encodedArgs = null;
            int position = 0;
            if (args != null) {
                encodedArgs = StandardMessageCodec.INSTANCE.encodeMessage(args);
                position = encodedArgs.position();
            }
            nativeDispatchSemanticsAction(this.mNativeView.get(), id, action.value, encodedArgs, position);
        }
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        this.mAccessibilityEnabled = this.mAccessibilityManager.isEnabled();
        this.mTouchExplorationEnabled = this.mAccessibilityManager.isTouchExplorationEnabled();
        if (VERSION.SDK_INT >= 17) {
            getContext().getContentResolver().registerContentObserver(Global.getUriFor("transition_animation_scale"), false, this.mAnimationScaleObserver);
        }
        if (this.mAccessibilityEnabled || this.mTouchExplorationEnabled) {
            ensureAccessibilityEnabled();
        }
        if (this.mTouchExplorationEnabled) {
            this.mAccessibilityFeatureFlags |= AccessibilityFeature.ACCESSIBLE_NAVIGATION.value;
        } else {
            this.mAccessibilityFeatureFlags &= AccessibilityFeature.ACCESSIBLE_NAVIGATION.value ^ -1;
        }
        updateAccessibilityFeatures();
        resetWillNotDraw();
        this.mAccessibilityManager.addAccessibilityStateChangeListener(this);
        if (VERSION.SDK_INT >= 19) {
            if (this.mTouchExplorationListener == null) {
                this.mTouchExplorationListener = new TouchExplorationListener();
            }
            this.mAccessibilityManager.addTouchExplorationStateChangeListener(this.mTouchExplorationListener);
        }
    }

    private void updateAccessibilityFeatures() {
        if (VERSION.SDK_INT >= 17) {
            String transitionAnimationScale = Global.getString(getContext().getContentResolver(), "transition_animation_scale");
            if (transitionAnimationScale == null || !transitionAnimationScale.equals("0")) {
                this.mAccessibilityFeatureFlags &= AccessibilityFeature.DISABLE_ANIMATIONS.value ^ -1;
            } else {
                this.mAccessibilityFeatureFlags |= AccessibilityFeature.DISABLE_ANIMATIONS.value;
            }
        }
        nativeSetAccessibilityFeatures(this.mNativeView.get(), this.mAccessibilityFeatureFlags);
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        getContext().getContentResolver().unregisterContentObserver(this.mAnimationScaleObserver);
        this.mAccessibilityManager.removeAccessibilityStateChangeListener(this);
        if (VERSION.SDK_INT >= 19) {
            this.mAccessibilityManager.removeTouchExplorationStateChangeListener(this.mTouchExplorationListener);
        }
    }

    private void resetWillNotDraw() {
        boolean z = false;
        if (this.mIsSoftwareRenderingEnabled) {
            setWillNotDraw(false);
            return;
        }
        if (!(this.mAccessibilityEnabled || this.mTouchExplorationEnabled)) {
            z = true;
        }
        setWillNotDraw(z);
    }

    public void onAccessibilityStateChanged(boolean enabled) {
        if (enabled) {
            ensureAccessibilityEnabled();
        } else {
            this.mAccessibilityEnabled = false;
            if (this.mAccessibilityNodeProvider != null) {
                this.mAccessibilityNodeProvider.setAccessibilityEnabled(false);
            }
            nativeSetSemanticsEnabled(this.mNativeView.get(), false);
        }
        resetWillNotDraw();
    }

    public AccessibilityNodeProvider getAccessibilityNodeProvider() {
        if (this.mAccessibilityEnabled) {
            return this.mAccessibilityNodeProvider;
        }
        return null;
    }

    void ensureAccessibilityEnabled() {
        if (isAttached()) {
            this.mAccessibilityEnabled = true;
            if (this.mAccessibilityNodeProvider == null) {
                this.mAccessibilityNodeProvider = new AccessibilityBridge(this);
            }
            nativeSetSemanticsEnabled(this.mNativeView.get(), true);
            this.mAccessibilityNodeProvider.setAccessibilityEnabled(true);
        }
    }

    void resetAccessibilityTree() {
        if (this.mAccessibilityNodeProvider != null) {
            this.mAccessibilityNodeProvider.reset();
        }
    }

    private boolean handleAccessibilityHoverEvent(MotionEvent event) {
        if (!this.mTouchExplorationEnabled) {
            return false;
        }
        if (event.getAction() != 9) {
            if (event.getAction() != 7) {
                if (event.getAction() == 10) {
                    this.mAccessibilityNodeProvider.handleTouchExplorationExit();
                    return true;
                }
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("unexpected accessibility hover event: ");
                stringBuilder.append(event);
                Log.d("flutter", stringBuilder.toString());
                return false;
            }
        }
        this.mAccessibilityNodeProvider.handleTouchExploration(event.getX(), event.getY());
        return true;
    }

    public void send(String channel, ByteBuffer message) {
        send(channel, message, null);
    }

    public void send(String channel, ByteBuffer message, BinaryReply callback) {
        if (isAttached()) {
            this.mNativeView.send(channel, message, callback);
            return;
        }
        String str = TAG;
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("FlutterView.send called on a detached view, channel=");
        stringBuilder.append(channel);
        Log.d(str, stringBuilder.toString());
    }

    public void setMessageHandler(String channel, BinaryMessageHandler handler) {
        this.mNativeView.setMessageHandler(channel, handler);
    }

    public SurfaceTextureEntry createSurfaceTexture() {
        SurfaceTexture surfaceTexture = new SurfaceTexture(0);
        surfaceTexture.detachFromGLContext();
        SurfaceTextureRegistryEntry entry = new SurfaceTextureRegistryEntry(this.nextTextureId.getAndIncrement(), surfaceTexture);
        nativeRegisterTexture(this.mNativeView.get(), entry.id(), surfaceTexture);
        return entry;
    }
}
