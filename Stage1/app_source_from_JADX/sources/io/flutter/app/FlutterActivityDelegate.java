package io.flutter.app;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager.LayoutParams;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.plugin.platform.PlatformPlugin;
import io.flutter.util.Preconditions;
import io.flutter.view.FlutterMain;
import io.flutter.view.FlutterNativeView;
import io.flutter.view.FlutterRunArguments;
import io.flutter.view.FlutterView;
import io.flutter.view.FlutterView.FirstFrameListener;
import io.flutter.view.FlutterView.Provider;
import java.util.ArrayList;

public final class FlutterActivityDelegate implements FlutterActivityEvents, Provider, PluginRegistry {
    private static final String SPLASH_SCREEN_META_DATA_KEY = "io.flutter.app.android.SplashScreenUntilFirstFrame";
    private static final String TAG = "FlutterActivityDelegate";
    private static final LayoutParams matchParent = new LayoutParams(-1, -1);
    private final Activity activity;
    private FlutterView flutterView;
    private View launchView;
    private final ViewFactory viewFactory;

    public interface ViewFactory {
        FlutterNativeView createFlutterNativeView();

        FlutterView createFlutterView(Context context);

        boolean retainFlutterNativeView();
    }

    /* renamed from: io.flutter.app.FlutterActivityDelegate$1 */
    class C00141 implements FirstFrameListener {

        /* renamed from: io.flutter.app.FlutterActivityDelegate$1$1 */
        class C00011 extends AnimatorListenerAdapter {
            C00011() {
            }

            public void onAnimationEnd(Animator animation) {
                ((ViewGroup) FlutterActivityDelegate.this.launchView.getParent()).removeView(FlutterActivityDelegate.this.launchView);
                FlutterActivityDelegate.this.launchView = null;
            }
        }

        C00141() {
        }

        public void onFirstFrame() {
            FlutterActivityDelegate.this.launchView.animate().alpha(0.0f).setListener(new C00011());
            FlutterActivityDelegate.this.flutterView.removeFirstFrameListener(this);
        }
    }

    public FlutterActivityDelegate(Activity activity, ViewFactory viewFactory) {
        this.activity = (Activity) Preconditions.checkNotNull(activity);
        this.viewFactory = (ViewFactory) Preconditions.checkNotNull(viewFactory);
    }

    public FlutterView getFlutterView() {
        return this.flutterView;
    }

    public boolean hasPlugin(String key) {
        return this.flutterView.getPluginRegistry().hasPlugin(key);
    }

    public <T> T valuePublishedByPlugin(String pluginKey) {
        return this.flutterView.getPluginRegistry().valuePublishedByPlugin(pluginKey);
    }

    public Registrar registrarFor(String pluginKey) {
        return this.flutterView.getPluginRegistry().registrarFor(pluginKey);
    }

    public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        return this.flutterView.getPluginRegistry().onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        return this.flutterView.getPluginRegistry().onActivityResult(requestCode, resultCode, data);
    }

    public void onCreate(Bundle savedInstanceState) {
        if (VERSION.SDK_INT >= 21) {
            Window window = this.activity.getWindow();
            window.addFlags(Integer.MIN_VALUE);
            window.setStatusBarColor(1073741824);
            window.getDecorView().setSystemUiVisibility(PlatformPlugin.DEFAULT_SYSTEM_UI);
        }
        FlutterMain.ensureInitializationComplete(this.activity.getApplicationContext(), getArgsFromIntent(this.activity.getIntent()));
        this.flutterView = this.viewFactory.createFlutterView(this.activity);
        if (this.flutterView == null) {
            this.flutterView = new FlutterView(this.activity, null, this.viewFactory.createFlutterNativeView());
            this.flutterView.setLayoutParams(matchParent);
            this.activity.setContentView(this.flutterView);
            this.launchView = createLaunchView();
            if (this.launchView != null) {
                addLaunchView();
            }
        }
        if (!(loadIntent(this.activity.getIntent()) || this.flutterView.getFlutterNativeView().isApplicationRunning())) {
            String appBundlePath = FlutterMain.findAppBundlePath(this.activity.getApplicationContext());
            if (appBundlePath != null) {
                FlutterRunArguments arguments = new FlutterRunArguments();
                arguments.bundlePath = appBundlePath;
                arguments.entrypoint = "main";
                this.flutterView.runFromBundle(arguments);
            }
        }
    }

    public void onNewIntent(Intent intent) {
        if (!isDebuggable() || !loadIntent(intent)) {
            this.flutterView.getPluginRegistry().onNewIntent(intent);
        }
    }

    private boolean isDebuggable() {
        return (this.activity.getApplicationInfo().flags & 2) != 0;
    }

    public void onPause() {
        Application app = (Application) this.activity.getApplicationContext();
        if (app instanceof FlutterApplication) {
            FlutterApplication flutterApp = (FlutterApplication) app;
            if (this.activity.equals(flutterApp.getCurrentActivity())) {
                flutterApp.setCurrentActivity(null);
            }
        }
        if (this.flutterView != null) {
            this.flutterView.onPause();
        }
    }

    public void onStart() {
        if (this.flutterView != null) {
            this.flutterView.onStart();
        }
    }

    public void onResume() {
        Application app = (Application) this.activity.getApplicationContext();
        if (app instanceof FlutterApplication) {
            ((FlutterApplication) app).setCurrentActivity(this.activity);
        }
    }

    public void onStop() {
        this.flutterView.onStop();
    }

    public void onPostResume() {
        if (this.flutterView != null) {
            this.flutterView.onPostResume();
        }
    }

    public void onDestroy() {
        Application app = (Application) this.activity.getApplicationContext();
        if (app instanceof FlutterApplication) {
            FlutterApplication flutterApp = (FlutterApplication) app;
            if (this.activity.equals(flutterApp.getCurrentActivity())) {
                flutterApp.setCurrentActivity(null);
            }
        }
        if (this.flutterView != null) {
            if (!this.flutterView.getPluginRegistry().onViewDestroy(this.flutterView.getFlutterNativeView())) {
                if (!this.viewFactory.retainFlutterNativeView()) {
                    this.flutterView.destroy();
                    return;
                }
            }
            this.flutterView.detach();
        }
    }

    public boolean onBackPressed() {
        if (this.flutterView == null) {
            return false;
        }
        this.flutterView.popRoute();
        return true;
    }

    public void onUserLeaveHint() {
        this.flutterView.getPluginRegistry().onUserLeaveHint();
    }

    public void onTrimMemory(int level) {
        if (level == 10) {
            this.flutterView.onMemoryPressure();
        }
    }

    public void onLowMemory() {
        this.flutterView.onMemoryPressure();
    }

    public void onConfigurationChanged(Configuration newConfig) {
    }

    private static String[] getArgsFromIntent(Intent intent) {
        ArrayList<String> args = new ArrayList();
        if (intent.getBooleanExtra("trace-startup", false)) {
            args.add("--trace-startup");
        }
        if (intent.getBooleanExtra("start-paused", false)) {
            args.add("--start-paused");
        }
        if (intent.getBooleanExtra("use-test-fonts", false)) {
            args.add("--use-test-fonts");
        }
        if (intent.getBooleanExtra("enable-dart-profiling", false)) {
            args.add("--enable-dart-profiling");
        }
        if (intent.getBooleanExtra("enable-software-rendering", false)) {
            args.add("--enable-software-rendering");
        }
        if (intent.getBooleanExtra("skia-deterministic-rendering", false)) {
            args.add("--skia-deterministic-rendering");
        }
        if (intent.getBooleanExtra("trace-skia", false)) {
            args.add("--trace-skia");
        }
        if (intent.getBooleanExtra("verbose-logging", false)) {
            args.add("--verbose-logging");
        }
        if (args.isEmpty()) {
            return null;
        }
        return (String[]) args.toArray(new String[args.size()]);
    }

    private boolean loadIntent(Intent intent) {
        if (!"android.intent.action.RUN".equals(intent.getAction())) {
            return false;
        }
        String route = intent.getStringExtra("route");
        String appBundlePath = intent.getDataString();
        if (appBundlePath == null) {
            appBundlePath = FlutterMain.findAppBundlePath(this.activity.getApplicationContext());
        }
        if (route != null) {
            this.flutterView.setInitialRoute(route);
        }
        if (!this.flutterView.getFlutterNativeView().isApplicationRunning()) {
            FlutterRunArguments args = new FlutterRunArguments();
            args.bundlePath = appBundlePath;
            args.entrypoint = "main";
            this.flutterView.runFromBundle(args);
        }
        return true;
    }

    private View createLaunchView() {
        if (!showSplashScreenUntilFirstFrame().booleanValue()) {
            return null;
        }
        Drawable launchScreenDrawable = getLaunchScreenDrawableFromActivityTheme();
        if (launchScreenDrawable == null) {
            return null;
        }
        View view = new View(this.activity);
        view.setLayoutParams(matchParent);
        view.setBackground(launchScreenDrawable);
        return view;
    }

    private Drawable getLaunchScreenDrawableFromActivityTheme() {
        TypedValue typedValue = new TypedValue();
        if (!this.activity.getTheme().resolveAttribute(16842836, typedValue, true) || typedValue.resourceId == 0) {
            return null;
        }
        try {
            return this.activity.getResources().getDrawable(typedValue.resourceId);
        } catch (NotFoundException e) {
            Log.e(TAG, "Referenced launch screen windowBackground resource does not exist");
            return null;
        }
    }

    private Boolean showSplashScreenUntilFirstFrame() {
        try {
            Bundle metadata = this.activity.getPackageManager().getActivityInfo(this.activity.getComponentName(), 129).metaData;
            boolean z = metadata != null && metadata.getBoolean(SPLASH_SCREEN_META_DATA_KEY);
            return Boolean.valueOf(z);
        } catch (NameNotFoundException e) {
            return Boolean.valueOf(false);
        }
    }

    private void addLaunchView() {
        if (this.launchView != null) {
            this.activity.addContentView(this.launchView, matchParent);
            this.flutterView.addFirstFrameListener(new C00141());
            this.activity.setTheme(16973833);
        }
    }
}
