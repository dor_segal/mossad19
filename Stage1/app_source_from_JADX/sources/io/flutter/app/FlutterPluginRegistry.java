package io.flutter.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.ActivityResultListener;
import io.flutter.plugin.common.PluginRegistry.NewIntentListener;
import io.flutter.plugin.common.PluginRegistry.Registrar;
import io.flutter.plugin.common.PluginRegistry.RequestPermissionsResultListener;
import io.flutter.plugin.common.PluginRegistry.UserLeaveHintListener;
import io.flutter.plugin.common.PluginRegistry.ViewDestroyListener;
import io.flutter.plugin.platform.PlatformViewRegistry;
import io.flutter.plugin.platform.PlatformViewsController;
import io.flutter.view.FlutterMain;
import io.flutter.view.FlutterNativeView;
import io.flutter.view.FlutterView;
import io.flutter.view.TextureRegistry;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class FlutterPluginRegistry implements PluginRegistry, RequestPermissionsResultListener, ActivityResultListener, NewIntentListener, UserLeaveHintListener, ViewDestroyListener {
    private static final String TAG = "FlutterPluginRegistry";
    private Activity mActivity;
    private final List<ActivityResultListener> mActivityResultListeners = new ArrayList(0);
    private Context mAppContext;
    private FlutterView mFlutterView;
    private FlutterNativeView mNativeView;
    private final List<NewIntentListener> mNewIntentListeners = new ArrayList(0);
    private final PlatformViewsController mPlatformViewsController;
    private final Map<String, Object> mPluginMap = new LinkedHashMap(0);
    private final List<RequestPermissionsResultListener> mRequestPermissionsResultListeners = new ArrayList(0);
    private final List<UserLeaveHintListener> mUserLeaveHintListeners = new ArrayList(0);
    private final List<ViewDestroyListener> mViewDestroyListeners = new ArrayList(0);

    private class FlutterRegistrar implements Registrar {
        private final String pluginKey;

        FlutterRegistrar(String pluginKey) {
            this.pluginKey = pluginKey;
        }

        public Activity activity() {
            return FlutterPluginRegistry.this.mActivity;
        }

        public Context context() {
            return FlutterPluginRegistry.this.mAppContext;
        }

        public Context activeContext() {
            return FlutterPluginRegistry.this.mActivity != null ? FlutterPluginRegistry.this.mActivity : FlutterPluginRegistry.this.mAppContext;
        }

        public BinaryMessenger messenger() {
            return FlutterPluginRegistry.this.mNativeView;
        }

        public TextureRegistry textures() {
            return FlutterPluginRegistry.this.mFlutterView;
        }

        public PlatformViewRegistry platformViewRegistry() {
            return FlutterPluginRegistry.this.mPlatformViewsController.getRegistry();
        }

        public FlutterView view() {
            return FlutterPluginRegistry.this.mFlutterView;
        }

        public String lookupKeyForAsset(String asset) {
            return FlutterMain.getLookupKeyForAsset(asset);
        }

        public String lookupKeyForAsset(String asset, String packageName) {
            return FlutterMain.getLookupKeyForAsset(asset, packageName);
        }

        public Registrar publish(Object value) {
            FlutterPluginRegistry.this.mPluginMap.put(this.pluginKey, value);
            return this;
        }

        public Registrar addRequestPermissionsResultListener(RequestPermissionsResultListener listener) {
            FlutterPluginRegistry.this.mRequestPermissionsResultListeners.add(listener);
            return this;
        }

        public Registrar addActivityResultListener(ActivityResultListener listener) {
            FlutterPluginRegistry.this.mActivityResultListeners.add(listener);
            return this;
        }

        public Registrar addNewIntentListener(NewIntentListener listener) {
            FlutterPluginRegistry.this.mNewIntentListeners.add(listener);
            return this;
        }

        public Registrar addUserLeaveHintListener(UserLeaveHintListener listener) {
            FlutterPluginRegistry.this.mUserLeaveHintListeners.add(listener);
            return this;
        }

        public Registrar addViewDestroyListener(ViewDestroyListener listener) {
            FlutterPluginRegistry.this.mViewDestroyListeners.add(listener);
            return this;
        }
    }

    public FlutterPluginRegistry(FlutterNativeView nativeView, Context context) {
        this.mNativeView = nativeView;
        this.mAppContext = context;
        this.mPlatformViewsController = new PlatformViewsController();
    }

    public boolean hasPlugin(String key) {
        return this.mPluginMap.containsKey(key);
    }

    public <T> T valuePublishedByPlugin(String pluginKey) {
        return this.mPluginMap.get(pluginKey);
    }

    public Registrar registrarFor(String pluginKey) {
        if (this.mPluginMap.containsKey(pluginKey)) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Plugin key ");
            stringBuilder.append(pluginKey);
            stringBuilder.append(" is already in use");
            throw new IllegalStateException(stringBuilder.toString());
        }
        this.mPluginMap.put(pluginKey, null);
        return new FlutterRegistrar(pluginKey);
    }

    public void attach(FlutterView flutterView, Activity activity) {
        this.mFlutterView = flutterView;
        this.mActivity = activity;
        this.mPlatformViewsController.attach(activity, flutterView, flutterView);
    }

    public void detach() {
        this.mPlatformViewsController.detach();
        this.mPlatformViewsController.onFlutterViewDestroyed();
        this.mFlutterView = null;
        this.mActivity = null;
    }

    public void onPreEngineRestart() {
        this.mPlatformViewsController.onPreEngineRestart();
    }

    public boolean onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        for (RequestPermissionsResultListener listener : this.mRequestPermissionsResultListeners) {
            if (listener.onRequestPermissionsResult(requestCode, permissions, grantResults)) {
                return true;
            }
        }
        return false;
    }

    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {
        for (ActivityResultListener listener : this.mActivityResultListeners) {
            if (listener.onActivityResult(requestCode, resultCode, data)) {
                return true;
            }
        }
        return false;
    }

    public boolean onNewIntent(Intent intent) {
        for (NewIntentListener listener : this.mNewIntentListeners) {
            if (listener.onNewIntent(intent)) {
                return true;
            }
        }
        return false;
    }

    public void onUserLeaveHint() {
        for (UserLeaveHintListener listener : this.mUserLeaveHintListeners) {
            listener.onUserLeaveHint();
        }
    }

    public boolean onViewDestroy(FlutterNativeView view) {
        boolean handled = false;
        for (ViewDestroyListener listener : this.mViewDestroyListeners) {
            if (listener.onViewDestroy(view)) {
                handled = true;
            }
        }
        return handled;
    }

    public void destroy() {
        this.mPlatformViewsController.onFlutterViewDestroyed();
    }
}
