package io.flutter.plugin.editing;

import android.text.Editable;
import android.text.Editable.Factory;
import android.text.Selection;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.view.inputmethod.InputMethodManager;
import com.iwalk.locksmither.BuildConfig;
import io.flutter.plugin.common.JSONMethodCodec;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.view.FlutterView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class TextInputPlugin implements MethodCallHandler {
    private int mClient = 0;
    private JSONObject mConfiguration;
    private Editable mEditable;
    private final MethodChannel mFlutterChannel;
    private final InputMethodManager mImm;
    private boolean mRestartInputPending;
    private final FlutterView mView;

    public TextInputPlugin(FlutterView view) {
        this.mView = view;
        this.mImm = (InputMethodManager) view.getContext().getSystemService("input_method");
        this.mFlutterChannel = new MethodChannel(view, "flutter/textinput", JSONMethodCodec.INSTANCE);
        this.mFlutterChannel.setMethodCallHandler(this);
    }

    public void onMethodCall(MethodCall call, Result result) {
        String method = call.method;
        JSONArray args = call.arguments;
        try {
            if (method.equals("TextInput.show")) {
                showTextInput(this.mView);
                result.success(null);
            } else if (method.equals("TextInput.hide")) {
                hideTextInput(this.mView);
                result.success(null);
            } else if (method.equals("TextInput.setClient")) {
                JSONArray argumentList = args;
                setTextInputClient(this.mView, argumentList.getInt(0), argumentList.getJSONObject(1));
                result.success(null);
            } else if (method.equals("TextInput.setEditingState")) {
                setTextInputEditingState(this.mView, (JSONObject) args);
                result.success(null);
            } else if (method.equals("TextInput.clearClient")) {
                clearTextInputClient();
                result.success(null);
            } else {
                result.notImplemented();
            }
        } catch (JSONException e) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("JSON error: ");
            stringBuilder.append(e.getMessage());
            result.error("error", stringBuilder.toString(), null);
        }
    }

    private static int inputTypeFromTextInputType(JSONObject type, boolean obscureText, boolean autocorrect, String textCapitalization) throws JSONException {
        String inputType = type.getString("name");
        if (inputType.equals("TextInputType.datetime")) {
            return 4;
        }
        int textType;
        if (inputType.equals("TextInputType.number")) {
            textType = 2;
            if (type.optBoolean("signed")) {
                textType = 2 | 4096;
            }
            if (type.optBoolean("decimal")) {
                textType |= 8192;
            }
            return textType;
        } else if (inputType.equals("TextInputType.phone")) {
            return 3;
        } else {
            textType = 1;
            if (inputType.equals("TextInputType.multiline")) {
                textType = 1 | 131072;
            } else if (inputType.equals("TextInputType.emailAddress")) {
                textType = 1 | 32;
            } else if (inputType.equals("TextInputType.url")) {
                textType = 1 | 16;
            }
            if (obscureText) {
                textType = (textType | 524288) | 128;
            } else if (autocorrect) {
                textType |= 32768;
            }
            if (textCapitalization.equals("TextCapitalization.characters")) {
                textType |= 4096;
            } else if (textCapitalization.equals("TextCapitalization.words")) {
                textType |= 8192;
            } else if (textCapitalization.equals("TextCapitalization.sentences")) {
                textType |= 16384;
            }
            return textType;
        }
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    private static int inputActionFromTextInputAction(java.lang.String r9) {
        /*
        r0 = r9.hashCode();
        r1 = 7;
        r2 = 5;
        r3 = 4;
        r4 = 3;
        r5 = 2;
        r6 = 6;
        r7 = 1;
        r8 = 0;
        switch(r0) {
            case -810971940: goto L_0x0061;
            case -737377923: goto L_0x0057;
            case -737089298: goto L_0x004d;
            case -737080013: goto L_0x0043;
            case -736940669: goto L_0x0039;
            case 469250275: goto L_0x002f;
            case 1241689507: goto L_0x0025;
            case 1539450297: goto L_0x001b;
            case 2110497650: goto L_0x0010;
            default: goto L_0x000f;
        };
    L_0x000f:
        goto L_0x006b;
    L_0x0010:
        r0 = "TextInputAction.previous";
        r0 = r9.equals(r0);
        if (r0 == 0) goto L_0x006b;
    L_0x0018:
        r0 = 8;
        goto L_0x006c;
    L_0x001b:
        r0 = "TextInputAction.newline";
        r0 = r9.equals(r0);
        if (r0 == 0) goto L_0x006b;
    L_0x0023:
        r0 = 0;
        goto L_0x006c;
    L_0x0025:
        r0 = "TextInputAction.go";
        r0 = r9.equals(r0);
        if (r0 == 0) goto L_0x006b;
    L_0x002d:
        r0 = 4;
        goto L_0x006c;
    L_0x002f:
        r0 = "TextInputAction.search";
        r0 = r9.equals(r0);
        if (r0 == 0) goto L_0x006b;
    L_0x0037:
        r0 = 5;
        goto L_0x006c;
    L_0x0039:
        r0 = "TextInputAction.send";
        r0 = r9.equals(r0);
        if (r0 == 0) goto L_0x006b;
    L_0x0041:
        r0 = 6;
        goto L_0x006c;
    L_0x0043:
        r0 = "TextInputAction.none";
        r0 = r9.equals(r0);
        if (r0 == 0) goto L_0x006b;
    L_0x004b:
        r0 = 1;
        goto L_0x006c;
    L_0x004d:
        r0 = "TextInputAction.next";
        r0 = r9.equals(r0);
        if (r0 == 0) goto L_0x006b;
    L_0x0055:
        r0 = 7;
        goto L_0x006c;
    L_0x0057:
        r0 = "TextInputAction.done";
        r0 = r9.equals(r0);
        if (r0 == 0) goto L_0x006b;
    L_0x005f:
        r0 = 3;
        goto L_0x006c;
    L_0x0061:
        r0 = "TextInputAction.unspecified";
        r0 = r9.equals(r0);
        if (r0 == 0) goto L_0x006b;
    L_0x0069:
        r0 = 2;
        goto L_0x006c;
    L_0x006b:
        r0 = -1;
    L_0x006c:
        switch(r0) {
            case 0: goto L_0x0078;
            case 1: goto L_0x0077;
            case 2: goto L_0x0076;
            case 3: goto L_0x0075;
            case 4: goto L_0x0074;
            case 5: goto L_0x0073;
            case 6: goto L_0x0072;
            case 7: goto L_0x0071;
            case 8: goto L_0x0070;
            default: goto L_0x006f;
        };
    L_0x006f:
        return r8;
    L_0x0070:
        return r1;
    L_0x0071:
        return r2;
    L_0x0072:
        return r3;
    L_0x0073:
        return r4;
    L_0x0074:
        return r5;
    L_0x0075:
        return r6;
    L_0x0076:
        return r8;
    L_0x0077:
        return r7;
    L_0x0078:
        return r7;
        */
        throw new UnsupportedOperationException("Method not decompiled: io.flutter.plugin.editing.TextInputPlugin.inputActionFromTextInputAction(java.lang.String):int");
    }

    public InputConnection createInputConnection(FlutterView view, EditorInfo outAttrs) throws JSONException {
        if (this.mClient == 0) {
            return null;
        }
        int enterAction;
        boolean z = true;
        outAttrs.inputType = inputTypeFromTextInputType(this.mConfiguration.getJSONObject("inputType"), this.mConfiguration.optBoolean("obscureText"), this.mConfiguration.optBoolean("autocorrect", true), this.mConfiguration.getString("textCapitalization"));
        outAttrs.imeOptions = 33554432;
        if (this.mConfiguration.isNull("inputAction")) {
            if ((131072 & outAttrs.inputType) == 0) {
                z = true;
            }
            enterAction = z;
        } else {
            enterAction = inputActionFromTextInputAction(this.mConfiguration.getString("inputAction"));
        }
        if (!this.mConfiguration.isNull("actionLabel")) {
            outAttrs.actionLabel = this.mConfiguration.getString("actionLabel");
            outAttrs.actionId = enterAction;
        }
        outAttrs.imeOptions |= enterAction;
        InputConnectionAdaptor connection = new InputConnectionAdaptor(view, this.mClient, this.mFlutterChannel, this.mEditable);
        outAttrs.initialSelStart = Selection.getSelectionStart(this.mEditable);
        outAttrs.initialSelEnd = Selection.getSelectionEnd(this.mEditable);
        return connection;
    }

    private void showTextInput(FlutterView view) {
        this.mImm.showSoftInput(view, 0);
    }

    private void hideTextInput(FlutterView view) {
        this.mImm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    private void setTextInputClient(FlutterView view, int client, JSONObject configuration) {
        this.mClient = client;
        this.mConfiguration = configuration;
        this.mEditable = Factory.getInstance().newEditable(BuildConfig.FLAVOR);
        this.mRestartInputPending = true;
    }

    private void applyStateToSelection(JSONObject state) throws JSONException {
        int selStart = state.getInt("selectionBase");
        int selEnd = state.getInt("selectionExtent");
        if (selStart < 0 || selStart > this.mEditable.length() || selEnd < 0 || selEnd > this.mEditable.length()) {
            Selection.removeSelection(this.mEditable);
        } else {
            Selection.setSelection(this.mEditable, selStart, selEnd);
        }
    }

    private void setTextInputEditingState(FlutterView view, JSONObject state) throws JSONException {
        if (this.mRestartInputPending || !state.getString("text").equals(this.mEditable.toString())) {
            this.mEditable.replace(0, this.mEditable.length(), state.getString("text"));
            applyStateToSelection(state);
            this.mImm.restartInput(view);
            this.mRestartInputPending = false;
            return;
        }
        applyStateToSelection(state);
        this.mImm.updateSelection(this.mView, Math.max(Selection.getSelectionStart(this.mEditable), 0), Math.max(Selection.getSelectionEnd(this.mEditable), 0), BaseInputConnection.getComposingSpanStart(this.mEditable), BaseInputConnection.getComposingSpanEnd(this.mEditable));
    }

    private void clearTextInputClient() {
        this.mClient = 0;
    }
}
