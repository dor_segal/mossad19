package io.flutter.plugin.editing;

import android.text.Editable;
import android.text.Selection;
import android.view.KeyEvent;
import android.view.inputmethod.BaseInputConnection;
import android.view.inputmethod.InputMethodManager;
import com.iwalk.locksmither.BuildConfig;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.view.FlutterView;
import java.io.Serializable;
import java.util.Arrays;
import java.util.HashMap;

class InputConnectionAdaptor extends BaseInputConnection {
    private int mBatchCount = 0;
    private final int mClient;
    private final Editable mEditable;
    private final MethodChannel mFlutterChannel;
    private final FlutterView mFlutterView;
    private InputMethodManager mImm;

    public InputConnectionAdaptor(FlutterView view, int client, MethodChannel flutterChannel, Editable editable) {
        super(view, true);
        this.mFlutterView = view;
        this.mClient = client;
        this.mFlutterChannel = flutterChannel;
        this.mEditable = editable;
        this.mImm = (InputMethodManager) view.getContext().getSystemService("input_method");
    }

    private void updateEditingState() {
        if (this.mBatchCount <= 0) {
            int selectionStart = Selection.getSelectionStart(this.mEditable);
            int selectionEnd = Selection.getSelectionEnd(this.mEditable);
            int composingStart = BaseInputConnection.getComposingSpanStart(this.mEditable);
            int composingEnd = BaseInputConnection.getComposingSpanEnd(this.mEditable);
            this.mImm.updateSelection(this.mFlutterView, selectionStart, selectionEnd, composingStart, composingEnd);
            HashMap<Object, Object> state = new HashMap();
            state.put("text", this.mEditable.toString());
            state.put("selectionBase", Integer.valueOf(selectionStart));
            state.put("selectionExtent", Integer.valueOf(selectionEnd));
            state.put("composingBase", Integer.valueOf(composingStart));
            state.put("composingExtent", Integer.valueOf(composingEnd));
            this.mFlutterChannel.invokeMethod("TextInputClient.updateEditingState", Arrays.asList(new Serializable[]{Integer.valueOf(this.mClient), state}));
        }
    }

    public Editable getEditable() {
        return this.mEditable;
    }

    public boolean beginBatchEdit() {
        this.mBatchCount++;
        return super.beginBatchEdit();
    }

    public boolean endBatchEdit() {
        boolean result = super.endBatchEdit();
        this.mBatchCount--;
        updateEditingState();
        return result;
    }

    public boolean commitText(CharSequence text, int newCursorPosition) {
        boolean result = super.commitText(text, newCursorPosition);
        updateEditingState();
        return result;
    }

    public boolean deleteSurroundingText(int beforeLength, int afterLength) {
        if (Selection.getSelectionStart(this.mEditable) == -1) {
            return true;
        }
        boolean result = super.deleteSurroundingText(beforeLength, afterLength);
        updateEditingState();
        return result;
    }

    public boolean setComposingRegion(int start, int end) {
        boolean result = super.setComposingRegion(start, end);
        updateEditingState();
        return result;
    }

    public boolean setComposingText(CharSequence text, int newCursorPosition) {
        boolean result;
        if (text.length() == 0) {
            result = super.commitText(text, newCursorPosition);
        } else {
            result = super.setComposingText(text, newCursorPosition);
        }
        updateEditingState();
        return result;
    }

    public boolean setSelection(int start, int end) {
        boolean result = super.setSelection(start, end);
        updateEditingState();
        return result;
    }

    public boolean sendKeyEvent(KeyEvent event) {
        if (event.getAction() == 0) {
            int selStart;
            int selEnd;
            int newSel;
            if (event.getKeyCode() == 67) {
                selStart = Selection.getSelectionStart(this.mEditable);
                selEnd = Selection.getSelectionEnd(this.mEditable);
                if (selEnd > selStart) {
                    Selection.setSelection(this.mEditable, selStart);
                    this.mEditable.delete(selStart, selEnd);
                    updateEditingState();
                    return true;
                } else if (selStart > 0) {
                    newSel = Math.max(selStart - 1, 0);
                    Selection.setSelection(this.mEditable, newSel);
                    this.mEditable.delete(newSel, selStart);
                    updateEditingState();
                    return true;
                }
            } else if (event.getKeyCode() == 21) {
                newSel = Math.max(Selection.getSelectionStart(this.mEditable) - 1, 0);
                setSelection(newSel, newSel);
                return true;
            } else if (event.getKeyCode() == 22) {
                newSel = Math.min(Selection.getSelectionStart(this.mEditable) + 1, this.mEditable.length());
                setSelection(newSel, newSel);
                return true;
            } else {
                selStart = event.getUnicodeChar();
                if (selStart != 0) {
                    selEnd = Math.max(0, Selection.getSelectionStart(this.mEditable));
                    newSel = Math.max(0, Selection.getSelectionEnd(this.mEditable));
                    if (newSel != selEnd) {
                        this.mEditable.delete(selEnd, newSel);
                    }
                    this.mEditable.insert(selEnd, String.valueOf((char) selStart));
                    setSelection(selEnd + 1, selEnd + 1);
                    updateEditingState();
                }
                return true;
            }
        }
        return false;
    }

    public boolean performEditorAction(int actionCode) {
        if (actionCode != 7) {
            switch (actionCode) {
                case 0:
                    this.mFlutterChannel.invokeMethod("TextInputClient.performAction", Arrays.asList(new Serializable[]{Integer.valueOf(this.mClient), "TextInputAction.unspecified"}));
                    break;
                case BuildConfig.VERSION_CODE /*1*/:
                    this.mFlutterChannel.invokeMethod("TextInputClient.performAction", Arrays.asList(new Serializable[]{Integer.valueOf(this.mClient), "TextInputAction.newline"}));
                    break;
                case 2:
                    this.mFlutterChannel.invokeMethod("TextInputClient.performAction", Arrays.asList(new Serializable[]{Integer.valueOf(this.mClient), "TextInputAction.go"}));
                    break;
                case 3:
                    this.mFlutterChannel.invokeMethod("TextInputClient.performAction", Arrays.asList(new Serializable[]{Integer.valueOf(this.mClient), "TextInputAction.search"}));
                    break;
                case 4:
                    this.mFlutterChannel.invokeMethod("TextInputClient.performAction", Arrays.asList(new Serializable[]{Integer.valueOf(this.mClient), "TextInputAction.send"}));
                    break;
                case 5:
                    this.mFlutterChannel.invokeMethod("TextInputClient.performAction", Arrays.asList(new Serializable[]{Integer.valueOf(this.mClient), "TextInputAction.next"}));
                    break;
                default:
                    this.mFlutterChannel.invokeMethod("TextInputClient.performAction", Arrays.asList(new Serializable[]{Integer.valueOf(this.mClient), "TextInputAction.done"}));
                    break;
            }
        }
        this.mFlutterChannel.invokeMethod("TextInputClient.performAction", Arrays.asList(new Serializable[]{Integer.valueOf(this.mClient), "TextInputAction.previous"}));
        return true;
    }
}
