package io.flutter.plugin.platform;

import android.app.Activity;
import android.app.ActivityManager.TaskDescription;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.View;
import android.view.Window;
import com.iwalk.locksmither.BuildConfig;
import io.flutter.plugin.common.ActivityLifecycleListener;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class PlatformPlugin implements MethodCallHandler, ActivityLifecycleListener {
    public static final int DEFAULT_SYSTEM_UI = 1280;
    private static final String kTextPlainFormat = "text/plain";
    private final Activity mActivity;
    private JSONObject mCurrentTheme;
    private int mEnabledOverlays = DEFAULT_SYSTEM_UI;

    public PlatformPlugin(Activity activity) {
        this.mActivity = activity;
    }

    public void onMethodCall(MethodCall call, Result result) {
        String method = call.method;
        Object arguments = call.arguments;
        try {
            if (method.equals("SystemSound.play")) {
                playSystemSound((String) arguments);
                result.success(null);
            } else if (method.equals("HapticFeedback.vibrate")) {
                vibrateHapticFeedback((String) arguments);
                result.success(null);
            } else if (method.equals("SystemChrome.setPreferredOrientations")) {
                setSystemChromePreferredOrientations((JSONArray) arguments);
                result.success(null);
            } else if (method.equals("SystemChrome.setApplicationSwitcherDescription")) {
                setSystemChromeApplicationSwitcherDescription((JSONObject) arguments);
                result.success(null);
            } else if (method.equals("SystemChrome.setEnabledSystemUIOverlays")) {
                setSystemChromeEnabledSystemUIOverlays((JSONArray) arguments);
                result.success(null);
            } else if (method.equals("SystemChrome.restoreSystemUIOverlays")) {
                restoreSystemChromeSystemUIOverlays();
                result.success(null);
            } else if (method.equals("SystemChrome.setSystemUIOverlayStyle")) {
                setSystemChromeSystemUIOverlayStyle((JSONObject) arguments);
                result.success(null);
            } else if (method.equals("SystemNavigator.pop")) {
                popSystemNavigator();
                result.success(null);
            } else if (method.equals("Clipboard.getData")) {
                result.success(getClipboardData((String) arguments));
            } else if (method.equals("Clipboard.setData")) {
                setClipboardData((JSONObject) arguments);
                result.success(null);
            } else {
                result.notImplemented();
            }
        } catch (JSONException e) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("JSON error: ");
            stringBuilder.append(e.getMessage());
            result.error("error", stringBuilder.toString(), null);
        }
    }

    private void playSystemSound(String soundType) {
        if (soundType.equals("SystemSoundType.click")) {
            this.mActivity.getWindow().getDecorView().playSoundEffect(0);
        }
    }

    private void vibrateHapticFeedback(String feedbackType) {
        View view = this.mActivity.getWindow().getDecorView();
        if (feedbackType == null) {
            view.performHapticFeedback(0);
        } else if (feedbackType.equals("HapticFeedbackType.lightImpact")) {
            view.performHapticFeedback(1);
        } else if (feedbackType.equals("HapticFeedbackType.mediumImpact")) {
            view.performHapticFeedback(3);
        } else if (feedbackType.equals("HapticFeedbackType.heavyImpact")) {
            view.performHapticFeedback(6);
        } else if (feedbackType.equals("HapticFeedbackType.selectionClick")) {
            view.performHapticFeedback(4);
        }
    }

    private void setSystemChromePreferredOrientations(JSONArray orientations) throws JSONException {
        int firstRequestedOrientation = 0;
        int requestedOrientation = 0;
        for (int index = 0; index < orientations.length(); index++) {
            if (orientations.getString(index).equals("DeviceOrientation.portraitUp")) {
                requestedOrientation |= 1;
            } else if (orientations.getString(index).equals("DeviceOrientation.landscapeLeft")) {
                requestedOrientation |= 2;
            } else if (orientations.getString(index).equals("DeviceOrientation.portraitDown")) {
                requestedOrientation |= 4;
            } else if (orientations.getString(index).equals("DeviceOrientation.landscapeRight")) {
                requestedOrientation |= 8;
            }
            if (firstRequestedOrientation == 0) {
                firstRequestedOrientation = requestedOrientation;
            }
        }
        switch (requestedOrientation) {
            case 0:
                this.mActivity.setRequestedOrientation(-1);
                return;
            case BuildConfig.VERSION_CODE /*1*/:
                this.mActivity.setRequestedOrientation(1);
                return;
            case 2:
                this.mActivity.setRequestedOrientation(0);
                return;
            case 3:
            case 6:
            case 7:
            case 9:
            case 12:
            case 13:
            case 14:
                if (firstRequestedOrientation == 4) {
                    this.mActivity.setRequestedOrientation(9);
                    return;
                } else if (firstRequestedOrientation != 8) {
                    switch (firstRequestedOrientation) {
                        case BuildConfig.VERSION_CODE /*1*/:
                            this.mActivity.setRequestedOrientation(1);
                            return;
                        case 2:
                            this.mActivity.setRequestedOrientation(0);
                            return;
                        default:
                            return;
                    }
                } else {
                    this.mActivity.setRequestedOrientation(8);
                    return;
                }
            case 4:
                this.mActivity.setRequestedOrientation(9);
                return;
            case 5:
                this.mActivity.setRequestedOrientation(12);
                return;
            case 8:
                this.mActivity.setRequestedOrientation(8);
                return;
            case 10:
                this.mActivity.setRequestedOrientation(11);
                return;
            case 11:
                this.mActivity.setRequestedOrientation(2);
                return;
            case 15:
                this.mActivity.setRequestedOrientation(13);
                return;
            default:
                return;
        }
    }

    private void setSystemChromeApplicationSwitcherDescription(JSONObject description) throws JSONException {
        if (VERSION.SDK_INT >= 21) {
            int color = description.getInt("primaryColor");
            if (color != 0) {
                color |= -16777216;
            }
            this.mActivity.setTaskDescription(new TaskDescription(description.getString("label"), null, color));
        }
    }

    private void setSystemChromeEnabledSystemUIOverlays(JSONArray overlays) throws JSONException {
        int enabledOverlays = 1798;
        if (overlays.length() == 0) {
            enabledOverlays = 1798 | 4096;
        }
        for (int i = 0; i < overlays.length(); i++) {
            String overlay = overlays.getString(i);
            if (overlay.equals("SystemUiOverlay.top")) {
                enabledOverlays &= -5;
            } else if (overlay.equals("SystemUiOverlay.bottom")) {
                enabledOverlays = (enabledOverlays & -513) & -3;
            }
        }
        this.mEnabledOverlays = enabledOverlays;
        updateSystemUiOverlays();
    }

    private void updateSystemUiOverlays() {
        this.mActivity.getWindow().getDecorView().setSystemUiVisibility(this.mEnabledOverlays);
        if (this.mCurrentTheme != null) {
            setSystemChromeSystemUIOverlayStyle(this.mCurrentTheme);
        }
    }

    private void restoreSystemChromeSystemUIOverlays() {
        updateSystemUiOverlays();
    }

    private void setSystemChromeSystemUIOverlayStyle(JSONObject message) {
        Window window = this.mActivity.getWindow();
        View view = window.getDecorView();
        int flags = view.getSystemUiVisibility();
        try {
            String systemNavigationBarIconBrightness;
            int hashCode;
            Object obj = null;
            if (VERSION.SDK_INT >= 26) {
                if (!message.isNull("systemNavigationBarIconBrightness")) {
                    Object obj2;
                    systemNavigationBarIconBrightness = message.getString("systemNavigationBarIconBrightness");
                    hashCode = systemNavigationBarIconBrightness.hashCode();
                    if (hashCode == -1768926887) {
                        if (systemNavigationBarIconBrightness.equals("Brightness.light")) {
                            obj2 = 1;
                            switch (obj2) {
                                case null:
                                    flags |= 16;
                                    break;
                                case BuildConfig.VERSION_CODE /*1*/:
                                    flags &= -17;
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else if (hashCode == 358334163) {
                        if (systemNavigationBarIconBrightness.equals("Brightness.dark")) {
                            obj2 = null;
                            switch (obj2) {
                                case null:
                                    flags |= 16;
                                    break;
                                case BuildConfig.VERSION_CODE /*1*/:
                                    flags &= -17;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    obj2 = -1;
                    switch (obj2) {
                        case null:
                            flags |= 16;
                            break;
                        case BuildConfig.VERSION_CODE /*1*/:
                            flags &= -17;
                            break;
                        default:
                            break;
                    }
                }
                if (!message.isNull("systemNavigationBarColor")) {
                    window.setNavigationBarColor(message.getInt("systemNavigationBarColor"));
                }
            }
            if (VERSION.SDK_INT >= 23) {
                if (!message.isNull("statusBarIconBrightness")) {
                    systemNavigationBarIconBrightness = message.getString("statusBarIconBrightness");
                    hashCode = systemNavigationBarIconBrightness.hashCode();
                    if (hashCode == -1768926887) {
                        if (systemNavigationBarIconBrightness.equals("Brightness.light")) {
                            obj = 1;
                            switch (obj) {
                                case null:
                                    flags |= 8192;
                                    break;
                                case BuildConfig.VERSION_CODE /*1*/:
                                    flags &= -8193;
                                    break;
                                default:
                                    break;
                            }
                        }
                    } else if (hashCode == 358334163) {
                        if (systemNavigationBarIconBrightness.equals("Brightness.dark")) {
                            switch (obj) {
                                case null:
                                    flags |= 8192;
                                    break;
                                case BuildConfig.VERSION_CODE /*1*/:
                                    flags &= -8193;
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    obj = -1;
                    switch (obj) {
                        case null:
                            flags |= 8192;
                            break;
                        case BuildConfig.VERSION_CODE /*1*/:
                            flags &= -8193;
                            break;
                        default:
                            break;
                    }
                }
                if (!message.isNull("statusBarColor")) {
                    window.setStatusBarColor(message.getInt("statusBarColor"));
                }
            }
            message.isNull("systemNavigationBarDividerColor");
            view.setSystemUiVisibility(flags);
            this.mCurrentTheme = message;
        } catch (JSONException err) {
            Log.i("PlatformPlugin", err.toString());
        }
    }

    private void popSystemNavigator() {
        this.mActivity.finish();
    }

    private JSONObject getClipboardData(String format) throws JSONException {
        ClipData clip = ((ClipboardManager) this.mActivity.getSystemService("clipboard")).getPrimaryClip();
        if (clip == null) {
            return null;
        }
        if (format != null) {
            if (!format.equals(kTextPlainFormat)) {
                return null;
            }
        }
        JSONObject result = new JSONObject();
        result.put("text", clip.getItemAt(0).coerceToText(this.mActivity));
        return result;
    }

    private void setClipboardData(JSONObject data) throws JSONException {
        ((ClipboardManager) this.mActivity.getSystemService("clipboard")).setPrimaryClip(ClipData.newPlainText("text label?", data.getString("text")));
    }

    public void onPostResume() {
        updateSystemUiOverlays();
    }
}
