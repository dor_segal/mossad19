package io.flutter.plugin.platform;

import android.content.Context;
import io.flutter.plugin.common.MessageCodec;

public abstract class PlatformViewFactory {
    private final MessageCodec<Object> mCreateArgsCodec;

    public abstract PlatformView create(Context context, int i, Object obj);

    public PlatformViewFactory(MessageCodec<Object> createArgsCodec) {
        this.mCreateArgsCodec = createArgsCodec;
    }

    public final MessageCodec<Object> getCreateArgsCodec() {
        return this.mCreateArgsCodec;
    }
}
