package io.flutter.plugin.platform;

import android.annotation.TargetApi;
import android.app.Presentation;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.WindowManager.LayoutParams;
import android.widget.FrameLayout;
import com.iwalk.locksmither.BuildConfig;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

@TargetApi(17)
class SingleViewPresentation extends Presentation {
    private FrameLayout mContainer;
    private Object mCreateParams;
    private FrameLayout mRootView;
    private PresentationState mState;
    private final PlatformViewFactory mViewFactory;
    private int mViewId;

    static class FakeWindowViewGroup extends ViewGroup {
        private final Rect mChildRect = new Rect();
        private final Rect mViewBounds = new Rect();

        public FakeWindowViewGroup(Context context) {
            super(context);
        }

        protected void onLayout(boolean changed, int l, int t, int r, int b) {
            FakeWindowViewGroup fakeWindowViewGroup = this;
            for (int i = 0; i < getChildCount(); i++) {
                View child = getChildAt(i);
                LayoutParams params = (LayoutParams) child.getLayoutParams();
                fakeWindowViewGroup.mViewBounds.set(l, t, r, b);
                Gravity.apply(params.gravity, child.getMeasuredWidth(), child.getMeasuredHeight(), fakeWindowViewGroup.mViewBounds, params.x, params.y, fakeWindowViewGroup.mChildRect);
                child.layout(fakeWindowViewGroup.mChildRect.left, fakeWindowViewGroup.mChildRect.top, fakeWindowViewGroup.mChildRect.right, fakeWindowViewGroup.mChildRect.bottom);
            }
            int i2 = l;
            int i3 = t;
            int i4 = r;
            int i5 = b;
        }

        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            for (int i = 0; i < getChildCount(); i++) {
                getChildAt(i).measure(atMost(widthMeasureSpec), atMost(heightMeasureSpec));
            }
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        }

        private static int atMost(int measureSpec) {
            return MeasureSpec.makeMeasureSpec(MeasureSpec.getSize(measureSpec), Integer.MIN_VALUE);
        }
    }

    static class PresentationContext extends ContextWrapper {
        private WindowManager mWindowManager;
        private final WindowManagerHandler mWindowManagerHandler;

        PresentationContext(Context base, WindowManagerHandler windowManagerHandler) {
            super(base);
            this.mWindowManagerHandler = windowManagerHandler;
        }

        public Object getSystemService(String name) {
            if ("window".equals(name)) {
                return getWindowManager();
            }
            return super.getSystemService(name);
        }

        private WindowManager getWindowManager() {
            if (this.mWindowManager == null) {
                this.mWindowManager = this.mWindowManagerHandler.getWindowManager();
            }
            return this.mWindowManager;
        }
    }

    static class PresentationState {
        private FakeWindowViewGroup mFakeWindowRootView;
        private PlatformView mView;
        private WindowManagerHandler mWindowManagerHandler;

        PresentationState() {
        }
    }

    static class WindowManagerHandler implements InvocationHandler {
        private static final String TAG = "PlatformViewsController";
        private final WindowManager mDelegate;
        FakeWindowViewGroup mFakeWindowRootView;

        WindowManagerHandler(WindowManager delegate, FakeWindowViewGroup fakeWindowViewGroup) {
            this.mDelegate = delegate;
            this.mFakeWindowRootView = fakeWindowViewGroup;
        }

        public WindowManager getWindowManager() {
            return (WindowManager) Proxy.newProxyInstance(WindowManager.class.getClassLoader(), new Class[]{WindowManager.class}, this);
        }

        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
            Object obj;
            String name = method.getName();
            int hashCode = name.hashCode();
            if (hashCode != -1148522778) {
                if (hashCode != 542766184) {
                    if (hashCode != 931413976) {
                        if (hashCode == 1098630473) {
                            if (name.equals("removeView")) {
                                obj = 1;
                                switch (obj) {
                                    case null:
                                        addView(args);
                                        return null;
                                    case BuildConfig.VERSION_CODE /*1*/:
                                        removeView(args);
                                        return null;
                                    case 2:
                                        removeViewImmediate(args);
                                        return null;
                                    case 3:
                                        updateViewLayout(args);
                                        return null;
                                    default:
                                        try {
                                            return method.invoke(this.mDelegate, args);
                                        } catch (InvocationTargetException e) {
                                            throw e.getCause();
                                        }
                                }
                            }
                        }
                    } else if (name.equals("updateViewLayout")) {
                        obj = 3;
                        switch (obj) {
                            case null:
                                addView(args);
                                return null;
                            case BuildConfig.VERSION_CODE /*1*/:
                                removeView(args);
                                return null;
                            case 2:
                                removeViewImmediate(args);
                                return null;
                            case 3:
                                updateViewLayout(args);
                                return null;
                            default:
                                return method.invoke(this.mDelegate, args);
                        }
                    }
                } else if (name.equals("removeViewImmediate")) {
                    obj = 2;
                    switch (obj) {
                        case null:
                            addView(args);
                            return null;
                        case BuildConfig.VERSION_CODE /*1*/:
                            removeView(args);
                            return null;
                        case 2:
                            removeViewImmediate(args);
                            return null;
                        case 3:
                            updateViewLayout(args);
                            return null;
                        default:
                            return method.invoke(this.mDelegate, args);
                    }
                }
            } else if (name.equals("addView")) {
                obj = null;
                switch (obj) {
                    case null:
                        addView(args);
                        return null;
                    case BuildConfig.VERSION_CODE /*1*/:
                        removeView(args);
                        return null;
                    case 2:
                        removeViewImmediate(args);
                        return null;
                    case 3:
                        updateViewLayout(args);
                        return null;
                    default:
                        return method.invoke(this.mDelegate, args);
                }
            }
            obj = -1;
            switch (obj) {
                case null:
                    addView(args);
                    return null;
                case BuildConfig.VERSION_CODE /*1*/:
                    removeView(args);
                    return null;
                case 2:
                    removeViewImmediate(args);
                    return null;
                case 3:
                    updateViewLayout(args);
                    return null;
                default:
                    return method.invoke(this.mDelegate, args);
            }
        }

        private void addView(Object[] args) {
            if (this.mFakeWindowRootView == null) {
                Log.w(TAG, "Embedded view called addView while detached from presentation");
                return;
            }
            this.mFakeWindowRootView.addView(args[0], args[1]);
        }

        private void removeView(Object[] args) {
            if (this.mFakeWindowRootView == null) {
                Log.w(TAG, "Embedded view called removeView while detached from presentation");
                return;
            }
            this.mFakeWindowRootView.removeView(args[0]);
        }

        private void removeViewImmediate(Object[] args) {
            if (this.mFakeWindowRootView == null) {
                Log.w(TAG, "Embedded view called removeViewImmediate while detached from presentation");
                return;
            }
            View view = args[0];
            view.clearAnimation();
            this.mFakeWindowRootView.removeView(view);
        }

        private void updateViewLayout(Object[] args) {
            if (this.mFakeWindowRootView == null) {
                Log.w(TAG, "Embedded view called updateViewLayout while detached from presentation");
                return;
            }
            this.mFakeWindowRootView.updateViewLayout(args[0], args[1]);
        }
    }

    public SingleViewPresentation(Context outerContext, Display display, PlatformViewFactory viewFactory, int viewId, Object createParams) {
        super(outerContext, display);
        this.mViewFactory = viewFactory;
        this.mViewId = viewId;
        this.mCreateParams = createParams;
        this.mState = new PresentationState();
        getWindow().setFlags(8, 8);
    }

    public SingleViewPresentation(Context outerContext, Display display, PresentationState state) {
        super(outerContext, display);
        this.mViewFactory = null;
        this.mState = state;
        getWindow().setFlags(8, 8);
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (this.mState.mFakeWindowRootView == null) {
            this.mState.mFakeWindowRootView = new FakeWindowViewGroup(getContext());
        }
        if (this.mState.mWindowManagerHandler == null) {
            this.mState.mWindowManagerHandler = new WindowManagerHandler((WindowManager) getContext().getSystemService("window"), this.mState.mFakeWindowRootView);
        }
        this.mContainer = new FrameLayout(getContext());
        PresentationContext context = new PresentationContext(getContext(), this.mState.mWindowManagerHandler);
        if (this.mState.mView == null) {
            this.mState.mView = this.mViewFactory.create(context, this.mViewId, this.mCreateParams);
        }
        this.mContainer.addView(this.mState.mView.getView());
        this.mRootView = new FrameLayout(getContext());
        this.mRootView.addView(this.mContainer);
        this.mRootView.addView(this.mState.mFakeWindowRootView);
        setContentView(this.mRootView);
    }

    public PresentationState detachState() {
        this.mContainer.removeAllViews();
        this.mRootView.removeAllViews();
        return this.mState;
    }

    public PlatformView getView() {
        if (this.mState.mView == null) {
            return null;
        }
        return this.mState.mView;
    }
}
