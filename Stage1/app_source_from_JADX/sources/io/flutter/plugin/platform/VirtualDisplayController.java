package io.flutter.plugin.platform;

import android.annotation.TargetApi;
import android.content.Context;
import android.hardware.display.DisplayManager;
import android.hardware.display.VirtualDisplay;
import android.view.Surface;
import android.view.View;
import android.view.View.OnAttachStateChangeListener;
import android.view.ViewTreeObserver.OnDrawListener;
import io.flutter.view.TextureRegistry.SurfaceTextureEntry;

@TargetApi(20)
class VirtualDisplayController {
    private final Context mContext;
    private final int mDensityDpi;
    private SingleViewPresentation mPresentation;
    private Surface mSurface;
    private final SurfaceTextureEntry mTextureEntry;
    private VirtualDisplay mVirtualDisplay;

    @TargetApi(16)
    static class OneTimeOnDrawListener implements OnDrawListener {
        Runnable mOnDrawRunnable;
        final View mView;

        /* renamed from: io.flutter.plugin.platform.VirtualDisplayController$OneTimeOnDrawListener$1 */
        class C00071 implements Runnable {
            C00071() {
            }

            public void run() {
                OneTimeOnDrawListener.this.mView.getViewTreeObserver().removeOnDrawListener(OneTimeOnDrawListener.this);
            }
        }

        static void schedule(View view, Runnable runnable) {
            view.getViewTreeObserver().addOnDrawListener(new OneTimeOnDrawListener(view, runnable));
        }

        OneTimeOnDrawListener(View view, Runnable onDrawRunnable) {
            this.mView = view;
            this.mOnDrawRunnable = onDrawRunnable;
        }

        public void onDraw() {
            if (this.mOnDrawRunnable != null) {
                this.mOnDrawRunnable.run();
                this.mOnDrawRunnable = null;
                this.mView.post(new C00071());
            }
        }
    }

    public static VirtualDisplayController create(Context context, PlatformViewFactory viewFactory, SurfaceTextureEntry textureEntry, int width, int height, int viewId, Object createParams) {
        textureEntry.surfaceTexture().setDefaultBufferSize(width, height);
        Surface surface = new Surface(textureEntry.surfaceTexture());
        DisplayManager displayManager = (DisplayManager) context.getSystemService("display");
        int densityDpi = context.getResources().getDisplayMetrics().densityDpi;
        VirtualDisplay virtualDisplay = displayManager.createVirtualDisplay("flutter-vd", width, height, densityDpi, surface, 0);
        if (virtualDisplay == null) {
            return null;
        }
        return new VirtualDisplayController(context, virtualDisplay, viewFactory, surface, textureEntry, viewId, createParams);
    }

    private VirtualDisplayController(Context context, VirtualDisplay virtualDisplay, PlatformViewFactory viewFactory, Surface surface, SurfaceTextureEntry textureEntry, int viewId, Object createParams) {
        this.mTextureEntry = textureEntry;
        this.mSurface = surface;
        this.mContext = context;
        this.mVirtualDisplay = virtualDisplay;
        this.mDensityDpi = context.getResources().getDisplayMetrics().densityDpi;
        this.mPresentation = new SingleViewPresentation(context, this.mVirtualDisplay.getDisplay(), viewFactory, viewId, createParams);
        this.mPresentation.show();
    }

    public void resize(int width, int height, final Runnable onNewSizeFrameAvailable) {
        PresentationState presentationState = this.mPresentation.detachState();
        this.mVirtualDisplay.setSurface(null);
        this.mVirtualDisplay.release();
        this.mTextureEntry.surfaceTexture().setDefaultBufferSize(width, height);
        this.mVirtualDisplay = ((DisplayManager) this.mContext.getSystemService("display")).createVirtualDisplay("flutter-vd", width, height, this.mDensityDpi, this.mSurface, 0);
        final View embeddedView = getView();
        embeddedView.addOnAttachStateChangeListener(new OnAttachStateChangeListener() {

            /* renamed from: io.flutter.plugin.platform.VirtualDisplayController$1$1 */
            class C00051 implements Runnable {
                C00051() {
                }

                public void run() {
                    embeddedView.postDelayed(onNewSizeFrameAvailable, 128);
                }
            }

            public void onViewAttachedToWindow(View v) {
                OneTimeOnDrawListener.schedule(embeddedView, new C00051());
                embeddedView.removeOnAttachStateChangeListener(this);
            }

            public void onViewDetachedFromWindow(View v) {
            }
        });
        this.mPresentation = new SingleViewPresentation(this.mContext, this.mVirtualDisplay.getDisplay(), presentationState);
        this.mPresentation.show();
    }

    public void dispose() {
        PlatformView view = this.mPresentation.getView();
        this.mPresentation.detachState();
        view.dispose();
        this.mVirtualDisplay.release();
        this.mTextureEntry.release();
    }

    public View getView() {
        if (this.mPresentation == null) {
            return null;
        }
        return this.mPresentation.getView().getView();
    }
}
