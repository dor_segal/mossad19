package io.flutter.plugin.platform;

import android.view.View;

public interface PlatformView {
    void dispose();

    View getView();
}
