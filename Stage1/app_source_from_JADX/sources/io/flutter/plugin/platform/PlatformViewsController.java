package io.flutter.plugin.platform;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build.VERSION;
import android.util.Log;
import android.view.MotionEvent;
import android.view.MotionEvent.PointerCoords;
import android.view.MotionEvent.PointerProperties;
import android.view.View;
import com.iwalk.locksmither.BuildConfig;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.StandardMethodCodec;
import io.flutter.view.TextureRegistry;
import io.flutter.view.TextureRegistry.SurfaceTextureEntry;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PlatformViewsController implements MethodCallHandler {
    private static final String CHANNEL_NAME = "flutter/platform_views";
    private static final int MINIMAL_SDK = 20;
    private static final String TAG = "PlatformViewsController";
    private Context mContext;
    private BinaryMessenger mMessenger;
    private final PlatformViewRegistryImpl mRegistry = new PlatformViewRegistryImpl();
    private TextureRegistry mTextureRegistry;
    private final HashMap<Integer, VirtualDisplayController> vdControllers = new HashMap();

    public void attach(Context context, TextureRegistry textureRegistry, BinaryMessenger messenger) {
        if (this.mContext == null) {
            this.mContext = context;
            this.mTextureRegistry = textureRegistry;
            this.mMessenger = messenger;
            new MethodChannel(messenger, CHANNEL_NAME, StandardMethodCodec.INSTANCE).setMethodCallHandler(this);
            return;
        }
        throw new AssertionError("A PlatformViewsController can only be attached to a single output target.\nattach was called while the PlatformViewsController was already attached.");
    }

    public void detach() {
        this.mMessenger.setMessageHandler(CHANNEL_NAME, null);
        this.mMessenger = null;
        this.mContext = null;
        this.mTextureRegistry = null;
    }

    public PlatformViewRegistry getRegistry() {
        return this.mRegistry;
    }

    public void onFlutterViewDestroyed() {
        flushAllViews();
    }

    public void onPreEngineRestart() {
        flushAllViews();
    }

    public void onMethodCall(MethodCall call, Result result) {
        if (VERSION.SDK_INT < MINIMAL_SDK) {
            String str = TAG;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Trying to use platform views with API ");
            stringBuilder.append(VERSION.SDK_INT);
            stringBuilder.append(", required API level is: ");
            stringBuilder.append(MINIMAL_SDK);
            Log.e(str, stringBuilder.toString());
            return;
        }
        str = call.method;
        Object obj = -1;
        switch (str.hashCode()) {
            case -1352294148:
                if (str.equals("create")) {
                    obj = null;
                    break;
                }
                break;
            case -934437708:
                if (str.equals("resize")) {
                    obj = 2;
                    break;
                }
                break;
            case 110550847:
                if (str.equals("touch")) {
                    obj = 3;
                    break;
                }
                break;
            case 576796989:
                if (str.equals("setDirection")) {
                    obj = 4;
                    break;
                }
                break;
            case 1671767583:
                if (str.equals("dispose")) {
                    obj = 1;
                    break;
                }
                break;
            default:
                break;
        }
        switch (obj) {
            case null:
                createPlatformView(call, result);
                return;
            case BuildConfig.VERSION_CODE /*1*/:
                disposePlatformView(call, result);
                return;
            case 2:
                resizePlatformView(call, result);
                return;
            case 3:
                onTouch(call, result);
                return;
            case 4:
                setDirection(call, result);
                return;
            default:
                result.notImplemented();
                return;
        }
    }

    @TargetApi(17)
    private void createPlatformView(MethodCall call, Result result) {
        PlatformViewsController platformViewsController = this;
        Result result2 = result;
        Map<String, Object> args = (Map) call.arguments();
        int id = ((Integer) args.get("id")).intValue();
        String viewType = (String) args.get("viewType");
        double logicalWidth = ((Double) args.get("width")).doubleValue();
        double logicalHeight = ((Double) args.get("height")).doubleValue();
        int direction = ((Integer) args.get("direction")).intValue();
        StringBuilder stringBuilder;
        if (!validateDirection(direction)) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Trying to create a view with unknown direction value: ");
            stringBuilder.append(direction);
            stringBuilder.append("(view id: ");
            stringBuilder.append(id);
            stringBuilder.append(")");
            result2.error("error", stringBuilder.toString(), null);
        } else if (platformViewsController.vdControllers.containsKey(Integer.valueOf(id))) {
            stringBuilder = new StringBuilder();
            stringBuilder.append("Trying to create an already created platform view, view id: ");
            stringBuilder.append(id);
            result2.error("error", stringBuilder.toString(), null);
        } else {
            PlatformViewFactory viewFactory = platformViewsController.mRegistry.getFactory(viewType);
            if (viewFactory == null) {
                stringBuilder = new StringBuilder();
                stringBuilder.append("Trying to create a platform view of unregistered type: ");
                stringBuilder.append(viewType);
                result2.error("error", stringBuilder.toString(), null);
                return;
            }
            Object createParams = null;
            if (args.containsKey("params")) {
                createParams = viewFactory.getCreateArgsCodec().decodeMessage(ByteBuffer.wrap((byte[]) args.get("params")));
            }
            Object createParams2 = createParams;
            SurfaceTextureEntry textureEntry = platformViewsController.mTextureRegistry.createSurfaceTexture();
            Object obj = null;
            int direction2 = direction;
            VirtualDisplayController vdController = VirtualDisplayController.create(platformViewsController.mContext, viewFactory, textureEntry, toPhysicalPixels(logicalWidth), toPhysicalPixels(logicalHeight), id, createParams2);
            if (vdController == null) {
                StringBuilder stringBuilder2 = new StringBuilder();
                stringBuilder2.append("Failed creating virtual display for a ");
                stringBuilder2.append(viewType);
                stringBuilder2.append(" with id: ");
                stringBuilder2.append(id);
                result2.error("error", stringBuilder2.toString(), obj);
                return;
            }
            platformViewsController.vdControllers.put(Integer.valueOf(id), vdController);
            vdController.getView().setLayoutDirection(direction2);
            result2.success(Long.valueOf(textureEntry.id()));
        }
    }

    private void disposePlatformView(MethodCall call, Result result) {
        int id = ((Integer) call.arguments()).intValue();
        VirtualDisplayController vdController = (VirtualDisplayController) this.vdControllers.get(Integer.valueOf(id));
        if (vdController == null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Trying to dispose a platform view with unknown id: ");
            stringBuilder.append(id);
            result.error("error", stringBuilder.toString(), null);
            return;
        }
        vdController.dispose();
        this.vdControllers.remove(Integer.valueOf(id));
        result.success(null);
    }

    private void resizePlatformView(MethodCall call, final Result result) {
        Map<String, Object> args = (Map) call.arguments();
        int id = ((Integer) args.get("id")).intValue();
        double width = ((Double) args.get("width")).doubleValue();
        double height = ((Double) args.get("height")).doubleValue();
        VirtualDisplayController vdController = (VirtualDisplayController) this.vdControllers.get(Integer.valueOf(id));
        if (vdController == null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Trying to resize a platform view with unknown id: ");
            stringBuilder.append(id);
            result.error("error", stringBuilder.toString(), null);
            return;
        }
        vdController.resize(toPhysicalPixels(width), toPhysicalPixels(height), new Runnable() {
            public void run() {
                result.success(null);
            }
        });
    }

    private void onTouch(MethodCall call, Result result) {
        Result result2 = result;
        List<Object> args = (List) call.arguments();
        float density = this.mContext.getResources().getDisplayMetrics().density;
        int id = ((Integer) args.get(0)).intValue();
        Number downTime = (Number) args.get(1);
        Number eventTime = (Number) args.get(2);
        int action = ((Integer) args.get(3)).intValue();
        int pointerCount = ((Integer) args.get(4)).intValue();
        PointerProperties[] pointerProperties = (PointerProperties[]) parsePointerPropertiesList(args.get(5)).toArray(new PointerProperties[pointerCount]);
        PointerCoords[] pointerCoords = (PointerCoords[]) parsePointerCoordsList(args.get(6), density).toArray(new PointerCoords[pointerCount]);
        int metaState = ((Integer) args.get(7)).intValue();
        int buttonState = ((Integer) args.get(8)).intValue();
        float xPrecision = (float) ((Double) args.get(9)).doubleValue();
        float yPrecision = (float) ((Double) args.get(10)).doubleValue();
        int deviceId = ((Integer) args.get(11)).intValue();
        int edgeFlags = ((Integer) args.get(12)).intValue();
        int source = ((Integer) args.get(13)).intValue();
        int flags = ((Integer) args.get(14)).intValue();
        View view = ((VirtualDisplayController) this.vdControllers.get(Integer.valueOf(id))).getView();
        if (view == null) {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Sending touch to an unknown view with id: ");
            stringBuilder.append(id);
            result2.error("error", stringBuilder.toString(), null);
            return;
        }
        Object obj = null;
        float xPrecision2 = xPrecision;
        view.dispatchTouchEvent(MotionEvent.obtain(downTime.longValue(), eventTime.longValue(), action, pointerCount, pointerProperties, pointerCoords, metaState, buttonState, xPrecision2, yPrecision, deviceId, edgeFlags, source, flags));
        result2.success(null);
    }

    @TargetApi(17)
    private void setDirection(MethodCall call, Result result) {
        Map<String, Object> args = (Map) call.arguments();
        int id = ((Integer) args.get("id")).intValue();
        int direction = ((Integer) args.get("direction")).intValue();
        if (validateDirection(direction)) {
            View view = ((VirtualDisplayController) this.vdControllers.get(Integer.valueOf(id))).getView();
            if (view == null) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Sending touch to an unknown view with id: ");
                stringBuilder.append(id);
                result.error("error", stringBuilder.toString(), null);
                return;
            }
            view.setLayoutDirection(direction);
            result.success(null);
            return;
        }
        StringBuilder stringBuilder2 = new StringBuilder();
        stringBuilder2.append("Trying to set unknown direction value: ");
        stringBuilder2.append(direction);
        stringBuilder2.append("(view id: ");
        stringBuilder2.append(id);
        stringBuilder2.append(")");
        result.error("error", stringBuilder2.toString(), null);
    }

    private static boolean validateDirection(int direction) {
        if (direction != 0) {
            return direction == 1;
        } else {
            return true;
        }
    }

    private static List<PointerProperties> parsePointerPropertiesList(Object rawPropertiesList) {
        List<Object> rawProperties = (List) rawPropertiesList;
        List<PointerProperties> pointerProperties = new ArrayList();
        for (Object o : rawProperties) {
            pointerProperties.add(parsePointerProperties(o));
        }
        return pointerProperties;
    }

    private static PointerProperties parsePointerProperties(Object rawProperties) {
        List<Object> propertiesList = (List) rawProperties;
        PointerProperties properties = new PointerProperties();
        properties.id = ((Integer) propertiesList.get(0)).intValue();
        properties.toolType = ((Integer) propertiesList.get(1)).intValue();
        return properties;
    }

    private static List<PointerCoords> parsePointerCoordsList(Object rawCoordsList, float density) {
        List<Object> rawCoords = (List) rawCoordsList;
        List<PointerCoords> pointerCoords = new ArrayList();
        for (Object o : rawCoords) {
            pointerCoords.add(parsePointerCoords(o, density));
        }
        return pointerCoords;
    }

    private static PointerCoords parsePointerCoords(Object rawCoords, float density) {
        List<Object> coordsList = (List) rawCoords;
        PointerCoords coords = new PointerCoords();
        coords.orientation = (float) ((Double) coordsList.get(0)).doubleValue();
        coords.pressure = (float) ((Double) coordsList.get(1)).doubleValue();
        coords.size = (float) ((Double) coordsList.get(2)).doubleValue();
        coords.toolMajor = ((float) ((Double) coordsList.get(3)).doubleValue()) * density;
        coords.toolMinor = ((float) ((Double) coordsList.get(4)).doubleValue()) * density;
        coords.touchMajor = ((float) ((Double) coordsList.get(5)).doubleValue()) * density;
        coords.touchMinor = ((float) ((Double) coordsList.get(6)).doubleValue()) * density;
        coords.x = ((float) ((Double) coordsList.get(7)).doubleValue()) * density;
        coords.y = ((float) ((Double) coordsList.get(8)).doubleValue()) * density;
        return coords;
    }

    private int toPhysicalPixels(double logicalPixels) {
        double d = (double) this.mContext.getResources().getDisplayMetrics().density;
        Double.isNaN(d);
        return (int) Math.round(d * logicalPixels);
    }

    private void flushAllViews() {
        for (VirtualDisplayController controller : this.vdControllers.values()) {
            controller.dispose();
        }
        this.vdControllers.clear();
    }
}
