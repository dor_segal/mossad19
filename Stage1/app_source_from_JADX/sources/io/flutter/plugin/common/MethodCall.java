package io.flutter.plugin.common;

import android.support.annotation.Nullable;
import java.util.Map;
import org.json.JSONObject;

public final class MethodCall {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    public final Object arguments;
    public final String method;

    public MethodCall(String method, Object arguments) {
        this.method = method;
        this.arguments = arguments;
    }

    public <T> T arguments() {
        return this.arguments;
    }

    @Nullable
    public <T> T argument(String key) {
        if (this.arguments == null) {
            return null;
        }
        if (this.arguments instanceof Map) {
            return ((Map) this.arguments).get(key);
        }
        if (this.arguments instanceof JSONObject) {
            return ((JSONObject) this.arguments).opt(key);
        }
        throw new ClassCastException();
    }

    public boolean hasArgument(String key) {
        if (this.arguments == null) {
            return false;
        }
        if (this.arguments instanceof Map) {
            return ((Map) this.arguments).containsKey(key);
        }
        if (this.arguments instanceof JSONObject) {
            return ((JSONObject) this.arguments).has(key);
        }
        throw new ClassCastException();
    }
}
