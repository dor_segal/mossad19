package io.flutter.plugin.common;

import android.util.Log;
import io.flutter.plugin.common.BinaryMessenger.BinaryMessageHandler;
import io.flutter.plugin.common.BinaryMessenger.BinaryReply;
import java.nio.ByteBuffer;

public final class BasicMessageChannel<T> {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final String TAG = "BasicMessageChannel#";
    private final MessageCodec<T> codec;
    private final BinaryMessenger messenger;
    private final String name;

    public interface MessageHandler<T> {
        void onMessage(T t, Reply<T> reply);
    }

    public interface Reply<T> {
        void reply(T t);
    }

    private final class IncomingMessageHandler implements BinaryMessageHandler {
        private final MessageHandler<T> handler;

        private IncomingMessageHandler(MessageHandler<T> handler) {
            this.handler = handler;
        }

        public void onMessage(ByteBuffer message, final BinaryReply callback) {
            try {
                this.handler.onMessage(BasicMessageChannel.this.codec.decodeMessage(message), new Reply<T>() {
                    public void reply(T reply) {
                        callback.reply(BasicMessageChannel.this.codec.encodeMessage(reply));
                    }
                });
            } catch (RuntimeException e) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(BasicMessageChannel.TAG);
                stringBuilder.append(BasicMessageChannel.this.name);
                Log.e(stringBuilder.toString(), "Failed to handle message", e);
                callback.reply(null);
            }
        }
    }

    private final class IncomingReplyHandler implements BinaryReply {
        private final Reply<T> callback;

        private IncomingReplyHandler(Reply<T> callback) {
            this.callback = callback;
        }

        public void reply(ByteBuffer reply) {
            try {
                this.callback.reply(BasicMessageChannel.this.codec.decodeMessage(reply));
            } catch (RuntimeException e) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(BasicMessageChannel.TAG);
                stringBuilder.append(BasicMessageChannel.this.name);
                Log.e(stringBuilder.toString(), "Failed to handle message reply", e);
            }
        }
    }

    public BasicMessageChannel(BinaryMessenger messenger, String name, MessageCodec<T> codec) {
        this.messenger = messenger;
        this.name = name;
        this.codec = codec;
    }

    public void send(T message) {
        send(message, null);
    }

    public void send(T message, Reply<T> callback) {
        BinaryMessenger binaryMessenger = this.messenger;
        String str = this.name;
        ByteBuffer encodeMessage = this.codec.encodeMessage(message);
        BinaryReply binaryReply = null;
        if (callback != null) {
            binaryReply = new IncomingReplyHandler(callback);
        }
        binaryMessenger.send(str, encodeMessage, binaryReply);
    }

    public void setMessageHandler(MessageHandler<T> handler) {
        BinaryMessenger binaryMessenger = this.messenger;
        String str = this.name;
        BinaryMessageHandler binaryMessageHandler = null;
        if (handler != null) {
            binaryMessageHandler = new IncomingMessageHandler(handler);
        }
        binaryMessenger.setMessageHandler(str, binaryMessageHandler);
    }
}
