package io.flutter.plugin.common;

public class FlutterException extends RuntimeException {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    public final String code;
    public final Object details;

    FlutterException(String code, String message, Object details) {
        super(message);
        this.code = code;
        this.details = details;
    }
}
