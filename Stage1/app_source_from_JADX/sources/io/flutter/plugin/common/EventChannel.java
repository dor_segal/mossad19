package io.flutter.plugin.common;

import android.util.Log;
import io.flutter.plugin.common.BinaryMessenger.BinaryMessageHandler;
import io.flutter.plugin.common.BinaryMessenger.BinaryReply;
import java.nio.ByteBuffer;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public final class EventChannel {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final String TAG = "EventChannel#";
    private final MethodCodec codec;
    private final BinaryMessenger messenger;
    private final String name;

    public interface EventSink {
        void endOfStream();

        void error(String str, String str2, Object obj);

        void success(Object obj);
    }

    public interface StreamHandler {
        void onCancel(Object obj);

        void onListen(Object obj, EventSink eventSink);
    }

    private final class IncomingStreamRequestHandler implements BinaryMessageHandler {
        private final AtomicReference<EventSink> activeSink = new AtomicReference(null);
        private final StreamHandler handler;

        private final class EventSinkImplementation implements EventSink {
            final AtomicBoolean hasEnded;

            private EventSinkImplementation() {
                this.hasEnded = new AtomicBoolean(false);
            }

            public void success(Object event) {
                if (!this.hasEnded.get()) {
                    if (IncomingStreamRequestHandler.this.activeSink.get() == this) {
                        EventChannel.this.messenger.send(EventChannel.this.name, EventChannel.this.codec.encodeSuccessEnvelope(event));
                    }
                }
            }

            public void error(String errorCode, String errorMessage, Object errorDetails) {
                if (!this.hasEnded.get()) {
                    if (IncomingStreamRequestHandler.this.activeSink.get() == this) {
                        EventChannel.this.messenger.send(EventChannel.this.name, EventChannel.this.codec.encodeErrorEnvelope(errorCode, errorMessage, errorDetails));
                    }
                }
            }

            public void endOfStream() {
                if (!this.hasEnded.getAndSet(true)) {
                    if (IncomingStreamRequestHandler.this.activeSink.get() == this) {
                        EventChannel.this.messenger.send(EventChannel.this.name, null);
                    }
                }
            }
        }

        IncomingStreamRequestHandler(StreamHandler handler) {
            this.handler = handler;
        }

        public void onMessage(ByteBuffer message, BinaryReply reply) {
            MethodCall call = EventChannel.this.codec.decodeMethodCall(message);
            if (call.method.equals("listen")) {
                onListen(call.arguments, reply);
            } else if (call.method.equals("cancel")) {
                onCancel(call.arguments, reply);
            } else {
                reply.reply(null);
            }
        }

        private void onListen(Object arguments, BinaryReply callback) {
            EventSink eventSink = new EventSinkImplementation();
            if (((EventSink) this.activeSink.getAndSet(eventSink)) != null) {
                try {
                    this.handler.onCancel(null);
                } catch (RuntimeException e) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(EventChannel.TAG);
                    stringBuilder.append(EventChannel.this.name);
                    Log.e(stringBuilder.toString(), "Failed to close existing event stream", e);
                }
            }
            try {
                this.handler.onListen(arguments, eventSink);
                callback.reply(EventChannel.this.codec.encodeSuccessEnvelope(null));
            } catch (RuntimeException e2) {
                this.activeSink.set(null);
                stringBuilder = new StringBuilder();
                stringBuilder.append(EventChannel.TAG);
                stringBuilder.append(EventChannel.this.name);
                Log.e(stringBuilder.toString(), "Failed to open event stream", e2);
                callback.reply(EventChannel.this.codec.encodeErrorEnvelope("error", e2.getMessage(), null));
            }
        }

        private void onCancel(Object arguments, BinaryReply callback) {
            if (((EventSink) this.activeSink.getAndSet(null)) != null) {
                try {
                    this.handler.onCancel(arguments);
                    callback.reply(EventChannel.this.codec.encodeSuccessEnvelope(null));
                } catch (RuntimeException e) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(EventChannel.TAG);
                    stringBuilder.append(EventChannel.this.name);
                    Log.e(stringBuilder.toString(), "Failed to close event stream", e);
                    callback.reply(EventChannel.this.codec.encodeErrorEnvelope("error", e.getMessage(), null));
                }
                return;
            }
            callback.reply(EventChannel.this.codec.encodeErrorEnvelope("error", "No active stream to cancel", null));
        }
    }

    public EventChannel(BinaryMessenger messenger, String name) {
        this(messenger, name, StandardMethodCodec.INSTANCE);
    }

    public EventChannel(BinaryMessenger messenger, String name, MethodCodec codec) {
        this.messenger = messenger;
        this.name = name;
        this.codec = codec;
    }

    public void setStreamHandler(StreamHandler handler) {
        this.messenger.setMessageHandler(this.name, handler == null ? null : new IncomingStreamRequestHandler(handler));
    }
}
