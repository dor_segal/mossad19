package io.flutter.plugin.common;

import android.support.annotation.Nullable;
import android.util.Log;
import io.flutter.plugin.common.BinaryMessenger.BinaryMessageHandler;
import io.flutter.plugin.common.BinaryMessenger.BinaryReply;
import java.nio.ByteBuffer;

public final class MethodChannel {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    private static final String TAG = "MethodChannel#";
    private final MethodCodec codec;
    private final BinaryMessenger messenger;
    private final String name;

    public interface MethodCallHandler {
        void onMethodCall(MethodCall methodCall, Result result);
    }

    public interface Result {
        void error(String str, @Nullable String str2, @Nullable Object obj);

        void notImplemented();

        void success(@Nullable Object obj);
    }

    private final class IncomingMethodCallHandler implements BinaryMessageHandler {
        private final MethodCallHandler handler;

        IncomingMethodCallHandler(MethodCallHandler handler) {
            this.handler = handler;
        }

        public void onMessage(ByteBuffer message, final BinaryReply reply) {
            try {
                this.handler.onMethodCall(MethodChannel.this.codec.decodeMethodCall(message), new Result() {
                    public void success(Object result) {
                        reply.reply(MethodChannel.this.codec.encodeSuccessEnvelope(result));
                    }

                    public void error(String errorCode, String errorMessage, Object errorDetails) {
                        reply.reply(MethodChannel.this.codec.encodeErrorEnvelope(errorCode, errorMessage, errorDetails));
                    }

                    public void notImplemented() {
                        reply.reply(null);
                    }
                });
            } catch (RuntimeException e) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append(MethodChannel.TAG);
                stringBuilder.append(MethodChannel.this.name);
                Log.e(stringBuilder.toString(), "Failed to handle method call", e);
                reply.reply(MethodChannel.this.codec.encodeErrorEnvelope("error", e.getMessage(), null));
            }
        }
    }

    private final class IncomingResultHandler implements BinaryReply {
        private final Result callback;

        IncomingResultHandler(Result callback) {
            this.callback = callback;
        }

        public void reply(ByteBuffer reply) {
            if (reply == null) {
                try {
                    this.callback.notImplemented();
                } catch (RuntimeException e) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append(MethodChannel.TAG);
                    stringBuilder.append(MethodChannel.this.name);
                    Log.e(stringBuilder.toString(), "Failed to handle method call result", e);
                    return;
                }
            }
            try {
                this.callback.success(MethodChannel.this.codec.decodeEnvelope(reply));
            } catch (RuntimeException e2) {
                this.callback.error(e2.code, e2.getMessage(), e2.details);
            }
        }
    }

    public MethodChannel(BinaryMessenger messenger, String name) {
        this(messenger, name, StandardMethodCodec.INSTANCE);
    }

    public MethodChannel(BinaryMessenger messenger, String name, MethodCodec codec) {
        this.messenger = messenger;
        this.name = name;
        this.codec = codec;
    }

    public void invokeMethod(String method, @Nullable Object arguments) {
        invokeMethod(method, arguments, null);
    }

    public void invokeMethod(String method, @Nullable Object arguments, Result callback) {
        this.messenger.send(this.name, this.codec.encodeMethodCall(new MethodCall(method, arguments)), callback == null ? null : new IncomingResultHandler(callback));
    }

    public void setMethodCallHandler(@Nullable MethodCallHandler handler) {
        this.messenger.setMessageHandler(this.name, handler == null ? null : new IncomingMethodCallHandler(handler));
    }
}
