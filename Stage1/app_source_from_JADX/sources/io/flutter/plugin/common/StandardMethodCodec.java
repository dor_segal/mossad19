package io.flutter.plugin.common;

import com.iwalk.locksmither.BuildConfig;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public final class StandardMethodCodec implements MethodCodec {
    public static final StandardMethodCodec INSTANCE = new StandardMethodCodec(StandardMessageCodec.INSTANCE);
    private final StandardMessageCodec messageCodec;

    public StandardMethodCodec(StandardMessageCodec messageCodec) {
        this.messageCodec = messageCodec;
    }

    public ByteBuffer encodeMethodCall(MethodCall methodCall) {
        ExposedByteArrayOutputStream stream = new ExposedByteArrayOutputStream();
        this.messageCodec.writeValue(stream, methodCall.method);
        this.messageCodec.writeValue(stream, methodCall.arguments);
        ByteBuffer buffer = ByteBuffer.allocateDirect(stream.size());
        buffer.put(stream.buffer(), 0, stream.size());
        return buffer;
    }

    public MethodCall decodeMethodCall(ByteBuffer methodCall) {
        methodCall.order(ByteOrder.nativeOrder());
        Object method = this.messageCodec.readValue(methodCall);
        Object arguments = this.messageCodec.readValue(methodCall);
        if ((method instanceof String) && !methodCall.hasRemaining()) {
            return new MethodCall((String) method, arguments);
        }
        throw new IllegalArgumentException("Method call corrupted");
    }

    public ByteBuffer encodeSuccessEnvelope(Object result) {
        ExposedByteArrayOutputStream stream = new ExposedByteArrayOutputStream();
        stream.write(0);
        this.messageCodec.writeValue(stream, result);
        ByteBuffer buffer = ByteBuffer.allocateDirect(stream.size());
        buffer.put(stream.buffer(), 0, stream.size());
        return buffer;
    }

    public ByteBuffer encodeErrorEnvelope(String errorCode, String errorMessage, Object errorDetails) {
        ExposedByteArrayOutputStream stream = new ExposedByteArrayOutputStream();
        stream.write(1);
        this.messageCodec.writeValue(stream, errorCode);
        this.messageCodec.writeValue(stream, errorMessage);
        this.messageCodec.writeValue(stream, errorDetails);
        ByteBuffer buffer = ByteBuffer.allocateDirect(stream.size());
        buffer.put(stream.buffer(), 0, stream.size());
        return buffer;
    }

    public Object decodeEnvelope(ByteBuffer envelope) {
        Object result;
        envelope.order(ByteOrder.nativeOrder());
        switch (envelope.get()) {
            case (byte) 0:
                result = this.messageCodec.readValue(envelope);
                if (!envelope.hasRemaining()) {
                    return result;
                }
                break;
            case BuildConfig.VERSION_CODE /*1*/:
                break;
            default:
                break;
        }
        result = this.messageCodec.readValue(envelope);
        Object message = this.messageCodec.readValue(envelope);
        Object details = this.messageCodec.readValue(envelope);
        if ((result instanceof String) && ((message == null || (message instanceof String)) && !envelope.hasRemaining())) {
            throw new FlutterException((String) result, (String) message, details);
        }
        throw new IllegalArgumentException("Envelope corrupted");
    }
}
