package io.flutter.plugin.common;

import android.util.Log;
import com.iwalk.locksmither.BuildConfig;
import java.io.ByteArrayOutputStream;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class StandardMessageCodec implements MessageCodec<Object> {
    static final /* synthetic */ boolean $assertionsDisabled = false;
    @Deprecated
    private static final byte BIGINT = (byte) 5;
    private static final byte BYTE_ARRAY = (byte) 8;
    private static final byte DOUBLE = (byte) 6;
    private static final byte DOUBLE_ARRAY = (byte) 11;
    private static final byte FALSE = (byte) 2;
    public static final StandardMessageCodec INSTANCE = new StandardMessageCodec();
    private static final byte INT = (byte) 3;
    private static final byte INT_ARRAY = (byte) 9;
    private static final byte LIST = (byte) 12;
    private static final boolean LITTLE_ENDIAN = (ByteOrder.nativeOrder() == ByteOrder.LITTLE_ENDIAN);
    private static final byte LONG = (byte) 4;
    private static final byte LONG_ARRAY = (byte) 10;
    private static final byte MAP = (byte) 13;
    private static final byte NULL = (byte) 0;
    private static final byte STRING = (byte) 7;
    private static final byte TRUE = (byte) 1;
    private static final Charset UTF8 = Charset.forName("UTF8");

    static final class ExposedByteArrayOutputStream extends ByteArrayOutputStream {
        ExposedByteArrayOutputStream() {
        }

        byte[] buffer() {
            return this.buf;
        }
    }

    public ByteBuffer encodeMessage(Object message) {
        if (message == null) {
            return null;
        }
        ExposedByteArrayOutputStream stream = new ExposedByteArrayOutputStream();
        writeValue(stream, message);
        ByteBuffer buffer = ByteBuffer.allocateDirect(stream.size());
        buffer.put(stream.buffer(), 0, stream.size());
        return buffer;
    }

    public Object decodeMessage(ByteBuffer message) {
        if (message == null) {
            return null;
        }
        message.order(ByteOrder.nativeOrder());
        Object value = readValue(message);
        if (!message.hasRemaining()) {
            return value;
        }
        throw new IllegalArgumentException("Message corrupted");
    }

    protected static final void writeSize(ByteArrayOutputStream stream, int value) {
        if (value < 254) {
            stream.write(value);
        } else if (value <= 65535) {
            stream.write(254);
            writeChar(stream, value);
        } else {
            stream.write(255);
            writeInt(stream, value);
        }
    }

    protected static final void writeChar(ByteArrayOutputStream stream, int value) {
        if (LITTLE_ENDIAN) {
            stream.write(value);
            stream.write(value >>> 8);
            return;
        }
        stream.write(value >>> 8);
        stream.write(value);
    }

    protected static final void writeInt(ByteArrayOutputStream stream, int value) {
        if (LITTLE_ENDIAN) {
            stream.write(value);
            stream.write(value >>> 8);
            stream.write(value >>> 16);
            stream.write(value >>> 24);
            return;
        }
        stream.write(value >>> 24);
        stream.write(value >>> 16);
        stream.write(value >>> 8);
        stream.write(value);
    }

    protected static final void writeLong(ByteArrayOutputStream stream, long value) {
        if (LITTLE_ENDIAN) {
            stream.write((byte) ((int) value));
            stream.write((byte) ((int) (value >>> 8)));
            stream.write((byte) ((int) (value >>> 16)));
            stream.write((byte) ((int) (value >>> 24)));
            stream.write((byte) ((int) (value >>> 32)));
            stream.write((byte) ((int) (value >>> 40)));
            stream.write((byte) ((int) (value >>> 48)));
            stream.write((byte) ((int) (value >>> 56)));
            return;
        }
        stream.write((byte) ((int) (value >>> 56)));
        stream.write((byte) ((int) (value >>> 48)));
        stream.write((byte) ((int) (value >>> 40)));
        stream.write((byte) ((int) (value >>> 32)));
        stream.write((byte) ((int) (value >>> 24)));
        stream.write((byte) ((int) (value >>> 16)));
        stream.write((byte) ((int) (value >>> 8)));
        stream.write((byte) ((int) value));
    }

    protected static final void writeDouble(ByteArrayOutputStream stream, double value) {
        writeLong(stream, Double.doubleToLongBits(value));
    }

    protected static final void writeBytes(ByteArrayOutputStream stream, byte[] bytes) {
        writeSize(stream, bytes.length);
        stream.write(bytes, 0, bytes.length);
    }

    protected static final void writeAlignment(ByteArrayOutputStream stream, int alignment) {
        int mod = stream.size() % alignment;
        if (mod != 0) {
            for (int i = 0; i < alignment - mod; i++) {
                stream.write(0);
            }
        }
    }

    protected void writeValue(ByteArrayOutputStream stream, Object value) {
        int i = 0;
        if (value == null) {
            stream.write(0);
        } else if (value == Boolean.TRUE) {
            stream.write(1);
        } else if (value == Boolean.FALSE) {
            stream.write(2);
        } else if (value instanceof Number) {
            if (!((value instanceof Integer) || (value instanceof Short))) {
                if (!(value instanceof Byte)) {
                    if (value instanceof Long) {
                        stream.write(4);
                        writeLong(stream, ((Long) value).longValue());
                        return;
                    }
                    if (!(value instanceof Float)) {
                        if (!(value instanceof Double)) {
                            if (value instanceof BigInteger) {
                                Log.w("Flutter", "Support for BigIntegers has been deprecated. Use String encoding instead.");
                                stream.write(5);
                                writeBytes(stream, ((BigInteger) value).toString(16).getBytes(UTF8));
                                return;
                            }
                            r1 = new StringBuilder();
                            r1.append("Unsupported Number type: ");
                            r1.append(value.getClass());
                            throw new IllegalArgumentException(r1.toString());
                        }
                    }
                    stream.write(6);
                    writeAlignment(stream, 8);
                    writeDouble(stream, ((Number) value).doubleValue());
                    return;
                }
            }
            stream.write(3);
            writeInt(stream, ((Number) value).intValue());
        } else if (value instanceof String) {
            stream.write(7);
            writeBytes(stream, ((String) value).getBytes(UTF8));
        } else if (value instanceof byte[]) {
            stream.write(8);
            writeBytes(stream, (byte[]) value);
        } else if (value instanceof int[]) {
            stream.write(9);
            int[] array = (int[]) value;
            writeSize(stream, array.length);
            writeAlignment(stream, 4);
            r2 = array.length;
            while (i < r2) {
                writeInt(stream, array[i]);
                i++;
            }
        } else if (value instanceof long[]) {
            stream.write(10);
            long[] array2 = (long[]) value;
            writeSize(stream, array2.length);
            writeAlignment(stream, 8);
            r2 = array2.length;
            while (i < r2) {
                writeLong(stream, array2[i]);
                i++;
            }
        } else if (value instanceof double[]) {
            stream.write(11);
            double[] array3 = (double[]) value;
            writeSize(stream, array3.length);
            writeAlignment(stream, 8);
            r2 = array3.length;
            while (i < r2) {
                writeDouble(stream, array3[i]);
                i++;
            }
        } else if (value instanceof List) {
            stream.write(12);
            List<?> list = (List) value;
            writeSize(stream, list.size());
            for (Object o : list) {
                writeValue(stream, o);
            }
        } else if (value instanceof Map) {
            stream.write(13);
            Map<?, ?> map = (Map) value;
            writeSize(stream, map.size());
            for (Entry entry : map.entrySet()) {
                writeValue(stream, entry.getKey());
                writeValue(stream, entry.getValue());
            }
        } else {
            r1 = new StringBuilder();
            r1.append("Unsupported value: ");
            r1.append(value);
            throw new IllegalArgumentException(r1.toString());
        }
    }

    protected static final int readSize(ByteBuffer buffer) {
        if (buffer.hasRemaining()) {
            int value = buffer.get() & 255;
            if (value < 254) {
                return value;
            }
            if (value == 254) {
                return buffer.getChar();
            }
            return buffer.getInt();
        }
        throw new IllegalArgumentException("Message corrupted");
    }

    protected static final byte[] readBytes(ByteBuffer buffer) {
        byte[] bytes = new byte[readSize(buffer)];
        buffer.get(bytes);
        return bytes;
    }

    protected static final void readAlignment(ByteBuffer buffer, int alignment) {
        int mod = buffer.position() % alignment;
        if (mod != 0) {
            buffer.position((buffer.position() + alignment) - mod);
        }
    }

    protected final Object readValue(ByteBuffer buffer) {
        if (buffer.hasRemaining()) {
            return readValueOfType(buffer.get(), buffer);
        }
        throw new IllegalArgumentException("Message corrupted");
    }

    protected Object readValueOfType(byte type, ByteBuffer buffer) {
        Object result;
        Object result2;
        int i = 0;
        Object result3;
        int size;
        switch (type) {
            case (byte) 0:
                result = null;
                break;
            case BuildConfig.VERSION_CODE /*1*/:
                result = Boolean.valueOf(true);
                break;
            case (byte) 2:
                result = Boolean.valueOf(false);
                break;
            case (byte) 3:
                result = Integer.valueOf(buffer.getInt());
                break;
            case (byte) 4:
                result = Long.valueOf(buffer.getLong());
                break;
            case (byte) 5:
                Log.w("Flutter", "Support for BigIntegers has been deprecated. Use String encoding instead.");
                result2 = new BigInteger(new String(readBytes(buffer), UTF8), 16);
                break;
            case (byte) 6:
                readAlignment(buffer, 8);
                result = Double.valueOf(buffer.getDouble());
                break;
            case (byte) 7:
                result2 = new String(readBytes(buffer), UTF8);
                break;
            case (byte) 8:
                result = readBytes(buffer);
                break;
            case (byte) 9:
                i = readSize(buffer);
                result2 = new int[i];
                readAlignment(buffer, 4);
                buffer.asIntBuffer().get(result2);
                result3 = result2;
                buffer.position(buffer.position() + (i * 4));
                result = result3;
                break;
            case (byte) 10:
                i = readSize(buffer);
                result3 = new long[i];
                readAlignment(buffer, 8);
                buffer.asLongBuffer().get(result3);
                result2 = result3;
                buffer.position(buffer.position() + (i * 8));
                break;
            case (byte) 11:
                i = readSize(buffer);
                result3 = new double[i];
                readAlignment(buffer, 8);
                buffer.asDoubleBuffer().get(result3);
                result2 = result3;
                buffer.position(buffer.position() + (i * 8));
                break;
            case (byte) 12:
                size = readSize(buffer);
                List<Object> list = new ArrayList(size);
                while (i < size) {
                    list.add(readValue(buffer));
                    i++;
                }
                result = list;
                break;
            case (byte) 13:
                size = readSize(buffer);
                Map<Object, Object> map = new HashMap();
                while (i < size) {
                    map.put(readValue(buffer), readValue(buffer));
                    i++;
                }
                result = map;
                break;
            default:
                throw new IllegalArgumentException("Message corrupted");
        }
        result = result2;
        return result;
    }
}
