package io.flutter.plugin.common;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.json.JSONArray;
import org.json.JSONObject;

public class JSONUtil {
    private JSONUtil() {
    }

    public static Object unwrap(Object o) {
        if (!JSONObject.NULL.equals(o)) {
            if (o != null) {
                if (!((o instanceof Boolean) || (o instanceof Byte) || (o instanceof Character) || (o instanceof Double) || (o instanceof Float) || (o instanceof Integer) || (o instanceof Long) || (o instanceof Short))) {
                    if (!(o instanceof String)) {
                        try {
                            if (o instanceof JSONArray) {
                                List<Object> list = new ArrayList();
                                JSONArray array = (JSONArray) o;
                                for (int i = 0; i < array.length(); i++) {
                                    list.add(unwrap(array.get(i)));
                                }
                                return list;
                            } else if (!(o instanceof JSONObject)) {
                                return null;
                            } else {
                                Map<String, Object> map = new HashMap();
                                JSONObject jsonObject = (JSONObject) o;
                                Iterator<String> keyIterator = jsonObject.keys();
                                while (keyIterator.hasNext()) {
                                    String key = (String) keyIterator.next();
                                    map.put(key, unwrap(jsonObject.get(key)));
                                }
                                return map;
                            }
                        } catch (Exception e) {
                        }
                    }
                }
                return o;
            }
        }
        return null;
    }

    public static Object wrap(Object o) {
        if (o == null) {
            return JSONObject.NULL;
        }
        if (!(o instanceof JSONArray)) {
            if (!(o instanceof JSONObject)) {
                if (o.equals(JSONObject.NULL)) {
                    return o;
                }
                try {
                    JSONArray result;
                    if (o instanceof Collection) {
                        result = new JSONArray();
                        for (Object e : (Collection) o) {
                            result.put(wrap(e));
                        }
                        return result;
                    } else if (o.getClass().isArray()) {
                        result = new JSONArray();
                        int length = Array.getLength(o);
                        for (int i = 0; i < length; i++) {
                            result.put(wrap(Array.get(o, i)));
                        }
                        return result;
                    } else if (o instanceof Map) {
                        JSONObject result2 = new JSONObject();
                        for (Entry<?, ?> entry : ((Map) o).entrySet()) {
                            result2.put((String) entry.getKey(), wrap(entry.getValue()));
                        }
                        return result2;
                    } else {
                        if (!((o instanceof Boolean) || (o instanceof Byte) || (o instanceof Character) || (o instanceof Double) || (o instanceof Float) || (o instanceof Integer) || (o instanceof Long) || (o instanceof Short))) {
                            if (!(o instanceof String)) {
                                if (o.getClass().getPackage().getName().startsWith("java.")) {
                                    return o.toString();
                                }
                                return null;
                            }
                        }
                        return o;
                    }
                } catch (Exception e2) {
                }
            }
        }
        return o;
    }
}
