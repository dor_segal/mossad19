.class final Lio/flutter/plugin/common/BasicMessageChannel$IncomingReplyHandler;
.super Ljava/lang/Object;
.source "BasicMessageChannel.java"

# interfaces
.implements Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/flutter/plugin/common/BasicMessageChannel;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "IncomingReplyHandler"
.end annotation


# instance fields
.field private final callback:Lio/flutter/plugin/common/BasicMessageChannel$Reply;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/flutter/plugin/common/BasicMessageChannel$Reply<",
            "TT;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lio/flutter/plugin/common/BasicMessageChannel;


# direct methods
.method private constructor <init>(Lio/flutter/plugin/common/BasicMessageChannel;Lio/flutter/plugin/common/BasicMessageChannel$Reply;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lio/flutter/plugin/common/BasicMessageChannel$Reply<",
            "TT;>;)V"
        }
    .end annotation

    .line 130
    .local p0, "this":Lio/flutter/plugin/common/BasicMessageChannel$IncomingReplyHandler;, "Lio/flutter/plugin/common/BasicMessageChannel<TT;>.IncomingReplyHandler;"
    .local p2, "callback":Lio/flutter/plugin/common/BasicMessageChannel$Reply;, "Lio/flutter/plugin/common/BasicMessageChannel$Reply<TT;>;"
    iput-object p1, p0, Lio/flutter/plugin/common/BasicMessageChannel$IncomingReplyHandler;->this$0:Lio/flutter/plugin/common/BasicMessageChannel;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    iput-object p2, p0, Lio/flutter/plugin/common/BasicMessageChannel$IncomingReplyHandler;->callback:Lio/flutter/plugin/common/BasicMessageChannel$Reply;

    .line 132
    return-void
.end method

.method synthetic constructor <init>(Lio/flutter/plugin/common/BasicMessageChannel;Lio/flutter/plugin/common/BasicMessageChannel$Reply;Lio/flutter/plugin/common/BasicMessageChannel$1;)V
    .locals 0
    .param p1, "x0"    # Lio/flutter/plugin/common/BasicMessageChannel;
    .param p2, "x1"    # Lio/flutter/plugin/common/BasicMessageChannel$Reply;
    .param p3, "x2"    # Lio/flutter/plugin/common/BasicMessageChannel$1;

    .line 127
    .local p0, "this":Lio/flutter/plugin/common/BasicMessageChannel$IncomingReplyHandler;, "Lio/flutter/plugin/common/BasicMessageChannel<TT;>.IncomingReplyHandler;"
    invoke-direct {p0, p1, p2}, Lio/flutter/plugin/common/BasicMessageChannel$IncomingReplyHandler;-><init>(Lio/flutter/plugin/common/BasicMessageChannel;Lio/flutter/plugin/common/BasicMessageChannel$Reply;)V

    return-void
.end method


# virtual methods
.method public reply(Ljava/nio/ByteBuffer;)V
    .locals 3
    .param p1, "reply"    # Ljava/nio/ByteBuffer;

    .line 137
    .local p0, "this":Lio/flutter/plugin/common/BasicMessageChannel$IncomingReplyHandler;, "Lio/flutter/plugin/common/BasicMessageChannel<TT;>.IncomingReplyHandler;"
    :try_start_0
    iget-object v0, p0, Lio/flutter/plugin/common/BasicMessageChannel$IncomingReplyHandler;->callback:Lio/flutter/plugin/common/BasicMessageChannel$Reply;

    iget-object v1, p0, Lio/flutter/plugin/common/BasicMessageChannel$IncomingReplyHandler;->this$0:Lio/flutter/plugin/common/BasicMessageChannel;

    invoke-static {v1}, Lio/flutter/plugin/common/BasicMessageChannel;->access$200(Lio/flutter/plugin/common/BasicMessageChannel;)Lio/flutter/plugin/common/MessageCodec;

    move-result-object v1

    invoke-interface {v1, p1}, Lio/flutter/plugin/common/MessageCodec;->decodeMessage(Ljava/nio/ByteBuffer;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Lio/flutter/plugin/common/BasicMessageChannel$Reply;->reply(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 140
    goto :goto_0

    .line 138
    :catch_0
    move-exception v0

    .line 139
    .local v0, "e":Ljava/lang/RuntimeException;
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "BasicMessageChannel#"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lio/flutter/plugin/common/BasicMessageChannel$IncomingReplyHandler;->this$0:Lio/flutter/plugin/common/BasicMessageChannel;

    invoke-static {v2}, Lio/flutter/plugin/common/BasicMessageChannel;->access$300(Lio/flutter/plugin/common/BasicMessageChannel;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Failed to handle message reply"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 141
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :goto_0
    return-void
.end method
