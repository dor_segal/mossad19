.class public Lio/flutter/plugin/editing/TextInputPlugin;
.super Ljava/lang/Object;
.source "TextInputPlugin.java"

# interfaces
.implements Lio/flutter/plugin/common/MethodChannel$MethodCallHandler;


# instance fields
.field private mClient:I

.field private mConfiguration:Lorg/json/JSONObject;

.field private mEditable:Landroid/text/Editable;

.field private final mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

.field private final mImm:Landroid/view/inputmethod/InputMethodManager;

.field private mRestartInputPending:Z

.field private final mView:Lio/flutter/view/FlutterView;


# direct methods
.method public constructor <init>(Lio/flutter/view/FlutterView;)V
    .locals 3
    .param p1, "view"    # Lio/flutter/view/FlutterView;

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 32
    const/4 v0, 0x0

    iput v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mClient:I

    .line 38
    iput-object p1, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mView:Lio/flutter/view/FlutterView;

    .line 39
    invoke-virtual {p1}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 41
    new-instance v0, Lio/flutter/plugin/common/MethodChannel;

    const-string v1, "flutter/textinput"

    sget-object v2, Lio/flutter/plugin/common/JSONMethodCodec;->INSTANCE:Lio/flutter/plugin/common/JSONMethodCodec;

    invoke-direct {v0, p1, v1, v2}, Lio/flutter/plugin/common/MethodChannel;-><init>(Lio/flutter/plugin/common/BinaryMessenger;Ljava/lang/String;Lio/flutter/plugin/common/MethodCodec;)V

    iput-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    .line 42
    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    invoke-virtual {v0, p0}, Lio/flutter/plugin/common/MethodChannel;->setMethodCallHandler(Lio/flutter/plugin/common/MethodChannel$MethodCallHandler;)V

    .line 43
    return-void
.end method

.method private applyStateToSelection(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "state"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 188
    const-string v0, "selectionBase"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 189
    .local v0, "selStart":I
    const-string v1, "selectionExtent"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 190
    .local v1, "selEnd":I
    if-ltz v0, :cond_0

    iget-object v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-gt v0, v2, :cond_0

    if-ltz v1, :cond_0

    iget-object v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    .line 191
    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    if-gt v1, v2, :cond_0

    .line 192
    iget-object v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    invoke-static {v2, v0, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;II)V

    goto :goto_0

    .line 194
    :cond_0
    iget-object v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->removeSelection(Landroid/text/Spannable;)V

    .line 196
    :goto_0
    return-void
.end method

.method private clearTextInputClient()V
    .locals 1

    .line 214
    const/4 v0, 0x0

    iput v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mClient:I

    .line 215
    return-void
.end method

.method private hideTextInput(Lio/flutter/view/FlutterView;)V
    .locals 3
    .param p1, "view"    # Lio/flutter/view/FlutterView;

    .line 174
    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {p1}, Lio/flutter/view/FlutterView;->getApplicationWindowToken()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 175
    return-void
.end method

.method private static inputActionFromTextInputAction(Ljava/lang/String;)I
    .locals 9
    .param p0, "inputAction"    # Ljava/lang/String;

    .line 111
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    const/4 v1, 0x7

    const/4 v2, 0x5

    const/4 v3, 0x4

    const/4 v4, 0x3

    const/4 v5, 0x2

    const/4 v6, 0x6

    const/4 v7, 0x1

    const/4 v8, 0x0

    sparse-switch v0, :sswitch_data_0

    goto :goto_0

    :sswitch_0
    const-string v0, "TextInputAction.previous"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    goto :goto_1

    :sswitch_1
    const-string v0, "TextInputAction.newline"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    goto :goto_1

    :sswitch_2
    const-string v0, "TextInputAction.go"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x4

    goto :goto_1

    :sswitch_3
    const-string v0, "TextInputAction.search"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x5

    goto :goto_1

    :sswitch_4
    const-string v0, "TextInputAction.send"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    goto :goto_1

    :sswitch_5
    const-string v0, "TextInputAction.none"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_1

    :sswitch_6
    const-string v0, "TextInputAction.next"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x7

    goto :goto_1

    :sswitch_7
    const-string v0, "TextInputAction.done"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x3

    goto :goto_1

    :sswitch_8
    const-string v0, "TextInputAction.unspecified"

    invoke-virtual {p0, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x2

    goto :goto_1

    :cond_0
    :goto_0
    const/4 v0, -0x1

    :goto_1
    packed-switch v0, :pswitch_data_0

    .line 132
    return v8

    .line 129
    :pswitch_0
    return v1

    .line 127
    :pswitch_1
    return v2

    .line 125
    :pswitch_2
    return v3

    .line 123
    :pswitch_3
    return v4

    .line 121
    :pswitch_4
    return v5

    .line 119
    :pswitch_5
    return v6

    .line 117
    :pswitch_6
    return v8

    .line 115
    :pswitch_7
    return v7

    .line 113
    :pswitch_8
    return v7

    nop

    :sswitch_data_0
    .sparse-switch
        -0x30567324 -> :sswitch_8
        -0x2bf37e83 -> :sswitch_7
        -0x2bef1712 -> :sswitch_6
        -0x2beef2cd -> :sswitch_5
        -0x2becd27d -> :sswitch_4
        0x1bf830e3 -> :sswitch_3
        0x4a02ada3 -> :sswitch_2
        0x5bc225b9 -> :sswitch_1
        0x7dcba372 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_8
        :pswitch_7
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private static inputTypeFromTextInputType(Lorg/json/JSONObject;ZZLjava/lang/String;)I
    .locals 3
    .param p0, "type"    # Lorg/json/JSONObject;
    .param p1, "obscureText"    # Z
    .param p2, "autocorrect"    # Z
    .param p3, "textCapitalization"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 76
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    .local v0, "inputType":Ljava/lang/String;
    const-string v1, "TextInputType.datetime"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x4

    return v1

    .line 78
    :cond_0
    const-string v1, "TextInputType.number"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 79
    const/4 v1, 0x2

    .line 80
    .local v1, "textType":I
    const-string v2, "signed"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    or-int/lit16 v1, v1, 0x1000

    .line 81
    :cond_1
    const-string v2, "decimal"

    invoke-virtual {p0, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    or-int/lit16 v1, v1, 0x2000

    .line 82
    :cond_2
    return v1

    .line 84
    .end local v1    # "textType":I
    :cond_3
    const-string v1, "TextInputType.phone"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    const/4 v1, 0x3

    return v1

    .line 86
    :cond_4
    const/4 v1, 0x1

    .line 87
    .restart local v1    # "textType":I
    const-string v2, "TextInputType.multiline"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 88
    const/high16 v2, 0x20000

    or-int/2addr v1, v2

    goto :goto_0

    .line 89
    :cond_5
    const-string v2, "TextInputType.emailAddress"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 90
    or-int/lit8 v1, v1, 0x20

    goto :goto_0

    .line 91
    :cond_6
    const-string v2, "TextInputType.url"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 92
    or-int/lit8 v1, v1, 0x10

    .line 93
    :cond_7
    :goto_0
    if-eqz p1, :cond_8

    .line 95
    const/high16 v2, 0x80000

    or-int/2addr v1, v2

    .line 96
    or-int/lit16 v1, v1, 0x80

    goto :goto_1

    .line 98
    :cond_8
    if-eqz p2, :cond_9

    const v2, 0x8000

    or-int/2addr v1, v2

    .line 100
    :cond_9
    :goto_1
    const-string v2, "TextCapitalization.characters"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 101
    or-int/lit16 v1, v1, 0x1000

    goto :goto_2

    .line 102
    :cond_a
    const-string v2, "TextCapitalization.words"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 103
    or-int/lit16 v1, v1, 0x2000

    goto :goto_2

    .line 104
    :cond_b
    const-string v2, "TextCapitalization.sentences"

    invoke-virtual {p3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 105
    or-int/lit16 v1, v1, 0x4000

    .line 107
    :cond_c
    :goto_2
    return v1
.end method

.method private setTextInputClient(Lio/flutter/view/FlutterView;ILorg/json/JSONObject;)V
    .locals 2
    .param p1, "view"    # Lio/flutter/view/FlutterView;
    .param p2, "client"    # I
    .param p3, "configuration"    # Lorg/json/JSONObject;

    .line 178
    iput p2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mClient:I

    .line 179
    iput-object p3, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mConfiguration:Lorg/json/JSONObject;

    .line 180
    invoke-static {}, Landroid/text/Editable$Factory;->getInstance()Landroid/text/Editable$Factory;

    move-result-object v0

    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/text/Editable$Factory;->newEditable(Ljava/lang/CharSequence;)Landroid/text/Editable;

    move-result-object v0

    iput-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    .line 184
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mRestartInputPending:Z

    .line 185
    return-void
.end method

.method private setTextInputEditingState(Lio/flutter/view/FlutterView;Lorg/json/JSONObject;)V
    .locals 8
    .param p1, "view"    # Lio/flutter/view/FlutterView;
    .param p2, "state"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 199
    iget-boolean v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mRestartInputPending:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    const-string v0, "text"

    invoke-virtual {p2, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    invoke-direct {p0, p2}, Lio/flutter/plugin/editing/TextInputPlugin;->applyStateToSelection(Lorg/json/JSONObject;)V

    .line 201
    iget-object v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mImm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v3, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mView:Lio/flutter/view/FlutterView;

    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v4

    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    .line 202
    invoke-static {v0}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v0

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v5

    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    .line 203
    invoke-static {v0}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v6

    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    .line 204
    invoke-static {v0}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v7

    .line 201
    invoke-virtual/range {v2 .. v7}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    goto :goto_0

    .line 206
    :cond_0
    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    iget-object v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    const-string v3, "text"

    invoke-virtual {p2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Landroid/text/Editable;->replace(IILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 207
    invoke-direct {p0, p2}, Lio/flutter/plugin/editing/TextInputPlugin;->applyStateToSelection(Lorg/json/JSONObject;)V

    .line 208
    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0, p1}, Landroid/view/inputmethod/InputMethodManager;->restartInput(Landroid/view/View;)V

    .line 209
    iput-boolean v1, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mRestartInputPending:Z

    .line 211
    :goto_0
    return-void
.end method

.method private showTextInput(Lio/flutter/view/FlutterView;)V
    .locals 2
    .param p1, "view"    # Lio/flutter/view/FlutterView;

    .line 170
    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mImm:Landroid/view/inputmethod/InputMethodManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 171
    return-void
.end method


# virtual methods
.method public createInputConnection(Lio/flutter/view/FlutterView;Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 6
    .param p1, "view"    # Lio/flutter/view/FlutterView;
    .param p2, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 138
    iget v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mClient:I

    if-nez v0, :cond_0

    const/4 v0, 0x0

    return-object v0

    .line 140
    :cond_0
    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mConfiguration:Lorg/json/JSONObject;

    const-string v1, "inputType"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    iget-object v1, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mConfiguration:Lorg/json/JSONObject;

    const-string v2, "obscureText"

    .line 141
    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;)Z

    move-result v1

    iget-object v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mConfiguration:Lorg/json/JSONObject;

    const-string v3, "autocorrect"

    .line 142
    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->optBoolean(Ljava/lang/String;Z)Z

    move-result v2

    iget-object v3, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mConfiguration:Lorg/json/JSONObject;

    const-string v5, "textCapitalization"

    .line 143
    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 140
    invoke-static {v0, v1, v2, v3}, Lio/flutter/plugin/editing/TextInputPlugin;->inputTypeFromTextInputType(Lorg/json/JSONObject;ZZLjava/lang/String;)I

    move-result v0

    iput v0, p2, Landroid/view/inputmethod/EditorInfo;->inputType:I

    .line 144
    const/high16 v0, 0x2000000

    iput v0, p2, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 146
    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mConfiguration:Lorg/json/JSONObject;

    const-string v1, "inputAction"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 149
    const/high16 v0, 0x20000

    iget v1, p2, Landroid/view/inputmethod/EditorInfo;->inputType:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_1

    goto :goto_0

    :cond_1
    const/4 v4, 0x6

    :goto_0
    move v0, v4

    goto :goto_1

    .line 153
    :cond_2
    iget-object v0, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mConfiguration:Lorg/json/JSONObject;

    const-string v1, "inputAction"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/flutter/plugin/editing/TextInputPlugin;->inputActionFromTextInputAction(Ljava/lang/String;)I

    move-result v0

    .line 155
    .local v0, "enterAction":I
    :goto_1
    iget-object v1, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mConfiguration:Lorg/json/JSONObject;

    const-string v2, "actionLabel"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 156
    iget-object v1, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mConfiguration:Lorg/json/JSONObject;

    const-string v2, "actionLabel"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p2, Landroid/view/inputmethod/EditorInfo;->actionLabel:Ljava/lang/CharSequence;

    .line 157
    iput v0, p2, Landroid/view/inputmethod/EditorInfo;->actionId:I

    .line 159
    :cond_3
    iget v1, p2, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    or-int/2addr v1, v0

    iput v1, p2, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    .line 161
    new-instance v1, Lio/flutter/plugin/editing/InputConnectionAdaptor;

    iget v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mClient:I

    iget-object v3, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    iget-object v4, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    invoke-direct {v1, p1, v2, v3, v4}, Lio/flutter/plugin/editing/InputConnectionAdaptor;-><init>(Lio/flutter/view/FlutterView;ILio/flutter/plugin/common/MethodChannel;Landroid/text/Editable;)V

    .line 163
    .local v1, "connection":Lio/flutter/plugin/editing/InputConnectionAdaptor;
    iget-object v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, p2, Landroid/view/inputmethod/EditorInfo;->initialSelStart:I

    .line 164
    iget-object v2, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    iput v2, p2, Landroid/view/inputmethod/EditorInfo;->initialSelEnd:I

    .line 166
    return-object v1
.end method

.method public onMethodCall(Lio/flutter/plugin/common/MethodCall;Lio/flutter/plugin/common/MethodChannel$Result;)V
    .locals 7
    .param p1, "call"    # Lio/flutter/plugin/common/MethodCall;
    .param p2, "result"    # Lio/flutter/plugin/common/MethodChannel$Result;

    .line 47
    iget-object v0, p1, Lio/flutter/plugin/common/MethodCall;->method:Ljava/lang/String;

    .line 48
    .local v0, "method":Ljava/lang/String;
    iget-object v1, p1, Lio/flutter/plugin/common/MethodCall;->arguments:Ljava/lang/Object;

    .line 50
    .local v1, "args":Ljava/lang/Object;
    const/4 v2, 0x0

    :try_start_0
    const-string v3, "TextInput.show"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 51
    iget-object v3, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mView:Lio/flutter/view/FlutterView;

    invoke-direct {p0, v3}, Lio/flutter/plugin/editing/TextInputPlugin;->showTextInput(Lio/flutter/view/FlutterView;)V

    .line 52
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 53
    :cond_0
    const-string v3, "TextInput.hide"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 54
    iget-object v3, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mView:Lio/flutter/view/FlutterView;

    invoke-direct {p0, v3}, Lio/flutter/plugin/editing/TextInputPlugin;->hideTextInput(Lio/flutter/view/FlutterView;)V

    .line 55
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 56
    :cond_1
    const-string v3, "TextInput.setClient"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 57
    move-object v3, v1

    check-cast v3, Lorg/json/JSONArray;

    .line 58
    .local v3, "argumentList":Lorg/json/JSONArray;
    iget-object v4, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mView:Lio/flutter/view/FlutterView;

    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Lorg/json/JSONArray;->getInt(I)I

    move-result v5

    const/4 v6, 0x1

    invoke-virtual {v3, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v6

    invoke-direct {p0, v4, v5, v6}, Lio/flutter/plugin/editing/TextInputPlugin;->setTextInputClient(Lio/flutter/view/FlutterView;ILorg/json/JSONObject;)V

    .line 59
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    .line 60
    .end local v3    # "argumentList":Lorg/json/JSONArray;
    goto :goto_0

    :cond_2
    const-string v3, "TextInput.setEditingState"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 61
    iget-object v3, p0, Lio/flutter/plugin/editing/TextInputPlugin;->mView:Lio/flutter/view/FlutterView;

    move-object v4, v1

    check-cast v4, Lorg/json/JSONObject;

    invoke-direct {p0, v3, v4}, Lio/flutter/plugin/editing/TextInputPlugin;->setTextInputEditingState(Lio/flutter/view/FlutterView;Lorg/json/JSONObject;)V

    .line 62
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 63
    :cond_3
    const-string v3, "TextInput.clearClient"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 64
    invoke-direct {p0}, Lio/flutter/plugin/editing/TextInputPlugin;->clearTextInputClient()V

    .line 65
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    :cond_4
    invoke-interface {p2}, Lio/flutter/plugin/common/MethodChannel$Result;->notImplemented()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    :goto_0
    goto :goto_1

    .line 69
    :catch_0
    move-exception v3

    .line 70
    .local v3, "e":Lorg/json/JSONException;
    const-string v4, "error"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "JSON error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v4, v5, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 72
    .end local v3    # "e":Lorg/json/JSONException;
    :goto_1
    return-void
.end method
