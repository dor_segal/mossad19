.class Lio/flutter/plugin/editing/InputConnectionAdaptor;
.super Landroid/view/inputmethod/BaseInputConnection;
.source "InputConnectionAdaptor.java"


# instance fields
.field private mBatchCount:I

.field private final mClient:I

.field private final mEditable:Landroid/text/Editable;

.field private final mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

.field private final mFlutterView:Lio/flutter/view/FlutterView;

.field private mImm:Landroid/view/inputmethod/InputMethodManager;


# direct methods
.method public constructor <init>(Lio/flutter/view/FlutterView;ILio/flutter/plugin/common/MethodChannel;Landroid/text/Editable;)V
    .locals 2
    .param p1, "view"    # Lio/flutter/view/FlutterView;
    .param p2, "client"    # I
    .param p3, "flutterChannel"    # Lio/flutter/plugin/common/MethodChannel;
    .param p4, "editable"    # Landroid/text/Editable;

    .line 30
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Landroid/view/inputmethod/BaseInputConnection;-><init>(Landroid/view/View;Z)V

    .line 31
    iput-object p1, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterView:Lio/flutter/view/FlutterView;

    .line 32
    iput p2, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mClient:I

    .line 33
    iput-object p3, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    .line 34
    iput-object p4, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    .line 35
    const/4 v0, 0x0

    iput v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mBatchCount:I

    .line 36
    invoke-virtual {p1}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 37
    return-void
.end method

.method private updateEditingState()V
    .locals 10

    .line 42
    iget v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mBatchCount:I

    if-lez v0, :cond_0

    .line 43
    return-void

    .line 45
    :cond_0
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    .line 46
    .local v0, "selectionStart":I
    iget-object v1, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v7

    .line 47
    .local v7, "selectionEnd":I
    iget-object v1, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanStart(Landroid/text/Spannable;)I

    move-result v8

    .line 48
    .local v8, "composingStart":I
    iget-object v1, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v1}, Landroid/view/inputmethod/BaseInputConnection;->getComposingSpanEnd(Landroid/text/Spannable;)I

    move-result v9

    .line 50
    .local v9, "composingEnd":I
    iget-object v1, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mImm:Landroid/view/inputmethod/InputMethodManager;

    iget-object v2, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterView:Lio/flutter/view/FlutterView;

    move v3, v0

    move v4, v7

    move v5, v8

    move v6, v9

    invoke-virtual/range {v1 .. v6}, Landroid/view/inputmethod/InputMethodManager;->updateSelection(Landroid/view/View;IIII)V

    .line 54
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 55
    .local v1, "state":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/Object;Ljava/lang/Object;>;"
    const-string v2, "text"

    iget-object v3, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    const-string v2, "selectionBase"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    const-string v2, "selectionExtent"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    const-string v2, "composingBase"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    const-string v2, "composingExtent"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 60
    iget-object v2, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v3, "TextInputClient.updateEditingState"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/io/Serializable;

    const/4 v5, 0x0

    iget v6, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mClient:I

    .line 61
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x1

    aput-object v1, v4, v5

    invoke-static {v4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v4

    .line 60
    invoke-virtual {v2, v3, v4}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 62
    return-void
.end method


# virtual methods
.method public beginBatchEdit()Z
    .locals 1

    .line 71
    iget v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mBatchCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mBatchCount:I

    .line 72
    invoke-super {p0}, Landroid/view/inputmethod/BaseInputConnection;->beginBatchEdit()Z

    move-result v0

    return v0
.end method

.method public commitText(Ljava/lang/CharSequence;I)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "newCursorPosition"    # I

    .line 85
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    .line 86
    .local v0, "result":Z
    invoke-direct {p0}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->updateEditingState()V

    .line 87
    return v0
.end method

.method public deleteSurroundingText(II)Z
    .locals 2
    .param p1, "beforeLength"    # I
    .param p2, "afterLength"    # I

    .line 92
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 93
    const/4 v0, 0x1

    return v0

    .line 95
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->deleteSurroundingText(II)Z

    move-result v0

    .line 96
    .local v0, "result":Z
    invoke-direct {p0}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->updateEditingState()V

    .line 97
    return v0
.end method

.method public endBatchEdit()Z
    .locals 2

    .line 77
    invoke-super {p0}, Landroid/view/inputmethod/BaseInputConnection;->endBatchEdit()Z

    move-result v0

    .line 78
    .local v0, "result":Z
    iget v1, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mBatchCount:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mBatchCount:I

    .line 79
    invoke-direct {p0}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->updateEditingState()V

    .line 80
    return v0
.end method

.method public getEditable()Landroid/text/Editable;
    .locals 1

    .line 66
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    return-object v0
.end method

.method public performEditorAction(I)Z
    .locals 6
    .param p1, "actionCode"    # I

    .line 176
    const/4 v0, 0x7

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x1

    if-eq p1, v0, :cond_0

    packed-switch p1, :pswitch_data_0

    .line 208
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v4, "TextInputClient.performAction"

    new-array v2, v2, [Ljava/io/Serializable;

    iget v5, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mClient:I

    .line 209
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    const-string v1, "TextInputAction.done"

    aput-object v1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 208
    invoke-virtual {v0, v4, v1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 199
    :pswitch_0
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v4, "TextInputClient.performAction"

    new-array v2, v2, [Ljava/io/Serializable;

    iget v5, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mClient:I

    .line 200
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    const-string v1, "TextInputAction.next"

    aput-object v1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 199
    invoke-virtual {v0, v4, v1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 201
    goto/16 :goto_0

    .line 195
    :pswitch_1
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v4, "TextInputClient.performAction"

    new-array v2, v2, [Ljava/io/Serializable;

    iget v5, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mClient:I

    .line 196
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    const-string v1, "TextInputAction.send"

    aput-object v1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 195
    invoke-virtual {v0, v4, v1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 197
    goto/16 :goto_0

    .line 191
    :pswitch_2
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v4, "TextInputClient.performAction"

    new-array v2, v2, [Ljava/io/Serializable;

    iget v5, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mClient:I

    .line 192
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    const-string v1, "TextInputAction.search"

    aput-object v1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 191
    invoke-virtual {v0, v4, v1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 193
    goto :goto_0

    .line 187
    :pswitch_3
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v4, "TextInputClient.performAction"

    new-array v2, v2, [Ljava/io/Serializable;

    iget v5, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mClient:I

    .line 188
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    const-string v1, "TextInputAction.go"

    aput-object v1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 187
    invoke-virtual {v0, v4, v1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 189
    goto :goto_0

    .line 179
    :pswitch_4
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v4, "TextInputClient.performAction"

    new-array v2, v2, [Ljava/io/Serializable;

    iget v5, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mClient:I

    .line 180
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    const-string v1, "TextInputAction.newline"

    aput-object v1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 179
    invoke-virtual {v0, v4, v1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 181
    goto :goto_0

    .line 183
    :pswitch_5
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v4, "TextInputClient.performAction"

    new-array v2, v2, [Ljava/io/Serializable;

    iget v5, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mClient:I

    .line 184
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    const-string v1, "TextInputAction.unspecified"

    aput-object v1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 183
    invoke-virtual {v0, v4, v1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 185
    goto :goto_0

    .line 203
    :cond_0
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mFlutterChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v4, "TextInputClient.performAction"

    new-array v2, v2, [Ljava/io/Serializable;

    iget v5, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mClient:I

    .line 204
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v1

    const-string v1, "TextInputAction.previous"

    aput-object v1, v2, v3

    invoke-static {v2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    .line 203
    invoke-virtual {v0, v4, v1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 205
    nop

    .line 212
    :goto_0
    return v3

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_5
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public sendKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 6
    .param p1, "event"    # Landroid/view/KeyEvent;

    .line 128
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_7

    .line 129
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v2, 0x43

    const/4 v3, 0x1

    if-ne v0, v2, :cond_2

    .line 130
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    .line 131
    .local v0, "selStart":I
    iget-object v2, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v2

    .line 132
    .local v2, "selEnd":I
    if-le v2, v0, :cond_0

    .line 134
    iget-object v1, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v1, v0}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 135
    iget-object v1, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-interface {v1, v0, v2}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 136
    invoke-direct {p0}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->updateEditingState()V

    .line 137
    return v3

    .line 138
    :cond_0
    if-lez v0, :cond_1

    .line 140
    add-int/lit8 v4, v0, -0x1

    invoke-static {v4, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 141
    .local v1, "newSel":I
    iget-object v4, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v4, v1}, Landroid/text/Selection;->setSelection(Landroid/text/Spannable;I)V

    .line 142
    iget-object v4, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-interface {v4, v1, v0}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 143
    invoke-direct {p0}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->updateEditingState()V

    .line 144
    return v3

    .line 146
    .end local v0    # "selStart":I
    .end local v1    # "newSel":I
    .end local v2    # "selEnd":I
    :cond_1
    goto :goto_0

    :cond_2
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v2, 0x15

    if-ne v0, v2, :cond_3

    .line 147
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    .line 148
    .restart local v0    # "selStart":I
    add-int/lit8 v2, v0, -0x1

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 149
    .restart local v1    # "newSel":I
    invoke-virtual {p0, v1, v1}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->setSelection(II)Z

    .line 150
    return v3

    .line 151
    .end local v0    # "selStart":I
    .end local v1    # "newSel":I
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    const/16 v2, 0x16

    if-ne v0, v2, :cond_4

    .line 152
    iget-object v0, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v0}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v0

    .line 153
    .restart local v0    # "selStart":I
    add-int/lit8 v1, v0, 0x1

    iget-object v2, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-interface {v2}, Landroid/text/Editable;->length()I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 154
    .restart local v1    # "newSel":I
    invoke-virtual {p0, v1, v1}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->setSelection(II)Z

    .line 155
    return v3

    .line 158
    .end local v0    # "selStart":I
    .end local v1    # "newSel":I
    :cond_4
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v0

    .line 159
    .local v0, "character":I
    if-eqz v0, :cond_6

    .line 160
    iget-object v2, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v2}, Landroid/text/Selection;->getSelectionStart(Ljava/lang/CharSequence;)I

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 161
    .local v2, "selStart":I
    iget-object v4, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-static {v4}, Landroid/text/Selection;->getSelectionEnd(Ljava/lang/CharSequence;)I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 162
    .local v1, "selEnd":I
    if-eq v1, v2, :cond_5

    .line 163
    iget-object v4, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    invoke-interface {v4, v2, v1}, Landroid/text/Editable;->delete(II)Landroid/text/Editable;

    .line 164
    :cond_5
    iget-object v4, p0, Lio/flutter/plugin/editing/InputConnectionAdaptor;->mEditable:Landroid/text/Editable;

    int-to-char v5, v0

    invoke-static {v5}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v2, v5}, Landroid/text/Editable;->insert(ILjava/lang/CharSequence;)Landroid/text/Editable;

    .line 165
    add-int/lit8 v4, v2, 0x1

    add-int/lit8 v5, v2, 0x1

    invoke-virtual {p0, v4, v5}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->setSelection(II)Z

    .line 166
    invoke-direct {p0}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->updateEditingState()V

    .line 168
    .end local v1    # "selEnd":I
    .end local v2    # "selStart":I
    :cond_6
    return v3

    .line 171
    .end local v0    # "character":I
    :cond_7
    :goto_0
    return v1
.end method

.method public setComposingRegion(II)Z
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 102
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingRegion(II)Z

    move-result v0

    .line 103
    .local v0, "result":Z
    invoke-direct {p0}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->updateEditingState()V

    .line 104
    return v0
.end method

.method public setComposingText(Ljava/lang/CharSequence;I)Z
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;
    .param p2, "newCursorPosition"    # I

    .line 110
    invoke-interface {p1}, Ljava/lang/CharSequence;->length()I

    move-result v0

    if-nez v0, :cond_0

    .line 111
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->commitText(Ljava/lang/CharSequence;I)Z

    move-result v0

    goto :goto_0

    .line 113
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setComposingText(Ljava/lang/CharSequence;I)Z

    move-result v0

    .line 115
    .local v0, "result":Z
    :goto_0
    invoke-direct {p0}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->updateEditingState()V

    .line 116
    return v0
.end method

.method public setSelection(II)Z
    .locals 1
    .param p1, "start"    # I
    .param p2, "end"    # I

    .line 121
    invoke-super {p0, p1, p2}, Landroid/view/inputmethod/BaseInputConnection;->setSelection(II)Z

    move-result v0

    .line 122
    .local v0, "result":Z
    invoke-direct {p0}, Lio/flutter/plugin/editing/InputConnectionAdaptor;->updateEditingState()V

    .line 123
    return v0
.end method
