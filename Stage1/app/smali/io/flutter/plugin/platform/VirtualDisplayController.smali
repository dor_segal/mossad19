.class Lio/flutter/plugin/platform/VirtualDisplayController;
.super Ljava/lang/Object;
.source "VirtualDisplayController.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x14
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/flutter/plugin/platform/VirtualDisplayController$OneTimeOnDrawListener;
    }
.end annotation


# instance fields
.field private final mContext:Landroid/content/Context;

.field private final mDensityDpi:I

.field private mPresentation:Lio/flutter/plugin/platform/SingleViewPresentation;

.field private mSurface:Landroid/view/Surface;

.field private final mTextureEntry:Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;

.field private mVirtualDisplay:Landroid/hardware/display/VirtualDisplay;


# direct methods
.method private constructor <init>(Landroid/content/Context;Landroid/hardware/display/VirtualDisplay;Lio/flutter/plugin/platform/PlatformViewFactory;Landroid/view/Surface;Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;ILjava/lang/Object;)V
    .locals 7
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "virtualDisplay"    # Landroid/hardware/display/VirtualDisplay;
    .param p3, "viewFactory"    # Lio/flutter/plugin/platform/PlatformViewFactory;
    .param p4, "surface"    # Landroid/view/Surface;
    .param p5, "textureEntry"    # Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;
    .param p6, "viewId"    # I
    .param p7, "createParams"    # Ljava/lang/Object;

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput-object p5, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mTextureEntry:Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;

    .line 69
    iput-object p4, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mSurface:Landroid/view/Surface;

    .line 70
    iput-object p1, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mContext:Landroid/content/Context;

    .line 71
    iput-object p2, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mVirtualDisplay:Landroid/hardware/display/VirtualDisplay;

    .line 72
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v0

    iget v0, v0, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v0, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mDensityDpi:I

    .line 73
    new-instance v0, Lio/flutter/plugin/platform/SingleViewPresentation;

    iget-object v1, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mVirtualDisplay:Landroid/hardware/display/VirtualDisplay;

    .line 74
    invoke-virtual {v1}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v3

    move-object v1, v0

    move-object v2, p1

    move-object v4, p3

    move v5, p6

    move-object v6, p7

    invoke-direct/range {v1 .. v6}, Lio/flutter/plugin/platform/SingleViewPresentation;-><init>(Landroid/content/Context;Landroid/view/Display;Lio/flutter/plugin/platform/PlatformViewFactory;ILjava/lang/Object;)V

    iput-object v0, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mPresentation:Lio/flutter/plugin/platform/SingleViewPresentation;

    .line 75
    iget-object v0, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mPresentation:Lio/flutter/plugin/platform/SingleViewPresentation;

    invoke-virtual {v0}, Lio/flutter/plugin/platform/SingleViewPresentation;->show()V

    .line 76
    return-void
.end method

.method public static create(Landroid/content/Context;Lio/flutter/plugin/platform/PlatformViewFactory;Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;IIILjava/lang/Object;)Lio/flutter/plugin/platform/VirtualDisplayController;
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "viewFactory"    # Lio/flutter/plugin/platform/PlatformViewFactory;
    .param p2, "textureEntry"    # Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "viewId"    # I
    .param p6, "createParams"    # Ljava/lang/Object;

    .line 29
    invoke-interface/range {p2 .. p2}, Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;->surfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    move/from16 v8, p3

    move/from16 v9, p4

    invoke-virtual {v0, v8, v9}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 30
    new-instance v6, Landroid/view/Surface;

    invoke-interface/range {p2 .. p2}, Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;->surfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    .line 31
    .local v6, "surface":Landroid/view/Surface;
    const-string v0, "display"

    move-object/from16 v15, p0

    invoke-virtual {v15, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/display/DisplayManager;

    .line 33
    .local v0, "displayManager":Landroid/hardware/display/DisplayManager;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v14, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    .line 34
    .local v14, "densityDpi":I
    const-string v2, "flutter-vd"

    const/4 v7, 0x0

    move-object v1, v0

    move/from16 v3, p3

    move/from16 v4, p4

    move v5, v14

    invoke-virtual/range {v1 .. v7}, Landroid/hardware/display/DisplayManager;->createVirtualDisplay(Ljava/lang/String;IIILandroid/view/Surface;I)Landroid/hardware/display/VirtualDisplay;

    move-result-object v1

    .line 43
    .local v1, "virtualDisplay":Landroid/hardware/display/VirtualDisplay;
    if-nez v1, :cond_0

    .line 44
    const/4 v2, 0x0

    return-object v2

    .line 47
    :cond_0
    new-instance v2, Lio/flutter/plugin/platform/VirtualDisplayController;

    move-object v10, v2

    move-object/from16 v11, p0

    move-object v12, v1

    move-object/from16 v13, p1

    move v3, v14

    .end local v14    # "densityDpi":I
    .local v3, "densityDpi":I
    move-object v14, v6

    move-object/from16 v15, p2

    move/from16 v16, p5

    move-object/from16 v17, p6

    invoke-direct/range {v10 .. v17}, Lio/flutter/plugin/platform/VirtualDisplayController;-><init>(Landroid/content/Context;Landroid/hardware/display/VirtualDisplay;Lio/flutter/plugin/platform/PlatformViewFactory;Landroid/view/Surface;Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;ILjava/lang/Object;)V

    return-object v2
.end method


# virtual methods
.method public dispose()V
    .locals 2

    .line 129
    iget-object v0, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mPresentation:Lio/flutter/plugin/platform/SingleViewPresentation;

    invoke-virtual {v0}, Lio/flutter/plugin/platform/SingleViewPresentation;->getView()Lio/flutter/plugin/platform/PlatformView;

    move-result-object v0

    .line 130
    .local v0, "view":Lio/flutter/plugin/platform/PlatformView;
    iget-object v1, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mPresentation:Lio/flutter/plugin/platform/SingleViewPresentation;

    invoke-virtual {v1}, Lio/flutter/plugin/platform/SingleViewPresentation;->detachState()Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    .line 131
    invoke-interface {v0}, Lio/flutter/plugin/platform/PlatformView;->dispose()V

    .line 132
    iget-object v1, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mVirtualDisplay:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v1}, Landroid/hardware/display/VirtualDisplay;->release()V

    .line 133
    iget-object v1, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mTextureEntry:Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;

    invoke-interface {v1}, Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;->release()V

    .line 134
    return-void
.end method

.method public getView()Landroid/view/View;
    .locals 2

    .line 137
    iget-object v0, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mPresentation:Lio/flutter/plugin/platform/SingleViewPresentation;

    if-nez v0, :cond_0

    .line 138
    const/4 v0, 0x0

    return-object v0

    .line 139
    :cond_0
    iget-object v0, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mPresentation:Lio/flutter/plugin/platform/SingleViewPresentation;

    invoke-virtual {v0}, Lio/flutter/plugin/platform/SingleViewPresentation;->getView()Lio/flutter/plugin/platform/PlatformView;

    move-result-object v0

    .line 140
    .local v0, "platformView":Lio/flutter/plugin/platform/PlatformView;
    invoke-interface {v0}, Lio/flutter/plugin/platform/PlatformView;->getView()Landroid/view/View;

    move-result-object v1

    return-object v1
.end method

.method public resize(IILjava/lang/Runnable;)V
    .locals 9
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "onNewSizeFrameAvailable"    # Ljava/lang/Runnable;

    .line 79
    iget-object v0, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mPresentation:Lio/flutter/plugin/platform/SingleViewPresentation;

    invoke-virtual {v0}, Lio/flutter/plugin/platform/SingleViewPresentation;->detachState()Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    move-result-object v0

    .line 85
    .local v0, "presentationState":Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;
    iget-object v1, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mVirtualDisplay:Landroid/hardware/display/VirtualDisplay;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/hardware/display/VirtualDisplay;->setSurface(Landroid/view/Surface;)V

    .line 86
    iget-object v1, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mVirtualDisplay:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v1}, Landroid/hardware/display/VirtualDisplay;->release()V

    .line 88
    iget-object v1, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mTextureEntry:Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;

    invoke-interface {v1}, Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;->surfaceTexture()Landroid/graphics/SurfaceTexture;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Landroid/graphics/SurfaceTexture;->setDefaultBufferSize(II)V

    .line 89
    iget-object v1, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mContext:Landroid/content/Context;

    const-string v2, "display"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    .line 90
    .local v1, "displayManager":Landroid/hardware/display/DisplayManager;
    const-string v3, "flutter-vd"

    iget v6, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mDensityDpi:I

    iget-object v7, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mSurface:Landroid/view/Surface;

    const/4 v8, 0x0

    move-object v2, v1

    move v4, p1

    move v5, p2

    invoke-virtual/range {v2 .. v8}, Landroid/hardware/display/DisplayManager;->createVirtualDisplay(Ljava/lang/String;IIILandroid/view/Surface;I)Landroid/hardware/display/VirtualDisplay;

    move-result-object v2

    iput-object v2, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mVirtualDisplay:Landroid/hardware/display/VirtualDisplay;

    .line 99
    invoke-virtual {p0}, Lio/flutter/plugin/platform/VirtualDisplayController;->getView()Landroid/view/View;

    move-result-object v2

    .line 102
    .local v2, "embeddedView":Landroid/view/View;
    new-instance v3, Lio/flutter/plugin/platform/VirtualDisplayController$1;

    invoke-direct {v3, p0, v2, p3}, Lio/flutter/plugin/platform/VirtualDisplayController$1;-><init>(Lio/flutter/plugin/platform/VirtualDisplayController;Landroid/view/View;Ljava/lang/Runnable;)V

    invoke-virtual {v2, v3}, Landroid/view/View;->addOnAttachStateChangeListener(Landroid/view/View$OnAttachStateChangeListener;)V

    .line 124
    new-instance v3, Lio/flutter/plugin/platform/SingleViewPresentation;

    iget-object v4, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mContext:Landroid/content/Context;

    iget-object v5, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mVirtualDisplay:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v5}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v5

    invoke-direct {v3, v4, v5, v0}, Lio/flutter/plugin/platform/SingleViewPresentation;-><init>(Landroid/content/Context;Landroid/view/Display;Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)V

    iput-object v3, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mPresentation:Lio/flutter/plugin/platform/SingleViewPresentation;

    .line 125
    iget-object v3, p0, Lio/flutter/plugin/platform/VirtualDisplayController;->mPresentation:Lio/flutter/plugin/platform/SingleViewPresentation;

    invoke-virtual {v3}, Lio/flutter/plugin/platform/SingleViewPresentation;->show()V

    .line 126
    return-void
.end method
