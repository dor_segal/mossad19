.class Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;
.super Ljava/lang/Object;
.source "SingleViewPresentation.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/flutter/plugin/platform/SingleViewPresentation;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "PresentationState"
.end annotation


# instance fields
.field private mFakeWindowRootView:Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;

.field private mView:Lio/flutter/plugin/platform/PlatformView;

.field private mWindowManagerHandler:Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;


# direct methods
.method constructor <init>()V
    .locals 0

    .line 46
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;
    .locals 1
    .param p0, "x0"    # Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    .line 46
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->mFakeWindowRootView:Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;

    return-object v0
.end method

.method static synthetic access$002(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;)Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;
    .locals 0
    .param p0, "x0"    # Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;
    .param p1, "x1"    # Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;

    .line 46
    iput-object p1, p0, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->mFakeWindowRootView:Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;

    return-object p1
.end method

.method static synthetic access$100(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;
    .locals 1
    .param p0, "x0"    # Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    .line 46
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->mWindowManagerHandler:Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;

    return-object v0
.end method

.method static synthetic access$102(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;)Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;
    .locals 0
    .param p0, "x0"    # Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;
    .param p1, "x1"    # Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;

    .line 46
    iput-object p1, p0, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->mWindowManagerHandler:Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;

    return-object p1
.end method

.method static synthetic access$200(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/PlatformView;
    .locals 1
    .param p0, "x0"    # Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    .line 46
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->mView:Lio/flutter/plugin/platform/PlatformView;

    return-object v0
.end method

.method static synthetic access$202(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;Lio/flutter/plugin/platform/PlatformView;)Lio/flutter/plugin/platform/PlatformView;
    .locals 0
    .param p0, "x0"    # Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;
    .param p1, "x1"    # Lio/flutter/plugin/platform/PlatformView;

    .line 46
    iput-object p1, p0, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->mView:Lio/flutter/plugin/platform/PlatformView;

    return-object p1
.end method
