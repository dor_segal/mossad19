.class Lio/flutter/plugin/platform/SingleViewPresentation;
.super Landroid/app/Presentation;
.source "SingleViewPresentation.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x11
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;,
        Lio/flutter/plugin/platform/SingleViewPresentation$PresentationContext;,
        Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;,
        Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;
    }
.end annotation


# instance fields
.field private mContainer:Landroid/widget/FrameLayout;

.field private mCreateParams:Ljava/lang/Object;

.field private mRootView:Landroid/widget/FrameLayout;

.field private mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

.field private final mViewFactory:Lio/flutter/plugin/platform/PlatformViewFactory;

.field private mViewId:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/view/Display;Lio/flutter/plugin/platform/PlatformViewFactory;ILjava/lang/Object;)V
    .locals 2
    .param p1, "outerContext"    # Landroid/content/Context;
    .param p2, "display"    # Landroid/view/Display;
    .param p3, "viewFactory"    # Lio/flutter/plugin/platform/PlatformViewFactory;
    .param p4, "viewId"    # I
    .param p5, "createParams"    # Ljava/lang/Object;

    .line 87
    invoke-direct {p0, p1, p2}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    .line 88
    iput-object p3, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mViewFactory:Lio/flutter/plugin/platform/PlatformViewFactory;

    .line 89
    iput p4, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mViewId:I

    .line 90
    iput-object p5, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mCreateParams:Ljava/lang/Object;

    .line 91
    new-instance v0, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    invoke-direct {v0}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;-><init>()V

    iput-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    .line 92
    invoke-virtual {p0}, Lio/flutter/plugin/platform/SingleViewPresentation;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 96
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/view/Display;Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)V
    .locals 2
    .param p1, "outerContext"    # Landroid/content/Context;
    .param p2, "display"    # Landroid/view/Display;
    .param p3, "state"    # Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    .line 106
    invoke-direct {p0, p1, p2}, Landroid/app/Presentation;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mViewFactory:Lio/flutter/plugin/platform/PlatformViewFactory;

    .line 108
    iput-object p3, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    .line 109
    invoke-virtual {p0}, Lio/flutter/plugin/platform/SingleViewPresentation;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/16 v1, 0x8

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 113
    return-void
.end method


# virtual methods
.method public detachState()Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;
    .locals 1

    .line 141
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 142
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mRootView:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 143
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    return-object v0
.end method

.method public getView()Lio/flutter/plugin/platform/PlatformView;
    .locals 1

    .line 147
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    invoke-static {v0}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$200(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/PlatformView;

    move-result-object v0

    if-nez v0, :cond_0

    .line 148
    const/4 v0, 0x0

    return-object v0

    .line 149
    :cond_0
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    invoke-static {v0}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$200(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/PlatformView;

    move-result-object v0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .line 117
    invoke-super {p0, p1}, Landroid/app/Presentation;->onCreate(Landroid/os/Bundle;)V

    .line 118
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    invoke-static {v0}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$000(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;

    move-result-object v0

    if-nez v0, :cond_0

    .line 119
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    new-instance v1, Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;

    invoke-virtual {p0}, Lio/flutter/plugin/platform/SingleViewPresentation;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;-><init>(Landroid/content/Context;)V

    invoke-static {v0, v1}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$002(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;)Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;

    .line 121
    :cond_0
    iget-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    invoke-static {v0}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$100(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;

    move-result-object v0

    if-nez v0, :cond_1

    .line 122
    invoke-virtual {p0}, Lio/flutter/plugin/platform/SingleViewPresentation;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 123
    .local v0, "windowManagerDelegate":Landroid/view/WindowManager;
    iget-object v1, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    new-instance v2, Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;

    iget-object v3, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    invoke-static {v3}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$000(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;-><init>(Landroid/view/WindowManager;Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;)V

    invoke-static {v1, v2}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$102(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;)Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;

    .line 126
    .end local v0    # "windowManagerDelegate":Landroid/view/WindowManager;
    :cond_1
    new-instance v0, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lio/flutter/plugin/platform/SingleViewPresentation;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mContainer:Landroid/widget/FrameLayout;

    .line 127
    new-instance v0, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationContext;

    invoke-virtual {p0}, Lio/flutter/plugin/platform/SingleViewPresentation;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    invoke-static {v2}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$100(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationContext;-><init>(Landroid/content/Context;Lio/flutter/plugin/platform/SingleViewPresentation$WindowManagerHandler;)V

    .line 129
    .local v0, "context":Lio/flutter/plugin/platform/SingleViewPresentation$PresentationContext;
    iget-object v1, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    invoke-static {v1}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$200(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/PlatformView;

    move-result-object v1

    if-nez v1, :cond_2

    .line 130
    iget-object v1, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    iget-object v2, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mViewFactory:Lio/flutter/plugin/platform/PlatformViewFactory;

    iget v3, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mViewId:I

    iget-object v4, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mCreateParams:Ljava/lang/Object;

    invoke-virtual {v2, v0, v3, v4}, Lio/flutter/plugin/platform/PlatformViewFactory;->create(Landroid/content/Context;ILjava/lang/Object;)Lio/flutter/plugin/platform/PlatformView;

    move-result-object v2

    invoke-static {v1, v2}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$202(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;Lio/flutter/plugin/platform/PlatformView;)Lio/flutter/plugin/platform/PlatformView;

    .line 133
    :cond_2
    iget-object v1, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mContainer:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    invoke-static {v2}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$200(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/PlatformView;

    move-result-object v2

    invoke-interface {v2}, Lio/flutter/plugin/platform/PlatformView;->getView()Landroid/view/View;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 134
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lio/flutter/plugin/platform/SingleViewPresentation;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mRootView:Landroid/widget/FrameLayout;

    .line 135
    iget-object v1, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mRootView:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mContainer:Landroid/widget/FrameLayout;

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 136
    iget-object v1, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mRootView:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mState:Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;

    invoke-static {v2}, Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;->access$000(Lio/flutter/plugin/platform/SingleViewPresentation$PresentationState;)Lio/flutter/plugin/platform/SingleViewPresentation$FakeWindowViewGroup;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 137
    iget-object v1, p0, Lio/flutter/plugin/platform/SingleViewPresentation;->mRootView:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v1}, Lio/flutter/plugin/platform/SingleViewPresentation;->setContentView(Landroid/view/View;)V

    .line 138
    return-void
.end method
