.class public Lio/flutter/plugin/platform/PlatformPlugin;
.super Ljava/lang/Object;
.source "PlatformPlugin.java"

# interfaces
.implements Lio/flutter/plugin/common/MethodChannel$MethodCallHandler;
.implements Lio/flutter/plugin/common/ActivityLifecycleListener;


# static fields
.field public static final DEFAULT_SYSTEM_UI:I = 0x500

.field private static final kTextPlainFormat:Ljava/lang/String; = "text/plain"


# instance fields
.field private final mActivity:Landroid/app/Activity;

.field private mCurrentTheme:Lorg/json/JSONObject;

.field private mEnabledOverlays:I


# direct methods
.method public constructor <init>(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput-object p1, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    .line 38
    const/16 v0, 0x500

    iput v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mEnabledOverlays:I

    .line 39
    return-void
.end method

.method private getClipboardData(Ljava/lang/String;)Lorg/json/JSONObject;
    .locals 6
    .param p1, "format"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 294
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 295
    .local v0, "clipboard":Landroid/content/ClipboardManager;
    invoke-virtual {v0}, Landroid/content/ClipboardManager;->getPrimaryClip()Landroid/content/ClipData;

    move-result-object v1

    .line 296
    .local v1, "clip":Landroid/content/ClipData;
    const/4 v2, 0x0

    if-nez v1, :cond_0

    .line 297
    return-object v2

    .line 299
    :cond_0
    if-eqz p1, :cond_2

    const-string v3, "text/plain"

    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    goto :goto_0

    .line 305
    :cond_1
    return-object v2

    .line 300
    :cond_2
    :goto_0
    new-instance v2, Lorg/json/JSONObject;

    invoke-direct {v2}, Lorg/json/JSONObject;-><init>()V

    .line 301
    .local v2, "result":Lorg/json/JSONObject;
    const-string v3, "text"

    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/content/ClipData;->getItemAt(I)Landroid/content/ClipData$Item;

    move-result-object v4

    iget-object v5, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v4, v5}, Landroid/content/ClipData$Item;->coerceToText(Landroid/content/Context;)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 302
    return-object v2
.end method

.method private playSystemSound(Ljava/lang/String;)V
    .locals 2
    .param p1, "soundType"    # Ljava/lang/String;

    .line 84
    const-string v0, "SystemSoundType.click"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 86
    .local v0, "view":Landroid/view/View;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->playSoundEffect(I)V

    .line 88
    .end local v0    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method private popSystemNavigator()V
    .locals 1

    .line 290
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 291
    return-void
.end method

.method private restoreSystemChromeSystemUIOverlays()V
    .locals 0

    .line 231
    invoke-direct {p0}, Lio/flutter/plugin/platform/PlatformPlugin;->updateSystemUiOverlays()V

    .line 232
    return-void
.end method

.method private setClipboardData(Lorg/json/JSONObject;)V
    .locals 3
    .param p1, "data"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 309
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    const-string v1, "clipboard"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ClipboardManager;

    .line 310
    .local v0, "clipboard":Landroid/content/ClipboardManager;
    const-string v1, "text label?"

    const-string v2, "text"

    invoke-virtual {p1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/content/ClipData;->newPlainText(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Landroid/content/ClipData;

    move-result-object v1

    .line 311
    .local v1, "clip":Landroid/content/ClipData;
    invoke-virtual {v0, v1}, Landroid/content/ClipboardManager;->setPrimaryClip(Landroid/content/ClipData;)V

    .line 312
    return-void
.end method

.method private setSystemChromeApplicationSwitcherDescription(Lorg/json/JSONObject;)V
    .locals 5
    .param p1, "description"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 179
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 180
    return-void

    .line 183
    :cond_0
    const-string v0, "primaryColor"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 184
    .local v0, "color":I
    if-eqz v0, :cond_1

    .line 185
    const/high16 v1, -0x1000000

    or-int/2addr v0, v1

    .line 188
    :cond_1
    iget-object v1, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    new-instance v2, Landroid/app/ActivityManager$TaskDescription;

    const-string v3, "label"

    .line 190
    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v2, v3, v4, v0}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;I)V

    .line 188
    invoke-virtual {v1, v2}, Landroid/app/Activity;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 195
    return-void
.end method

.method private setSystemChromeEnabledSystemUIOverlays(Lorg/json/JSONArray;)V
    .locals 4
    .param p1, "overlays"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 200
    const/16 v0, 0x706

    .line 205
    .local v0, "enabledOverlays":I
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-nez v1, :cond_0

    .line 206
    or-int/lit16 v0, v0, 0x1000

    .line 209
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v1, v2, :cond_3

    .line 210
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 211
    .local v2, "overlay":Ljava/lang/String;
    const-string v3, "SystemUiOverlay.top"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 212
    and-int/lit8 v0, v0, -0x5

    goto :goto_1

    .line 213
    :cond_1
    const-string v3, "SystemUiOverlay.bottom"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 214
    and-int/lit16 v0, v0, -0x201

    .line 215
    and-int/lit8 v0, v0, -0x3

    .line 209
    .end local v2    # "overlay":Ljava/lang/String;
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 219
    .end local v1    # "i":I
    :cond_3
    iput v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mEnabledOverlays:I

    .line 220
    invoke-direct {p0}, Lio/flutter/plugin/platform/PlatformPlugin;->updateSystemUiOverlays()V

    .line 221
    return-void
.end method

.method private setSystemChromePreferredOrientations(Lorg/json/JSONArray;)V
    .locals 7
    .param p1, "orientations"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .line 107
    const/4 v0, 0x0

    .line 108
    .local v0, "requestedOrientation":I
    const/4 v1, 0x0

    .line 109
    .local v1, "firstRequestedOrientation":I
    const/4 v2, 0x0

    move v3, v1

    move v1, v0

    const/4 v0, 0x0

    .local v0, "index":I
    .local v1, "requestedOrientation":I
    .local v3, "firstRequestedOrientation":I
    :goto_0
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v0, v4, :cond_5

    .line 110
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "DeviceOrientation.portraitUp"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 111
    or-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 112
    :cond_0
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "DeviceOrientation.landscapeLeft"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 113
    or-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 114
    :cond_1
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "DeviceOrientation.portraitDown"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 115
    or-int/lit8 v1, v1, 0x4

    goto :goto_1

    .line 116
    :cond_2
    invoke-virtual {p1, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v5, "DeviceOrientation.landscapeRight"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 117
    or-int/lit8 v1, v1, 0x8

    .line 119
    :cond_3
    :goto_1
    if-nez v3, :cond_4

    .line 120
    move v3, v1

    .line 109
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 123
    .end local v0    # "index":I
    :cond_5
    const/16 v0, 0x9

    const/16 v4, 0x8

    const/4 v5, 0x1

    packed-switch v1, :pswitch_data_0

    goto :goto_2

    .line 149
    :pswitch_0
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    const/16 v2, 0xd

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 150
    goto :goto_2

    .line 146
    :pswitch_1
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 147
    goto :goto_2

    .line 143
    :pswitch_2
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    const/16 v2, 0xb

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 144
    goto :goto_2

    .line 140
    :pswitch_3
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 141
    goto :goto_2

    .line 137
    :pswitch_4
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    const/16 v2, 0xc

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 138
    goto :goto_2

    .line 134
    :pswitch_5
    iget-object v2, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 135
    goto :goto_2

    .line 160
    :pswitch_6
    const/4 v6, 0x4

    if-eq v3, v6, :cond_7

    if-eq v3, v4, :cond_6

    packed-switch v3, :pswitch_data_1

    goto :goto_2

    .line 165
    :pswitch_7
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 166
    goto :goto_2

    .line 162
    :pswitch_8
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 163
    goto :goto_2

    .line 171
    :cond_6
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v4}, Landroid/app/Activity;->setRequestedOrientation(I)V

    goto :goto_2

    .line 168
    :cond_7
    iget-object v2, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v2, v0}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 169
    goto :goto_2

    .line 131
    :pswitch_9
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 132
    goto :goto_2

    .line 128
    :pswitch_a
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0, v5}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 129
    goto :goto_2

    .line 125
    :pswitch_b
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    const/4 v2, -0x1

    invoke-virtual {v0, v2}, Landroid/app/Activity;->setRequestedOrientation(I)V

    .line 126
    nop

    .line 176
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_b
        :pswitch_a
        :pswitch_9
        :pswitch_6
        :pswitch_5
        :pswitch_4
        :pswitch_6
        :pswitch_6
        :pswitch_3
        :pswitch_6
        :pswitch_2
        :pswitch_1
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_8
        :pswitch_7
    .end packed-switch
.end method

.method private setSystemChromeSystemUIOverlayStyle(Lorg/json/JSONObject;)V
    .locals 10
    .param p1, "message"    # Lorg/json/JSONObject;

    .line 235
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    .line 236
    .local v0, "window":Landroid/view/Window;
    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    .line 237
    .local v1, "view":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getSystemUiVisibility()I

    move-result v2

    .line 243
    .local v2, "flags":I
    :try_start_0
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x1a

    const/4 v5, 0x0

    const/4 v6, 0x1

    const v7, 0x155bbed3

    const v8, -0x696faea7

    const/4 v9, -0x1

    if-lt v3, v4, :cond_4

    .line 244
    const-string v3, "systemNavigationBarIconBrightness"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_3

    .line 245
    const-string v3, "systemNavigationBarIconBrightness"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 246
    .local v3, "systemNavigationBarIconBrightness":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    if-eq v4, v8, :cond_1

    if-eq v4, v7, :cond_0

    goto :goto_0

    :cond_0
    const-string v4, "Brightness.dark"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x0

    goto :goto_1

    :cond_1
    const-string v4, "Brightness.light"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    goto :goto_1

    :cond_2
    :goto_0
    const/4 v4, -0x1

    :goto_1
    packed-switch v4, :pswitch_data_0

    .end local v3    # "systemNavigationBarIconBrightness":Ljava/lang/String;
    goto :goto_2

    .line 252
    .restart local v3    # "systemNavigationBarIconBrightness":Ljava/lang/String;
    :pswitch_0
    and-int/lit8 v2, v2, -0x11

    goto :goto_2

    .line 249
    :pswitch_1
    or-int/lit8 v2, v2, 0x10

    .line 250
    nop

    .line 256
    .end local v3    # "systemNavigationBarIconBrightness":Ljava/lang/String;
    :cond_3
    :goto_2
    const-string v3, "systemNavigationBarColor"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_4

    .line 257
    const-string v3, "systemNavigationBarColor"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/Window;->setNavigationBarColor(I)V

    .line 261
    :cond_4
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-lt v3, v4, :cond_9

    .line 262
    const-string v3, "statusBarIconBrightness"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_8

    .line 263
    const-string v3, "statusBarIconBrightness"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 264
    .local v3, "statusBarIconBrightness":Ljava/lang/String;
    invoke-virtual {v3}, Ljava/lang/String;->hashCode()I

    move-result v4

    if-eq v4, v8, :cond_6

    if-eq v4, v7, :cond_5

    goto :goto_3

    :cond_5
    const-string v4, "Brightness.dark"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    goto :goto_4

    :cond_6
    const-string v4, "Brightness.light"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v5, 0x1

    goto :goto_4

    :cond_7
    :goto_3
    const/4 v5, -0x1

    :goto_4
    packed-switch v5, :pswitch_data_1

    .end local v3    # "statusBarIconBrightness":Ljava/lang/String;
    goto :goto_5

    .line 270
    .restart local v3    # "statusBarIconBrightness":Ljava/lang/String;
    :pswitch_2
    and-int/lit16 v2, v2, -0x2001

    goto :goto_5

    .line 267
    :pswitch_3
    or-int/lit16 v2, v2, 0x2000

    .line 268
    nop

    .line 274
    .end local v3    # "statusBarIconBrightness":Ljava/lang/String;
    :cond_8
    :goto_5
    const-string v3, "statusBarColor"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 275
    const-string v3, "statusBarColor"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getInt(Ljava/lang/String;)I

    move-result v3

    invoke-virtual {v0, v3}, Landroid/view/Window;->setStatusBarColor(I)V

    .line 278
    :cond_9
    const-string v3, "systemNavigationBarDividerColor"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->isNull(Ljava/lang/String;)Z

    .line 282
    invoke-virtual {v1, v2}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 283
    iput-object p1, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mCurrentTheme:Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 286
    goto :goto_6

    .line 284
    :catch_0
    move-exception v3

    .line 285
    .local v3, "err":Lorg/json/JSONException;
    const-string v4, "PlatformPlugin"

    invoke-virtual {v3}, Lorg/json/JSONException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 287
    .end local v3    # "err":Lorg/json/JSONException;
    :goto_6
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method private updateSystemUiOverlays()V
    .locals 2

    .line 224
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mEnabledOverlays:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    .line 225
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mCurrentTheme:Lorg/json/JSONObject;

    if-eqz v0, :cond_0

    .line 226
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mCurrentTheme:Lorg/json/JSONObject;

    invoke-direct {p0, v0}, Lio/flutter/plugin/platform/PlatformPlugin;->setSystemChromeSystemUIOverlayStyle(Lorg/json/JSONObject;)V

    .line 228
    :cond_0
    return-void
.end method

.method private vibrateHapticFeedback(Ljava/lang/String;)V
    .locals 2
    .param p1, "feedbackType"    # Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lio/flutter/plugin/platform/PlatformPlugin;->mActivity:Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    .line 92
    .local v0, "view":Landroid/view/View;
    if-nez p1, :cond_0

    .line 93
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    goto :goto_0

    .line 94
    :cond_0
    const-string v1, "HapticFeedbackType.lightImpact"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 95
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    goto :goto_0

    .line 96
    :cond_1
    const-string v1, "HapticFeedbackType.mediumImpact"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 97
    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    goto :goto_0

    .line 98
    :cond_2
    const-string v1, "HapticFeedbackType.heavyImpact"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 100
    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    goto :goto_0

    .line 101
    :cond_3
    const-string v1, "HapticFeedbackType.selectionClick"

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 102
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->performHapticFeedback(I)Z

    .line 104
    :cond_4
    :goto_0
    return-void
.end method


# virtual methods
.method public onMethodCall(Lio/flutter/plugin/common/MethodCall;Lio/flutter/plugin/common/MethodChannel$Result;)V
    .locals 7
    .param p1, "call"    # Lio/flutter/plugin/common/MethodCall;
    .param p2, "result"    # Lio/flutter/plugin/common/MethodChannel$Result;

    .line 43
    iget-object v0, p1, Lio/flutter/plugin/common/MethodCall;->method:Ljava/lang/String;

    .line 44
    .local v0, "method":Ljava/lang/String;
    iget-object v1, p1, Lio/flutter/plugin/common/MethodCall;->arguments:Ljava/lang/Object;

    .line 46
    .local v1, "arguments":Ljava/lang/Object;
    const/4 v2, 0x0

    :try_start_0
    const-string v3, "SystemSound.play"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 47
    move-object v3, v1

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lio/flutter/plugin/platform/PlatformPlugin;->playSystemSound(Ljava/lang/String;)V

    .line 48
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 49
    :cond_0
    const-string v3, "HapticFeedback.vibrate"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 50
    move-object v3, v1

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lio/flutter/plugin/platform/PlatformPlugin;->vibrateHapticFeedback(Ljava/lang/String;)V

    .line 51
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 52
    :cond_1
    const-string v3, "SystemChrome.setPreferredOrientations"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 53
    move-object v3, v1

    check-cast v3, Lorg/json/JSONArray;

    invoke-direct {p0, v3}, Lio/flutter/plugin/platform/PlatformPlugin;->setSystemChromePreferredOrientations(Lorg/json/JSONArray;)V

    .line 54
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 55
    :cond_2
    const-string v3, "SystemChrome.setApplicationSwitcherDescription"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 56
    move-object v3, v1

    check-cast v3, Lorg/json/JSONObject;

    invoke-direct {p0, v3}, Lio/flutter/plugin/platform/PlatformPlugin;->setSystemChromeApplicationSwitcherDescription(Lorg/json/JSONObject;)V

    .line 57
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 58
    :cond_3
    const-string v3, "SystemChrome.setEnabledSystemUIOverlays"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 59
    move-object v3, v1

    check-cast v3, Lorg/json/JSONArray;

    invoke-direct {p0, v3}, Lio/flutter/plugin/platform/PlatformPlugin;->setSystemChromeEnabledSystemUIOverlays(Lorg/json/JSONArray;)V

    .line 60
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 61
    :cond_4
    const-string v3, "SystemChrome.restoreSystemUIOverlays"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 62
    invoke-direct {p0}, Lio/flutter/plugin/platform/PlatformPlugin;->restoreSystemChromeSystemUIOverlays()V

    .line 63
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 64
    :cond_5
    const-string v3, "SystemChrome.setSystemUIOverlayStyle"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_6

    .line 65
    move-object v3, v1

    check-cast v3, Lorg/json/JSONObject;

    invoke-direct {p0, v3}, Lio/flutter/plugin/platform/PlatformPlugin;->setSystemChromeSystemUIOverlayStyle(Lorg/json/JSONObject;)V

    .line 66
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 67
    :cond_6
    const-string v3, "SystemNavigator.pop"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    .line 68
    invoke-direct {p0}, Lio/flutter/plugin/platform/PlatformPlugin;->popSystemNavigator()V

    .line 69
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 70
    :cond_7
    const-string v3, "Clipboard.getData"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_8

    .line 71
    move-object v3, v1

    check-cast v3, Ljava/lang/String;

    invoke-direct {p0, v3}, Lio/flutter/plugin/platform/PlatformPlugin;->getClipboardData(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    invoke-interface {p2, v3}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 72
    :cond_8
    const-string v3, "Clipboard.setData"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_9

    .line 73
    move-object v3, v1

    check-cast v3, Lorg/json/JSONObject;

    invoke-direct {p0, v3}, Lio/flutter/plugin/platform/PlatformPlugin;->setClipboardData(Lorg/json/JSONObject;)V

    .line 74
    invoke-interface {p2, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->success(Ljava/lang/Object;)V

    goto :goto_0

    .line 76
    :cond_9
    invoke-interface {p2}, Lio/flutter/plugin/common/MethodChannel$Result;->notImplemented()V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_0
    goto :goto_1

    .line 78
    :catch_0
    move-exception v3

    .line 79
    .local v3, "e":Lorg/json/JSONException;
    const-string v4, "error"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "JSON error: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {p2, v4, v5, v2}, Lio/flutter/plugin/common/MethodChannel$Result;->error(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Object;)V

    .line 81
    .end local v3    # "e":Lorg/json/JSONException;
    :goto_1
    return-void
.end method

.method public onPostResume()V
    .locals 0

    .line 316
    invoke-direct {p0}, Lio/flutter/plugin/platform/PlatformPlugin;->updateSystemUiOverlays()V

    .line 317
    return-void
.end method
