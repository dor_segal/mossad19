.class Lio/flutter/view/ResourceCleaner;
.super Ljava/lang/Object;
.source "ResourceCleaner.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/flutter/view/ResourceCleaner$CleanTask;
    }
.end annotation


# static fields
.field private static final DELAY_MS:J = 0x1388L

.field private static final TAG:Ljava/lang/String; = "ResourceCleaner"


# instance fields
.field private final mContext:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    iput-object p1, p0, Lio/flutter/view/ResourceCleaner;->mContext:Landroid/content/Context;

    .line 58
    return-void
.end method


# virtual methods
.method start()V
    .locals 6

    .line 61
    iget-object v0, p0, Lio/flutter/view/ResourceCleaner;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 62
    .local v0, "cacheDir":Ljava/io/File;
    if-nez v0, :cond_0

    .line 63
    return-void

    .line 66
    :cond_0
    new-instance v1, Lio/flutter/view/ResourceCleaner$CleanTask;

    new-instance v2, Lio/flutter/view/ResourceCleaner$1;

    invoke-direct {v2, p0}, Lio/flutter/view/ResourceCleaner$1;-><init>(Lio/flutter/view/ResourceCleaner;)V

    invoke-virtual {v0, v2}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, p0, v2}, Lio/flutter/view/ResourceCleaner$CleanTask;-><init>(Lio/flutter/view/ResourceCleaner;[Ljava/io/File;)V

    .line 74
    .local v1, "task":Lio/flutter/view/ResourceCleaner$CleanTask;
    invoke-virtual {v1}, Lio/flutter/view/ResourceCleaner$CleanTask;->hasFilesToDelete()Z

    move-result v2

    if-nez v2, :cond_1

    .line 75
    return-void

    .line 78
    :cond_1
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    new-instance v3, Lio/flutter/view/ResourceCleaner$2;

    invoke-direct {v3, p0, v1}, Lio/flutter/view/ResourceCleaner$2;-><init>(Lio/flutter/view/ResourceCleaner;Lio/flutter/view/ResourceCleaner$CleanTask;)V

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 84
    return-void
.end method
