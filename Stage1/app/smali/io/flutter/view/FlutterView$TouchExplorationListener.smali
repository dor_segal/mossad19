.class Lio/flutter/view/FlutterView$TouchExplorationListener;
.super Ljava/lang/Object;
.source "FlutterView.java"

# interfaces
.implements Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/flutter/view/FlutterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "TouchExplorationListener"
.end annotation


# instance fields
.field final synthetic this$0:Lio/flutter/view/FlutterView;


# direct methods
.method constructor <init>(Lio/flutter/view/FlutterView;)V
    .locals 0
    .param p1, "this$0"    # Lio/flutter/view/FlutterView;

    .line 979
    iput-object p1, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onTouchExplorationStateChanged(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .line 982
    if-eqz p1, :cond_0

    .line 983
    iget-object v0, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lio/flutter/view/FlutterView;->access$602(Lio/flutter/view/FlutterView;Z)Z

    .line 984
    iget-object v0, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterView;->ensureAccessibilityEnabled()V

    .line 985
    iget-object v0, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    iget-object v1, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v1}, Lio/flutter/view/FlutterView;->access$400(Lio/flutter/view/FlutterView;)I

    move-result v1

    sget-object v2, Lio/flutter/view/FlutterView$AccessibilityFeature;->ACCESSIBLE_NAVIGATION:Lio/flutter/view/FlutterView$AccessibilityFeature;

    iget v2, v2, Lio/flutter/view/FlutterView$AccessibilityFeature;->value:I

    or-int/2addr v1, v2

    invoke-static {v0, v1}, Lio/flutter/view/FlutterView;->access$402(Lio/flutter/view/FlutterView;I)I

    .line 986
    iget-object v0, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v0}, Lio/flutter/view/FlutterView;->access$000(Lio/flutter/view/FlutterView;)Lio/flutter/view/FlutterNativeView;

    move-result-object v0

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v0

    iget-object v2, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v2}, Lio/flutter/view/FlutterView;->access$400(Lio/flutter/view/FlutterView;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lio/flutter/view/FlutterView;->access$500(JI)V

    goto :goto_0

    .line 988
    :cond_0
    iget-object v0, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lio/flutter/view/FlutterView;->access$602(Lio/flutter/view/FlutterView;Z)Z

    .line 989
    iget-object v0, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v0}, Lio/flutter/view/FlutterView;->access$700(Lio/flutter/view/FlutterView;)Lio/flutter/view/AccessibilityBridge;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 990
    iget-object v0, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v0}, Lio/flutter/view/FlutterView;->access$700(Lio/flutter/view/FlutterView;)Lio/flutter/view/AccessibilityBridge;

    move-result-object v0

    invoke-virtual {v0}, Lio/flutter/view/AccessibilityBridge;->handleTouchExplorationExit()V

    .line 992
    :cond_1
    iget-object v0, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    iget-object v1, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v1}, Lio/flutter/view/FlutterView;->access$400(Lio/flutter/view/FlutterView;)I

    move-result v1

    sget-object v2, Lio/flutter/view/FlutterView$AccessibilityFeature;->ACCESSIBLE_NAVIGATION:Lio/flutter/view/FlutterView$AccessibilityFeature;

    iget v2, v2, Lio/flutter/view/FlutterView$AccessibilityFeature;->value:I

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v1, v2

    invoke-static {v0, v1}, Lio/flutter/view/FlutterView;->access$402(Lio/flutter/view/FlutterView;I)I

    .line 993
    iget-object v0, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v0}, Lio/flutter/view/FlutterView;->access$000(Lio/flutter/view/FlutterView;)Lio/flutter/view/FlutterNativeView;

    move-result-object v0

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v0

    iget-object v2, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v2}, Lio/flutter/view/FlutterView;->access$400(Lio/flutter/view/FlutterView;)I

    move-result v2

    invoke-static {v0, v1, v2}, Lio/flutter/view/FlutterView;->access$500(JI)V

    .line 995
    :goto_0
    iget-object v0, p0, Lio/flutter/view/FlutterView$TouchExplorationListener;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v0}, Lio/flutter/view/FlutterView;->access$800(Lio/flutter/view/FlutterView;)V

    .line 996
    return-void
.end method
