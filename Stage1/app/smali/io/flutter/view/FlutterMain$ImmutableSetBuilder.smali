.class final Lio/flutter/view/FlutterMain$ImmutableSetBuilder;
.super Ljava/lang/Object;
.source "FlutterMain.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/flutter/view/FlutterMain;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ImmutableSetBuilder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field set:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "TT;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>()V
    .locals 1

    .line 93
    .local p0, "this":Lio/flutter/view/FlutterMain$ImmutableSetBuilder;, "Lio/flutter/view/FlutterMain$ImmutableSetBuilder<TT;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lio/flutter/view/FlutterMain$ImmutableSetBuilder;->set:Ljava/util/HashSet;

    .line 93
    return-void
.end method

.method static newInstance()Lio/flutter/view/FlutterMain$ImmutableSetBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">()",
            "Lio/flutter/view/FlutterMain$ImmutableSetBuilder<",
            "TT;>;"
        }
    .end annotation

    .line 88
    new-instance v0, Lio/flutter/view/FlutterMain$ImmutableSetBuilder;

    invoke-direct {v0}, Lio/flutter/view/FlutterMain$ImmutableSetBuilder;-><init>()V

    return-object v0
.end method


# virtual methods
.method add(Ljava/lang/Object;)Lio/flutter/view/FlutterMain$ImmutableSetBuilder;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)",
            "Lio/flutter/view/FlutterMain$ImmutableSetBuilder<",
            "TT;>;"
        }
    .end annotation

    .line 96
    .local p0, "this":Lio/flutter/view/FlutterMain$ImmutableSetBuilder;, "Lio/flutter/view/FlutterMain$ImmutableSetBuilder<TT;>;"
    .local p1, "element":Ljava/lang/Object;, "TT;"
    iget-object v0, p0, Lio/flutter/view/FlutterMain$ImmutableSetBuilder;->set:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 97
    return-object p0
.end method

.method final varargs add([Ljava/lang/Object;)Lio/flutter/view/FlutterMain$ImmutableSetBuilder;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)",
            "Lio/flutter/view/FlutterMain$ImmutableSetBuilder<",
            "TT;>;"
        }
    .end annotation

    .annotation runtime Ljava/lang/SafeVarargs;
    .end annotation

    .line 102
    .local p0, "this":Lio/flutter/view/FlutterMain$ImmutableSetBuilder;, "Lio/flutter/view/FlutterMain$ImmutableSetBuilder<TT;>;"
    .local p1, "elements":[Ljava/lang/Object;, "[TT;"
    array-length v0, p1

    const/4 v1, 0x0

    :goto_0
    if-ge v1, v0, :cond_0

    aget-object v2, p1, v1

    .line 103
    .local v2, "element":Ljava/lang/Object;, "TT;"
    iget-object v3, p0, Lio/flutter/view/FlutterMain$ImmutableSetBuilder;->set:Ljava/util/HashSet;

    invoke-virtual {v3, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 102
    .end local v2    # "element":Ljava/lang/Object;, "TT;"
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 105
    :cond_0
    return-object p0
.end method

.method build()Ljava/util/Set;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set<",
            "TT;>;"
        }
    .end annotation

    .line 109
    .local p0, "this":Lio/flutter/view/FlutterMain$ImmutableSetBuilder;, "Lio/flutter/view/FlutterMain$ImmutableSetBuilder<TT;>;"
    iget-object v0, p0, Lio/flutter/view/FlutterMain$ImmutableSetBuilder;->set:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method
