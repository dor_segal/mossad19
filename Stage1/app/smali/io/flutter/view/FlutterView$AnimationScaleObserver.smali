.class Lio/flutter/view/FlutterView$AnimationScaleObserver;
.super Landroid/database/ContentObserver;
.source "FlutterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/flutter/view/FlutterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AnimationScaleObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lio/flutter/view/FlutterView;


# direct methods
.method public constructor <init>(Lio/flutter/view/FlutterView;Landroid/os/Handler;)V
    .locals 0
    .param p2, "handler"    # Landroid/os/Handler;

    .line 957
    iput-object p1, p0, Lio/flutter/view/FlutterView$AnimationScaleObserver;->this$0:Lio/flutter/view/FlutterView;

    .line 958
    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 959
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 1
    .param p1, "selfChange"    # Z

    .line 963
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lio/flutter/view/FlutterView$AnimationScaleObserver;->onChange(ZLandroid/net/Uri;)V

    .line 964
    return-void
.end method

.method public onChange(ZLandroid/net/Uri;)V
    .locals 4
    .param p1, "selfChange"    # Z
    .param p2, "uri"    # Landroid/net/Uri;

    .line 968
    iget-object v0, p0, Lio/flutter/view/FlutterView$AnimationScaleObserver;->this$0:Lio/flutter/view/FlutterView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "transition_animation_scale"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 970
    .local v0, "value":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 971
    iget-object v1, p0, Lio/flutter/view/FlutterView$AnimationScaleObserver;->this$0:Lio/flutter/view/FlutterView;

    iget-object v2, p0, Lio/flutter/view/FlutterView$AnimationScaleObserver;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v2}, Lio/flutter/view/FlutterView;->access$400(Lio/flutter/view/FlutterView;)I

    move-result v2

    sget-object v3, Lio/flutter/view/FlutterView$AccessibilityFeature;->DISABLE_ANIMATIONS:Lio/flutter/view/FlutterView$AccessibilityFeature;

    iget v3, v3, Lio/flutter/view/FlutterView$AccessibilityFeature;->value:I

    or-int/2addr v2, v3

    invoke-static {v1, v2}, Lio/flutter/view/FlutterView;->access$402(Lio/flutter/view/FlutterView;I)I

    goto :goto_0

    .line 973
    :cond_0
    iget-object v1, p0, Lio/flutter/view/FlutterView$AnimationScaleObserver;->this$0:Lio/flutter/view/FlutterView;

    iget-object v2, p0, Lio/flutter/view/FlutterView$AnimationScaleObserver;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v2}, Lio/flutter/view/FlutterView;->access$400(Lio/flutter/view/FlutterView;)I

    move-result v2

    sget-object v3, Lio/flutter/view/FlutterView$AccessibilityFeature;->DISABLE_ANIMATIONS:Lio/flutter/view/FlutterView$AccessibilityFeature;

    iget v3, v3, Lio/flutter/view/FlutterView$AccessibilityFeature;->value:I

    xor-int/lit8 v3, v3, -0x1

    and-int/2addr v2, v3

    invoke-static {v1, v2}, Lio/flutter/view/FlutterView;->access$402(Lio/flutter/view/FlutterView;I)I

    .line 975
    :goto_0
    iget-object v1, p0, Lio/flutter/view/FlutterView$AnimationScaleObserver;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v1}, Lio/flutter/view/FlutterView;->access$000(Lio/flutter/view/FlutterView;)Lio/flutter/view/FlutterNativeView;

    move-result-object v1

    invoke-virtual {v1}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v1

    iget-object v3, p0, Lio/flutter/view/FlutterView$AnimationScaleObserver;->this$0:Lio/flutter/view/FlutterView;

    invoke-static {v3}, Lio/flutter/view/FlutterView;->access$400(Lio/flutter/view/FlutterView;)I

    move-result v3

    invoke-static {v1, v2, v3}, Lio/flutter/view/FlutterView;->access$500(JI)V

    .line 976
    return-void
.end method
