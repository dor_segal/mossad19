.class Lio/flutter/view/ResourceExtractor$ExtractTask;
.super Landroid/os/AsyncTask;
.source "ResourceExtractor.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/flutter/view/ResourceExtractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ExtractTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# static fields
.field private static final BUFFER_SIZE:I = 0x4000


# instance fields
.field final synthetic this$0:Lio/flutter/view/ResourceExtractor;


# direct methods
.method constructor <init>(Lio/flutter/view/ResourceExtractor;)V
    .locals 0

    .line 31
    iput-object p1, p0, Lio/flutter/view/ResourceExtractor$ExtractTask;->this$0:Lio/flutter/view/ResourceExtractor;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private checkTimestamp(Ljava/io/File;)Ljava/lang/String;
    .locals 8
    .param p1, "dataDir"    # Ljava/io/File;

    .line 87
    iget-object v0, p0, Lio/flutter/view/ResourceExtractor$ExtractTask;->this$0:Lio/flutter/view/ResourceExtractor;

    invoke-static {v0}, Lio/flutter/view/ResourceExtractor;->access$000(Lio/flutter/view/ResourceExtractor;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 88
    .local v0, "packageManager":Landroid/content/pm/PackageManager;
    const/4 v1, 0x0

    move-object v2, v1

    .line 91
    .local v2, "packageInfo":Landroid/content/pm/PackageInfo;
    :try_start_0
    iget-object v3, p0, Lio/flutter/view/ResourceExtractor$ExtractTask;->this$0:Lio/flutter/view/ResourceExtractor;

    invoke-static {v3}, Lio/flutter/view/ResourceExtractor;->access$000(Lio/flutter/view/ResourceExtractor;)Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v2, v3

    .line 94
    nop

    .line 96
    if-nez v2, :cond_0

    .line 97
    const-string v1, "res_timestamp-"

    return-object v1

    .line 100
    :cond_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "res_timestamp-"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v5, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v5, "-"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-wide v5, v2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 103
    .local v3, "expectedTimestamp":Ljava/lang/String;
    iget-object v5, p0, Lio/flutter/view/ResourceExtractor$ExtractTask;->this$0:Lio/flutter/view/ResourceExtractor;

    invoke-static {v5, p1}, Lio/flutter/view/ResourceExtractor;->access$300(Lio/flutter/view/ResourceExtractor;Ljava/io/File;)[Ljava/lang/String;

    move-result-object v5

    .line 105
    .local v5, "existingTimestamps":[Ljava/lang/String;
    if-nez v5, :cond_1

    .line 106
    return-object v1

    .line 109
    :cond_1
    array-length v6, v5

    const/4 v7, 0x1

    if-ne v6, v7, :cond_3

    aget-object v4, v5, v4

    .line 110
    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    goto :goto_0

    .line 114
    :cond_2
    return-object v1

    .line 111
    :cond_3
    :goto_0
    return-object v3

    .line 92
    .end local v3    # "expectedTimestamp":Ljava/lang/String;
    .end local v5    # "existingTimestamps":[Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v3, "res_timestamp-"

    return-object v3
.end method

.method private extractResources()V
    .locals 16

    .line 34
    move-object/from16 v1, p0

    new-instance v0, Ljava/io/File;

    iget-object v2, v1, Lio/flutter/view/ResourceExtractor$ExtractTask;->this$0:Lio/flutter/view/ResourceExtractor;

    invoke-static {v2}, Lio/flutter/view/ResourceExtractor;->access$000(Lio/flutter/view/ResourceExtractor;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lio/flutter/util/PathUtils;->getDataDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    move-object v2, v0

    .line 36
    .local v2, "dataDir":Ljava/io/File;
    invoke-direct {v1, v2}, Lio/flutter/view/ResourceExtractor$ExtractTask;->checkTimestamp(Ljava/io/File;)Ljava/lang/String;

    move-result-object v3

    .line 37
    .local v3, "timestamp":Ljava/lang/String;
    if-eqz v3, :cond_0

    .line 38
    iget-object v0, v1, Lio/flutter/view/ResourceExtractor$ExtractTask;->this$0:Lio/flutter/view/ResourceExtractor;

    invoke-static {v0}, Lio/flutter/view/ResourceExtractor;->access$100(Lio/flutter/view/ResourceExtractor;)V

    .line 41
    :cond_0
    iget-object v0, v1, Lio/flutter/view/ResourceExtractor$ExtractTask;->this$0:Lio/flutter/view/ResourceExtractor;

    invoke-static {v0}, Lio/flutter/view/ResourceExtractor;->access$000(Lio/flutter/view/ResourceExtractor;)Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v4

    .line 43
    .local v4, "manager":Landroid/content/res/AssetManager;
    const/4 v0, 0x0

    .line 44
    .local v0, "buffer":[B
    iget-object v5, v1, Lio/flutter/view/ResourceExtractor$ExtractTask;->this$0:Lio/flutter/view/ResourceExtractor;

    invoke-static {v5}, Lio/flutter/view/ResourceExtractor;->access$200(Lio/flutter/view/ResourceExtractor;)Ljava/util/HashSet;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move-object v6, v0

    .end local v0    # "buffer":[B
    .local v6, "buffer":[B
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    move-object v7, v0

    .line 46
    .local v7, "asset":Ljava/lang/String;
    :try_start_0
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2, v7}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    move-object v8, v0

    .line 48
    .local v8, "output":Ljava/io/File;
    invoke-virtual {v8}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    goto :goto_0

    .line 51
    :cond_1
    invoke-virtual {v8}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 52
    invoke-virtual {v8}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 55
    :cond_2
    invoke-virtual {v4, v7}, Landroid/content/res/AssetManager;->open(Ljava/lang/String;)Ljava/io/InputStream;

    move-result-object v0
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_8
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_7

    move-object v9, v0

    .line 56
    .local v9, "is":Ljava/io/InputStream;
    const/4 v10, 0x0

    :try_start_1
    new-instance v0, Ljava/io/FileOutputStream;

    invoke-direct {v0, v8}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_3

    move-object v11, v0

    .line 57
    .local v11, "os":Ljava/io/OutputStream;
    const/16 v0, 0x4000

    if-nez v6, :cond_3

    .line 58
    :try_start_2
    new-array v12, v0, [B

    move-object v6, v12

    goto :goto_2

    .line 66
    :catchall_0
    move-exception v0

    move-object v13, v6

    move-object v12, v10

    .end local v6    # "buffer":[B
    .local v13, "buffer":[B
    :goto_1
    move-object v6, v0

    goto :goto_5

    .line 56
    .end local v13    # "buffer":[B
    .restart local v6    # "buffer":[B
    :catch_0
    move-exception v0

    move-object v12, v6

    move-object v6, v0

    goto :goto_4

    .line 61
    :cond_3
    :goto_2
    const/4 v12, 0x0

    const/4 v13, 0x0

    .line 62
    .local v13, "count":I
    :goto_3
    invoke-virtual {v9, v6, v12, v0}, Ljava/io/InputStream;->read([BII)I

    move-result v14

    move v13, v14

    const/4 v15, -0x1

    if-eq v14, v15, :cond_4

    .line 63
    invoke-virtual {v11, v6, v12, v13}, Ljava/io/OutputStream;->write([BII)V

    goto :goto_3

    .line 65
    :cond_4
    invoke-virtual {v11}, Ljava/io/OutputStream;->flush()V
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 66
    .end local v13    # "count":I
    :try_start_3
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    .line 67
    .end local v11    # "os":Ljava/io/OutputStream;
    if-eqz v9, :cond_5

    :try_start_4
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_8
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_7

    .line 74
    .end local v8    # "output":Ljava/io/File;
    .end local v9    # "is":Ljava/io/InputStream;
    :cond_5
    nop

    .line 75
    .end local v7    # "asset":Ljava/lang/String;
    goto :goto_0

    .line 56
    .end local v6    # "buffer":[B
    .restart local v7    # "asset":Ljava/lang/String;
    .restart local v8    # "output":Ljava/io/File;
    .restart local v9    # "is":Ljava/io/InputStream;
    .restart local v11    # "os":Ljava/io/OutputStream;
    .local v12, "buffer":[B
    :goto_4
    :try_start_5
    throw v6
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 66
    :catchall_1
    move-exception v0

    move-object v13, v12

    move-object v12, v6

    goto :goto_1

    .end local v12    # "buffer":[B
    .local v13, "buffer":[B
    :goto_5
    if-eqz v12, :cond_6

    :try_start_6
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V
    :try_end_6
    .catch Ljava/lang/Throwable; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    goto :goto_6

    :catch_1
    move-exception v0

    move-object v14, v0

    :try_start_7
    invoke-virtual {v12, v14}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_6

    :cond_6
    invoke-virtual {v11}, Ljava/io/OutputStream;->close()V

    :goto_6
    throw v6
    :try_end_7
    .catch Ljava/lang/Throwable; {:try_start_7 .. :try_end_7} :catch_2
    .catchall {:try_start_7 .. :try_end_7} :catchall_2

    .line 67
    .end local v11    # "os":Ljava/io/OutputStream;
    :catchall_2
    move-exception v0

    goto :goto_7

    .line 55
    :catch_2
    move-exception v0

    move-object v10, v0

    move-object v6, v13

    goto :goto_8

    .line 67
    .end local v13    # "buffer":[B
    .restart local v6    # "buffer":[B
    :catchall_3
    move-exception v0

    move-object v13, v6

    .end local v6    # "buffer":[B
    .restart local v13    # "buffer":[B
    :goto_7
    move-object v6, v0

    goto :goto_9

    .line 55
    .end local v13    # "buffer":[B
    .restart local v6    # "buffer":[B
    :catch_3
    move-exception v0

    move-object v10, v0

    :goto_8
    :try_start_8
    throw v10
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    .line 67
    .end local v6    # "buffer":[B
    .restart local v13    # "buffer":[B
    :goto_9
    if-eqz v9, :cond_8

    if-eqz v10, :cond_7

    :try_start_9
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V
    :try_end_9
    .catch Ljava/lang/Throwable; {:try_start_9 .. :try_end_9} :catch_4
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_6
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_5

    goto :goto_a

    :catch_4
    move-exception v0

    move-object v11, v0

    :try_start_a
    invoke-virtual {v10, v11}, Ljava/lang/Throwable;->addSuppressed(Ljava/lang/Throwable;)V

    goto :goto_a

    :cond_7
    invoke-virtual {v9}, Ljava/io/InputStream;->close()V

    :cond_8
    :goto_a
    throw v6
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_6
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5

    .line 70
    .end local v8    # "output":Ljava/io/File;
    .end local v9    # "is":Ljava/io/InputStream;
    :catch_5
    move-exception v0

    move-object v6, v13

    goto :goto_b

    .line 68
    :catch_6
    move-exception v0

    move-object v6, v13

    goto :goto_c

    .line 70
    .end local v13    # "buffer":[B
    .restart local v6    # "buffer":[B
    :catch_7
    move-exception v0

    .line 71
    .local v0, "ioe":Ljava/io/IOException;
    :goto_b
    const-string v5, "ResourceExtractor"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "Exception unpacking resources: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v5, v8}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    iget-object v5, v1, Lio/flutter/view/ResourceExtractor$ExtractTask;->this$0:Lio/flutter/view/ResourceExtractor;

    invoke-static {v5}, Lio/flutter/view/ResourceExtractor;->access$100(Lio/flutter/view/ResourceExtractor;)V

    .line 73
    return-void

    .line 68
    .end local v0    # "ioe":Ljava/io/IOException;
    :catch_8
    move-exception v0

    .line 69
    .local v0, "fnfe":Ljava/io/FileNotFoundException;
    :goto_c
    goto/16 :goto_0

    .line 77
    .end local v0    # "fnfe":Ljava/io/FileNotFoundException;
    .end local v7    # "asset":Ljava/lang/String;
    :cond_9
    if-eqz v3, :cond_a

    .line 79
    :try_start_b
    new-instance v0, Ljava/io/File;

    invoke-direct {v0, v2, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->createNewFile()Z
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_9

    .line 82
    goto :goto_d

    .line 80
    :catch_9
    move-exception v0

    .line 81
    .local v0, "e":Ljava/io/IOException;
    const-string v5, "ResourceExtractor"

    const-string v7, "Failed to write resource timestamp"

    invoke-static {v5, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 84
    .end local v0    # "e":Ljava/io/IOException;
    :cond_a
    :goto_d
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 28
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lio/flutter/view/ResourceExtractor$ExtractTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 1
    .param p1, "unused"    # [Ljava/lang/Void;

    .line 119
    invoke-direct {p0}, Lio/flutter/view/ResourceExtractor$ExtractTask;->extractResources()V

    .line 120
    const/4 v0, 0x0

    return-object v0
.end method
