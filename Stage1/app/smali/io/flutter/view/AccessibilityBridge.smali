.class Lio/flutter/view/AccessibilityBridge;
.super Landroid/view/accessibility/AccessibilityNodeProvider;
.source "AccessibilityBridge.java"

# interfaces
.implements Lio/flutter/plugin/common/BasicMessageChannel$MessageHandler;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/flutter/view/AccessibilityBridge$SemanticsObject;,
        Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;,
        Lio/flutter/view/AccessibilityBridge$TextDirection;,
        Lio/flutter/view/AccessibilityBridge$Flag;,
        Lio/flutter/view/AccessibilityBridge$Action;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/view/accessibility/AccessibilityNodeProvider;",
        "Lio/flutter/plugin/common/BasicMessageChannel$MessageHandler<",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final ACTION_SHOW_ON_SCREEN:I = 0x1020036

.field private static final ROOT_NODE_ID:I = 0x0

.field private static final SCROLL_EXTENT_FOR_INFINITY:F = 100000.0f

.field private static final SCROLL_POSITION_CAP_FOR_INFINITY:F = 70000.0f

.field private static final TAG:Ljava/lang/String; = "FlutterView"

.field static firstResourceId:I


# instance fields
.field private mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

.field private mAccessibilityEnabled:Z

.field private mCustomAccessibilityActions:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;",
            ">;"
        }
    .end annotation
.end field

.field private final mDecorView:Landroid/view/View;

.field private final mFlutterAccessibilityChannel:Lio/flutter/plugin/common/BasicMessageChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/flutter/plugin/common/BasicMessageChannel<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

.field private mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

.field private mLastLeftFrameInset:Ljava/lang/Integer;

.field private mObjects:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lio/flutter/view/AccessibilityBridge$SemanticsObject;",
            ">;"
        }
    .end annotation
.end field

.field private final mOwner:Lio/flutter/view/FlutterView;

.field private previousRouteId:I

.field private previousRoutes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 23
    nop

    .line 961
    const v0, 0xff00001

    sput v0, Lio/flutter/view/AccessibilityBridge;->firstResourceId:I

    return-void
.end method

.method constructor <init>(Lio/flutter/view/FlutterView;)V
    .locals 3
    .param p1, "owner"    # Lio/flutter/view/FlutterView;

    .line 108
    invoke-direct {p0}, Landroid/view/accessibility/AccessibilityNodeProvider;-><init>()V

    .line 39
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/flutter/view/AccessibilityBridge;->mAccessibilityEnabled:Z

    .line 43
    iput v0, p0, Lio/flutter/view/AccessibilityBridge;->previousRouteId:I

    .line 46
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mLastLeftFrameInset:Ljava/lang/Integer;

    .line 109
    nop

    .line 110
    iput-object p1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    .line 111
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mObjects:Ljava/util/Map;

    .line 112
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mCustomAccessibilityActions:Ljava/util/Map;

    .line 113
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->previousRoutes:Ljava/util/List;

    .line 114
    new-instance v0, Lio/flutter/plugin/common/BasicMessageChannel;

    const-string v1, "flutter/accessibility"

    sget-object v2, Lio/flutter/plugin/common/StandardMessageCodec;->INSTANCE:Lio/flutter/plugin/common/StandardMessageCodec;

    invoke-direct {v0, p1, v1, v2}, Lio/flutter/plugin/common/BasicMessageChannel;-><init>(Lio/flutter/plugin/common/BinaryMessenger;Ljava/lang/String;Lio/flutter/plugin/common/MessageCodec;)V

    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mFlutterAccessibilityChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    .line 116
    invoke-virtual {p1}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    invoke-virtual {v0}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mDecorView:Landroid/view/View;

    .line 117
    return-void
.end method

.method static synthetic access$300(Lio/flutter/view/AccessibilityBridge;I)Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    .locals 1
    .param p0, "x0"    # Lio/flutter/view/AccessibilityBridge;
    .param p1, "x1"    # I

    .line 23
    invoke-direct {p0, p1}, Lio/flutter/view/AccessibilityBridge;->getOrCreateObject(I)Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$400(Lio/flutter/view/AccessibilityBridge;I)Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    .locals 1
    .param p0, "x0"    # Lio/flutter/view/AccessibilityBridge;
    .param p1, "x1"    # I

    .line 23
    invoke-direct {p0, p1}, Lio/flutter/view/AccessibilityBridge;->getOrCreateAction(I)Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    move-result-object v0

    return-object v0
.end method

.method private createTextChangedEvent(ILjava/lang/String;Ljava/lang/String;)Landroid/view/accessibility/AccessibilityEvent;
    .locals 7
    .param p1, "id"    # I
    .param p2, "oldValue"    # Ljava/lang/String;
    .param p3, "newValue"    # Ljava/lang/String;

    .line 794
    nop

    .line 795
    const/16 v0, 0x10

    invoke-direct {p0, p1, v0}, Lio/flutter/view/AccessibilityBridge;->obtainAccessibilityEvent(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 796
    .local v0, "e":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v0, p2}, Landroid/view/accessibility/AccessibilityEvent;->setBeforeText(Ljava/lang/CharSequence;)V

    .line 797
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1, p3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 800
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v1, v2, :cond_1

    .line 801
    invoke-virtual {p2, v1}, Ljava/lang/String;->charAt(I)C

    move-result v2

    invoke-virtual {p3, v1}, Ljava/lang/String;->charAt(I)C

    move-result v3

    if-eq v2, v3, :cond_0

    .line 802
    goto :goto_1

    .line 800
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 805
    :cond_1
    :goto_1
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_2

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v2

    if-lt v1, v2, :cond_2

    .line 806
    const/4 v2, 0x0

    return-object v2

    .line 808
    :cond_2
    move v2, v1

    .line 809
    .local v2, "firstDifference":I
    invoke-virtual {v0, v2}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 811
    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    .line 812
    .local v3, "oldIndex":I
    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    .line 813
    .local v4, "newIndex":I
    :goto_2
    if-lt v3, v2, :cond_4

    if-lt v4, v2, :cond_4

    .line 814
    invoke-virtual {p2, v3}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-virtual {p3, v4}, Ljava/lang/String;->charAt(I)C

    move-result v6

    if-eq v5, v6, :cond_3

    .line 815
    goto :goto_3

    .line 817
    :cond_3
    add-int/lit8 v3, v3, -0x1

    .line 818
    add-int/lit8 v4, v4, -0x1

    goto :goto_2

    .line 820
    :cond_4
    :goto_3
    sub-int v5, v3, v2

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0, v5}, Landroid/view/accessibility/AccessibilityEvent;->setRemovedCount(I)V

    .line 821
    sub-int v5, v4, v2

    add-int/lit8 v5, v5, 0x1

    invoke-virtual {v0, v5}, Landroid/view/accessibility/AccessibilityEvent;->setAddedCount(I)V

    .line 823
    return-object v0
.end method

.method private createWindowChangeEvent(Lio/flutter/view/AccessibilityBridge$SemanticsObject;)V
    .locals 3
    .param p1, "route"    # Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 891
    iget v0, p1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    .line 892
    const/16 v1, 0x20

    invoke-direct {p0, v0, v1}, Lio/flutter/view/AccessibilityBridge;->obtainAccessibilityEvent(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 893
    .local v0, "e":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {p1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->getRouteName()Ljava/lang/String;

    move-result-object v1

    .line 894
    .local v1, "routeName":Ljava/lang/String;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 895
    invoke-direct {p0, v0}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 896
    return-void
.end method

.method private getOrCreateAction(I)Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    .locals 3
    .param p1, "id"    # I

    .line 567
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mCustomAccessibilityActions:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    .line 568
    .local v0, "action":Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    if-nez v0, :cond_0

    .line 569
    new-instance v1, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    invoke-direct {v1, p0}, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;-><init>(Lio/flutter/view/AccessibilityBridge;)V

    move-object v0, v1

    .line 570
    iput p1, v0, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->id:I

    .line 571
    sget v1, Lio/flutter/view/AccessibilityBridge;->firstResourceId:I

    add-int/2addr v1, p1

    iput v1, v0, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->resourceId:I

    .line 572
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mCustomAccessibilityActions:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 574
    :cond_0
    return-object v0
.end method

.method private getOrCreateObject(I)Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    .locals 3
    .param p1, "id"    # I

    .line 557
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mObjects:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 558
    .local v0, "object":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    if-nez v0, :cond_0

    .line 559
    new-instance v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    invoke-direct {v1, p0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;-><init>(Lio/flutter/view/AccessibilityBridge;)V

    move-object v0, v1

    .line 560
    iput p1, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    .line 561
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mObjects:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 563
    :cond_0
    return-object v0
.end method

.method private getRootObject()Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    .locals 2

    .line 552
    nop

    .line 553
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mObjects:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    return-object v0
.end method

.method private obtainAccessibilityEvent(II)Landroid/view/accessibility/AccessibilityEvent;
    .locals 2
    .param p1, "virtualViewId"    # I
    .param p2, "eventType"    # I

    .line 827
    nop

    .line 828
    invoke-static {p2}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    .line 829
    .local v0, "event":Landroid/view/accessibility/AccessibilityEvent;
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-virtual {v1}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 830
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-virtual {v0, v1, p1}, Landroid/view/accessibility/AccessibilityEvent;->setSource(Landroid/view/View;I)V

    .line 831
    return-object v0
.end method

.method private sendAccessibilityEvent(II)V
    .locals 1
    .param p1, "virtualViewId"    # I
    .param p2, "eventType"    # I

    .line 835
    iget-boolean v0, p0, Lio/flutter/view/AccessibilityBridge;->mAccessibilityEnabled:Z

    if-nez v0, :cond_0

    .line 836
    return-void

    .line 838
    :cond_0
    if-nez p1, :cond_1

    .line 839
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-virtual {v0, p2}, Lio/flutter/view/FlutterView;->sendAccessibilityEvent(I)V

    goto :goto_0

    .line 841
    :cond_1
    invoke-direct {p0, p1, p2}, Lio/flutter/view/AccessibilityBridge;->obtainAccessibilityEvent(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v0

    invoke-direct {p0, v0}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 843
    :goto_0
    return-void
.end method

.method private sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V
    .locals 2
    .param p1, "event"    # Landroid/view/accessibility/AccessibilityEvent;

    .line 846
    iget-boolean v0, p0, Lio/flutter/view/AccessibilityBridge;->mAccessibilityEnabled:Z

    if-nez v0, :cond_0

    .line 847
    return-void

    .line 849
    :cond_0
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-interface {v0, v1, p1}, Landroid/view/ViewParent;->requestSendAccessibilityEvent(Landroid/view/View;Landroid/view/accessibility/AccessibilityEvent;)Z

    .line 850
    return-void
.end method

.method private willRemoveSemanticsObject(Lio/flutter/view/AccessibilityBridge$SemanticsObject;)V
    .locals 3
    .param p1, "object"    # Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 899
    nop

    .line 900
    nop

    .line 901
    const/4 v0, 0x0

    iput-object v0, p1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->parent:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 902
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-ne v1, p1, :cond_0

    .line 903
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v1, v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    const/high16 v2, 0x10000

    invoke-direct {p0, v1, v2}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 905
    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 907
    :cond_0
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-ne v1, p1, :cond_1

    .line 908
    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 910
    :cond_1
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-ne v1, p1, :cond_2

    .line 911
    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 913
    :cond_2
    return-void
.end method


# virtual methods
.method public createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .locals 10
    .param p1, "virtualViewId"    # I

    .line 131
    const/4 v0, -0x1

    const/4 v1, 0x0

    if-ne p1, v0, :cond_1

    .line 132
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-static {v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/View;)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    .line 133
    .local v0, "result":Landroid/view/accessibility/AccessibilityNodeInfo;
    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-virtual {v2, v0}, Lio/flutter/view/FlutterView;->onInitializeAccessibilityNodeInfo(Landroid/view/accessibility/AccessibilityNodeInfo;)V

    .line 134
    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge;->mObjects:Ljava/util/Map;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 135
    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-virtual {v0, v2, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    .line 137
    :cond_0
    return-object v0

    .line 140
    .end local v0    # "result":Landroid/view/accessibility/AccessibilityNodeInfo;
    :cond_1
    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge;->mObjects:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 141
    .local v2, "object":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    if-nez v2, :cond_2

    .line 142
    const/4 v0, 0x0

    return-object v0

    .line 145
    :cond_2
    iget-object v3, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-static {v3, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->obtain(Landroid/view/View;I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v3

    .line 147
    .local v3, "result":Landroid/view/accessibility/AccessibilityNodeInfo;
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v4, v5, :cond_3

    .line 148
    const-string v4, ""

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setViewIdResourceName(Ljava/lang/String;)V

    .line 150
    :cond_3
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-virtual {v4}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setPackageName(Ljava/lang/CharSequence;)V

    .line 151
    const-string v4, "android.view.View"

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 152
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-virtual {v3, v4, p1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSource(Landroid/view/View;I)V

    .line 153
    invoke-virtual {v2}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->isFocusable()Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setFocusable(Z)V

    .line 154
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    const/4 v6, 0x1

    if-eqz v4, :cond_5

    .line 155
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v4, v4, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    if-ne v4, p1, :cond_4

    const/4 v4, 0x1

    goto :goto_0

    :cond_4
    const/4 v4, 0x0

    :goto_0
    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setFocused(Z)V

    .line 158
    :cond_5
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v4, :cond_7

    .line 159
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v4, v4, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    if-ne v4, p1, :cond_6

    const/4 v4, 0x1

    goto :goto_1

    :cond_6
    const/4 v4, 0x0

    :goto_1
    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setAccessibilityFocused(Z)V

    .line 162
    :cond_7
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Flag;->IS_TEXT_FIELD:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v4

    if-eqz v4, :cond_e

    .line 163
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Flag;->IS_OBSCURED:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setPassword(Z)V

    .line 164
    const-string v4, "android.widget.EditText"

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 165
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v4, v5, :cond_9

    .line 166
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEditable(Z)V

    .line 167
    iget v4, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionBase:I

    if-eq v4, v0, :cond_8

    iget v4, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionExtent:I

    if-eq v4, v0, :cond_8

    .line 168
    iget v0, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionBase:I

    iget v4, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionExtent:I

    invoke-virtual {v3, v0, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setTextSelection(II)V

    .line 173
    :cond_8
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v0, v5, :cond_9

    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v0, :cond_9

    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v0, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    if-ne v0, p1, :cond_9

    .line 174
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLiveRegion(I)V

    .line 179
    :cond_9
    const/4 v0, 0x0

    .line 180
    .local v0, "granularities":I
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_FORWARD_BY_CHARACTER:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    const/16 v7, 0x100

    if-eqz v4, :cond_a

    .line 181
    invoke-virtual {v3, v7}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 182
    or-int/lit8 v0, v0, 0x1

    .line 184
    :cond_a
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_BACKWARD_BY_CHARACTER:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    const/16 v8, 0x200

    if-eqz v4, :cond_b

    .line 185
    invoke-virtual {v3, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 186
    or-int/lit8 v0, v0, 0x1

    .line 188
    :cond_b
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_FORWARD_BY_WORD:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 189
    invoke-virtual {v3, v7}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 190
    or-int/lit8 v0, v0, 0x2

    .line 192
    :cond_c
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_BACKWARD_BY_WORD:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_d

    .line 193
    invoke-virtual {v3, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 194
    or-int/lit8 v0, v0, 0x2

    .line 196
    :cond_d
    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setMovementGranularities(I)V

    .line 198
    .end local v0    # "granularities":I
    :cond_e
    sget-object v0, Lio/flutter/view/AccessibilityBridge$Action;->SET_SELECTION:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v0

    if-eqz v0, :cond_f

    .line 199
    const/high16 v0, 0x20000

    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 201
    :cond_f
    sget-object v0, Lio/flutter/view/AccessibilityBridge$Action;->COPY:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 202
    const/16 v0, 0x4000

    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 204
    :cond_10
    sget-object v0, Lio/flutter/view/AccessibilityBridge$Action;->CUT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v0

    if-eqz v0, :cond_11

    .line 205
    const/high16 v0, 0x10000

    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 207
    :cond_11
    sget-object v0, Lio/flutter/view/AccessibilityBridge$Action;->PASTE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 208
    const v0, 0x8000

    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 211
    :cond_12
    sget-object v0, Lio/flutter/view/AccessibilityBridge$Flag;->IS_BUTTON:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 212
    const-string v0, "android.widget.Button"

    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 214
    :cond_13
    sget-object v0, Lio/flutter/view/AccessibilityBridge$Flag;->IS_IMAGE:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 215
    const-string v0, "android.widget.ImageView"

    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 219
    :cond_14
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v0, v5, :cond_15

    sget-object v0, Lio/flutter/view/AccessibilityBridge$Action;->DISMISS:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v0

    if-eqz v0, :cond_15

    .line 220
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setDismissable(Z)V

    .line 221
    const/high16 v0, 0x100000

    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 224
    :cond_15
    iget-object v0, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->parent:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v0, :cond_16

    .line 225
    nop

    .line 226
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    iget-object v4, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->parent:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v4, v4, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    invoke-virtual {v3, v0, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setParent(Landroid/view/View;I)V

    goto :goto_2

    .line 228
    :cond_16
    nop

    .line 229
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setParent(Landroid/view/View;)V

    .line 232
    :goto_2
    invoke-virtual {v2}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->getGlobalRect()Landroid/graphics/Rect;

    move-result-object v0

    .line 233
    .local v0, "bounds":Landroid/graphics/Rect;
    iget-object v4, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->parent:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v4, :cond_17

    .line 234
    iget-object v4, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->parent:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    invoke-virtual {v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->getGlobalRect()Landroid/graphics/Rect;

    move-result-object v4

    .line 235
    .local v4, "parentBounds":Landroid/graphics/Rect;
    new-instance v7, Landroid/graphics/Rect;

    invoke-direct {v7, v0}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 236
    .local v7, "boundsInParent":Landroid/graphics/Rect;
    iget v8, v4, Landroid/graphics/Rect;->left:I

    neg-int v8, v8

    iget v9, v4, Landroid/graphics/Rect;->top:I

    neg-int v9, v9

    invoke-virtual {v7, v8, v9}, Landroid/graphics/Rect;->offset(II)V

    .line 237
    invoke-virtual {v3, v7}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    .line 238
    .end local v4    # "parentBounds":Landroid/graphics/Rect;
    .end local v7    # "boundsInParent":Landroid/graphics/Rect;
    goto :goto_3

    .line 239
    :cond_17
    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInParent(Landroid/graphics/Rect;)V

    .line 241
    :goto_3
    invoke-virtual {v3, v0}, Landroid/view/accessibility/AccessibilityNodeInfo;->setBoundsInScreen(Landroid/graphics/Rect;)V

    .line 242
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setVisibleToUser(Z)V

    .line 243
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Flag;->HAS_ENABLED_STATE:Lio/flutter/view/AccessibilityBridge$Flag;

    .line 244
    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v4

    if-eqz v4, :cond_19

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Flag;->IS_ENABLED:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v4

    if-eqz v4, :cond_18

    goto :goto_4

    .line 243
    :cond_18
    const/4 v4, 0x0

    goto :goto_5

    .line 244
    :cond_19
    :goto_4
    nop

    .line 243
    const/4 v4, 0x1

    :goto_5
    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setEnabled(Z)V

    .line 246
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->TAP:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    const/16 v7, 0x15

    if-eqz v4, :cond_1b

    .line 247
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x10

    if-lt v4, v7, :cond_1a

    iget-object v4, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->onTapOverride:Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    if-eqz v4, :cond_1a

    .line 248
    new-instance v4, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    iget-object v9, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->onTapOverride:Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    iget-object v9, v9, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->hint:Ljava/lang/String;

    invoke-direct {v4, v8, v9}, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;-><init>(ILjava/lang/CharSequence;)V

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;)V

    .line 250
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClickable(Z)V

    goto :goto_6

    .line 252
    :cond_1a
    invoke-virtual {v3, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 253
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClickable(Z)V

    .line 256
    :cond_1b
    :goto_6
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->LONG_PRESS:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_1d

    .line 257
    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x20

    if-lt v4, v7, :cond_1c

    iget-object v4, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->onLongPressOverride:Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    if-eqz v4, :cond_1c

    .line 258
    new-instance v4, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    iget-object v9, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->onLongPressOverride:Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    iget-object v9, v9, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->hint:Ljava/lang/String;

    invoke-direct {v4, v8, v9}, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;-><init>(ILjava/lang/CharSequence;)V

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;)V

    .line 260
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLongClickable(Z)V

    goto :goto_7

    .line 262
    :cond_1c
    invoke-virtual {v3, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 263
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLongClickable(Z)V

    .line 266
    :cond_1d
    :goto_7
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_LEFT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    const/16 v8, 0x2000

    const/16 v9, 0x1000

    if-nez v4, :cond_1e

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_UP:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-nez v4, :cond_1e

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_RIGHT:Lio/flutter/view/AccessibilityBridge$Action;

    .line 267
    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-nez v4, :cond_1e

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_DOWN:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 268
    :cond_1e
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setScrollable(Z)V

    .line 272
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Flag;->HAS_IMPLICIT_SCROLLING:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v4

    if-eqz v4, :cond_21

    .line 273
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_LEFT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-nez v4, :cond_20

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_RIGHT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_1f

    goto :goto_8

    .line 276
    :cond_1f
    const-string v4, "android.widget.ScrollView"

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    goto :goto_9

    .line 274
    :cond_20
    :goto_8
    const-string v4, "android.widget.HorizontalScrollView"

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 282
    :cond_21
    :goto_9
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_LEFT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-nez v4, :cond_22

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_UP:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_23

    .line 283
    :cond_22
    invoke-virtual {v3, v9}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 285
    :cond_23
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_RIGHT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-nez v4, :cond_24

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_DOWN:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_25

    .line 286
    :cond_24
    invoke-virtual {v3, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 289
    :cond_25
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->INCREASE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-nez v4, :cond_26

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->DECREASE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 292
    :cond_26
    const-string v4, "android.widget.SeekBar"

    invoke-virtual {v3, v4}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 293
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->INCREASE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_27

    .line 294
    invoke-virtual {v3, v9}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 296
    :cond_27
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->DECREASE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_28

    .line 297
    invoke-virtual {v3, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 300
    :cond_28
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Flag;->IS_LIVE_REGION:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v4

    if-eqz v4, :cond_29

    sget v4, Landroid/os/Build$VERSION;->SDK_INT:I

    if-le v4, v5, :cond_29

    .line 301
    invoke-virtual {v3, v6}, Landroid/view/accessibility/AccessibilityNodeInfo;->setLiveRegion(I)V

    .line 304
    :cond_29
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Flag;->HAS_CHECKED_STATE:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v4

    .line 305
    .local v4, "hasCheckedState":Z
    sget-object v5, Lio/flutter/view/AccessibilityBridge$Flag;->HAS_TOGGLED_STATE:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v5}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v5

    .line 306
    .local v5, "hasToggledState":Z
    nop

    .line 307
    if-nez v4, :cond_2b

    if-eqz v5, :cond_2a

    goto :goto_a

    :cond_2a
    goto :goto_b

    :cond_2b
    :goto_a
    const/4 v1, 0x1

    :goto_b
    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setCheckable(Z)V

    .line 308
    if-eqz v4, :cond_2d

    .line 309
    sget-object v1, Lio/flutter/view/AccessibilityBridge$Flag;->IS_CHECKED:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v1

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 310
    invoke-static {v2}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->access$000(Lio/flutter/view/AccessibilityBridge$SemanticsObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 311
    sget-object v1, Lio/flutter/view/AccessibilityBridge$Flag;->IS_IN_MUTUALLY_EXCLUSIVE_GROUP:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v1

    if-eqz v1, :cond_2c

    .line 312
    const-string v1, "android.widget.RadioButton"

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    goto :goto_c

    .line 314
    :cond_2c
    const-string v1, "android.widget.CheckBox"

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    goto :goto_c

    .line 315
    :cond_2d
    if-eqz v5, :cond_2e

    .line 316
    sget-object v1, Lio/flutter/view/AccessibilityBridge$Flag;->IS_TOGGLED:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v1

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setChecked(Z)V

    .line 317
    const-string v1, "android.widget.Switch"

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setClassName(Ljava/lang/CharSequence;)V

    .line 318
    invoke-static {v2}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->access$000(Lio/flutter/view/AccessibilityBridge$SemanticsObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_c

    .line 322
    :cond_2e
    invoke-static {v2}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->access$000(Lio/flutter/view/AccessibilityBridge$SemanticsObject;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setText(Ljava/lang/CharSequence;)V

    .line 325
    :goto_c
    sget-object v1, Lio/flutter/view/AccessibilityBridge$Flag;->IS_SELECTED:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v2, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v1

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->setSelected(Z)V

    .line 328
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v1, :cond_2f

    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v1, v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    if-ne v1, p1, :cond_2f

    .line 329
    const/16 v1, 0x80

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    goto :goto_d

    .line 331
    :cond_2f
    const/16 v1, 0x40

    invoke-virtual {v3, v1}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(I)V

    .line 335
    :goto_d
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v7, :cond_30

    .line 336
    iget-object v1, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->customAccessibilityActions:Ljava/util/List;

    if-eqz v1, :cond_30

    .line 337
    iget-object v1, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->customAccessibilityActions:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_e
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_30

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    .line 338
    .local v6, "action":Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    new-instance v7, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;

    iget v8, v6, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->resourceId:I

    iget-object v9, v6, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->label:Ljava/lang/String;

    invoke-direct {v7, v8, v9}, Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;-><init>(ILjava/lang/CharSequence;)V

    invoke-virtual {v3, v7}, Landroid/view/accessibility/AccessibilityNodeInfo;->addAction(Landroid/view/accessibility/AccessibilityNodeInfo$AccessibilityAction;)V

    .line 340
    .end local v6    # "action":Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    goto :goto_e

    .line 344
    :cond_30
    iget-object v1, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    if-eqz v1, :cond_32

    .line 345
    iget-object v1, v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_f
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_32

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 346
    .local v6, "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    sget-object v7, Lio/flutter/view/AccessibilityBridge$Flag;->IS_HIDDEN:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v6, v7}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v7

    if-nez v7, :cond_31

    .line 347
    iget-object v7, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    iget v8, v6, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    invoke-virtual {v3, v7, v8}, Landroid/view/accessibility/AccessibilityNodeInfo;->addChild(Landroid/view/View;I)V

    .line 349
    .end local v6    # "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    :cond_31
    goto :goto_f

    .line 352
    :cond_32
    return-object v3
.end method

.method public findFocus(I)Landroid/view/accessibility/AccessibilityNodeInfo;
    .locals 1
    .param p1, "focus"    # I

    .line 537
    packed-switch p1, :pswitch_data_0

    goto :goto_0

    .line 539
    :pswitch_0
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v0, :cond_0

    .line 540
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v0, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    invoke-virtual {p0, v0}, Lio/flutter/view/AccessibilityBridge;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    return-object v0

    .line 544
    :cond_0
    :pswitch_1
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v0, :cond_1

    .line 545
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v0, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    invoke-virtual {p0, v0}, Lio/flutter/view/AccessibilityBridge;->createAccessibilityNodeInfo(I)Landroid/view/accessibility/AccessibilityNodeInfo;

    move-result-object v0

    return-object v0

    .line 548
    :cond_1
    :goto_0
    const/4 v0, 0x0

    return-object v0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method handleTouchExploration(FF)V
    .locals 4
    .param p1, "x"    # F
    .param p2, "y"    # F

    .line 585
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mObjects:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 586
    return-void

    .line 588
    :cond_0
    invoke-direct {p0}, Lio/flutter/view/AccessibilityBridge;->getRootObject()Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    move-result-object v0

    const/4 v1, 0x4

    new-array v1, v1, [F

    const/4 v2, 0x0

    aput p1, v1, v2

    const/4 v2, 0x1

    aput p2, v1, v2

    const/4 v2, 0x2

    const/4 v3, 0x0

    aput v3, v1, v2

    const/4 v2, 0x3

    const/high16 v3, 0x3f800000    # 1.0f

    aput v3, v1, v2

    invoke-virtual {v0, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hitTest([F)Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    move-result-object v0

    .line 589
    .local v0, "newObject":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eq v0, v1, :cond_3

    .line 591
    if-eqz v0, :cond_1

    .line 592
    iget v1, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    const/16 v2, 0x80

    invoke-direct {p0, v1, v2}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 594
    :cond_1
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v1, :cond_2

    .line 595
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v1, v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    const/16 v2, 0x100

    invoke-direct {p0, v1, v2}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 597
    :cond_2
    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 599
    :cond_3
    return-void
.end method

.method handleTouchExplorationExit()V
    .locals 2

    .line 578
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v0, :cond_0

    .line 579
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v0, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    const/16 v1, 0x100

    invoke-direct {p0, v0, v1}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 580
    const/4 v0, 0x0

    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 582
    :cond_0
    return-void
.end method

.method public onMessage(Ljava/lang/Object;Lio/flutter/plugin/common/BasicMessageChannel$Reply;)V
    .locals 8
    .param p1, "message"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Object;",
            "Lio/flutter/plugin/common/BasicMessageChannel$Reply<",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 855
    .local p2, "reply":Lio/flutter/plugin/common/BasicMessageChannel$Reply;, "Lio/flutter/plugin/common/BasicMessageChannel$Reply<Ljava/lang/Object;>;"
    move-object v0, p1

    check-cast v0, Ljava/util/HashMap;

    .line 856
    .local v0, "annotatedEvent":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "type"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 858
    .local v1, "type":Ljava/lang/String;
    const-string v2, "data"

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    .line 860
    .local v2, "data":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Ljava/lang/Object;>;"
    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v3

    const v4, -0x43f42ffd

    const/4 v5, 0x1

    const/4 v6, 0x2

    const/4 v7, 0x0

    if-eq v3, v4, :cond_3

    const v4, -0x26b86b97

    if-eq v3, v4, :cond_2

    const v4, 0x1bfa3

    if-eq v3, v4, :cond_1

    const v4, 0x6ce9b27

    if-eq v3, v4, :cond_0

    goto :goto_0

    :cond_0
    const-string v3, "longPress"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x1

    goto :goto_1

    :cond_1
    const-string v3, "tap"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x2

    goto :goto_1

    :cond_2
    const-string v3, "announce"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x0

    goto :goto_1

    :cond_3
    const-string v3, "tooltip"

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    const/4 v3, 0x3

    goto :goto_1

    :cond_4
    :goto_0
    const/4 v3, -0x1

    :goto_1
    packed-switch v3, :pswitch_data_0

    goto :goto_2

    .line 881
    :pswitch_0
    const/16 v3, 0x20

    invoke-direct {p0, v7, v3}, Lio/flutter/view/AccessibilityBridge;->obtainAccessibilityEvent(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v3

    .line 883
    .local v3, "e":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v3}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v4

    const-string v5, "message"

    invoke-virtual {v2, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 884
    invoke-direct {p0, v3}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 885
    goto :goto_2

    .line 873
    .end local v3    # "e":Landroid/view/accessibility/AccessibilityEvent;
    :pswitch_1
    const-string v3, "nodeId"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 874
    .local v3, "nodeId":Ljava/lang/Integer;
    if-nez v3, :cond_5

    .line 875
    return-void

    .line 877
    :cond_5
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {p0, v4, v5}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 878
    goto :goto_2

    .line 865
    .end local v3    # "nodeId":Ljava/lang/Integer;
    :pswitch_2
    const-string v3, "nodeId"

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 866
    .restart local v3    # "nodeId":Ljava/lang/Integer;
    if-nez v3, :cond_6

    .line 867
    return-void

    .line 869
    :cond_6
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-direct {p0, v4, v6}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 870
    goto :goto_2

    .line 862
    .end local v3    # "nodeId":Ljava/lang/Integer;
    :pswitch_3
    iget-object v3, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    const-string v4, "message"

    invoke-virtual {v2, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v3, v4}, Lio/flutter/view/FlutterView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 863
    nop

    .line 888
    :goto_2
    return-void

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public performAction(IILandroid/os/Bundle;)Z
    .locals 7
    .param p1, "virtualViewId"    # I
    .param p2, "action"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;

    .line 357
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mObjects:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 358
    .local v0, "object":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 359
    return v1

    .line 361
    :cond_0
    const/4 v2, 0x4

    const/4 v3, 0x1

    sparse-switch p2, :sswitch_data_0

    .line 485
    sget v2, Lio/flutter/view/AccessibilityBridge;->firstResourceId:I

    sub-int v2, p2, v2

    .line 486
    .local v2, "flutterId":I
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge;->mCustomAccessibilityActions:Ljava/util/Map;

    .line 487
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    .line 488
    .local v4, "contextAction":Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    if-eqz v4, :cond_c

    .line 489
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v5, Lio/flutter/view/AccessibilityBridge$Action;->CUSTOM_ACTION:Lio/flutter/view/AccessibilityBridge$Action;

    iget v6, v4, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->id:I

    .line 490
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    .line 489
    invoke-virtual {v1, p1, v5, v6}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;Ljava/lang/Object;)V

    .line 491
    return v3

    .line 442
    .end local v2    # "flutterId":I
    .end local v4    # "contextAction":Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    :sswitch_0
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->SHOW_ON_SCREEN:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 443
    return v3

    .line 480
    :sswitch_1
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->DISMISS:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 481
    return v3

    .line 446
    :sswitch_2
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 447
    .local v2, "selection":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    if-eqz p3, :cond_1

    const-string v4, "ACTION_ARGUMENT_SELECTION_START_INT"

    .line 448
    invoke-virtual {p3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const-string v4, "ACTION_ARGUMENT_SELECTION_END_INT"

    .line 450
    invoke-virtual {p3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_1

    const/4 v1, 0x1

    nop

    .line 452
    .local v1, "hasSelection":Z
    :cond_1
    if-eqz v1, :cond_2

    .line 453
    const-string v4, "base"

    const-string v5, "ACTION_ARGUMENT_SELECTION_START_INT"

    .line 454
    invoke-virtual {p3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 453
    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 456
    const-string v4, "extent"

    const-string v5, "ACTION_ARGUMENT_SELECTION_END_INT"

    .line 457
    invoke-virtual {p3, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 456
    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 461
    :cond_2
    const-string v4, "base"

    iget v5, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionExtent:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 462
    const-string v4, "extent"

    iget v5, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionExtent:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 464
    :goto_0
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v5, Lio/flutter/view/AccessibilityBridge$Action;->SET_SELECTION:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v4, p1, v5, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;Ljava/lang/Object;)V

    .line 465
    return v3

    .line 472
    .end local v1    # "hasSelection":Z
    .end local v2    # "selection":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Integer;>;"
    :sswitch_3
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->CUT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 473
    return v3

    .line 476
    :sswitch_4
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->PASTE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 477
    return v3

    .line 468
    :sswitch_5
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->COPY:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 469
    return v3

    .line 393
    :sswitch_6
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_DOWN:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v0, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 394
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_DOWN:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    goto :goto_1

    .line 395
    :cond_3
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_RIGHT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v0, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 397
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_RIGHT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    goto :goto_1

    .line 398
    :cond_4
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->DECREASE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v0, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 399
    iget-object v1, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->decreasedValue:Ljava/lang/String;

    iput-object v1, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->value:Ljava/lang/String;

    .line 401
    invoke-direct {p0, p1, v2}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 402
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->DECREASE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 406
    :goto_1
    return v3

    .line 404
    :cond_5
    return v1

    .line 377
    :sswitch_7
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_UP:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v0, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 378
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_UP:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    goto :goto_2

    .line 379
    :cond_6
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_LEFT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v0, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 381
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_LEFT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    goto :goto_2

    .line 382
    :cond_7
    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->INCREASE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v0, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v4

    if-eqz v4, :cond_8

    .line 383
    iget-object v1, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->increasedValue:Ljava/lang/String;

    iput-object v1, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->value:Ljava/lang/String;

    .line 385
    invoke-direct {p0, p1, v2}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 386
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->INCREASE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 390
    :goto_2
    return v3

    .line 388
    :cond_8
    return v1

    .line 409
    :sswitch_8
    invoke-virtual {p0, v0, p1, p3, v1}, Lio/flutter/view/AccessibilityBridge;->performCursorMoveAction(Lio/flutter/view/AccessibilityBridge$SemanticsObject;ILandroid/os/Bundle;Z)Z

    move-result v1

    return v1

    .line 412
    :sswitch_9
    invoke-virtual {p0, v0, p1, p3, v3}, Lio/flutter/view/AccessibilityBridge;->performCursorMoveAction(Lio/flutter/view/AccessibilityBridge$SemanticsObject;ILandroid/os/Bundle;Z)Z

    move-result v1

    return v1

    .line 415
    :sswitch_a
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->DID_LOSE_ACCESSIBILITY_FOCUS:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 416
    const/high16 v1, 0x10000

    invoke-direct {p0, p1, v1}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 418
    const/4 v1, 0x0

    iput-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 419
    return v3

    .line 422
    :sswitch_b
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->DID_GAIN_ACCESSIBILITY_FOCUS:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v4}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 423
    const v1, 0x8000

    invoke-direct {p0, p1, v1}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 426
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-nez v1, :cond_9

    .line 430
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    invoke-virtual {v1}, Lio/flutter/view/FlutterView;->invalidate()V

    .line 432
    :cond_9
    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 434
    sget-object v1, Lio/flutter/view/AccessibilityBridge$Action;->INCREASE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v0, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v1

    if-nez v1, :cond_a

    sget-object v1, Lio/flutter/view/AccessibilityBridge$Action;->DECREASE:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v0, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 436
    :cond_a
    invoke-direct {p0, p1, v2}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 439
    :cond_b
    return v3

    .line 373
    :sswitch_c
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->LONG_PRESS:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 374
    return v3

    .line 366
    :sswitch_d
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->TAP:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v1, p1, v2}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V

    .line 367
    return v3

    .line 494
    :cond_c
    return v1

    :sswitch_data_0
    .sparse-switch
        0x10 -> :sswitch_d
        0x20 -> :sswitch_c
        0x40 -> :sswitch_b
        0x80 -> :sswitch_a
        0x100 -> :sswitch_9
        0x200 -> :sswitch_8
        0x1000 -> :sswitch_7
        0x2000 -> :sswitch_6
        0x4000 -> :sswitch_5
        0x8000 -> :sswitch_4
        0x10000 -> :sswitch_3
        0x20000 -> :sswitch_2
        0x100000 -> :sswitch_1
        0x1020036 -> :sswitch_0
    .end sparse-switch
.end method

.method performCursorMoveAction(Lio/flutter/view/AccessibilityBridge$SemanticsObject;ILandroid/os/Bundle;Z)Z
    .locals 6
    .param p1, "object"    # Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    .param p2, "virtualViewId"    # I
    .param p3, "arguments"    # Landroid/os/Bundle;
    .param p4, "forward"    # Z

    .line 499
    const-string v0, "ACTION_ARGUMENT_MOVEMENT_GRANULARITY_INT"

    .line 500
    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    .line 501
    .local v0, "granularity":I
    const-string v1, "ACTION_ARGUMENT_EXTEND_SELECTION_BOOLEAN"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    .line 503
    .local v1, "extendSelection":Z
    const/4 v2, 0x1

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 518
    :pswitch_0
    if-eqz p4, :cond_0

    sget-object v3, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_FORWARD_BY_WORD:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {p1, v3}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 519
    iget-object v3, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_FORWARD_BY_WORD:Lio/flutter/view/AccessibilityBridge$Action;

    .line 520
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 519
    invoke-virtual {v3, p2, v4, v5}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;Ljava/lang/Object;)V

    .line 521
    return v2

    .line 523
    :cond_0
    if-nez p4, :cond_2

    sget-object v3, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_BACKWARD_BY_WORD:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {p1, v3}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 524
    iget-object v3, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_BACKWARD_BY_WORD:Lio/flutter/view/AccessibilityBridge$Action;

    .line 525
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 524
    invoke-virtual {v3, p2, v4, v5}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;Ljava/lang/Object;)V

    .line 526
    return v2

    .line 505
    :pswitch_1
    if-eqz p4, :cond_1

    sget-object v3, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_FORWARD_BY_CHARACTER:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {p1, v3}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 506
    iget-object v3, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_FORWARD_BY_CHARACTER:Lio/flutter/view/AccessibilityBridge$Action;

    .line 507
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 506
    invoke-virtual {v3, p2, v4, v5}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;Ljava/lang/Object;)V

    .line 508
    return v2

    .line 510
    :cond_1
    if-nez p4, :cond_2

    sget-object v3, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_BACKWARD_BY_CHARACTER:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {p1, v3}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 511
    iget-object v3, p0, Lio/flutter/view/AccessibilityBridge;->mOwner:Lio/flutter/view/FlutterView;

    sget-object v4, Lio/flutter/view/AccessibilityBridge$Action;->MOVE_CURSOR_BACKWARD_BY_CHARACTER:Lio/flutter/view/AccessibilityBridge$Action;

    .line 512
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    .line 511
    invoke-virtual {v3, p2, v4, v5}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;Ljava/lang/Object;)V

    .line 513
    return v2

    .line 530
    :cond_2
    :goto_0
    const/4 v2, 0x0

    return v2

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method reset()V
    .locals 2

    .line 916
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mObjects:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 917
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v0, :cond_0

    .line 918
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v0, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    const/high16 v1, 0x10000

    invoke-direct {p0, v0, v1}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 920
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 921
    iput-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mHoveredObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 922
    const/4 v0, 0x0

    const/16 v1, 0x800

    invoke-direct {p0, v0, v1}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 923
    return-void
.end method

.method setAccessibilityEnabled(Z)V
    .locals 2
    .param p1, "accessibilityEnabled"    # Z

    .line 120
    iput-boolean p1, p0, Lio/flutter/view/AccessibilityBridge;->mAccessibilityEnabled:Z

    .line 121
    if-eqz p1, :cond_0

    .line 122
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mFlutterAccessibilityChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    invoke-virtual {v0, p0}, Lio/flutter/plugin/common/BasicMessageChannel;->setMessageHandler(Lio/flutter/plugin/common/BasicMessageChannel$MessageHandler;)V

    goto :goto_0

    .line 124
    :cond_0
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge;->mFlutterAccessibilityChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lio/flutter/plugin/common/BasicMessageChannel;->setMessageHandler(Lio/flutter/plugin/common/BasicMessageChannel$MessageHandler;)V

    .line 126
    :goto_0
    return-void
.end method

.method updateCustomAccessibilityActions(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V
    .locals 6
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "strings"    # [Ljava/lang/String;

    .line 602
    :goto_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 603
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 604
    .local v0, "id":I
    invoke-direct {p0, v0}, Lio/flutter/view/AccessibilityBridge;->getOrCreateAction(I)Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    move-result-object v1

    .line 605
    .local v1, "action":Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    iput v2, v1, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->overrideId:I

    .line 606
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 607
    .local v2, "stringIndex":I
    const/4 v3, 0x0

    const/4 v4, -0x1

    if-ne v2, v4, :cond_0

    move-object v5, v3

    goto :goto_1

    :cond_0
    aget-object v5, p2, v2

    :goto_1
    iput-object v5, v1, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->label:Ljava/lang/String;

    .line 608
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 609
    if-ne v2, v4, :cond_1

    goto :goto_2

    :cond_1
    aget-object v3, p2, v2

    :goto_2
    iput-object v3, v1, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->hint:Ljava/lang/String;

    .line 610
    .end local v0    # "id":I
    .end local v1    # "action":Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    .end local v2    # "stringIndex":I
    goto :goto_0

    .line 611
    :cond_2
    return-void
.end method

.method updateSemantics(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V
    .locals 19
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "strings"    # [Ljava/lang/String;

    .line 614
    move-object/from16 v0, p0

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 615
    .local v1, "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    :goto_0
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->hasRemaining()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 616
    invoke-virtual/range {p1 .. p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    .line 617
    .local v2, "id":I
    invoke-direct {v0, v2}, Lio/flutter/view/AccessibilityBridge;->getOrCreateObject(I)Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    move-result-object v3

    .line 618
    .local v3, "object":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    move-object/from16 v4, p1

    move-object/from16 v5, p2

    invoke-virtual {v3, v4, v5}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->updateWith(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V

    .line 619
    sget-object v6, Lio/flutter/view/AccessibilityBridge$Flag;->IS_HIDDEN:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v3, v6}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 620
    goto :goto_0

    .line 622
    :cond_0
    sget-object v6, Lio/flutter/view/AccessibilityBridge$Flag;->IS_FOCUSED:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v3, v6}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v6

    if-eqz v6, :cond_1

    .line 623
    iput-object v3, v0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 625
    :cond_1
    iget-boolean v6, v3, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hadPreviousConfig:Z

    if-eqz v6, :cond_2

    .line 626
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 628
    .end local v2    # "id":I
    .end local v3    # "object":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    :cond_2
    goto :goto_0

    .line 630
    :cond_3
    move-object/from16 v4, p1

    move-object/from16 v5, p2

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 631
    .local v2, "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    invoke-direct/range {p0 .. p0}, Lio/flutter/view/AccessibilityBridge;->getRootObject()Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    move-result-object v3

    .line 632
    .local v3, "rootObject":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 633
    .local v6, "newRoutes":Ljava/util/List;, "Ljava/util/List<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    const/4 v7, 0x0

    const/4 v8, 0x1

    if-eqz v3, :cond_6

    .line 634
    const/16 v9, 0x10

    new-array v9, v9, [F

    .line 635
    .local v9, "identity":[F
    invoke-static {v9, v7}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 639
    sget v10, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v11, 0x17

    if-lt v10, v11, :cond_5

    .line 640
    new-instance v10, Landroid/graphics/Rect;

    invoke-direct {v10}, Landroid/graphics/Rect;-><init>()V

    .line 641
    .local v10, "visibleFrame":Landroid/graphics/Rect;
    iget-object v11, v0, Lio/flutter/view/AccessibilityBridge;->mDecorView:Landroid/view/View;

    invoke-virtual {v11, v10}, Landroid/view/View;->getWindowVisibleDisplayFrame(Landroid/graphics/Rect;)V

    .line 642
    iget-object v11, v0, Lio/flutter/view/AccessibilityBridge;->mLastLeftFrameInset:Ljava/lang/Integer;

    iget v12, v10, Landroid/graphics/Rect;->left:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_4

    .line 643
    invoke-static {v3, v8}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->access$102(Lio/flutter/view/AccessibilityBridge$SemanticsObject;Z)Z

    .line 644
    invoke-static {v3, v8}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->access$202(Lio/flutter/view/AccessibilityBridge$SemanticsObject;Z)Z

    .line 646
    :cond_4
    iget v11, v10, Landroid/graphics/Rect;->left:I

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    iput-object v11, v0, Lio/flutter/view/AccessibilityBridge;->mLastLeftFrameInset:Ljava/lang/Integer;

    .line 647
    iget v11, v10, Landroid/graphics/Rect;->left:I

    int-to-float v11, v11

    const/4 v12, 0x0

    invoke-static {v9, v7, v11, v12, v12}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 649
    .end local v10    # "visibleFrame":Landroid/graphics/Rect;
    :cond_5
    invoke-virtual {v3, v9, v2, v7}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->updateRecursively([FLjava/util/Set;Z)V

    .line 650
    invoke-virtual {v3, v6}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->collectRoutes(Ljava/util/List;)V

    .line 655
    .end local v9    # "identity":[F
    :cond_6
    const/4 v9, 0x0

    .line 656
    .local v9, "lastAdded":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_1
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_8

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 657
    .local v11, "semanticsObject":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    iget-object v12, v0, Lio/flutter/view/AccessibilityBridge;->previousRoutes:Ljava/util/List;

    iget v13, v11, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_7

    .line 658
    move-object v9, v11

    .line 660
    .end local v11    # "semanticsObject":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    :cond_7
    goto :goto_1

    .line 661
    :cond_8
    if-nez v9, :cond_9

    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    if-lez v10, :cond_9

    .line 662
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v10

    sub-int/2addr v10, v8

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    move-object v9, v10

    check-cast v9, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 664
    :cond_9
    if-eqz v9, :cond_a

    iget v10, v9, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    iget v11, v0, Lio/flutter/view/AccessibilityBridge;->previousRouteId:I

    if-eq v10, v11, :cond_a

    .line 665
    iget v10, v9, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    iput v10, v0, Lio/flutter/view/AccessibilityBridge;->previousRouteId:I

    .line 666
    invoke-direct {v0, v9}, Lio/flutter/view/AccessibilityBridge;->createWindowChangeEvent(Lio/flutter/view/AccessibilityBridge$SemanticsObject;)V

    .line 668
    :cond_a
    iget-object v10, v0, Lio/flutter/view/AccessibilityBridge;->previousRoutes:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->clear()V

    .line 669
    invoke-interface {v6}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :goto_2
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_b

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 670
    .restart local v11    # "semanticsObject":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    iget-object v12, v0, Lio/flutter/view/AccessibilityBridge;->previousRoutes:Ljava/util/List;

    iget v13, v11, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-interface {v12, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 671
    .end local v11    # "semanticsObject":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    goto :goto_2

    .line 673
    :cond_b
    iget-object v10, v0, Lio/flutter/view/AccessibilityBridge;->mObjects:Ljava/util/Map;

    invoke-interface {v10}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v10

    invoke-interface {v10}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    .line 674
    .local v10, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/lang/Integer;Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;>;"
    :goto_3
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_d

    .line 675
    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v11

    check-cast v11, Ljava/util/Map$Entry;

    .line 676
    .local v11, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    invoke-interface {v11}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 677
    .local v12, "object":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    invoke-interface {v2, v12}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v13

    if-nez v13, :cond_c

    .line 678
    invoke-direct {v0, v12}, Lio/flutter/view/AccessibilityBridge;->willRemoveSemanticsObject(Lio/flutter/view/AccessibilityBridge$SemanticsObject;)V

    .line 679
    invoke-interface {v10}, Ljava/util/Iterator;->remove()V

    .line 681
    .end local v11    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Integer;Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .end local v12    # "object":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    :cond_c
    goto :goto_3

    .line 685
    :cond_d
    const/16 v11, 0x800

    invoke-direct {v0, v7, v11}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 687
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_4
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_26

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 688
    .restart local v12    # "object":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    invoke-virtual {v12}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->didScroll()Z

    move-result v13

    if-eqz v13, :cond_18

    .line 689
    iget v13, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    const/16 v14, 0x1000

    .line 690
    invoke-direct {v0, v13, v14}, Lio/flutter/view/AccessibilityBridge;->obtainAccessibilityEvent(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v13

    .line 694
    .local v13, "event":Landroid/view/accessibility/AccessibilityEvent;
    iget v14, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollPosition:F

    .line 695
    .local v14, "position":F
    iget v15, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollExtentMax:F

    .line 696
    .local v15, "max":F
    iget v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollExtentMax:F

    invoke-static {v11}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v11

    if-eqz v11, :cond_e

    .line 697
    const v15, 0x47c35000    # 100000.0f

    .line 698
    const v11, 0x4788b800    # 70000.0f

    cmpl-float v11, v14, v11

    if-lez v11, :cond_e

    .line 699
    const v14, 0x4788b800    # 70000.0f

    .line 702
    :cond_e
    iget v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollExtentMin:F

    invoke-static {v11}, Ljava/lang/Float;->isInfinite(F)Z

    move-result v11

    if-eqz v11, :cond_10

    .line 703
    const v11, 0x47c35000    # 100000.0f

    add-float/2addr v15, v11

    .line 704
    const v16, -0x38774800    # -70000.0f

    cmpg-float v16, v14, v16

    if-gez v16, :cond_f

    .line 705
    const v14, -0x38774800    # -70000.0f

    .line 707
    :cond_f
    add-float/2addr v14, v11

    goto :goto_5

    .line 709
    :cond_10
    iget v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollExtentMin:F

    sub-float/2addr v15, v11

    .line 710
    iget v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollExtentMin:F

    sub-float/2addr v14, v11

    .line 713
    :goto_5
    sget-object v11, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_UP:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v12, v11}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hadAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v11

    if-nez v11, :cond_13

    sget-object v11, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_DOWN:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v12, v11}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hadAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v11

    if-eqz v11, :cond_11

    goto :goto_6

    .line 716
    :cond_11
    sget-object v11, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_LEFT:Lio/flutter/view/AccessibilityBridge$Action;

    invoke-virtual {v12, v11}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hadAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v11

    if-nez v11, :cond_12

    sget-object v11, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_RIGHT:Lio/flutter/view/AccessibilityBridge$Action;

    .line 717
    invoke-virtual {v12, v11}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hadAction(Lio/flutter/view/AccessibilityBridge$Action;)Z

    move-result v11

    if-eqz v11, :cond_14

    .line 718
    :cond_12
    float-to-int v11, v14

    invoke-virtual {v13, v11}, Landroid/view/accessibility/AccessibilityEvent;->setScrollX(I)V

    .line 719
    float-to-int v11, v15

    invoke-virtual {v13, v11}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollX(I)V

    goto :goto_7

    .line 714
    :cond_13
    :goto_6
    float-to-int v11, v14

    invoke-virtual {v13, v11}, Landroid/view/accessibility/AccessibilityEvent;->setScrollY(I)V

    .line 715
    float-to-int v11, v15

    invoke-virtual {v13, v11}, Landroid/view/accessibility/AccessibilityEvent;->setMaxScrollY(I)V

    .line 721
    :cond_14
    :goto_7
    iget v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollChildren:I

    if-lez v11, :cond_17

    .line 723
    iget v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollChildren:I

    invoke-virtual {v13, v11}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 724
    iget v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollIndex:I

    invoke-virtual {v13, v11}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 725
    const/4 v11, 0x0

    .line 727
    .local v11, "visibleChildren":I
    iget-object v8, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInHitTestOrder:Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_8
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v16

    if-eqz v16, :cond_16

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v16

    move-object/from16 v17, v1

    .end local v1    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .local v17, "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    move-object/from16 v1, v16

    check-cast v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 728
    .local v1, "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    move-object/from16 v18, v2

    .end local v2    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .local v18, "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    sget-object v2, Lio/flutter/view/AccessibilityBridge$Flag;->IS_HIDDEN:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v1, v2}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v2

    if-nez v2, :cond_15

    .line 729
    add-int/lit8 v11, v11, 0x1

    .line 731
    .end local v1    # "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    :cond_15
    nop

    .line 727
    move-object/from16 v1, v17

    move-object/from16 v2, v18

    goto :goto_8

    .line 732
    .end local v17    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .end local v18    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .local v1, "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v2    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    :cond_16
    move-object/from16 v17, v1

    move-object/from16 v18, v2

    .line 733
    .end local v1    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .end local v2    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v17    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v18    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    nop

    .line 742
    iget v1, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollIndex:I

    add-int/2addr v1, v11

    const/4 v2, 0x1

    sub-int/2addr v1, v2

    invoke-virtual {v13, v1}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    goto :goto_9

    .line 744
    .end local v11    # "visibleChildren":I
    .end local v17    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .end local v18    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v1    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v2    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    :cond_17
    move-object/from16 v17, v1

    move-object/from16 v18, v2

    const/4 v2, 0x1

    .end local v1    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .end local v2    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v17    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v18    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    :goto_9
    invoke-direct {v0, v13}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_a

    .line 746
    .end local v13    # "event":Landroid/view/accessibility/AccessibilityEvent;
    .end local v14    # "position":F
    .end local v15    # "max":F
    .end local v17    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .end local v18    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v1    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v2    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    :cond_18
    move-object/from16 v17, v1

    move-object/from16 v18, v2

    const/4 v2, 0x1

    .end local v1    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .end local v2    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v17    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v18    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    :goto_a
    sget-object v1, Lio/flutter/view/AccessibilityBridge$Flag;->IS_LIVE_REGION:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v12, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v1

    if-eqz v1, :cond_1d

    .line 747
    iget-object v1, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    if-nez v1, :cond_19

    const-string v1, ""

    goto :goto_b

    :cond_19
    iget-object v1, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    .line 748
    .local v1, "label":Ljava/lang/String;
    :goto_b
    iget-object v8, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousLabel:Ljava/lang/String;

    if-nez v8, :cond_1a

    const-string v8, ""

    goto :goto_c

    :cond_1a
    iget-object v8, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    .line 749
    .local v8, "previousLabel":Ljava/lang/String;
    :goto_c
    invoke-virtual {v1, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1b

    sget-object v11, Lio/flutter/view/AccessibilityBridge$Flag;->IS_LIVE_REGION:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v12, v11}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hadFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v11

    if-nez v11, :cond_1c

    .line 750
    :cond_1b
    iget v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    const/16 v13, 0x800

    invoke-direct {v0, v11, v13}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    .line 752
    .end local v1    # "label":Ljava/lang/String;
    .end local v8    # "previousLabel":Ljava/lang/String;
    :cond_1c
    goto :goto_d

    :cond_1d
    sget-object v1, Lio/flutter/view/AccessibilityBridge$Flag;->IS_TEXT_FIELD:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v12, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v1

    if-eqz v1, :cond_1e

    invoke-virtual {v12}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->didChangeLabel()Z

    move-result v1

    if-eqz v1, :cond_1e

    iget-object v1, v0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v1, :cond_1e

    iget-object v1, v0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v1, v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    iget v8, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    if-ne v1, v8, :cond_1e

    .line 756
    iget v1, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    const/16 v8, 0x800

    invoke-direct {v0, v1, v8}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(II)V

    goto :goto_e

    .line 758
    :cond_1e
    :goto_d
    const/16 v8, 0x800

    :goto_e
    iget-object v1, v0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v1, :cond_1f

    iget-object v1, v0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v1, v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    iget v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    if-ne v1, v11, :cond_1f

    sget-object v1, Lio/flutter/view/AccessibilityBridge$Flag;->IS_SELECTED:Lio/flutter/view/AccessibilityBridge$Flag;

    .line 759
    invoke-virtual {v12, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hadFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v1

    if-nez v1, :cond_1f

    sget-object v1, Lio/flutter/view/AccessibilityBridge$Flag;->IS_SELECTED:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v12, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v1

    if-eqz v1, :cond_1f

    .line 760
    iget v1, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    const/4 v11, 0x4

    .line 761
    invoke-direct {v0, v1, v11}, Lio/flutter/view/AccessibilityBridge;->obtainAccessibilityEvent(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 762
    .local v1, "event":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v11

    iget-object v13, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    invoke-interface {v11, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 763
    invoke-direct {v0, v1}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 765
    .end local v1    # "event":Landroid/view/accessibility/AccessibilityEvent;
    :cond_1f
    iget-object v1, v0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v1, :cond_25

    iget-object v1, v0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v1, v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    iget v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    if-ne v1, v11, :cond_25

    sget-object v1, Lio/flutter/view/AccessibilityBridge$Flag;->IS_TEXT_FIELD:Lio/flutter/view/AccessibilityBridge$Flag;

    .line 766
    invoke-virtual {v12, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hadFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v1

    if-eqz v1, :cond_25

    sget-object v1, Lio/flutter/view/AccessibilityBridge$Flag;->IS_TEXT_FIELD:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v12, v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v1

    if-eqz v1, :cond_25

    iget-object v1, v0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    if-eqz v1, :cond_20

    iget-object v1, v0, Lio/flutter/view/AccessibilityBridge;->mA11yFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v1, v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    iget-object v11, v0, Lio/flutter/view/AccessibilityBridge;->mInputFocusedObject:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget v11, v11, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    if-ne v1, v11, :cond_25

    .line 772
    :cond_20
    iget-object v1, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousValue:Ljava/lang/String;

    if-eqz v1, :cond_21

    iget-object v1, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousValue:Ljava/lang/String;

    goto :goto_f

    :cond_21
    const-string v1, ""

    .line 773
    .local v1, "oldValue":Ljava/lang/String;
    :goto_f
    iget-object v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->value:Ljava/lang/String;

    if-eqz v11, :cond_22

    iget-object v11, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->value:Ljava/lang/String;

    goto :goto_10

    :cond_22
    const-string v11, ""

    .line 774
    .local v11, "newValue":Ljava/lang/String;
    :goto_10
    iget v13, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    invoke-direct {v0, v13, v1, v11}, Lio/flutter/view/AccessibilityBridge;->createTextChangedEvent(ILjava/lang/String;Ljava/lang/String;)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v13

    .line 775
    .restart local v13    # "event":Landroid/view/accessibility/AccessibilityEvent;
    if-eqz v13, :cond_23

    .line 776
    invoke-direct {v0, v13}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 779
    :cond_23
    iget v14, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousTextSelectionBase:I

    iget v15, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionBase:I

    if-ne v14, v15, :cond_24

    iget v14, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousTextSelectionExtent:I

    iget v15, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionExtent:I

    if-eq v14, v15, :cond_25

    .line 781
    :cond_24
    iget v14, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    const/16 v15, 0x2000

    invoke-direct {v0, v14, v15}, Lio/flutter/view/AccessibilityBridge;->obtainAccessibilityEvent(II)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v14

    .line 783
    .local v14, "selectionEvent":Landroid/view/accessibility/AccessibilityEvent;
    invoke-virtual {v14}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v15

    invoke-interface {v15, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 784
    iget v15, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionBase:I

    invoke-virtual {v14, v15}, Landroid/view/accessibility/AccessibilityEvent;->setFromIndex(I)V

    .line 785
    iget v15, v12, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionExtent:I

    invoke-virtual {v14, v15}, Landroid/view/accessibility/AccessibilityEvent;->setToIndex(I)V

    .line 786
    invoke-virtual {v11}, Ljava/lang/String;->length()I

    move-result v15

    invoke-virtual {v14, v15}, Landroid/view/accessibility/AccessibilityEvent;->setItemCount(I)V

    .line 787
    invoke-direct {v0, v14}, Lio/flutter/view/AccessibilityBridge;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    .line 790
    .end local v1    # "oldValue":Ljava/lang/String;
    .end local v11    # "newValue":Ljava/lang/String;
    .end local v12    # "object":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    .end local v13    # "event":Landroid/view/accessibility/AccessibilityEvent;
    .end local v14    # "selectionEvent":Landroid/view/accessibility/AccessibilityEvent;
    :cond_25
    nop

    .line 687
    move-object/from16 v1, v17

    move-object/from16 v2, v18

    const/4 v8, 0x1

    const/16 v11, 0x800

    goto/16 :goto_4

    .line 791
    .end local v17    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .end local v18    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .local v1, "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v2    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    :cond_26
    move-object/from16 v17, v1

    move-object/from16 v18, v2

    .end local v1    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .end local v2    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v17    # "updated":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    .restart local v18    # "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    return-void
.end method
