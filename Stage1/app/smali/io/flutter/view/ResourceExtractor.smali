.class Lio/flutter/view/ResourceExtractor;
.super Ljava/lang/Object;
.source "ResourceExtractor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/flutter/view/ResourceExtractor$ExtractTask;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final TAG:Ljava/lang/String; = "ResourceExtractor"

.field private static final TIMESTAMP_PREFIX:Ljava/lang/String; = "res_timestamp-"


# instance fields
.field private final mContext:Landroid/content/Context;

.field private mExtractTask:Lio/flutter/view/ResourceExtractor$ExtractTask;

.field private final mResources:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 24
    return-void
.end method

.method constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 129
    iput-object p1, p0, Lio/flutter/view/ResourceExtractor;->mContext:Landroid/content/Context;

    .line 130
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lio/flutter/view/ResourceExtractor;->mResources:Ljava/util/HashSet;

    .line 131
    return-void
.end method

.method static synthetic access$000(Lio/flutter/view/ResourceExtractor;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lio/flutter/view/ResourceExtractor;

    .line 24
    iget-object v0, p0, Lio/flutter/view/ResourceExtractor;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$100(Lio/flutter/view/ResourceExtractor;)V
    .locals 0
    .param p0, "x0"    # Lio/flutter/view/ResourceExtractor;

    .line 24
    invoke-direct {p0}, Lio/flutter/view/ResourceExtractor;->deleteFiles()V

    return-void
.end method

.method static synthetic access$200(Lio/flutter/view/ResourceExtractor;)Ljava/util/HashSet;
    .locals 1
    .param p0, "x0"    # Lio/flutter/view/ResourceExtractor;

    .line 24
    iget-object v0, p0, Lio/flutter/view/ResourceExtractor;->mResources:Ljava/util/HashSet;

    return-object v0
.end method

.method static synthetic access$300(Lio/flutter/view/ResourceExtractor;Ljava/io/File;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lio/flutter/view/ResourceExtractor;
    .param p1, "x1"    # Ljava/io/File;

    .line 24
    invoke-direct {p0, p1}, Lio/flutter/view/ResourceExtractor;->getExistingTimestamps(Ljava/io/File;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private deleteFiles()V
    .locals 6

    .line 174
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lio/flutter/view/ResourceExtractor;->mContext:Landroid/content/Context;

    invoke-static {v1}, Lio/flutter/util/PathUtils;->getDataDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 175
    .local v0, "dataDir":Ljava/io/File;
    iget-object v1, p0, Lio/flutter/view/ResourceExtractor;->mResources:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 176
    .local v2, "resource":Ljava/lang/String;
    new-instance v3, Ljava/io/File;

    invoke-direct {v3, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 177
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 178
    invoke-virtual {v3}, Ljava/io/File;->delete()Z

    .line 180
    .end local v2    # "resource":Ljava/lang/String;
    .end local v3    # "file":Ljava/io/File;
    :cond_0
    goto :goto_0

    .line 181
    :cond_1
    invoke-direct {p0, v0}, Lio/flutter/view/ResourceExtractor;->getExistingTimestamps(Ljava/io/File;)[Ljava/lang/String;

    move-result-object v1

    .line 182
    .local v1, "existingTimestamps":[Ljava/lang/String;
    if-nez v1, :cond_2

    .line 183
    return-void

    .line 185
    :cond_2
    array-length v2, v1

    const/4 v3, 0x0

    :goto_1
    if-ge v3, v2, :cond_3

    aget-object v4, v1, v3

    .line 186
    .local v4, "timestamp":Ljava/lang/String;
    new-instance v5, Ljava/io/File;

    invoke-direct {v5, v0, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v5}, Ljava/io/File;->delete()Z

    .line 185
    .end local v4    # "timestamp":Ljava/lang/String;
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 188
    :cond_3
    return-void
.end method

.method private getExistingTimestamps(Ljava/io/File;)[Ljava/lang/String;
    .locals 1
    .param p1, "dataDir"    # Ljava/io/File;

    .line 165
    new-instance v0, Lio/flutter/view/ResourceExtractor$1;

    invoke-direct {v0, p0}, Lio/flutter/view/ResourceExtractor$1;-><init>(Lio/flutter/view/ResourceExtractor;)V

    invoke-virtual {p1, v0}, Ljava/io/File;->list(Ljava/io/FilenameFilter;)[Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;
    .locals 1
    .param p1, "resource"    # Ljava/lang/String;

    .line 134
    iget-object v0, p0, Lio/flutter/view/ResourceExtractor;->mResources:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 135
    return-object p0
.end method

.method addResources(Ljava/util/Collection;)Lio/flutter/view/ResourceExtractor;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection<",
            "Ljava/lang/String;",
            ">;)",
            "Lio/flutter/view/ResourceExtractor;"
        }
    .end annotation

    .line 139
    .local p1, "resources":Ljava/util/Collection;, "Ljava/util/Collection<Ljava/lang/String;>;"
    iget-object v0, p0, Lio/flutter/view/ResourceExtractor;->mResources:Ljava/util/HashSet;

    invoke-virtual {v0, p1}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 140
    return-object p0
.end method

.method start()Lio/flutter/view/ResourceExtractor;
    .locals 3

    .line 144
    nop

    .line 145
    new-instance v0, Lio/flutter/view/ResourceExtractor$ExtractTask;

    invoke-direct {v0, p0}, Lio/flutter/view/ResourceExtractor$ExtractTask;-><init>(Lio/flutter/view/ResourceExtractor;)V

    iput-object v0, p0, Lio/flutter/view/ResourceExtractor;->mExtractTask:Lio/flutter/view/ResourceExtractor$ExtractTask;

    .line 146
    iget-object v0, p0, Lio/flutter/view/ResourceExtractor;->mExtractTask:Lio/flutter/view/ResourceExtractor$ExtractTask;

    sget-object v1, Landroid/os/AsyncTask;->THREAD_POOL_EXECUTOR:Ljava/util/concurrent/Executor;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2}, Lio/flutter/view/ResourceExtractor$ExtractTask;->executeOnExecutor(Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 147
    return-object p0
.end method

.method waitForCompletion()V
    .locals 1

    .line 151
    nop

    .line 154
    :try_start_0
    iget-object v0, p0, Lio/flutter/view/ResourceExtractor;->mExtractTask:Lio/flutter/view/ResourceExtractor$ExtractTask;

    invoke-virtual {v0}, Lio/flutter/view/ResourceExtractor$ExtractTask;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 159
    :catch_0
    move-exception v0

    .line 160
    .local v0, "e3":Ljava/lang/InterruptedException;
    invoke-direct {p0}, Lio/flutter/view/ResourceExtractor;->deleteFiles()V

    goto :goto_1

    .line 157
    .end local v0    # "e3":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 158
    .local v0, "e2":Ljava/util/concurrent/ExecutionException;
    invoke-direct {p0}, Lio/flutter/view/ResourceExtractor;->deleteFiles()V

    .end local v0    # "e2":Ljava/util/concurrent/ExecutionException;
    goto :goto_0

    .line 155
    :catch_2
    move-exception v0

    .line 156
    .local v0, "e":Ljava/util/concurrent/CancellationException;
    invoke-direct {p0}, Lio/flutter/view/ResourceExtractor;->deleteFiles()V

    .line 161
    .end local v0    # "e":Ljava/util/concurrent/CancellationException;
    :goto_0
    nop

    .line 162
    :goto_1
    return-void
.end method
