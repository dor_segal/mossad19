.class Lio/flutter/view/ResourceCleaner$CleanTask;
.super Landroid/os/AsyncTask;
.source "ResourceCleaner.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/flutter/view/ResourceCleaner;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CleanTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mFilesToDelete:[Ljava/io/File;

.field final synthetic this$0:Lio/flutter/view/ResourceCleaner;


# direct methods
.method constructor <init>(Lio/flutter/view/ResourceCleaner;[Ljava/io/File;)V
    .locals 0
    .param p2, "filesToDelete"    # [Ljava/io/File;

    .line 25
    iput-object p1, p0, Lio/flutter/view/ResourceCleaner$CleanTask;->this$0:Lio/flutter/view/ResourceCleaner;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 26
    iput-object p2, p0, Lio/flutter/view/ResourceCleaner$CleanTask;->mFilesToDelete:[Ljava/io/File;

    .line 27
    return-void
.end method

.method private deleteRecursively(Ljava/io/File;)V
    .locals 4
    .param p1, "parent"    # Ljava/io/File;

    .line 45
    invoke-virtual {p1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    invoke-virtual {p1}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v0

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_0

    aget-object v3, v0, v2

    .line 47
    .local v3, "child":Ljava/io/File;
    invoke-direct {p0, v3}, Lio/flutter/view/ResourceCleaner$CleanTask;->deleteRecursively(Ljava/io/File;)V

    .line 46
    .end local v3    # "child":Ljava/io/File;
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 50
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 51
    return-void
.end method


# virtual methods
.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0

    .line 22
    check-cast p1, [Ljava/lang/Void;

    invoke-virtual {p0, p1}, Lio/flutter/view/ResourceCleaner$CleanTask;->doInBackground([Ljava/lang/Void;)Ljava/lang/Void;

    move-result-object p1

    return-object p1
.end method

.method protected varargs doInBackground([Ljava/lang/Void;)Ljava/lang/Void;
    .locals 5
    .param p1, "unused"    # [Ljava/lang/Void;

    .line 35
    const-string v0, "ResourceCleaner"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Cleaning "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lio/flutter/view/ResourceCleaner$CleanTask;->mFilesToDelete:[Ljava/io/File;

    array-length v2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " resources."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    iget-object v0, p0, Lio/flutter/view/ResourceCleaner$CleanTask;->mFilesToDelete:[Ljava/io/File;

    array-length v1, v0

    const/4 v2, 0x0

    :goto_0
    if-ge v2, v1, :cond_1

    aget-object v3, v0, v2

    .line 37
    .local v3, "file":Ljava/io/File;
    invoke-virtual {v3}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 38
    invoke-direct {p0, v3}, Lio/flutter/view/ResourceCleaner$CleanTask;->deleteRecursively(Ljava/io/File;)V

    .line 36
    .end local v3    # "file":Ljava/io/File;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 41
    :cond_1
    const/4 v0, 0x0

    return-object v0
.end method

.method hasFilesToDelete()Z
    .locals 1

    .line 30
    iget-object v0, p0, Lio/flutter/view/ResourceCleaner$CleanTask;->mFilesToDelete:[Ljava/io/File;

    array-length v0, v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method
