.class final enum Lio/flutter/view/FlutterView$AccessibilityFeature;
.super Ljava/lang/Enum;
.source "FlutterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/flutter/view/FlutterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "AccessibilityFeature"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum<",
        "Lio/flutter/view/FlutterView$AccessibilityFeature;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lio/flutter/view/FlutterView$AccessibilityFeature;

.field public static final enum ACCESSIBLE_NAVIGATION:Lio/flutter/view/FlutterView$AccessibilityFeature;

.field public static final enum DISABLE_ANIMATIONS:Lio/flutter/view/FlutterView$AccessibilityFeature;

.field public static final enum INVERT_COLORS:Lio/flutter/view/FlutterView$AccessibilityFeature;


# instance fields
.field final value:I


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .line 943
    new-instance v0, Lio/flutter/view/FlutterView$AccessibilityFeature;

    const-string v1, "ACCESSIBLE_NAVIGATION"

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-direct {v0, v1, v2, v3}, Lio/flutter/view/FlutterView$AccessibilityFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/flutter/view/FlutterView$AccessibilityFeature;->ACCESSIBLE_NAVIGATION:Lio/flutter/view/FlutterView$AccessibilityFeature;

    .line 944
    new-instance v0, Lio/flutter/view/FlutterView$AccessibilityFeature;

    const-string v1, "INVERT_COLORS"

    const/4 v4, 0x2

    invoke-direct {v0, v1, v3, v4}, Lio/flutter/view/FlutterView$AccessibilityFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/flutter/view/FlutterView$AccessibilityFeature;->INVERT_COLORS:Lio/flutter/view/FlutterView$AccessibilityFeature;

    .line 945
    new-instance v0, Lio/flutter/view/FlutterView$AccessibilityFeature;

    const-string v1, "DISABLE_ANIMATIONS"

    const/4 v5, 0x4

    invoke-direct {v0, v1, v4, v5}, Lio/flutter/view/FlutterView$AccessibilityFeature;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lio/flutter/view/FlutterView$AccessibilityFeature;->DISABLE_ANIMATIONS:Lio/flutter/view/FlutterView$AccessibilityFeature;

    .line 942
    const/4 v0, 0x3

    new-array v0, v0, [Lio/flutter/view/FlutterView$AccessibilityFeature;

    sget-object v1, Lio/flutter/view/FlutterView$AccessibilityFeature;->ACCESSIBLE_NAVIGATION:Lio/flutter/view/FlutterView$AccessibilityFeature;

    aput-object v1, v0, v2

    sget-object v1, Lio/flutter/view/FlutterView$AccessibilityFeature;->INVERT_COLORS:Lio/flutter/view/FlutterView$AccessibilityFeature;

    aput-object v1, v0, v3

    sget-object v1, Lio/flutter/view/FlutterView$AccessibilityFeature;->DISABLE_ANIMATIONS:Lio/flutter/view/FlutterView$AccessibilityFeature;

    aput-object v1, v0, v4

    sput-object v0, Lio/flutter/view/FlutterView$AccessibilityFeature;->$VALUES:[Lio/flutter/view/FlutterView$AccessibilityFeature;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .line 947
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 948
    iput p3, p0, Lio/flutter/view/FlutterView$AccessibilityFeature;->value:I

    .line 949
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lio/flutter/view/FlutterView$AccessibilityFeature;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .line 942
    const-class v0, Lio/flutter/view/FlutterView$AccessibilityFeature;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lio/flutter/view/FlutterView$AccessibilityFeature;

    return-object v0
.end method

.method public static values()[Lio/flutter/view/FlutterView$AccessibilityFeature;
    .locals 1

    .line 942
    sget-object v0, Lio/flutter/view/FlutterView$AccessibilityFeature;->$VALUES:[Lio/flutter/view/FlutterView$AccessibilityFeature;

    invoke-virtual {v0}, [Lio/flutter/view/FlutterView$AccessibilityFeature;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lio/flutter/view/FlutterView$AccessibilityFeature;

    return-object v0
.end method
