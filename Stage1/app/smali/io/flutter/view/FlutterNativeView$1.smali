.class Lio/flutter/view/FlutterNativeView$1;
.super Ljava/lang/Object;
.source "FlutterNativeView.java"

# interfaces
.implements Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lio/flutter/view/FlutterNativeView;->handlePlatformMessage(Ljava/lang/String;[BI)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field private final done:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final synthetic this$0:Lio/flutter/view/FlutterNativeView;

.field final synthetic val$channel:Ljava/lang/String;

.field final synthetic val$replyId:I


# direct methods
.method constructor <init>(Lio/flutter/view/FlutterNativeView;Ljava/lang/String;I)V
    .locals 0
    .param p1, "this$0"    # Lio/flutter/view/FlutterNativeView;

    .line 163
    iput-object p1, p0, Lio/flutter/view/FlutterNativeView$1;->this$0:Lio/flutter/view/FlutterNativeView;

    iput-object p2, p0, Lio/flutter/view/FlutterNativeView$1;->val$channel:Ljava/lang/String;

    iput p3, p0, Lio/flutter/view/FlutterNativeView$1;->val$replyId:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 164
    new-instance p2, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 p3, 0x0

    invoke-direct {p2, p3}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object p2, p0, Lio/flutter/view/FlutterNativeView$1;->done:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method


# virtual methods
.method public reply(Ljava/nio/ByteBuffer;)V
    .locals 4
    .param p1, "reply"    # Ljava/nio/ByteBuffer;

    .line 167
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView$1;->this$0:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 168
    const-string v0, "FlutterNativeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "handlePlatformMessage replying to a detached view, channel="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lio/flutter/view/FlutterNativeView$1;->val$channel:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    return-void

    .line 173
    :cond_0
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView$1;->done:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-nez v0, :cond_2

    .line 176
    if-nez p1, :cond_1

    .line 177
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView$1;->this$0:Lio/flutter/view/FlutterNativeView;

    .line 178
    invoke-static {v0}, Lio/flutter/view/FlutterNativeView;->access$000(Lio/flutter/view/FlutterNativeView;)J

    move-result-wide v0

    iget v2, p0, Lio/flutter/view/FlutterNativeView$1;->val$replyId:I

    .line 177
    invoke-static {v0, v1, v2}, Lio/flutter/view/FlutterNativeView;->access$100(JI)V

    goto :goto_0

    .line 180
    :cond_1
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView$1;->this$0:Lio/flutter/view/FlutterNativeView;

    .line 181
    invoke-static {v0}, Lio/flutter/view/FlutterNativeView;->access$000(Lio/flutter/view/FlutterNativeView;)J

    move-result-wide v0

    iget v2, p0, Lio/flutter/view/FlutterNativeView$1;->val$replyId:I

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v3

    .line 180
    invoke-static {v0, v1, v2, p1, v3}, Lio/flutter/view/FlutterNativeView;->access$200(JILjava/nio/ByteBuffer;I)V

    .line 183
    :goto_0
    return-void

    .line 174
    :cond_2
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Reply already submitted"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
