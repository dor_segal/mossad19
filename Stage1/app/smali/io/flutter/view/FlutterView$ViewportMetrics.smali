.class final Lio/flutter/view/FlutterView$ViewportMetrics;
.super Ljava/lang/Object;
.source "FlutterView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/flutter/view/FlutterView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "ViewportMetrics"
.end annotation


# instance fields
.field devicePixelRatio:F

.field physicalHeight:I

.field physicalPaddingBottom:I

.field physicalPaddingLeft:I

.field physicalPaddingRight:I

.field physicalPaddingTop:I

.field physicalViewInsetBottom:I

.field physicalViewInsetLeft:I

.field physicalViewInsetRight:I

.field physicalViewInsetTop:I

.field physicalWidth:I


# direct methods
.method constructor <init>()V
    .locals 1

    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->devicePixelRatio:F

    .line 69
    const/4 v0, 0x0

    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalWidth:I

    .line 70
    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalHeight:I

    .line 71
    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingTop:I

    .line 72
    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingRight:I

    .line 73
    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingBottom:I

    .line 74
    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingLeft:I

    .line 75
    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetTop:I

    .line 76
    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetRight:I

    .line 77
    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetBottom:I

    .line 78
    iput v0, p0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetLeft:I

    return-void
.end method
