.class public Lio/flutter/view/FlutterMain;
.super Ljava/lang/Object;
.source "FlutterMain.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/flutter/view/FlutterMain$Settings;,
        Lio/flutter/view/FlutterMain$ImmutableSetBuilder;
    }
.end annotation


# static fields
.field private static final AOT_ISOLATE_SNAPSHOT_DATA_KEY:Ljava/lang/String; = "isolate-snapshot-data"

.field private static final AOT_ISOLATE_SNAPSHOT_INSTR_KEY:Ljava/lang/String; = "isolate-snapshot-instr"

.field private static final AOT_SHARED_LIBRARY_PATH:Ljava/lang/String; = "aot-shared-library-path"

.field private static final AOT_SNAPSHOT_PATH_KEY:Ljava/lang/String; = "aot-snapshot-path"

.field private static final AOT_VM_SNAPSHOT_DATA_KEY:Ljava/lang/String; = "vm-snapshot-data"

.field private static final AOT_VM_SNAPSHOT_INSTR_KEY:Ljava/lang/String; = "vm-snapshot-instr"

.field private static final DEFAULT_AOT_ISOLATE_SNAPSHOT_DATA:Ljava/lang/String; = "isolate_snapshot_data"

.field private static final DEFAULT_AOT_ISOLATE_SNAPSHOT_INSTR:Ljava/lang/String; = "isolate_snapshot_instr"

.field private static final DEFAULT_AOT_SHARED_LIBRARY_PATH:Ljava/lang/String; = "app.so"

.field private static final DEFAULT_AOT_VM_SNAPSHOT_DATA:Ljava/lang/String; = "vm_snapshot_data"

.field private static final DEFAULT_AOT_VM_SNAPSHOT_INSTR:Ljava/lang/String; = "vm_snapshot_instr"

.field private static final DEFAULT_FLUTTER_ASSETS_DIR:Ljava/lang/String; = "flutter_assets"

.field private static final DEFAULT_FLX:Ljava/lang/String; = "app.flx"

.field private static final DEFAULT_KERNEL_BLOB:Ljava/lang/String; = "kernel_blob.bin"

.field private static final FLUTTER_ASSETS_DIR_KEY:Ljava/lang/String; = "flutter-assets-dir"

.field private static final FLX_KEY:Ljava/lang/String; = "flx"

.field public static final PUBLIC_AOT_AOT_SHARED_LIBRARY_PATH:Ljava/lang/String;

.field public static final PUBLIC_AOT_ISOLATE_SNAPSHOT_DATA_KEY:Ljava/lang/String;

.field public static final PUBLIC_AOT_ISOLATE_SNAPSHOT_INSTR_KEY:Ljava/lang/String;

.field public static final PUBLIC_AOT_VM_SNAPSHOT_DATA_KEY:Ljava/lang/String;

.field public static final PUBLIC_AOT_VM_SNAPSHOT_INSTR_KEY:Ljava/lang/String;

.field public static final PUBLIC_FLUTTER_ASSETS_DIR_KEY:Ljava/lang/String;

.field public static final PUBLIC_FLX_KEY:Ljava/lang/String;

.field private static final SHARED_ASSET_DIR:Ljava/lang/String; = "flutter_shared"

.field private static final SHARED_ASSET_ICU_DATA:Ljava/lang/String; = "icudtl.dat"

.field private static final TAG:Ljava/lang/String; = "FlutterMain"

.field private static sAotIsolateSnapshotData:Ljava/lang/String;

.field private static sAotIsolateSnapshotInstr:Ljava/lang/String;

.field private static sAotSharedLibraryPath:Ljava/lang/String;

.field private static sAotVmSnapshotData:Ljava/lang/String;

.field private static sAotVmSnapshotInstr:Ljava/lang/String;

.field private static sFlutterAssetsDir:Ljava/lang/String;

.field private static sFlx:Ljava/lang/String;

.field private static sIcuDataPath:Ljava/lang/String;

.field private static sInitialized:Z

.field private static sIsPrecompiledAsBlobs:Z

.field private static sIsPrecompiledAsSharedLibrary:Z

.field private static sResourceExtractor:Lio/flutter/view/ResourceExtractor;

.field private static sSettings:Lio/flutter/view/FlutterMain$Settings;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .line 37
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lio/flutter/view/FlutterMain;

    .line 38
    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "aot-shared-library-path"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/flutter/view/FlutterMain;->PUBLIC_AOT_AOT_SHARED_LIBRARY_PATH:Ljava/lang/String;

    .line 39
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lio/flutter/view/FlutterMain;

    .line 40
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "vm-snapshot-data"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/flutter/view/FlutterMain;->PUBLIC_AOT_VM_SNAPSHOT_DATA_KEY:Ljava/lang/String;

    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lio/flutter/view/FlutterMain;

    .line 42
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "vm-snapshot-instr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/flutter/view/FlutterMain;->PUBLIC_AOT_VM_SNAPSHOT_INSTR_KEY:Ljava/lang/String;

    .line 43
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lio/flutter/view/FlutterMain;

    .line 44
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "isolate-snapshot-data"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/flutter/view/FlutterMain;->PUBLIC_AOT_ISOLATE_SNAPSHOT_DATA_KEY:Ljava/lang/String;

    .line 45
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lio/flutter/view/FlutterMain;

    .line 46
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "isolate-snapshot-instr"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/flutter/view/FlutterMain;->PUBLIC_AOT_ISOLATE_SNAPSHOT_INSTR_KEY:Ljava/lang/String;

    .line 47
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lio/flutter/view/FlutterMain;

    .line 48
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v2, "flx"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/flutter/view/FlutterMain;->PUBLIC_FLX_KEY:Ljava/lang/String;

    .line 49
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v2, Lio/flutter/view/FlutterMain;

    .line 50
    invoke-virtual {v2}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    const-string v1, "flutter-assets-dir"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lio/flutter/view/FlutterMain;->PUBLIC_FLUTTER_ASSETS_DIR_KEY:Ljava/lang/String;

    .line 71
    const-string v0, "app.so"

    sput-object v0, Lio/flutter/view/FlutterMain;->sAotSharedLibraryPath:Ljava/lang/String;

    .line 72
    const-string v0, "vm_snapshot_data"

    sput-object v0, Lio/flutter/view/FlutterMain;->sAotVmSnapshotData:Ljava/lang/String;

    .line 73
    const-string v0, "vm_snapshot_instr"

    sput-object v0, Lio/flutter/view/FlutterMain;->sAotVmSnapshotInstr:Ljava/lang/String;

    .line 74
    const-string v0, "isolate_snapshot_data"

    sput-object v0, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotData:Ljava/lang/String;

    .line 75
    const-string v0, "isolate_snapshot_instr"

    sput-object v0, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotInstr:Ljava/lang/String;

    .line 76
    const-string v0, "app.flx"

    sput-object v0, Lio/flutter/view/FlutterMain;->sFlx:Ljava/lang/String;

    .line 77
    const-string v0, "flutter_assets"

    sput-object v0, Lio/flutter/view/FlutterMain;->sFlutterAssetsDir:Ljava/lang/String;

    .line 79
    const/4 v0, 0x0

    sput-boolean v0, Lio/flutter/view/FlutterMain;->sInitialized:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    return-void
.end method

.method public static ensureInitializationComplete(Landroid/content/Context;[Ljava/lang/String;)V
    .locals 5
    .param p0, "applicationContext"    # Landroid/content/Context;
    .param p1, "args"    # [Ljava/lang/String;

    .line 174
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_6

    .line 177
    sget-object v0, Lio/flutter/view/FlutterMain;->sSettings:Lio/flutter/view/FlutterMain$Settings;

    if-eqz v0, :cond_5

    .line 180
    sget-boolean v0, Lio/flutter/view/FlutterMain;->sInitialized:Z

    if-eqz v0, :cond_0

    .line 181
    return-void

    .line 184
    :cond_0
    :try_start_0
    sget-object v0, Lio/flutter/view/FlutterMain;->sResourceExtractor:Lio/flutter/view/ResourceExtractor;

    invoke-virtual {v0}, Lio/flutter/view/ResourceExtractor;->waitForCompletion()V

    .line 186
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 187
    .local v0, "shellArgs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--icu-data-file-path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lio/flutter/view/FlutterMain;->sIcuDataPath:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 188
    if-eqz p1, :cond_1

    .line 189
    invoke-static {v0, p1}, Ljava/util/Collections;->addAll(Ljava/util/Collection;[Ljava/lang/Object;)Z

    .line 191
    :cond_1
    sget-boolean v1, Lio/flutter/view/FlutterMain;->sIsPrecompiledAsSharedLibrary:Z

    if-eqz v1, :cond_2

    .line 192
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--aot-shared-library-path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v2, Ljava/io/File;

    .line 193
    invoke-static {p0}, Lio/flutter/util/PathUtils;->getDataDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lio/flutter/view/FlutterMain;->sAotSharedLibraryPath:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 192
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 195
    :cond_2
    sget-boolean v1, Lio/flutter/view/FlutterMain;->sIsPrecompiledAsBlobs:Z

    if-eqz v1, :cond_3

    .line 196
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--aot-snapshot-path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 197
    invoke-static {p0}, Lio/flutter/util/PathUtils;->getDataDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 196
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 199
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--cache-dir-path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 200
    invoke-static {p0}, Lio/flutter/util/PathUtils;->getCacheDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 199
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 202
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--aot-snapshot-path="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 203
    invoke-static {p0}, Lio/flutter/util/PathUtils;->getDataDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "/"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lio/flutter/view/FlutterMain;->sFlutterAssetsDir:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 202
    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 205
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--vm-snapshot-data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lio/flutter/view/FlutterMain;->sAotVmSnapshotData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 206
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--vm-snapshot-instr="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lio/flutter/view/FlutterMain;->sAotVmSnapshotInstr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 207
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--isolate-snapshot-data="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotData:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 208
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--isolate-snapshot-instr="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotInstr:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 211
    :goto_1
    sget-object v1, Lio/flutter/view/FlutterMain;->sSettings:Lio/flutter/view/FlutterMain$Settings;

    invoke-virtual {v1}, Lio/flutter/view/FlutterMain$Settings;->getLogTag()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_4

    .line 212
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "--log-tag="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Lio/flutter/view/FlutterMain;->sSettings:Lio/flutter/view/FlutterMain$Settings;

    invoke-virtual {v2}, Lio/flutter/view/FlutterMain$Settings;->getLogTag()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 215
    :cond_4
    invoke-static {p0}, Lio/flutter/view/FlutterMain;->findAppBundlePath(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 216
    .local v1, "appBundlePath":Ljava/lang/String;
    invoke-static {p0}, Lio/flutter/util/PathUtils;->getFilesDir(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 217
    .local v2, "appStoragePath":Ljava/lang/String;
    invoke-static {p0}, Lio/flutter/util/PathUtils;->getCacheDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 218
    .local v3, "engineCachesPath":Ljava/lang/String;
    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v4

    check-cast v4, [Ljava/lang/String;

    invoke-static {p0, v4, v1, v2, v3}, Lio/flutter/view/FlutterMain;->nativeInit(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    const/4 v4, 0x1

    sput-boolean v4, Lio/flutter/view/FlutterMain;->sInitialized:Z
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    .end local v0    # "shellArgs":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v1    # "appBundlePath":Ljava/lang/String;
    .end local v2    # "appStoragePath":Ljava/lang/String;
    .end local v3    # "engineCachesPath":Ljava/lang/String;
    nop

    .line 226
    return-void

    .line 222
    :catch_0
    move-exception v0

    .line 223
    .local v0, "e":Ljava/lang/Exception;
    const-string v1, "FlutterMain"

    const-string v2, "Flutter initialization failed."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 224
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 178
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ensureInitializationComplete must be called after startInitialization"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_6
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "ensureInitializationComplete must be called on the main thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static findAppBundlePath(Landroid/content/Context;)Ljava/lang/String;
    .locals 3
    .param p0, "applicationContext"    # Landroid/content/Context;

    .line 319
    invoke-static {p0}, Lio/flutter/util/PathUtils;->getDataDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 320
    .local v0, "dataDirectory":Ljava/lang/String;
    new-instance v1, Ljava/io/File;

    sget-object v2, Lio/flutter/view/FlutterMain;->sFlutterAssetsDir:Ljava/lang/String;

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 321
    .local v1, "appBundle":Ljava/io/File;
    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v2

    goto :goto_0

    :cond_0
    const/4 v2, 0x0

    :goto_0
    return-object v2
.end method

.method private static fromFlutterAssets(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "filePath"    # Ljava/lang/String;

    .line 67
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v1, Lio/flutter/view/FlutterMain;->sFlutterAssetsDir:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLookupKeyForAsset(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "asset"    # Ljava/lang/String;

    .line 333
    invoke-static {p0}, Lio/flutter/view/FlutterMain;->fromFlutterAssets(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getLookupKeyForAsset(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "asset"    # Ljava/lang/String;
    .param p1, "packageName"    # Ljava/lang/String;

    .line 346
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "packages"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v1, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lio/flutter/view/FlutterMain;->getLookupKeyForAsset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static initAot(Landroid/content/Context;)V
    .locals 4
    .param p0, "applicationContext"    # Landroid/content/Context;

    .line 300
    const-string v0, ""

    invoke-static {p0, v0}, Lio/flutter/view/FlutterMain;->listAssets(Landroid/content/Context;Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    .line 301
    .local v0, "assets":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/String;>;"
    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/String;

    sget-object v2, Lio/flutter/view/FlutterMain;->sAotVmSnapshotData:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    sget-object v2, Lio/flutter/view/FlutterMain;->sAotVmSnapshotInstr:Ljava/lang/String;

    const/4 v3, 0x1

    aput-object v2, v1, v3

    sget-object v2, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotData:Ljava/lang/String;

    const/4 v3, 0x2

    aput-object v2, v1, v3

    sget-object v2, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotInstr:Ljava/lang/String;

    const/4 v3, 0x3

    aput-object v2, v1, v3

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    sput-boolean v1, Lio/flutter/view/FlutterMain;->sIsPrecompiledAsBlobs:Z

    .line 307
    sget-object v1, Lio/flutter/view/FlutterMain;->sAotSharedLibraryPath:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    sput-boolean v1, Lio/flutter/view/FlutterMain;->sIsPrecompiledAsSharedLibrary:Z

    .line 308
    sget-boolean v1, Lio/flutter/view/FlutterMain;->sIsPrecompiledAsBlobs:Z

    if-eqz v1, :cond_1

    sget-boolean v1, Lio/flutter/view/FlutterMain;->sIsPrecompiledAsSharedLibrary:Z

    if-nez v1, :cond_0

    goto :goto_0

    .line 309
    :cond_0
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Found precompiled app as shared library and as Dart VM snapshots."

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 312
    :cond_1
    :goto_0
    return-void
.end method

.method private static initConfig(Landroid/content/Context;)V
    .locals 3
    .param p0, "applicationContext"    # Landroid/content/Context;

    .line 237
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 238
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x80

    .line 237
    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    .line 239
    .local v0, "metadata":Landroid/os/Bundle;
    if-eqz v0, :cond_0

    .line 240
    sget-object v1, Lio/flutter/view/FlutterMain;->PUBLIC_AOT_AOT_SHARED_LIBRARY_PATH:Ljava/lang/String;

    const-string v2, "app.so"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lio/flutter/view/FlutterMain;->sAotSharedLibraryPath:Ljava/lang/String;

    .line 241
    sget-object v1, Lio/flutter/view/FlutterMain;->PUBLIC_AOT_VM_SNAPSHOT_DATA_KEY:Ljava/lang/String;

    const-string v2, "vm_snapshot_data"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lio/flutter/view/FlutterMain;->sAotVmSnapshotData:Ljava/lang/String;

    .line 242
    sget-object v1, Lio/flutter/view/FlutterMain;->PUBLIC_AOT_VM_SNAPSHOT_INSTR_KEY:Ljava/lang/String;

    const-string v2, "vm_snapshot_instr"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lio/flutter/view/FlutterMain;->sAotVmSnapshotInstr:Ljava/lang/String;

    .line 243
    sget-object v1, Lio/flutter/view/FlutterMain;->PUBLIC_AOT_ISOLATE_SNAPSHOT_DATA_KEY:Ljava/lang/String;

    const-string v2, "isolate_snapshot_data"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotData:Ljava/lang/String;

    .line 244
    sget-object v1, Lio/flutter/view/FlutterMain;->PUBLIC_AOT_ISOLATE_SNAPSHOT_INSTR_KEY:Ljava/lang/String;

    const-string v2, "isolate_snapshot_instr"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotInstr:Ljava/lang/String;

    .line 245
    sget-object v1, Lio/flutter/view/FlutterMain;->PUBLIC_FLX_KEY:Ljava/lang/String;

    const-string v2, "app.flx"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lio/flutter/view/FlutterMain;->sFlx:Ljava/lang/String;

    .line 246
    sget-object v1, Lio/flutter/view/FlutterMain;->PUBLIC_FLUTTER_ASSETS_DIR_KEY:Ljava/lang/String;

    const-string v2, "flutter_assets"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lio/flutter/view/FlutterMain;->sFlutterAssetsDir:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 250
    .end local v0    # "metadata":Landroid/os/Bundle;
    :cond_0
    nop

    .line 251
    return-void

    .line 248
    :catch_0
    move-exception v0

    .line 249
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method private static initResources(Landroid/content/Context;)V
    .locals 4
    .param p0, "applicationContext"    # Landroid/content/Context;

    .line 254
    move-object v0, p0

    .line 255
    .local v0, "context":Landroid/content/Context;
    new-instance v1, Lio/flutter/view/ResourceCleaner;

    invoke-direct {v1, v0}, Lio/flutter/view/ResourceCleaner;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1}, Lio/flutter/view/ResourceCleaner;->start()V

    .line 257
    new-instance v1, Lio/flutter/view/ResourceExtractor;

    invoke-direct {v1, v0}, Lio/flutter/view/ResourceExtractor;-><init>(Landroid/content/Context;)V

    sput-object v1, Lio/flutter/view/FlutterMain;->sResourceExtractor:Lio/flutter/view/ResourceExtractor;

    .line 259
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "flutter_shared"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "icudtl.dat"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 260
    .local v1, "icuAssetPath":Ljava/lang/String;
    sget-object v2, Lio/flutter/view/FlutterMain;->sResourceExtractor:Lio/flutter/view/ResourceExtractor;

    invoke-virtual {v2, v1}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    .line 261
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {p0}, Lio/flutter/util/PathUtils;->getDataDirectory(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    sget-object v3, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lio/flutter/view/FlutterMain;->sIcuDataPath:Ljava/lang/String;

    .line 263
    sget-object v2, Lio/flutter/view/FlutterMain;->sResourceExtractor:Lio/flutter/view/ResourceExtractor;

    sget-object v3, Lio/flutter/view/FlutterMain;->sFlx:Ljava/lang/String;

    .line 264
    invoke-static {v3}, Lio/flutter/view/FlutterMain;->fromFlutterAssets(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    move-result-object v2

    sget-object v3, Lio/flutter/view/FlutterMain;->sAotVmSnapshotData:Ljava/lang/String;

    .line 265
    invoke-static {v3}, Lio/flutter/view/FlutterMain;->fromFlutterAssets(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    move-result-object v2

    sget-object v3, Lio/flutter/view/FlutterMain;->sAotVmSnapshotInstr:Ljava/lang/String;

    .line 266
    invoke-static {v3}, Lio/flutter/view/FlutterMain;->fromFlutterAssets(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    move-result-object v2

    sget-object v3, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotData:Ljava/lang/String;

    .line 267
    invoke-static {v3}, Lio/flutter/view/FlutterMain;->fromFlutterAssets(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    move-result-object v2

    sget-object v3, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotInstr:Ljava/lang/String;

    .line 268
    invoke-static {v3}, Lio/flutter/view/FlutterMain;->fromFlutterAssets(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    move-result-object v2

    const-string v3, "kernel_blob.bin"

    .line 269
    invoke-static {v3}, Lio/flutter/view/FlutterMain;->fromFlutterAssets(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    .line 270
    sget-boolean v2, Lio/flutter/view/FlutterMain;->sIsPrecompiledAsSharedLibrary:Z

    if-eqz v2, :cond_0

    .line 271
    sget-object v2, Lio/flutter/view/FlutterMain;->sResourceExtractor:Lio/flutter/view/ResourceExtractor;

    sget-object v3, Lio/flutter/view/FlutterMain;->sAotSharedLibraryPath:Ljava/lang/String;

    .line 272
    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    goto :goto_0

    .line 274
    :cond_0
    sget-object v2, Lio/flutter/view/FlutterMain;->sResourceExtractor:Lio/flutter/view/ResourceExtractor;

    sget-object v3, Lio/flutter/view/FlutterMain;->sAotVmSnapshotData:Ljava/lang/String;

    .line 275
    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    move-result-object v2

    sget-object v3, Lio/flutter/view/FlutterMain;->sAotVmSnapshotInstr:Ljava/lang/String;

    .line 276
    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    move-result-object v2

    sget-object v3, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotData:Ljava/lang/String;

    .line 277
    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    move-result-object v2

    sget-object v3, Lio/flutter/view/FlutterMain;->sAotIsolateSnapshotInstr:Ljava/lang/String;

    .line 278
    invoke-virtual {v2, v3}, Lio/flutter/view/ResourceExtractor;->addResource(Ljava/lang/String;)Lio/flutter/view/ResourceExtractor;

    .line 280
    :goto_0
    sget-object v2, Lio/flutter/view/FlutterMain;->sResourceExtractor:Lio/flutter/view/ResourceExtractor;

    invoke-virtual {v2}, Lio/flutter/view/ResourceExtractor;->start()Lio/flutter/view/ResourceExtractor;

    .line 281
    return-void
.end method

.method public static isRunningPrecompiledCode()Z
    .locals 1

    .line 315
    sget-boolean v0, Lio/flutter/view/FlutterMain;->sIsPrecompiledAsBlobs:Z

    if-nez v0, :cond_1

    sget-boolean v0, Lio/flutter/view/FlutterMain;->sIsPrecompiledAsSharedLibrary:Z

    if-eqz v0, :cond_0

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :goto_0
    const/4 v0, 0x1

    :goto_1
    return v0
.end method

.method private static listAssets(Landroid/content/Context;Ljava/lang/String;)Ljava/util/Set;
    .locals 4
    .param p0, "applicationContext"    # Landroid/content/Context;
    .param p1, "path"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/Set<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .line 288
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    .line 290
    .local v0, "manager":Landroid/content/res/AssetManager;
    :try_start_0
    invoke-static {}, Lio/flutter/view/FlutterMain$ImmutableSetBuilder;->newInstance()Lio/flutter/view/FlutterMain$ImmutableSetBuilder;

    move-result-object v1

    .line 291
    invoke-virtual {v0, p1}, Landroid/content/res/AssetManager;->list(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lio/flutter/view/FlutterMain$ImmutableSetBuilder;->add([Ljava/lang/Object;)Lio/flutter/view/FlutterMain$ImmutableSetBuilder;

    move-result-object v1

    .line 292
    invoke-virtual {v1}, Lio/flutter/view/FlutterMain$ImmutableSetBuilder;->build()Ljava/util/Set;

    move-result-object v1
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 293
    :catch_0
    move-exception v1

    .line 294
    .local v1, "e":Ljava/io/IOException;
    const-string v2, "FlutterMain"

    const-string v3, "Unable to list assets"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 295
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method

.method private static native nativeInit(Landroid/content/Context;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method private static native nativeRecordStartTimestamp(J)V
.end method

.method public static startInitialization(Landroid/content/Context;)V
    .locals 1
    .param p0, "applicationContext"    # Landroid/content/Context;

    .line 134
    new-instance v0, Lio/flutter/view/FlutterMain$Settings;

    invoke-direct {v0}, Lio/flutter/view/FlutterMain$Settings;-><init>()V

    invoke-static {p0, v0}, Lio/flutter/view/FlutterMain;->startInitialization(Landroid/content/Context;Lio/flutter/view/FlutterMain$Settings;)V

    .line 135
    return-void
.end method

.method public static startInitialization(Landroid/content/Context;Lio/flutter/view/FlutterMain$Settings;)V
    .locals 4
    .param p0, "applicationContext"    # Landroid/content/Context;
    .param p1, "settings"    # Lio/flutter/view/FlutterMain$Settings;

    .line 143
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_1

    .line 147
    sget-object v0, Lio/flutter/view/FlutterMain;->sSettings:Lio/flutter/view/FlutterMain$Settings;

    if-eqz v0, :cond_0

    .line 148
    return-void

    .line 151
    :cond_0
    sput-object p1, Lio/flutter/view/FlutterMain;->sSettings:Lio/flutter/view/FlutterMain$Settings;

    .line 153
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 154
    .local v0, "initStartTimestampMillis":J
    invoke-static {p0}, Lio/flutter/view/FlutterMain;->initConfig(Landroid/content/Context;)V

    .line 155
    invoke-static {p0}, Lio/flutter/view/FlutterMain;->initAot(Landroid/content/Context;)V

    .line 156
    invoke-static {p0}, Lio/flutter/view/FlutterMain;->initResources(Landroid/content/Context;)V

    .line 157
    const-string v2, "flutter"

    invoke-static {v2}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 164
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    sub-long/2addr v2, v0

    .line 165
    .local v2, "initTimeMillis":J
    invoke-static {v2, v3}, Lio/flutter/view/FlutterMain;->nativeRecordStartTimestamp(J)V

    .line 166
    return-void

    .line 144
    .end local v0    # "initStartTimestampMillis":J
    .end local v2    # "initTimeMillis":J
    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "startInitialization must be called on the main thread"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
