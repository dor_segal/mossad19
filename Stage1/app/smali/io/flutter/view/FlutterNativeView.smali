.class public Lio/flutter/view/FlutterNativeView;
.super Ljava/lang/Object;
.source "FlutterNativeView.java"

# interfaces
.implements Lio/flutter/plugin/common/BinaryMessenger;


# static fields
.field private static final TAG:Ljava/lang/String; = "FlutterNativeView"


# instance fields
.field private applicationIsRunning:Z

.field private final mContext:Landroid/content/Context;

.field private mFlutterView:Lio/flutter/view/FlutterView;

.field private final mMessageHandlers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Lio/flutter/plugin/common/BinaryMessenger$BinaryMessageHandler;",
            ">;"
        }
    .end annotation
.end field

.field private mNativePlatformView:J

.field private mNextReplyId:I

.field private final mPendingReplies:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map<",
            "Ljava/lang/Integer;",
            "Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;",
            ">;"
        }
    .end annotation
.end field

.field private final mPluginRegistry:Lio/flutter/app/FlutterPluginRegistry;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lio/flutter/view/FlutterNativeView;-><init>(Landroid/content/Context;Z)V

    .line 33
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "isBackgroundView"    # Z

    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x1

    iput v0, p0, Lio/flutter/view/FlutterNativeView;->mNextReplyId:I

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/flutter/view/FlutterNativeView;->mPendingReplies:Ljava/util/Map;

    .line 36
    iput-object p1, p0, Lio/flutter/view/FlutterNativeView;->mContext:Landroid/content/Context;

    .line 37
    new-instance v0, Lio/flutter/app/FlutterPluginRegistry;

    invoke-direct {v0, p0, p1}, Lio/flutter/app/FlutterPluginRegistry;-><init>(Lio/flutter/view/FlutterNativeView;Landroid/content/Context;)V

    iput-object v0, p0, Lio/flutter/view/FlutterNativeView;->mPluginRegistry:Lio/flutter/app/FlutterPluginRegistry;

    .line 38
    invoke-direct {p0, p0, p2}, Lio/flutter/view/FlutterNativeView;->attach(Lio/flutter/view/FlutterNativeView;Z)V

    .line 39
    invoke-virtual {p0}, Lio/flutter/view/FlutterNativeView;->assertAttached()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lio/flutter/view/FlutterNativeView;->mMessageHandlers:Ljava/util/Map;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lio/flutter/view/FlutterNativeView;)J
    .locals 2
    .param p0, "x0"    # Lio/flutter/view/FlutterNativeView;

    .line 18
    iget-wide v0, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    return-wide v0
.end method

.method static synthetic access$100(JI)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # I

    .line 18
    invoke-static {p0, p1, p2}, Lio/flutter/view/FlutterNativeView;->nativeInvokePlatformMessageEmptyResponseCallback(JI)V

    return-void
.end method

.method static synthetic access$200(JILjava/nio/ByteBuffer;I)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # I
    .param p3, "x2"    # Ljava/nio/ByteBuffer;
    .param p4, "x3"    # I

    .line 18
    invoke-static {p0, p1, p2, p3, p4}, Lio/flutter/view/FlutterNativeView;->nativeInvokePlatformMessageResponseCallback(JILjava/nio/ByteBuffer;I)V

    return-void
.end method

.method private attach(Lio/flutter/view/FlutterNativeView;Z)V
    .locals 2
    .param p1, "view"    # Lio/flutter/view/FlutterNativeView;
    .param p2, "isBackgroundView"    # Z

    .line 153
    invoke-static {p1, p2}, Lio/flutter/view/FlutterNativeView;->nativeAttach(Lio/flutter/view/FlutterNativeView;Z)J

    move-result-wide v0

    iput-wide v0, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    .line 154
    return-void
.end method

.method public static getObservatoryUri()Ljava/lang/String;
    .locals 1

    .line 115
    invoke-static {}, Lio/flutter/view/FlutterNativeView;->nativeGetObservatoryUri()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private handlePlatformMessage(Ljava/lang/String;[BI)V
    .locals 4
    .param p1, "channel"    # Ljava/lang/String;
    .param p2, "message"    # [B
    .param p3, "replyId"    # I

    .line 158
    invoke-virtual {p0}, Lio/flutter/view/FlutterNativeView;->assertAttached()V

    .line 159
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mMessageHandlers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/flutter/plugin/common/BinaryMessenger$BinaryMessageHandler;

    .line 160
    .local v0, "handler":Lio/flutter/plugin/common/BinaryMessenger$BinaryMessageHandler;
    if-eqz v0, :cond_1

    .line 162
    if-nez p2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    :try_start_0
    invoke-static {p2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 163
    .local v1, "buffer":Ljava/nio/ByteBuffer;
    :goto_0
    new-instance v2, Lio/flutter/view/FlutterNativeView$1;

    invoke-direct {v2, p0, p1, p3}, Lio/flutter/view/FlutterNativeView$1;-><init>(Lio/flutter/view/FlutterNativeView;Ljava/lang/String;I)V

    invoke-interface {v0, v1, v2}, Lio/flutter/plugin/common/BinaryMessenger$BinaryMessageHandler;->onMessage(Ljava/nio/ByteBuffer;Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 188
    .end local v1    # "buffer":Ljava/nio/ByteBuffer;
    goto :goto_1

    .line 185
    :catch_0
    move-exception v1

    .line 186
    .local v1, "ex":Ljava/lang/Exception;
    const-string v2, "FlutterNativeView"

    const-string v3, "Uncaught exception in binary message listener"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 187
    iget-wide v2, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    invoke-static {v2, v3, p3}, Lio/flutter/view/FlutterNativeView;->nativeInvokePlatformMessageEmptyResponseCallback(JI)V

    .line 189
    .end local v1    # "ex":Ljava/lang/Exception;
    :goto_1
    return-void

    .line 191
    :cond_1
    iget-wide v1, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    invoke-static {v1, v2, p3}, Lio/flutter/view/FlutterNativeView;->nativeInvokePlatformMessageEmptyResponseCallback(JI)V

    .line 192
    return-void
.end method

.method private handlePlatformMessageResponse(I[B)V
    .locals 4
    .param p1, "replyId"    # I
    .param p2, "reply"    # [B

    .line 196
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mPendingReplies:Ljava/util/Map;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;

    .line 197
    .local v0, "callback":Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;
    if-eqz v0, :cond_1

    .line 199
    if-nez p2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :cond_0
    :try_start_0
    invoke-static {p2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v1

    :goto_0
    invoke-interface {v0, v1}, Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;->reply(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    goto :goto_1

    .line 200
    :catch_0
    move-exception v1

    .line 201
    .local v1, "ex":Ljava/lang/Exception;
    const-string v2, "FlutterNativeView"

    const-string v3, "Uncaught exception in binary message reply handler"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 204
    .end local v1    # "ex":Ljava/lang/Exception;
    :cond_1
    :goto_1
    return-void
.end method

.method private static native nativeAttach(Lio/flutter/view/FlutterNativeView;Z)J
.end method

.method private static native nativeDestroy(J)V
.end method

.method private static native nativeDetach(J)V
.end method

.method private static native nativeDispatchEmptyPlatformMessage(JLjava/lang/String;I)V
.end method

.method private static native nativeDispatchPlatformMessage(JLjava/lang/String;Ljava/nio/ByteBuffer;II)V
.end method

.method private static native nativeGetObservatoryUri()Ljava/lang/String;
.end method

.method private static native nativeInvokePlatformMessageEmptyResponseCallback(JI)V
.end method

.method private static native nativeInvokePlatformMessageResponseCallback(JILjava/nio/ByteBuffer;I)V
.end method

.method private static native nativeRunBundleAndSnapshotFromLibrary(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/AssetManager;)V
.end method

.method private onFirstFrame()V
    .locals 1

    .line 222
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    if-nez v0, :cond_0

    .line 223
    return-void

    .line 224
    :cond_0
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterView;->onFirstFrame()V

    .line 225
    return-void
.end method

.method private onPreEngineRestart()V
    .locals 1

    .line 230
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterView;->resetAccessibilityTree()V

    .line 233
    :cond_0
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mPluginRegistry:Lio/flutter/app/FlutterPluginRegistry;

    if-nez v0, :cond_1

    .line 234
    return-void

    .line 235
    :cond_1
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mPluginRegistry:Lio/flutter/app/FlutterPluginRegistry;

    invoke-virtual {v0}, Lio/flutter/app/FlutterPluginRegistry;->onPreEngineRestart()V

    .line 236
    return-void
.end method

.method private runFromBundleInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "bundlePath"    # Ljava/lang/String;
    .param p2, "entrypoint"    # Ljava/lang/String;
    .param p3, "libraryPath"    # Ljava/lang/String;
    .param p4, "defaultPath"    # Ljava/lang/String;

    .line 100
    invoke-virtual {p0}, Lio/flutter/view/FlutterNativeView;->assertAttached()V

    .line 101
    iget-boolean v0, p0, Lio/flutter/view/FlutterNativeView;->applicationIsRunning:Z

    if-nez v0, :cond_0

    .line 104
    iget-wide v1, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mContext:Landroid/content/Context;

    .line 105
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v7

    .line 104
    move-object v3, p1

    move-object v4, p4

    move-object v5, p2

    move-object v6, p3

    invoke-static/range {v1 .. v7}, Lio/flutter/view/FlutterNativeView;->nativeRunBundleAndSnapshotFromLibrary(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/res/AssetManager;)V

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/flutter/view/FlutterNativeView;->applicationIsRunning:Z

    .line 108
    return-void

    .line 102
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "This Flutter engine instance is already running an application"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method private updateCustomAccessibilityActions(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V
    .locals 1
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "strings"    # [Ljava/lang/String;

    .line 215
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    if-nez v0, :cond_0

    .line 216
    return-void

    .line 217
    :cond_0
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    invoke-virtual {v0, p1, p2}, Lio/flutter/view/FlutterView;->updateCustomAccessibilityActions(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V

    .line 218
    return-void
.end method

.method private updateSemantics(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V
    .locals 1
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "strings"    # [Ljava/lang/String;

    .line 208
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    if-nez v0, :cond_0

    .line 209
    return-void

    .line 210
    :cond_0
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    invoke-virtual {v0, p1, p2}, Lio/flutter/view/FlutterView;->updateSemantics(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V

    .line 211
    return-void
.end method


# virtual methods
.method public assertAttached()V
    .locals 2

    .line 75
    invoke-virtual {p0}, Lio/flutter/view/FlutterNativeView;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 76
    return-void

    .line 75
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Platform view is not attached"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public attachViewAndActivity(Lio/flutter/view/FlutterView;Landroid/app/Activity;)V
    .locals 1
    .param p1, "flutterView"    # Lio/flutter/view/FlutterView;
    .param p2, "activity"    # Landroid/app/Activity;

    .line 62
    iput-object p1, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    .line 63
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mPluginRegistry:Lio/flutter/app/FlutterPluginRegistry;

    invoke-virtual {v0, p1, p2}, Lio/flutter/app/FlutterPluginRegistry;->attach(Lio/flutter/view/FlutterView;Landroid/app/Activity;)V

    .line 64
    return-void
.end method

.method public destroy()V
    .locals 2

    .line 50
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mPluginRegistry:Lio/flutter/app/FlutterPluginRegistry;

    invoke-virtual {v0}, Lio/flutter/app/FlutterPluginRegistry;->destroy()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    .line 52
    iget-wide v0, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    invoke-static {v0, v1}, Lio/flutter/view/FlutterNativeView;->nativeDestroy(J)V

    .line 53
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/flutter/view/FlutterNativeView;->applicationIsRunning:Z

    .line 55
    return-void
.end method

.method public detach()V
    .locals 2

    .line 44
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mPluginRegistry:Lio/flutter/app/FlutterPluginRegistry;

    invoke-virtual {v0}, Lio/flutter/app/FlutterPluginRegistry;->detach()V

    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lio/flutter/view/FlutterNativeView;->mFlutterView:Lio/flutter/view/FlutterView;

    .line 46
    iget-wide v0, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    invoke-static {v0, v1}, Lio/flutter/view/FlutterNativeView;->nativeDetach(J)V

    .line 47
    return-void
.end method

.method public get()J
    .locals 2

    .line 71
    iget-wide v0, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    return-wide v0
.end method

.method public getPluginRegistry()Lio/flutter/app/FlutterPluginRegistry;
    .locals 1

    .line 58
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mPluginRegistry:Lio/flutter/app/FlutterPluginRegistry;

    return-object v0
.end method

.method public isApplicationRunning()Z
    .locals 1

    .line 111
    iget-boolean v0, p0, Lio/flutter/view/FlutterNativeView;->applicationIsRunning:Z

    return v0
.end method

.method public isAttached()Z
    .locals 5

    .line 67
    iget-wide v0, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    const-wide/16 v2, 0x0

    cmp-long v4, v0, v2

    if-eqz v4, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method public runFromBundle(Lio/flutter/view/FlutterRunArguments;)V
    .locals 4
    .param p1, "args"    # Lio/flutter/view/FlutterRunArguments;

    .line 79
    iget-object v0, p1, Lio/flutter/view/FlutterRunArguments;->bundlePath:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 81
    iget-object v0, p1, Lio/flutter/view/FlutterRunArguments;->entrypoint:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 84
    iget-object v0, p1, Lio/flutter/view/FlutterRunArguments;->bundlePath:Ljava/lang/String;

    iget-object v1, p1, Lio/flutter/view/FlutterRunArguments;->entrypoint:Ljava/lang/String;

    iget-object v2, p1, Lio/flutter/view/FlutterRunArguments;->libraryPath:Ljava/lang/String;

    iget-object v3, p1, Lio/flutter/view/FlutterRunArguments;->defaultPath:Ljava/lang/String;

    invoke-direct {p0, v0, v1, v2, v3}, Lio/flutter/view/FlutterNativeView;->runFromBundleInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    return-void

    .line 82
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "An entrypoint must be specified"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0

    .line 80
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "A bundlePath must be specified"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public runFromBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "bundlePath"    # Ljava/lang/String;
    .param p2, "defaultPath"    # Ljava/lang/String;
    .param p3, "entrypoint"    # Ljava/lang/String;
    .param p4, "reuseRuntimeController"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 95
    const/4 v0, 0x0

    invoke-direct {p0, p1, p3, v0, p2}, Lio/flutter/view/FlutterNativeView;->runFromBundleInternal(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method public send(Ljava/lang/String;Ljava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "channel"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/nio/ByteBuffer;

    .line 120
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lio/flutter/view/FlutterNativeView;->send(Ljava/lang/String;Ljava/nio/ByteBuffer;Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;)V

    .line 121
    return-void
.end method

.method public send(Ljava/lang/String;Ljava/nio/ByteBuffer;Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;)V
    .locals 9
    .param p1, "channel"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/nio/ByteBuffer;
    .param p3, "callback"    # Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;

    .line 125
    invoke-virtual {p0}, Lio/flutter/view/FlutterNativeView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 126
    const-string v0, "FlutterNativeView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FlutterView.send called on a detached view, channel="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 127
    return-void

    .line 130
    :cond_0
    const/4 v0, 0x0

    .line 131
    .local v0, "replyId":I
    if-eqz p3, :cond_1

    .line 132
    iget v1, p0, Lio/flutter/view/FlutterNativeView;->mNextReplyId:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lio/flutter/view/FlutterNativeView;->mNextReplyId:I

    move v0, v1

    .line 133
    iget-object v1, p0, Lio/flutter/view/FlutterNativeView;->mPendingReplies:Ljava/util/Map;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v1, v2, p3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    :cond_1
    if-nez p2, :cond_2

    .line 136
    iget-wide v1, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    invoke-static {v1, v2, p1, v0}, Lio/flutter/view/FlutterNativeView;->nativeDispatchEmptyPlatformMessage(JLjava/lang/String;I)V

    goto :goto_0

    .line 138
    :cond_2
    iget-wide v3, p0, Lio/flutter/view/FlutterNativeView;->mNativePlatformView:J

    .line 139
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->position()I

    move-result v7

    .line 138
    move-object v5, p1

    move-object v6, p2

    move v8, v0

    invoke-static/range {v3 .. v8}, Lio/flutter/view/FlutterNativeView;->nativeDispatchPlatformMessage(JLjava/lang/String;Ljava/nio/ByteBuffer;II)V

    .line 141
    :goto_0
    return-void
.end method

.method public setMessageHandler(Ljava/lang/String;Lio/flutter/plugin/common/BinaryMessenger$BinaryMessageHandler;)V
    .locals 1
    .param p1, "channel"    # Ljava/lang/String;
    .param p2, "handler"    # Lio/flutter/plugin/common/BinaryMessenger$BinaryMessageHandler;

    .line 145
    if-nez p2, :cond_0

    .line 146
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mMessageHandlers:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 148
    :cond_0
    iget-object v0, p0, Lio/flutter/view/FlutterNativeView;->mMessageHandlers:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    :goto_0
    return-void
.end method
