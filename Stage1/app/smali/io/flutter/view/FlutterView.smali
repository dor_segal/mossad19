.class public Lio/flutter/view/FlutterView;
.super Landroid/view/SurfaceView;
.source "FlutterView.java"

# interfaces
.implements Lio/flutter/plugin/common/BinaryMessenger;
.implements Lio/flutter/view/TextureRegistry;
.implements Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lio/flutter/view/FlutterView$SurfaceTextureRegistryEntry;,
        Lio/flutter/view/FlutterView$FirstFrameListener;,
        Lio/flutter/view/FlutterView$TouchExplorationListener;,
        Lio/flutter/view/FlutterView$AnimationScaleObserver;,
        Lio/flutter/view/FlutterView$AccessibilityFeature;,
        Lio/flutter/view/FlutterView$ZeroSides;,
        Lio/flutter/view/FlutterView$ViewportMetrics;,
        Lio/flutter/view/FlutterView$Provider;
    }
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z = false

.field private static final TAG:Ljava/lang/String; = "FlutterView"

.field private static final kPointerChangeAdd:I = 0x1

.field private static final kPointerChangeCancel:I = 0x0

.field private static final kPointerChangeDown:I = 0x4

.field private static final kPointerChangeHover:I = 0x3

.field private static final kPointerChangeMove:I = 0x5

.field private static final kPointerChangeRemove:I = 0x2

.field private static final kPointerChangeUp:I = 0x6

.field private static final kPointerDeviceKindInvertedStylus:I = 0x3

.field private static final kPointerDeviceKindMouse:I = 0x1

.field private static final kPointerDeviceKindStylus:I = 0x2

.field private static final kPointerDeviceKindTouch:I = 0x0

.field private static final kPointerDeviceKindUnknown:I = 0x4


# instance fields
.field private mAccessibilityEnabled:Z

.field private mAccessibilityFeatureFlags:I

.field private final mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

.field private mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

.field private final mActivityLifecycleListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lio/flutter/plugin/common/ActivityLifecycleListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mAnimationScaleObserver:Lio/flutter/view/FlutterView$AnimationScaleObserver;

.field private final mFirstFrameListeners:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lio/flutter/view/FlutterView$FirstFrameListener;",
            ">;"
        }
    .end annotation
.end field

.field private final mFlutterKeyEventChannel:Lio/flutter/plugin/common/BasicMessageChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/flutter/plugin/common/BasicMessageChannel<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mFlutterLifecycleChannel:Lio/flutter/plugin/common/BasicMessageChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/flutter/plugin/common/BasicMessageChannel<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final mFlutterLocalizationChannel:Lio/flutter/plugin/common/MethodChannel;

.field private final mFlutterNavigationChannel:Lio/flutter/plugin/common/MethodChannel;

.field private final mFlutterSettingsChannel:Lio/flutter/plugin/common/BasicMessageChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/flutter/plugin/common/BasicMessageChannel<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mFlutterSystemChannel:Lio/flutter/plugin/common/BasicMessageChannel;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lio/flutter/plugin/common/BasicMessageChannel<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field

.field private final mImm:Landroid/view/inputmethod/InputMethodManager;

.field private mIsSoftwareRenderingEnabled:Z

.field private mLastInputConnection:Landroid/view/inputmethod/InputConnection;

.field private final mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

.field private mNativeView:Lio/flutter/view/FlutterNativeView;

.field private final mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

.field private final mTextInputPlugin:Lio/flutter/plugin/editing/TextInputPlugin;

.field private mTouchExplorationEnabled:Z

.field private mTouchExplorationListener:Lio/flutter/view/FlutterView$TouchExplorationListener;

.field private final nextTextureId:Ljava/util/concurrent/atomic/AtomicLong;


# direct methods
.method static constructor <clinit>()V
    .locals 0

    .line 43
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .line 101
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lio/flutter/view/FlutterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .line 105
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lio/flutter/view/FlutterView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;Lio/flutter/view/FlutterNativeView;)V

    .line 106
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;Lio/flutter/view/FlutterNativeView;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "nativeView"    # Lio/flutter/view/FlutterNativeView;

    .line 109
    invoke-direct {p0, p1, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 94
    new-instance v0, Ljava/util/concurrent/atomic/AtomicLong;

    const-wide/16 v1, 0x0

    invoke-direct {v0, v1, v2}, Ljava/util/concurrent/atomic/AtomicLong;-><init>(J)V

    iput-object v0, p0, Lio/flutter/view/FlutterView;->nextTextureId:Ljava/util/concurrent/atomic/AtomicLong;

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/flutter/view/FlutterView;->mIsSoftwareRenderingEnabled:Z

    .line 845
    iput-boolean v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityEnabled:Z

    .line 846
    iput-boolean v0, p0, Lio/flutter/view/FlutterView;->mTouchExplorationEnabled:Z

    .line 847
    iput v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    .line 111
    invoke-static {}, Lio/flutter/view/FlutterView;->nativeGetIsSoftwareRenderingEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lio/flutter/view/FlutterView;->mIsSoftwareRenderingEnabled:Z

    .line 112
    new-instance v0, Lio/flutter/view/FlutterView$AnimationScaleObserver;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lio/flutter/view/FlutterView$AnimationScaleObserver;-><init>(Lio/flutter/view/FlutterView;Landroid/os/Handler;)V

    iput-object v0, p0, Lio/flutter/view/FlutterView;->mAnimationScaleObserver:Lio/flutter/view/FlutterView$AnimationScaleObserver;

    .line 113
    new-instance v0, Lio/flutter/view/FlutterView$ViewportMetrics;

    invoke-direct {v0}, Lio/flutter/view/FlutterView$ViewportMetrics;-><init>()V

    iput-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    .line 114
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->density:F

    iput v1, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->devicePixelRatio:F

    .line 115
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lio/flutter/view/FlutterView;->setFocusable(Z)V

    .line 116
    invoke-virtual {p0, v0}, Lio/flutter/view/FlutterView;->setFocusableInTouchMode(Z)V

    .line 118
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 119
    .local v0, "activity":Landroid/app/Activity;
    if-nez p3, :cond_0

    .line 120
    new-instance v1, Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0}, Landroid/app/Activity;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2}, Lio/flutter/view/FlutterNativeView;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    goto :goto_0

    .line 122
    :cond_0
    iput-object p3, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    .line 124
    :goto_0
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v1, p0, v0}, Lio/flutter/view/FlutterNativeView;->attachViewAndActivity(Lio/flutter/view/FlutterView;Landroid/app/Activity;)V

    .line 126
    new-instance v1, Lio/flutter/view/FlutterView$1;

    invoke-direct {v1, p0}, Lio/flutter/view/FlutterView$1;-><init>(Lio/flutter/view/FlutterView;)V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    .line 145
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    iget-object v2, p0, Lio/flutter/view/FlutterView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v1, v2}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 147
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "accessibility"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/accessibility/AccessibilityManager;

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    .line 149
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mActivityLifecycleListeners:Ljava/util/List;

    .line 150
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mFirstFrameListeners:Ljava/util/List;

    .line 153
    new-instance v1, Lio/flutter/plugin/common/MethodChannel;

    const-string v2, "flutter/localization"

    sget-object v3, Lio/flutter/plugin/common/JSONMethodCodec;->INSTANCE:Lio/flutter/plugin/common/JSONMethodCodec;

    invoke-direct {v1, p0, v2, v3}, Lio/flutter/plugin/common/MethodChannel;-><init>(Lio/flutter/plugin/common/BinaryMessenger;Ljava/lang/String;Lio/flutter/plugin/common/MethodCodec;)V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mFlutterLocalizationChannel:Lio/flutter/plugin/common/MethodChannel;

    .line 154
    new-instance v1, Lio/flutter/plugin/common/MethodChannel;

    const-string v2, "flutter/navigation"

    sget-object v3, Lio/flutter/plugin/common/JSONMethodCodec;->INSTANCE:Lio/flutter/plugin/common/JSONMethodCodec;

    invoke-direct {v1, p0, v2, v3}, Lio/flutter/plugin/common/MethodChannel;-><init>(Lio/flutter/plugin/common/BinaryMessenger;Ljava/lang/String;Lio/flutter/plugin/common/MethodCodec;)V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mFlutterNavigationChannel:Lio/flutter/plugin/common/MethodChannel;

    .line 155
    new-instance v1, Lio/flutter/plugin/common/BasicMessageChannel;

    const-string v2, "flutter/keyevent"

    sget-object v3, Lio/flutter/plugin/common/JSONMessageCodec;->INSTANCE:Lio/flutter/plugin/common/JSONMessageCodec;

    invoke-direct {v1, p0, v2, v3}, Lio/flutter/plugin/common/BasicMessageChannel;-><init>(Lio/flutter/plugin/common/BinaryMessenger;Ljava/lang/String;Lio/flutter/plugin/common/MessageCodec;)V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mFlutterKeyEventChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    .line 156
    new-instance v1, Lio/flutter/plugin/common/BasicMessageChannel;

    const-string v2, "flutter/lifecycle"

    sget-object v3, Lio/flutter/plugin/common/StringCodec;->INSTANCE:Lio/flutter/plugin/common/StringCodec;

    invoke-direct {v1, p0, v2, v3}, Lio/flutter/plugin/common/BasicMessageChannel;-><init>(Lio/flutter/plugin/common/BinaryMessenger;Ljava/lang/String;Lio/flutter/plugin/common/MessageCodec;)V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mFlutterLifecycleChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    .line 157
    new-instance v1, Lio/flutter/plugin/common/BasicMessageChannel;

    const-string v2, "flutter/system"

    sget-object v3, Lio/flutter/plugin/common/JSONMessageCodec;->INSTANCE:Lio/flutter/plugin/common/JSONMessageCodec;

    invoke-direct {v1, p0, v2, v3}, Lio/flutter/plugin/common/BasicMessageChannel;-><init>(Lio/flutter/plugin/common/BinaryMessenger;Ljava/lang/String;Lio/flutter/plugin/common/MessageCodec;)V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mFlutterSystemChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    .line 158
    new-instance v1, Lio/flutter/plugin/common/BasicMessageChannel;

    const-string v2, "flutter/settings"

    sget-object v3, Lio/flutter/plugin/common/JSONMessageCodec;->INSTANCE:Lio/flutter/plugin/common/JSONMessageCodec;

    invoke-direct {v1, p0, v2, v3}, Lio/flutter/plugin/common/BasicMessageChannel;-><init>(Lio/flutter/plugin/common/BinaryMessenger;Ljava/lang/String;Lio/flutter/plugin/common/MessageCodec;)V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mFlutterSettingsChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    .line 160
    new-instance v1, Lio/flutter/plugin/platform/PlatformPlugin;

    invoke-direct {v1, v0}, Lio/flutter/plugin/platform/PlatformPlugin;-><init>(Landroid/app/Activity;)V

    .line 161
    .local v1, "platformPlugin":Lio/flutter/plugin/platform/PlatformPlugin;
    new-instance v2, Lio/flutter/plugin/common/MethodChannel;

    const-string v3, "flutter/platform"

    sget-object v4, Lio/flutter/plugin/common/JSONMethodCodec;->INSTANCE:Lio/flutter/plugin/common/JSONMethodCodec;

    invoke-direct {v2, p0, v3, v4}, Lio/flutter/plugin/common/MethodChannel;-><init>(Lio/flutter/plugin/common/BinaryMessenger;Ljava/lang/String;Lio/flutter/plugin/common/MethodCodec;)V

    .line 162
    .local v2, "flutterPlatformChannel":Lio/flutter/plugin/common/MethodChannel;
    invoke-virtual {v2, v1}, Lio/flutter/plugin/common/MethodChannel;->setMethodCallHandler(Lio/flutter/plugin/common/MethodChannel$MethodCallHandler;)V

    .line 163
    invoke-virtual {p0, v1}, Lio/flutter/view/FlutterView;->addActivityLifecycleListener(Lio/flutter/plugin/common/ActivityLifecycleListener;)V

    .line 164
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "input_method"

    invoke-virtual {v3, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/inputmethod/InputMethodManager;

    iput-object v3, p0, Lio/flutter/view/FlutterView;->mImm:Landroid/view/inputmethod/InputMethodManager;

    .line 165
    new-instance v3, Lio/flutter/plugin/editing/TextInputPlugin;

    invoke-direct {v3, p0}, Lio/flutter/plugin/editing/TextInputPlugin;-><init>(Lio/flutter/view/FlutterView;)V

    iput-object v3, p0, Lio/flutter/view/FlutterView;->mTextInputPlugin:Lio/flutter/plugin/editing/TextInputPlugin;

    .line 168
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v3

    invoke-direct {p0, v3}, Lio/flutter/view/FlutterView;->setLocales(Landroid/content/res/Configuration;)V

    .line 169
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->setUserSettings()V

    .line 170
    return-void
.end method

.method static synthetic access$000(Lio/flutter/view/FlutterView;)Lio/flutter/view/FlutterNativeView;
    .locals 1
    .param p0, "x0"    # Lio/flutter/view/FlutterView;

    .line 43
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    return-object v0
.end method

.method static synthetic access$100(JLandroid/view/Surface;)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # Landroid/view/Surface;

    .line 43
    invoke-static {p0, p1, p2}, Lio/flutter/view/FlutterView;->nativeSurfaceCreated(JLandroid/view/Surface;)V

    return-void
.end method

.method static synthetic access$1100(JJ)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # J

    .line 43
    invoke-static {p0, p1, p2, p3}, Lio/flutter/view/FlutterView;->nativeMarkTextureFrameAvailable(JJ)V

    return-void
.end method

.method static synthetic access$1200(JJ)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # J

    .line 43
    invoke-static {p0, p1, p2, p3}, Lio/flutter/view/FlutterView;->nativeUnregisterTexture(JJ)V

    return-void
.end method

.method static synthetic access$200(JII)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # I
    .param p3, "x2"    # I

    .line 43
    invoke-static {p0, p1, p2, p3}, Lio/flutter/view/FlutterView;->nativeSurfaceChanged(JII)V

    return-void
.end method

.method static synthetic access$300(J)V
    .locals 0
    .param p0, "x0"    # J

    .line 43
    invoke-static {p0, p1}, Lio/flutter/view/FlutterView;->nativeSurfaceDestroyed(J)V

    return-void
.end method

.method static synthetic access$400(Lio/flutter/view/FlutterView;)I
    .locals 1
    .param p0, "x0"    # Lio/flutter/view/FlutterView;

    .line 43
    iget v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    return v0
.end method

.method static synthetic access$402(Lio/flutter/view/FlutterView;I)I
    .locals 0
    .param p0, "x0"    # Lio/flutter/view/FlutterView;
    .param p1, "x1"    # I

    .line 43
    iput p1, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    return p1
.end method

.method static synthetic access$500(JI)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # I

    .line 43
    invoke-static {p0, p1, p2}, Lio/flutter/view/FlutterView;->nativeSetAccessibilityFeatures(JI)V

    return-void
.end method

.method static synthetic access$602(Lio/flutter/view/FlutterView;Z)Z
    .locals 0
    .param p0, "x0"    # Lio/flutter/view/FlutterView;
    .param p1, "x1"    # Z

    .line 43
    iput-boolean p1, p0, Lio/flutter/view/FlutterView;->mTouchExplorationEnabled:Z

    return p1
.end method

.method static synthetic access$700(Lio/flutter/view/FlutterView;)Lio/flutter/view/AccessibilityBridge;
    .locals 1
    .param p0, "x0"    # Lio/flutter/view/FlutterView;

    .line 43
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    return-object v0
.end method

.method static synthetic access$800(Lio/flutter/view/FlutterView;)V
    .locals 0
    .param p0, "x0"    # Lio/flutter/view/FlutterView;

    .line 43
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->resetWillNotDraw()V

    return-void
.end method

.method private addPointerForIndex(Landroid/view/MotionEvent;IIILjava/nio/ByteBuffer;)V
    .locals 9
    .param p1, "event"    # Landroid/view/MotionEvent;
    .param p2, "pointerIndex"    # I
    .param p3, "pointerChange"    # I
    .param p4, "pointerData"    # I
    .param p5, "packet"    # Ljava/nio/ByteBuffer;

    .line 445
    const/4 v0, -0x1

    if-ne p3, v0, :cond_0

    .line 446
    return-void

    .line 449
    :cond_0
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    invoke-direct {p0, v0}, Lio/flutter/view/FlutterView;->getPointerDeviceTypeForToolType(I)I

    move-result v0

    .line 451
    .local v0, "pointerKind":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getEventTime()J

    move-result-wide v1

    const-wide/16 v3, 0x3e8

    mul-long v1, v1, v3

    .line 453
    .local v1, "timeStamp":J
    invoke-virtual {p5, v1, v2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 454
    int-to-long v3, p3

    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 455
    int-to-long v3, v0

    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 456
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getPointerId(I)I

    move-result v3

    int-to-long v3, v3

    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 457
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getX(I)F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 458
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getY(I)F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 460
    const/4 v3, 0x1

    const-wide/16 v4, 0x0

    const/4 v6, 0x2

    if-ne v0, v3, :cond_1

    .line 461
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v3

    and-int/lit8 v3, v3, 0x1f

    int-to-long v7, v3

    invoke-virtual {p5, v7, v8}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 462
    :cond_1
    if-ne v0, v6, :cond_2

    .line 463
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getButtonState()I

    move-result v3

    shr-int/lit8 v3, v3, 0x4

    and-int/lit8 v3, v3, 0xf

    int-to-long v7, v3

    invoke-virtual {p5, v7, v8}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    goto :goto_0

    .line 465
    :cond_2
    invoke-virtual {p5, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 468
    :goto_0
    invoke-virtual {p5, v4, v5}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 472
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getPressure(I)F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 473
    const-wide/16 v3, 0x0

    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 474
    const-wide/high16 v7, 0x3ff0000000000000L    # 1.0

    invoke-virtual {p5, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 476
    if-ne v0, v6, :cond_3

    .line 477
    const/16 v5, 0x18

    invoke-virtual {p1, v5, p2}, Landroid/view/MotionEvent;->getAxisValue(II)F

    move-result v5

    float-to-double v7, v5

    invoke-virtual {p5, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 478
    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    goto :goto_1

    .line 480
    :cond_3
    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 481
    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 484
    :goto_1
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getSize(I)F

    move-result v5

    float-to-double v7, v5

    invoke-virtual {p5, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 486
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getToolMajor(I)F

    move-result v5

    float-to-double v7, v5

    invoke-virtual {p5, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 487
    invoke-virtual {p1, p2}, Landroid/view/MotionEvent;->getToolMinor(I)F

    move-result v5

    float-to-double v7, v5

    invoke-virtual {p5, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 489
    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 490
    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 492
    const/16 v5, 0x8

    invoke-virtual {p1, v5, p2}, Landroid/view/MotionEvent;->getAxisValue(II)F

    move-result v5

    float-to-double v7, v5

    invoke-virtual {p5, v7, v8}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 494
    if-ne v0, v6, :cond_4

    .line 495
    const/16 v3, 0x19

    invoke-virtual {p1, v3, p2}, Landroid/view/MotionEvent;->getAxisValue(II)F

    move-result v3

    float-to-double v3, v3

    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    goto :goto_2

    .line 497
    :cond_4
    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putDouble(D)Ljava/nio/ByteBuffer;

    .line 500
    :goto_2
    int-to-long v3, p4

    invoke-virtual {p5, v3, v4}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 501
    return-void
.end method

.method private encodeKeyEvent(Landroid/view/KeyEvent;Ljava/util/Map;)V
    .locals 2
    .param p1, "event"    # Landroid/view/KeyEvent;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/KeyEvent;",
            "Ljava/util/Map<",
            "Ljava/lang/String;",
            "Ljava/lang/Object;",
            ">;)V"
        }
    .end annotation

    .line 173
    .local p2, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v0, "flags"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    const-string v0, "codePoint"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getUnicodeChar()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    const-string v0, "keyCode"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    const-string v0, "scanCode"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getScanCode()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 177
    const-string v0, "metaState"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getMetaState()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {p2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 178
    return-void
.end method

.method private getPointerChangeForAction(I)I
    .locals 3
    .param p1, "maskedAction"    # I

    .line 404
    const/4 v0, 0x4

    if-nez p1, :cond_0

    .line 405
    return v0

    .line 407
    :cond_0
    const/4 v1, 0x1

    const/4 v2, 0x6

    if-ne p1, v1, :cond_1

    .line 408
    return v2

    .line 411
    :cond_1
    const/4 v1, 0x5

    if-ne p1, v1, :cond_2

    .line 412
    return v0

    .line 414
    :cond_2
    if-ne p1, v2, :cond_3

    .line 415
    return v2

    .line 418
    :cond_3
    const/4 v0, 0x2

    if-ne p1, v0, :cond_4

    .line 419
    return v1

    .line 421
    :cond_4
    const/4 v0, 0x3

    if-ne p1, v0, :cond_5

    .line 422
    const/4 v0, 0x0

    return v0

    .line 424
    :cond_5
    const/4 v0, -0x1

    return v0
.end method

.method private getPointerDeviceTypeForToolType(I)I
    .locals 1
    .param p1, "toolType"    # I

    .line 428
    packed-switch p1, :pswitch_data_0

    .line 439
    const/4 v0, 0x4

    return v0

    .line 436
    :pswitch_0
    const/4 v0, 0x3

    return v0

    .line 434
    :pswitch_1
    const/4 v0, 0x1

    return v0

    .line 432
    :pswitch_2
    const/4 v0, 0x2

    return v0

    .line 430
    :pswitch_3
    const/4 v0, 0x0

    return v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private handleAccessibilityHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 1029
    iget-boolean v0, p0, Lio/flutter/view/FlutterView;->mTouchExplorationEnabled:Z

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 1030
    return v1

    .line 1032
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v2, 0x9

    if-eq v0, v2, :cond_3

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v2, 0x7

    if-ne v0, v2, :cond_1

    goto :goto_0

    .line 1034
    :cond_1
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/16 v2, 0xa

    if-ne v0, v2, :cond_2

    .line 1035
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    invoke-virtual {v0}, Lio/flutter/view/AccessibilityBridge;->handleTouchExplorationExit()V

    goto :goto_1

    .line 1037
    :cond_2
    const-string v0, "flutter"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "unexpected accessibility hover event: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    return v1

    .line 1033
    :cond_3
    :goto_0
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v1

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v2

    invoke-virtual {v0, v1, v2}, Lio/flutter/view/AccessibilityBridge;->handleTouchExploration(FF)V

    .line 1040
    :goto_1
    const/4 v0, 0x1

    return v0
.end method

.method private isAttached()Z
    .locals 1

    .line 702
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method private static native nativeDispatchPointerDataPacket(JLjava/nio/ByteBuffer;I)V
.end method

.method private static native nativeDispatchSemanticsAction(JIILjava/nio/ByteBuffer;I)V
.end method

.method private static native nativeGetBitmap(J)Landroid/graphics/Bitmap;
.end method

.method private static native nativeGetIsSoftwareRenderingEnabled()Z
.end method

.method private static native nativeMarkTextureFrameAvailable(JJ)V
.end method

.method private static native nativeRegisterTexture(JJLandroid/graphics/SurfaceTexture;)V
.end method

.method private static native nativeSetAccessibilityFeatures(JI)V
.end method

.method private static native nativeSetSemanticsEnabled(JZ)V
.end method

.method private static native nativeSetViewportMetrics(JFIIIIIIIIII)V
.end method

.method private static native nativeSurfaceChanged(JII)V
.end method

.method private static native nativeSurfaceCreated(JLandroid/view/Surface;)V
.end method

.method private static native nativeSurfaceDestroyed(J)V
.end method

.method private static native nativeUnregisterTexture(JJ)V
.end method

.method private postRun()V
    .locals 0

    .line 715
    return-void
.end method

.method private preRun()V
    .locals 0

    .line 711
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->resetAccessibilityTree()V

    .line 712
    return-void
.end method

.method private resetWillNotDraw()V
    .locals 2

    .line 920
    iget-boolean v0, p0, Lio/flutter/view/FlutterView;->mIsSoftwareRenderingEnabled:Z

    const/4 v1, 0x0

    if-nez v0, :cond_1

    .line 921
    iget-boolean v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityEnabled:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lio/flutter/view/FlutterView;->mTouchExplorationEnabled:Z

    if-nez v0, :cond_0

    const/4 v1, 0x1

    nop

    :cond_0
    invoke-virtual {p0, v1}, Lio/flutter/view/FlutterView;->setWillNotDraw(Z)V

    goto :goto_0

    .line 923
    :cond_1
    invoke-virtual {p0, v1}, Lio/flutter/view/FlutterView;->setWillNotDraw(Z)V

    .line 925
    :goto_0
    return-void
.end method

.method private setLocales(Landroid/content/res/Configuration;)V
    .locals 10
    .param p1, "config"    # Landroid/content/res/Configuration;

    .line 315
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/16 v3, 0x18

    if-lt v0, v3, :cond_1

    .line 318
    :try_start_0
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const-string v3, "getLocales"

    new-array v4, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v3, v4}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    new-array v3, v2, [Ljava/lang/Object;

    invoke-virtual {v0, p1, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 319
    .local v0, "localeList":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "get"

    new-array v5, v1, [Ljava/lang/Class;

    sget-object v6, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v6, v5, v2

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 320
    .local v3, "localeListGet":Ljava/lang/reflect/Method;
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    const-string v5, "size"

    new-array v6, v2, [Ljava/lang/Class;

    invoke-virtual {v4, v5, v6}, Ljava/lang/Class;->getDeclaredMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v4

    .line 321
    .local v4, "localeListSize":Ljava/lang/reflect/Method;
    new-array v5, v2, [Ljava/lang/Object;

    invoke-virtual {v4, v0, v5}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 322
    .local v5, "localeCount":I
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 323
    .local v6, "data":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v7, 0x0

    .local v7, "index":I
    :goto_0
    if-ge v7, v5, :cond_0

    .line 324
    new-array v8, v1, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v2

    invoke-virtual {v3, v0, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/Locale;

    .line 325
    .local v8, "locale":Ljava/util/Locale;
    invoke-virtual {v8}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 326
    invoke-virtual {v8}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 327
    invoke-virtual {v8}, Ljava/util/Locale;->getScript()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 328
    invoke-virtual {v8}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v9

    invoke-interface {v6, v9}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 323
    .end local v8    # "locale":Ljava/util/Locale;
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 330
    .end local v7    # "index":I
    :cond_0
    iget-object v7, p0, Lio/flutter/view/FlutterView;->mFlutterLocalizationChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v8, "setLocale"

    invoke-virtual {v7, v8, v6}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 331
    return-void

    .line 332
    .end local v0    # "localeList":Ljava/lang/Object;
    .end local v3    # "localeListGet":Ljava/lang/reflect/Method;
    .end local v4    # "localeListSize":Ljava/lang/reflect/Method;
    .end local v5    # "localeCount":I
    .end local v6    # "data":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :catch_0
    move-exception v0

    .line 337
    :cond_1
    iget-object v0, p1, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    .line 339
    .local v0, "locale":Ljava/util/Locale;
    iget-object v3, p0, Lio/flutter/view/FlutterView;->mFlutterLocalizationChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v4, "setLocale"

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/String;

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v0}, Ljava/util/Locale;->getCountry()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v1, 0x2

    sget v2, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v6, 0x15

    if-lt v2, v6, :cond_2

    invoke-virtual {v0}, Ljava/util/Locale;->getScript()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_2
    const-string v2, ""

    :goto_1
    aput-object v2, v5, v1

    const/4 v1, 0x3

    invoke-virtual {v0}, Ljava/util/Locale;->getVariant()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 341
    return-void
.end method

.method private setUserSettings()V
    .locals 3

    .line 308
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 309
    .local v0, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "textScaleFactor"

    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->fontScale:F

    invoke-static {v2}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 310
    const-string v1, "alwaysUse24HourFormat"

    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 311
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mFlutterSettingsChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    invoke-virtual {v1, v0}, Lio/flutter/plugin/common/BasicMessageChannel;->send(Ljava/lang/Object;)V

    .line 312
    return-void
.end method

.method private updateAccessibilityFeatures()V
    .locals 3

    .line 897
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_1

    .line 898
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "transition_animation_scale"

    invoke-static {v0, v1}, Landroid/provider/Settings$Global;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 900
    .local v0, "transitionAnimationScale":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, "0"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 901
    iget v1, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    sget-object v2, Lio/flutter/view/FlutterView$AccessibilityFeature;->DISABLE_ANIMATIONS:Lio/flutter/view/FlutterView$AccessibilityFeature;

    iget v2, v2, Lio/flutter/view/FlutterView$AccessibilityFeature;->value:I

    or-int/2addr v1, v2

    iput v1, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    goto :goto_0

    .line 903
    :cond_0
    iget v1, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    sget-object v2, Lio/flutter/view/FlutterView$AccessibilityFeature;->DISABLE_ANIMATIONS:Lio/flutter/view/FlutterView$AccessibilityFeature;

    iget v2, v2, Lio/flutter/view/FlutterView$AccessibilityFeature;->value:I

    xor-int/lit8 v2, v2, -0x1

    and-int/2addr v1, v2

    iput v1, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    .line 906
    .end local v0    # "transitionAnimationScale":Ljava/lang/String;
    :cond_1
    :goto_0
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v0

    iget v2, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    invoke-static {v0, v1, v2}, Lio/flutter/view/FlutterView;->nativeSetAccessibilityFeatures(JI)V

    .line 907
    return-void
.end method

.method private updateViewportMetrics()V
    .locals 14

    .line 799
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 800
    return-void

    .line 801
    :cond_0
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v1

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v3, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->devicePixelRatio:F

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v4, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalWidth:I

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v5, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalHeight:I

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v6, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingTop:I

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v7, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingRight:I

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v8, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingBottom:I

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v9, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingLeft:I

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v10, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetTop:I

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v11, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetRight:I

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v12, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetBottom:I

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v13, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetLeft:I

    invoke-static/range {v1 .. v13}, Lio/flutter/view/FlutterView;->nativeSetViewportMetrics(JFIIIIIIIIII)V

    .line 806
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 807
    .local v0, "wm":Landroid/view/WindowManager;
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Display;->getRefreshRate()F

    move-result v1

    .line 808
    .local v1, "fps":F
    const-wide v2, 0x41cdcd6500000000L    # 1.0E9

    float-to-double v4, v1

    invoke-static {v4, v5}, Ljava/lang/Double;->isNaN(D)Z

    div-double/2addr v2, v4

    double-to-long v2, v2

    sput-wide v2, Lio/flutter/view/VsyncWaiter;->refreshPeriodNanos:J

    .line 809
    return-void
.end method


# virtual methods
.method public addActivityLifecycleListener(Lio/flutter/plugin/common/ActivityLifecycleListener;)V
    .locals 1
    .param p1, "listener"    # Lio/flutter/plugin/common/ActivityLifecycleListener;

    .line 231
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mActivityLifecycleListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 232
    return-void
.end method

.method public addFirstFrameListener(Lio/flutter/view/FlutterView$FirstFrameListener;)V
    .locals 1
    .param p1, "listener"    # Lio/flutter/view/FlutterView$FirstFrameListener;

    .line 265
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mFirstFrameListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 266
    return-void
.end method

.method assertAttached()V
    .locals 2

    .line 706
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 708
    return-void

    .line 707
    :cond_0
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "Platform view is not attached"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method calculateBottomKeyboardInset(Landroid/view/WindowInsets;)I
    .locals 9
    .param p1, "insets"    # Landroid/view/WindowInsets;

    .line 629
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getRootView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getHeight()I

    move-result v0

    .line 632
    .local v0, "screenHeight":I
    const-wide v1, 0x3fc70a3d70a3d70aL    # 0.18

    .line 633
    .local v1, "keyboardHeightRatioHeuristic":D
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetBottom()I

    move-result v3

    int-to-double v3, v3

    int-to-double v5, v0

    const-wide v7, 0x3fc70a3d70a3d70aL    # 0.18

    invoke-static {v5, v6}, Ljava/lang/Double;->isNaN(D)Z

    mul-double v5, v5, v7

    cmpg-double v7, v3, v5

    if-gez v7, :cond_0

    .line 635
    const/4 v3, 0x0

    return v3

    .line 639
    :cond_0
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetBottom()I

    move-result v3

    return v3
.end method

.method calculateShouldZeroSides()Lio/flutter/view/FlutterView$ZeroSides;
    .locals 5

    .line 598
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    .line 599
    .local v0, "activity":Landroid/app/Activity;
    invoke-virtual {v0}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v1

    iget v1, v1, Landroid/content/res/Configuration;->orientation:I

    .line 600
    .local v1, "orientation":I
    invoke-virtual {v0}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v2

    invoke-interface {v2}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Display;->getRotation()I

    move-result v2

    .line 602
    .local v2, "rotation":I
    const/4 v3, 0x2

    if-ne v1, v3, :cond_4

    .line 603
    const/4 v4, 0x1

    if-ne v2, v4, :cond_0

    .line 604
    sget-object v3, Lio/flutter/view/FlutterView$ZeroSides;->RIGHT:Lio/flutter/view/FlutterView$ZeroSides;

    return-object v3

    .line 606
    :cond_0
    const/4 v4, 0x3

    if-ne v2, v4, :cond_2

    .line 608
    sget v3, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v4, 0x17

    if-lt v3, v4, :cond_1

    sget-object v3, Lio/flutter/view/FlutterView$ZeroSides;->LEFT:Lio/flutter/view/FlutterView$ZeroSides;

    goto :goto_0

    :cond_1
    sget-object v3, Lio/flutter/view/FlutterView$ZeroSides;->RIGHT:Lio/flutter/view/FlutterView$ZeroSides;

    :goto_0
    return-object v3

    .line 611
    :cond_2
    if-eqz v2, :cond_3

    if-ne v2, v3, :cond_4

    .line 612
    :cond_3
    sget-object v3, Lio/flutter/view/FlutterView$ZeroSides;->BOTH:Lio/flutter/view/FlutterView$ZeroSides;

    return-object v3

    .line 617
    :cond_4
    sget-object v3, Lio/flutter/view/FlutterView$ZeroSides;->NONE:Lio/flutter/view/FlutterView$ZeroSides;

    return-object v3
.end method

.method public createSurfaceTexture()Lio/flutter/view/TextureRegistry$SurfaceTextureEntry;
    .locals 6

    .line 1072
    new-instance v0, Landroid/graphics/SurfaceTexture;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    .line 1073
    .local v0, "surfaceTexture":Landroid/graphics/SurfaceTexture;
    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->detachFromGLContext()V

    .line 1074
    new-instance v1, Lio/flutter/view/FlutterView$SurfaceTextureRegistryEntry;

    iget-object v2, p0, Lio/flutter/view/FlutterView;->nextTextureId:Ljava/util/concurrent/atomic/AtomicLong;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicLong;->getAndIncrement()J

    move-result-wide v2

    invoke-direct {v1, p0, v2, v3, v0}, Lio/flutter/view/FlutterView$SurfaceTextureRegistryEntry;-><init>(Lio/flutter/view/FlutterView;JLandroid/graphics/SurfaceTexture;)V

    .line 1076
    .local v1, "entry":Lio/flutter/view/FlutterView$SurfaceTextureRegistryEntry;
    iget-object v2, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v2}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v2

    invoke-virtual {v1}, Lio/flutter/view/FlutterView$SurfaceTextureRegistryEntry;->id()J

    move-result-wide v4

    invoke-static {v2, v3, v4, v5, v0}, Lio/flutter/view/FlutterView;->nativeRegisterTexture(JJLandroid/graphics/SurfaceTexture;)V

    .line 1077
    return-object v1
.end method

.method public destroy()V
    .locals 2

    .line 366
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 367
    return-void

    .line 369
    :cond_0
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v1, p0, Lio/flutter/view/FlutterView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 371
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->destroy()V

    .line 372
    const/4 v0, 0x0

    iput-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    .line 373
    return-void
.end method

.method public detach()Lio/flutter/view/FlutterNativeView;
    .locals 3

    .line 355
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 356
    return-object v1

    .line 357
    :cond_0
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    iget-object v2, p0, Lio/flutter/view/FlutterView;->mSurfaceCallback:Landroid/view/SurfaceHolder$Callback;

    invoke-interface {v0, v2}, Landroid/view/SurfaceHolder;->removeCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 358
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->detach()V

    .line 360
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    .line 361
    .local v0, "view":Lio/flutter/view/FlutterNativeView;
    iput-object v1, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    .line 362
    return-object v0
.end method

.method public disableTransparentBackground()V
    .locals 2

    .line 291
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lio/flutter/view/FlutterView;->setZOrderOnTop(Z)V

    .line 292
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 293
    return-void
.end method

.method protected dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "action"    # Lio/flutter/view/AccessibilityBridge$Action;

    .line 851
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lio/flutter/view/FlutterView;->dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;Ljava/lang/Object;)V

    .line 852
    return-void
.end method

.method protected dispatchSemanticsAction(ILio/flutter/view/AccessibilityBridge$Action;Ljava/lang/Object;)V
    .locals 8
    .param p1, "id"    # I
    .param p2, "action"    # Lio/flutter/view/AccessibilityBridge$Action;
    .param p3, "args"    # Ljava/lang/Object;

    .line 855
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 856
    return-void

    .line 857
    :cond_0
    const/4 v0, 0x0

    .line 858
    .local v0, "encodedArgs":Ljava/nio/ByteBuffer;
    const/4 v1, 0x0

    .line 859
    .local v1, "position":I
    if-eqz p3, :cond_1

    .line 860
    sget-object v2, Lio/flutter/plugin/common/StandardMessageCodec;->INSTANCE:Lio/flutter/plugin/common/StandardMessageCodec;

    invoke-virtual {v2, p3}, Lio/flutter/plugin/common/StandardMessageCodec;->encodeMessage(Ljava/lang/Object;)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 861
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    .line 863
    :cond_1
    iget-object v2, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v2}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v2

    iget v5, p2, Lio/flutter/view/AccessibilityBridge$Action;->value:I

    move v4, p1

    move-object v6, v0

    move v7, v1

    invoke-static/range {v2 .. v7}, Lio/flutter/view/FlutterView;->nativeDispatchSemanticsAction(JIILjava/nio/ByteBuffer;I)V

    .line 864
    return-void
.end method

.method public enableTransparentBackground()V
    .locals 2

    .line 282
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lio/flutter/view/FlutterView;->setZOrderOnTop(Z)V

    .line 283
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    const/4 v1, -0x2

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setFormat(I)V

    .line 284
    return-void
.end method

.method ensureAccessibilityEnabled()V
    .locals 3

    .line 1012
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1013
    return-void

    .line 1014
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityEnabled:Z

    .line 1015
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    if-nez v1, :cond_1

    .line 1016
    new-instance v1, Lio/flutter/view/AccessibilityBridge;

    invoke-direct {v1, p0}, Lio/flutter/view/AccessibilityBridge;-><init>(Lio/flutter/view/FlutterView;)V

    iput-object v1, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    .line 1018
    :cond_1
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v1}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v1

    invoke-static {v1, v2, v0}, Lio/flutter/view/FlutterView;->nativeSetSemanticsEnabled(JZ)V

    .line 1019
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    invoke-virtual {v1, v0}, Lio/flutter/view/AccessibilityBridge;->setAccessibilityEnabled(Z)V

    .line 1020
    return-void
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 3
    .param p1, "insets"    # Landroid/graphics/Rect;

    .line 682
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-gt v0, v1, :cond_0

    .line 684
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v1, p1, Landroid/graphics/Rect;->top:I

    iput v1, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingTop:I

    .line 685
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v1, p1, Landroid/graphics/Rect;->right:I

    iput v1, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingRight:I

    .line 686
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    const/4 v1, 0x0

    iput v1, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingBottom:I

    .line 687
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v2, p1, Landroid/graphics/Rect;->left:I

    iput v2, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingLeft:I

    .line 690
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iput v1, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetTop:I

    .line 691
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iput v1, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetRight:I

    .line 692
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v2, p1, Landroid/graphics/Rect;->bottom:I

    iput v2, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetBottom:I

    .line 693
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iput v1, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetLeft:I

    .line 694
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->updateViewportMetrics()V

    .line 695
    const/4 v0, 0x1

    return v0

    .line 697
    :cond_0
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public getAccessibilityNodeProvider()Landroid/view/accessibility/AccessibilityNodeProvider;
    .locals 1

    .line 1001
    iget-boolean v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityEnabled:Z

    if-eqz v0, :cond_0

    .line 1002
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    return-object v0

    .line 1006
    :cond_0
    const/4 v0, 0x0

    return-object v0
.end method

.method public getBitmap()Landroid/graphics/Bitmap;
    .locals 2

    .line 762
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->assertAttached()V

    .line 763
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v0

    invoke-static {v0, v1}, Lio/flutter/view/FlutterView;->nativeGetBitmap(J)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method getDevicePixelRatio()F
    .locals 1

    .line 351
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iget v0, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->devicePixelRatio:F

    return v0
.end method

.method public getFlutterNativeView()Lio/flutter/view/FlutterNativeView;
    .locals 1

    .line 215
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    return-object v0
.end method

.method public getLookupKeyForAsset(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "asset"    # Ljava/lang/String;

    .line 223
    invoke-static {p1}, Lio/flutter/view/FlutterMain;->getLookupKeyForAsset(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getLookupKeyForAsset(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "asset"    # Ljava/lang/String;
    .param p2, "packageName"    # Ljava/lang/String;

    .line 227
    invoke-static {p1, p2}, Lio/flutter/view/FlutterMain;->getLookupKeyForAsset(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getPluginRegistry()Lio/flutter/app/FlutterPluginRegistry;
    .locals 1

    .line 219
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->getPluginRegistry()Lio/flutter/app/FlutterPluginRegistry;

    move-result-object v0

    return-object v0
.end method

.method public onAccessibilityStateChanged(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .line 929
    if-eqz p1, :cond_0

    .line 930
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->ensureAccessibilityEnabled()V

    goto :goto_0

    .line 932
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityEnabled:Z

    .line 933
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    if-eqz v1, :cond_1

    .line 934
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    invoke-virtual {v1, v0}, Lio/flutter/view/AccessibilityBridge;->setAccessibilityEnabled(Z)V

    .line 936
    :cond_1
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v1}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v1

    invoke-static {v1, v2, v0}, Lio/flutter/view/FlutterView;->nativeSetSemanticsEnabled(JZ)V

    .line 938
    :goto_0
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->resetWillNotDraw()V

    .line 939
    return-void
.end method

.method public final onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;
    .locals 6
    .param p1, "insets"    # Landroid/view/WindowInsets;

    .line 647
    nop

    .line 648
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getWindowSystemUiVisibility()I

    move-result v0

    and-int/lit8 v0, v0, 0x4

    const/4 v1, 0x1

    const/4 v2, 0x0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    .line 649
    .local v0, "statusBarHidden":Z
    :goto_0
    nop

    .line 650
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getWindowSystemUiVisibility()I

    move-result v3

    and-int/lit8 v3, v3, 0x2

    if-eqz v3, :cond_1

    goto :goto_1

    :cond_1
    const/4 v1, 0x0

    .line 654
    .local v1, "navigationBarHidden":Z
    :goto_1
    sget-object v3, Lio/flutter/view/FlutterView$ZeroSides;->NONE:Lio/flutter/view/FlutterView$ZeroSides;

    .line 655
    .local v3, "zeroSides":Lio/flutter/view/FlutterView$ZeroSides;
    if-eqz v1, :cond_2

    .line 656
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->calculateShouldZeroSides()Lio/flutter/view/FlutterView$ZeroSides;

    move-result-object v3

    .line 660
    :cond_2
    iget-object v4, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    if-eqz v0, :cond_3

    const/4 v5, 0x0

    goto :goto_2

    :cond_3
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetTop()I

    move-result v5

    :goto_2
    iput v5, v4, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingTop:I

    .line 661
    iget-object v4, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    sget-object v5, Lio/flutter/view/FlutterView$ZeroSides;->RIGHT:Lio/flutter/view/FlutterView$ZeroSides;

    if-eq v3, v5, :cond_5

    sget-object v5, Lio/flutter/view/FlutterView$ZeroSides;->BOTH:Lio/flutter/view/FlutterView$ZeroSides;

    if-ne v3, v5, :cond_4

    goto :goto_3

    .line 662
    :cond_4
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetRight()I

    move-result v5

    goto :goto_4

    .line 661
    :cond_5
    :goto_3
    nop

    .line 662
    const/4 v5, 0x0

    :goto_4
    iput v5, v4, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingRight:I

    .line 663
    iget-object v4, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iput v2, v4, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingBottom:I

    .line 664
    iget-object v4, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    sget-object v5, Lio/flutter/view/FlutterView$ZeroSides;->LEFT:Lio/flutter/view/FlutterView$ZeroSides;

    if-eq v3, v5, :cond_7

    sget-object v5, Lio/flutter/view/FlutterView$ZeroSides;->BOTH:Lio/flutter/view/FlutterView$ZeroSides;

    if-ne v3, v5, :cond_6

    goto :goto_5

    .line 665
    :cond_6
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetLeft()I

    move-result v5

    goto :goto_6

    .line 664
    :cond_7
    :goto_5
    nop

    .line 665
    const/4 v5, 0x0

    :goto_6
    iput v5, v4, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalPaddingLeft:I

    .line 668
    iget-object v4, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iput v2, v4, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetTop:I

    .line 669
    iget-object v4, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iput v2, v4, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetRight:I

    .line 672
    iget-object v4, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    if-eqz v1, :cond_8

    .line 673
    invoke-virtual {p0, p1}, Lio/flutter/view/FlutterView;->calculateBottomKeyboardInset(Landroid/view/WindowInsets;)I

    move-result v5

    goto :goto_7

    :cond_8
    invoke-virtual {p1}, Landroid/view/WindowInsets;->getSystemWindowInsetBottom()I

    move-result v5

    :goto_7
    iput v5, v4, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetBottom:I

    .line 674
    iget-object v4, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iput v2, v4, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalViewInsetLeft:I

    .line 675
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->updateViewportMetrics()V

    .line 676
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onApplyWindowInsets(Landroid/view/WindowInsets;)Landroid/view/WindowInsets;

    move-result-object v2

    return-object v2
.end method

.method protected onAttachedToWindow()V
    .locals 4

    .line 868
    invoke-super {p0}, Landroid/view/SurfaceView;->onAttachedToWindow()V

    .line 869
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityEnabled:Z

    .line 870
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v0

    iput-boolean v0, p0, Lio/flutter/view/FlutterView;->mTouchExplorationEnabled:Z

    .line 871
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_0

    .line 872
    const-string v0, "transition_animation_scale"

    invoke-static {v0}, Landroid/provider/Settings$Global;->getUriFor(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 873
    .local v0, "transitionUri":Landroid/net/Uri;
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v2, 0x0

    iget-object v3, p0, Lio/flutter/view/FlutterView;->mAnimationScaleObserver:Lio/flutter/view/FlutterView$AnimationScaleObserver;

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 876
    .end local v0    # "transitionUri":Landroid/net/Uri;
    :cond_0
    iget-boolean v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityEnabled:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lio/flutter/view/FlutterView;->mTouchExplorationEnabled:Z

    if-eqz v0, :cond_2

    .line 877
    :cond_1
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->ensureAccessibilityEnabled()V

    .line 879
    :cond_2
    iget-boolean v0, p0, Lio/flutter/view/FlutterView;->mTouchExplorationEnabled:Z

    if-eqz v0, :cond_3

    .line 880
    iget v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    sget-object v1, Lio/flutter/view/FlutterView$AccessibilityFeature;->ACCESSIBLE_NAVIGATION:Lio/flutter/view/FlutterView$AccessibilityFeature;

    iget v1, v1, Lio/flutter/view/FlutterView$AccessibilityFeature;->value:I

    or-int/2addr v0, v1

    iput v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    goto :goto_0

    .line 882
    :cond_3
    iget v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    sget-object v1, Lio/flutter/view/FlutterView$AccessibilityFeature;->ACCESSIBLE_NAVIGATION:Lio/flutter/view/FlutterView$AccessibilityFeature;

    iget v1, v1, Lio/flutter/view/FlutterView$AccessibilityFeature;->value:I

    xor-int/lit8 v1, v1, -0x1

    and-int/2addr v0, v1

    iput v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityFeatureFlags:I

    .line 885
    :goto_0
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->updateAccessibilityFeatures()V

    .line 886
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->resetWillNotDraw()V

    .line 887
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0, p0}, Landroid/view/accessibility/AccessibilityManager;->addAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    .line 888
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_5

    .line 889
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mTouchExplorationListener:Lio/flutter/view/FlutterView$TouchExplorationListener;

    if-nez v0, :cond_4

    .line 890
    new-instance v0, Lio/flutter/view/FlutterView$TouchExplorationListener;

    invoke-direct {v0, p0}, Lio/flutter/view/FlutterView$TouchExplorationListener;-><init>(Lio/flutter/view/FlutterView;)V

    iput-object v0, p0, Lio/flutter/view/FlutterView;->mTouchExplorationListener:Lio/flutter/view/FlutterView$TouchExplorationListener;

    .line 892
    :cond_4
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lio/flutter/view/FlutterView;->mTouchExplorationListener:Lio/flutter/view/FlutterView$TouchExplorationListener;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->addTouchExplorationStateChangeListener(Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;)Z

    .line 894
    :cond_5
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .line 345
    invoke-super {p0, p1}, Landroid/view/SurfaceView;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 346
    invoke-direct {p0, p1}, Lio/flutter/view/FlutterView;->setLocales(Landroid/content/res/Configuration;)V

    .line 347
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->setUserSettings()V

    .line 348
    return-void
.end method

.method public onCreateInputConnection(Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;
    .locals 3
    .param p1, "outAttrs"    # Landroid/view/inputmethod/EditorInfo;

    .line 378
    :try_start_0
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mTextInputPlugin:Lio/flutter/plugin/editing/TextInputPlugin;

    invoke-virtual {v0, p0, p1}, Lio/flutter/plugin/editing/TextInputPlugin;->createInputConnection(Lio/flutter/view/FlutterView;Landroid/view/inputmethod/EditorInfo;)Landroid/view/inputmethod/InputConnection;

    move-result-object v0

    iput-object v0, p0, Lio/flutter/view/FlutterView;->mLastInputConnection:Landroid/view/inputmethod/InputConnection;

    .line 379
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mLastInputConnection:Landroid/view/inputmethod/InputConnection;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 380
    :catch_0
    move-exception v0

    .line 381
    .local v0, "e":Lorg/json/JSONException;
    const-string v1, "FlutterView"

    const-string v2, "Failed to create input connection"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 382
    const/4 v1, 0x0

    return-object v1
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .line 911
    invoke-super {p0}, Landroid/view/SurfaceView;->onDetachedFromWindow()V

    .line 912
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lio/flutter/view/FlutterView;->mAnimationScaleObserver:Lio/flutter/view/FlutterView$AnimationScaleObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 913
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    invoke-virtual {v0, p0}, Landroid/view/accessibility/AccessibilityManager;->removeAccessibilityStateChangeListener(Landroid/view/accessibility/AccessibilityManager$AccessibilityStateChangeListener;)Z

    .line 914
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 915
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityManager:Landroid/view/accessibility/AccessibilityManager;

    iget-object v1, p0, Lio/flutter/view/FlutterView;->mTouchExplorationListener:Lio/flutter/view/FlutterView$TouchExplorationListener;

    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->removeTouchExplorationStateChangeListener(Landroid/view/accessibility/AccessibilityManager$TouchExplorationStateChangeListener;)Z

    .line 917
    :cond_0
    return-void
.end method

.method public onFirstFrame()V
    .locals 3

    .line 837
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lio/flutter/view/FlutterView;->mFirstFrameListeners:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 838
    .local v0, "listeners":Ljava/util/List;, "Ljava/util/List<Lio/flutter/view/FlutterView$FirstFrameListener;>;"
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/flutter/view/FlutterView$FirstFrameListener;

    .line 839
    .local v2, "listener":Lio/flutter/view/FlutterView$FirstFrameListener;
    invoke-interface {v2}, Lio/flutter/view/FlutterView$FirstFrameListener;->onFirstFrame()V

    .line 840
    .end local v2    # "listener":Lio/flutter/view/FlutterView$FirstFrameListener;
    goto :goto_0

    .line 841
    :cond_0
    return-void
.end method

.method public onHoverEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 567
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 568
    const/4 v0, 0x0

    return v0

    .line 571
    :cond_0
    invoke-direct {p0, p1}, Lio/flutter/view/FlutterView;->handleAccessibilityHoverEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    .line 572
    .local v0, "handled":Z
    nop

    .line 576
    return v0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .line 196
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 197
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 200
    :cond_0
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getDeviceId()I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 201
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mLastInputConnection:Landroid/view/inputmethod/InputConnection;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lio/flutter/view/FlutterView;->mImm:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v0}, Landroid/view/inputmethod/InputMethodManager;->isAcceptingText()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 202
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mLastInputConnection:Landroid/view/inputmethod/InputConnection;

    invoke-interface {v0, p2}, Landroid/view/inputmethod/InputConnection;->sendKeyEvent(Landroid/view/KeyEvent;)Z

    .line 206
    :cond_1
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 207
    .local v0, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "type"

    const-string v2, "keydown"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    const-string v1, "keymap"

    const-string v2, "android"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 209
    invoke-direct {p0, p2, v0}, Lio/flutter/view/FlutterView;->encodeKeyEvent(Landroid/view/KeyEvent;Ljava/util/Map;)V

    .line 210
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mFlutterKeyEventChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    invoke-virtual {v1, v0}, Lio/flutter/plugin/common/BasicMessageChannel;->send(Ljava/lang/Object;)V

    .line 211
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .line 182
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 183
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    return v0

    .line 186
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 187
    .local v0, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "type"

    const-string v2, "keyup"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    const-string v1, "keymap"

    const-string v2, "android"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 189
    invoke-direct {p0, p2, v0}, Lio/flutter/view/FlutterView;->encodeKeyEvent(Landroid/view/KeyEvent;Ljava/util/Map;)V

    .line 190
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mFlutterKeyEventChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    invoke-virtual {v1, v0}, Lio/flutter/plugin/common/BasicMessageChannel;->send(Ljava/lang/Object;)V

    .line 191
    invoke-super {p0, p1, p2}, Landroid/view/SurfaceView;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    return v1
.end method

.method public onMemoryPressure()V
    .locals 3

    .line 255
    new-instance v0, Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 256
    .local v0, "message":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/Object;>;"
    const-string v1, "type"

    const-string v2, "memoryPressure"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 257
    iget-object v1, p0, Lio/flutter/view/FlutterView;->mFlutterSystemChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    invoke-virtual {v1, v0}, Lio/flutter/plugin/common/BasicMessageChannel;->send(Ljava/lang/Object;)V

    .line 258
    return-void
.end method

.method public onPause()V
    .locals 2

    .line 239
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mFlutterLifecycleChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    const-string v1, "AppLifecycleState.inactive"

    invoke-virtual {v0, v1}, Lio/flutter/plugin/common/BasicMessageChannel;->send(Ljava/lang/Object;)V

    .line 240
    return-void
.end method

.method public onPostResume()V
    .locals 2

    .line 243
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->updateAccessibilityFeatures()V

    .line 244
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mActivityLifecycleListeners:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/flutter/plugin/common/ActivityLifecycleListener;

    .line 245
    .local v1, "listener":Lio/flutter/plugin/common/ActivityLifecycleListener;
    invoke-interface {v1}, Lio/flutter/plugin/common/ActivityLifecycleListener;->onPostResume()V

    .line 246
    .end local v1    # "listener":Lio/flutter/plugin/common/ActivityLifecycleListener;
    goto :goto_0

    .line 247
    :cond_0
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mFlutterLifecycleChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    const-string v1, "AppLifecycleState.resumed"

    invoke-virtual {v0, v1}, Lio/flutter/plugin/common/BasicMessageChannel;->send(Ljava/lang/Object;)V

    .line 248
    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "oldWidth"    # I
    .param p4, "oldHeight"    # I

    .line 581
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iput p1, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalWidth:I

    .line 582
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mMetrics:Lio/flutter/view/FlutterView$ViewportMetrics;

    iput p2, v0, Lio/flutter/view/FlutterView$ViewportMetrics;->physicalHeight:I

    .line 583
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->updateViewportMetrics()V

    .line 584
    invoke-super {p0, p1, p2, p3, p4}, Landroid/view/SurfaceView;->onSizeChanged(IIII)V

    .line 585
    return-void
.end method

.method public onStart()V
    .locals 2

    .line 235
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mFlutterLifecycleChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    const-string v1, "AppLifecycleState.inactive"

    invoke-virtual {v0, v1}, Lio/flutter/plugin/common/BasicMessageChannel;->send(Ljava/lang/Object;)V

    .line 236
    return-void
.end method

.method public onStop()V
    .locals 2

    .line 251
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mFlutterLifecycleChannel:Lio/flutter/plugin/common/BasicMessageChannel;

    const-string v1, "AppLifecycleState.paused"

    invoke-virtual {v0, v1}, Lio/flutter/plugin/common/BasicMessageChannel;->send(Ljava/lang/Object;)V

    .line 252
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 16
    .param p1, "event"    # Landroid/view/MotionEvent;

    .line 505
    move-object/from16 v6, p0

    invoke-direct/range {p0 .. p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    const/4 v1, 0x0

    if-nez v0, :cond_0

    .line 506
    return v1

    .line 514
    :cond_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v0, v2, :cond_1

    .line 515
    invoke-virtual/range {p0 .. p1}, Lio/flutter/view/FlutterView;->requestUnbufferedDispatch(Landroid/view/MotionEvent;)V

    .line 519
    :cond_1
    const/16 v7, 0x15

    .line 520
    .local v7, "kPointerDataFieldCount":I
    const/16 v8, 0x8

    .line 524
    .local v8, "kBytePerField":I
    const/4 v9, 0x1

    .line 526
    .local v9, "kPointerDataFlagBatched":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getPointerCount()I

    move-result v10

    .line 528
    .local v10, "pointerCount":I
    mul-int/lit8 v0, v10, 0x15

    mul-int/lit8 v0, v0, 0x8

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v11

    .line 529
    .local v11, "packet":Ljava/nio/ByteBuffer;
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {v11, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 531
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v12

    .line 532
    .local v12, "maskedAction":I
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionMasked()I

    move-result v0

    invoke-direct {v6, v0}, Lio/flutter/view/FlutterView;->getPointerChangeForAction(I)I

    move-result v13

    .line 533
    .local v13, "pointerChange":I
    const/4 v14, 0x1

    if-eqz v12, :cond_7

    const/4 v0, 0x5

    if-ne v12, v0, :cond_2

    goto :goto_3

    .line 536
    :cond_2
    if-eq v12, v14, :cond_4

    const/4 v0, 0x6

    if-ne v12, v0, :cond_3

    goto :goto_1

    .line 555
    :cond_3
    nop

    .local v1, "p":I
    :goto_0
    move v15, v1

    .end local v1    # "p":I
    .local v15, "p":I
    if-ge v15, v10, :cond_8

    .line 556
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v15

    move v3, v13

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lio/flutter/view/FlutterView;->addPointerForIndex(Landroid/view/MotionEvent;IIILjava/nio/ByteBuffer;)V

    .line 555
    add-int/lit8 v1, v15, 0x1

    goto :goto_0

    .line 541
    .end local v15    # "p":I
    :cond_4
    :goto_1
    nop

    .restart local v1    # "p":I
    :goto_2
    move v15, v1

    .end local v1    # "p":I
    .restart local v15    # "p":I
    if-ge v15, v10, :cond_6

    .line 542
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v0

    if-eq v15, v0, :cond_5

    .line 543
    move-object/from16 v5, p1

    invoke-virtual {v5, v15}, Landroid/view/MotionEvent;->getToolType(I)I

    move-result v0

    if-ne v0, v14, :cond_5

    .line 544
    const/4 v3, 0x5

    const/4 v4, 0x1

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v2, v15

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lio/flutter/view/FlutterView;->addPointerForIndex(Landroid/view/MotionEvent;IIILjava/nio/ByteBuffer;)V

    .line 541
    :cond_5
    add-int/lit8 v1, v15, 0x1

    goto :goto_2

    .line 550
    .end local v15    # "p":I
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v2

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v3, v13

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lio/flutter/view/FlutterView;->addPointerForIndex(Landroid/view/MotionEvent;IIILjava/nio/ByteBuffer;)V

    goto :goto_4

    .line 535
    :cond_7
    :goto_3
    invoke-virtual/range {p1 .. p1}, Landroid/view/MotionEvent;->getActionIndex()I

    move-result v2

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move v3, v13

    move-object v5, v11

    invoke-direct/range {v0 .. v5}, Lio/flutter/view/FlutterView;->addPointerForIndex(Landroid/view/MotionEvent;IIILjava/nio/ByteBuffer;)V

    .line 560
    :cond_8
    :goto_4
    nop

    .line 561
    iget-object v0, v6, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0}, Lio/flutter/view/FlutterNativeView;->get()J

    move-result-wide v0

    invoke-virtual {v11}, Ljava/nio/ByteBuffer;->position()I

    move-result v2

    invoke-static {v0, v1, v11, v2}, Lio/flutter/view/FlutterView;->nativeDispatchPointerDataPacket(JLjava/nio/ByteBuffer;I)V

    .line 562
    return v14
.end method

.method public popRoute()V
    .locals 3

    .line 304
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mFlutterNavigationChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v1, "popRoute"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 305
    return-void
.end method

.method public pushRoute(Ljava/lang/String;)V
    .locals 2
    .param p1, "route"    # Ljava/lang/String;

    .line 300
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mFlutterNavigationChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v1, "pushRoute"

    invoke-virtual {v0, v1, p1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 301
    return-void
.end method

.method public removeFirstFrameListener(Lio/flutter/view/FlutterView$FirstFrameListener;)V
    .locals 1
    .param p1, "listener"    # Lio/flutter/view/FlutterView$FirstFrameListener;

    .line 272
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mFirstFrameListeners:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 273
    return-void
.end method

.method resetAccessibilityTree()V
    .locals 1

    .line 1023
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    if-eqz v0, :cond_0

    .line 1024
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    invoke-virtual {v0}, Lio/flutter/view/AccessibilityBridge;->reset()V

    .line 1026
    :cond_0
    return-void
.end method

.method public runFromBundle(Lio/flutter/view/FlutterRunArguments;)V
    .locals 1
    .param p1, "args"    # Lio/flutter/view/FlutterRunArguments;

    .line 718
    invoke-virtual {p0}, Lio/flutter/view/FlutterView;->assertAttached()V

    .line 719
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->preRun()V

    .line 720
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0, p1}, Lio/flutter/view/FlutterNativeView;->runFromBundle(Lio/flutter/view/FlutterRunArguments;)V

    .line 721
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->postRun()V

    .line 722
    return-void
.end method

.method public runFromBundle(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "bundlePath"    # Ljava/lang/String;
    .param p2, "defaultPath"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 730
    const-string v0, "main"

    const/4 v1, 0x0

    invoke-virtual {p0, p1, p2, v0, v1}, Lio/flutter/view/FlutterView;->runFromBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 731
    return-void
.end method

.method public runFromBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "bundlePath"    # Ljava/lang/String;
    .param p2, "defaultPath"    # Ljava/lang/String;
    .param p3, "entrypoint"    # Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 739
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, p3, v0}, Lio/flutter/view/FlutterView;->runFromBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 740
    return-void
.end method

.method public runFromBundle(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "bundlePath"    # Ljava/lang/String;
    .param p2, "defaultPath"    # Ljava/lang/String;
    .param p3, "entrypoint"    # Ljava/lang/String;
    .param p4, "reuseRuntimeController"    # Z
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .line 749
    new-instance v0, Lio/flutter/view/FlutterRunArguments;

    invoke-direct {v0}, Lio/flutter/view/FlutterRunArguments;-><init>()V

    .line 750
    .local v0, "args":Lio/flutter/view/FlutterRunArguments;
    iput-object p1, v0, Lio/flutter/view/FlutterRunArguments;->bundlePath:Ljava/lang/String;

    .line 751
    iput-object p3, v0, Lio/flutter/view/FlutterRunArguments;->entrypoint:Ljava/lang/String;

    .line 752
    iput-object p2, v0, Lio/flutter/view/FlutterRunArguments;->defaultPath:Ljava/lang/String;

    .line 753
    invoke-virtual {p0, v0}, Lio/flutter/view/FlutterView;->runFromBundle(Lio/flutter/view/FlutterRunArguments;)V

    .line 754
    return-void
.end method

.method public send(Ljava/lang/String;Ljava/nio/ByteBuffer;)V
    .locals 1
    .param p1, "channel"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/nio/ByteBuffer;

    .line 1045
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Lio/flutter/view/FlutterView;->send(Ljava/lang/String;Ljava/nio/ByteBuffer;Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;)V

    .line 1046
    return-void
.end method

.method public send(Ljava/lang/String;Ljava/nio/ByteBuffer;Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;)V
    .locals 3
    .param p1, "channel"    # Ljava/lang/String;
    .param p2, "message"    # Ljava/nio/ByteBuffer;
    .param p3, "callback"    # Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;

    .line 1050
    invoke-direct {p0}, Lio/flutter/view/FlutterView;->isAttached()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1051
    const-string v0, "FlutterView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "FlutterView.send called on a detached view, channel="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1052
    return-void

    .line 1054
    :cond_0
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0, p1, p2, p3}, Lio/flutter/view/FlutterNativeView;->send(Ljava/lang/String;Ljava/nio/ByteBuffer;Lio/flutter/plugin/common/BinaryMessenger$BinaryReply;)V

    .line 1055
    return-void
.end method

.method public setInitialRoute(Ljava/lang/String;)V
    .locals 2
    .param p1, "route"    # Ljava/lang/String;

    .line 296
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mFlutterNavigationChannel:Lio/flutter/plugin/common/MethodChannel;

    const-string v1, "setInitialRoute"

    invoke-virtual {v0, v1, p1}, Lio/flutter/plugin/common/MethodChannel;->invokeMethod(Ljava/lang/String;Ljava/lang/Object;)V

    .line 297
    return-void
.end method

.method public setMessageHandler(Ljava/lang/String;Lio/flutter/plugin/common/BinaryMessenger$BinaryMessageHandler;)V
    .locals 1
    .param p1, "channel"    # Ljava/lang/String;
    .param p2, "handler"    # Lio/flutter/plugin/common/BinaryMessenger$BinaryMessageHandler;

    .line 1059
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mNativeView:Lio/flutter/view/FlutterNativeView;

    invoke-virtual {v0, p1, p2}, Lio/flutter/view/FlutterNativeView;->setMessageHandler(Ljava/lang/String;Lio/flutter/plugin/common/BinaryMessenger$BinaryMessageHandler;)V

    .line 1060
    return-void
.end method

.method public updateCustomAccessibilityActions(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V
    .locals 3
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "strings"    # [Ljava/lang/String;

    .line 825
    :try_start_0
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    if-eqz v0, :cond_0

    .line 826
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 827
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    invoke-virtual {v0, p1, p2}, Lio/flutter/view/AccessibilityBridge;->updateCustomAccessibilityActions(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 831
    :cond_0
    goto :goto_0

    .line 829
    :catch_0
    move-exception v0

    .line 830
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "FlutterView"

    const-string v2, "Uncaught exception while updating local context actions"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 832
    .end local v0    # "ex":Ljava/lang/Exception;
    :goto_0
    return-void
.end method

.method public updateSemantics(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V
    .locals 3
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "strings"    # [Ljava/lang/String;

    .line 814
    :try_start_0
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    if-eqz v0, :cond_0

    .line 815
    sget-object v0, Ljava/nio/ByteOrder;->LITTLE_ENDIAN:Ljava/nio/ByteOrder;

    invoke-virtual {p1, v0}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    .line 816
    iget-object v0, p0, Lio/flutter/view/FlutterView;->mAccessibilityNodeProvider:Lio/flutter/view/AccessibilityBridge;

    invoke-virtual {v0, p1, p2}, Lio/flutter/view/AccessibilityBridge;->updateSemantics(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 820
    :cond_0
    goto :goto_0

    .line 818
    :catch_0
    move-exception v0

    .line 819
    .local v0, "ex":Ljava/lang/Exception;
    const-string v1, "FlutterView"

    const-string v2, "Uncaught exception while updating semantics"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 821
    .end local v0    # "ex":Ljava/lang/Exception;
    :goto_0
    return-void
.end method
