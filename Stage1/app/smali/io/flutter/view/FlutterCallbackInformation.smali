.class public final Lio/flutter/view/FlutterCallbackInformation;
.super Ljava/lang/Object;
.source "FlutterCallbackInformation.java"


# instance fields
.field public final callbackClassName:Ljava/lang/String;

.field public final callbackLibraryPath:Ljava/lang/String;

.field public final callbackName:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "callbackName"    # Ljava/lang/String;
    .param p2, "callbackClassName"    # Ljava/lang/String;
    .param p3, "callbackLibraryPath"    # Ljava/lang/String;

    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lio/flutter/view/FlutterCallbackInformation;->callbackName:Ljava/lang/String;

    .line 29
    iput-object p2, p0, Lio/flutter/view/FlutterCallbackInformation;->callbackClassName:Ljava/lang/String;

    .line 30
    iput-object p3, p0, Lio/flutter/view/FlutterCallbackInformation;->callbackLibraryPath:Ljava/lang/String;

    .line 31
    return-void
.end method

.method public static lookupCallbackInformation(J)Lio/flutter/view/FlutterCallbackInformation;
    .locals 1
    .param p0, "handle"    # J

    .line 23
    invoke-static {p0, p1}, Lio/flutter/view/FlutterCallbackInformation;->nativeLookupCallbackInformation(J)Lio/flutter/view/FlutterCallbackInformation;

    move-result-object v0

    return-object v0
.end method

.method private static native nativeLookupCallbackInformation(J)Lio/flutter/view/FlutterCallbackInformation;
.end method
