.class public Lio/flutter/view/VsyncWaiter;
.super Ljava/lang/Object;
.source "VsyncWaiter.java"


# static fields
.field public static refreshPeriodNanos:J


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .line 11
    const-wide/32 v0, 0xfe502a

    sput-wide v0, Lio/flutter/view/VsyncWaiter;->refreshPeriodNanos:J

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .line 9
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method static synthetic access$000(JJJ)V
    .locals 0
    .param p0, "x0"    # J
    .param p2, "x1"    # J
    .param p4, "x2"    # J

    .line 9
    invoke-static/range {p0 .. p5}, Lio/flutter/view/VsyncWaiter;->nativeOnVsync(JJJ)V

    return-void
.end method

.method public static asyncWaitForVsync(J)V
    .locals 2
    .param p0, "cookie"    # J

    .line 14
    invoke-static {}, Landroid/view/Choreographer;->getInstance()Landroid/view/Choreographer;

    move-result-object v0

    new-instance v1, Lio/flutter/view/VsyncWaiter$1;

    invoke-direct {v1, p0, p1}, Lio/flutter/view/VsyncWaiter$1;-><init>(J)V

    invoke-virtual {v0, v1}, Landroid/view/Choreographer;->postFrameCallback(Landroid/view/Choreographer$FrameCallback;)V

    .line 20
    return-void
.end method

.method private static native nativeOnVsync(JJJ)V
.end method
