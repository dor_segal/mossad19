.class Lio/flutter/view/AccessibilityBridge$SemanticsObject;
.super Ljava/lang/Object;
.source "AccessibilityBridge.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lio/flutter/view/AccessibilityBridge;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SemanticsObject"
.end annotation


# static fields
.field static final synthetic $assertionsDisabled:Z


# instance fields
.field actions:I

.field private bottom:F

.field childrenInHitTestOrder:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lio/flutter/view/AccessibilityBridge$SemanticsObject;",
            ">;"
        }
    .end annotation
.end field

.field childrenInTraversalOrder:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lio/flutter/view/AccessibilityBridge$SemanticsObject;",
            ">;"
        }
    .end annotation
.end field

.field customAccessibilityActions:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List<",
            "Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;",
            ">;"
        }
    .end annotation
.end field

.field decreasedValue:Ljava/lang/String;

.field flags:I

.field private globalGeometryDirty:Z

.field private globalRect:Landroid/graphics/Rect;

.field private globalTransform:[F

.field hadPreviousConfig:Z

.field hint:Ljava/lang/String;

.field id:I

.field increasedValue:Ljava/lang/String;

.field private inverseTransform:[F

.field private inverseTransformDirty:Z

.field label:Ljava/lang/String;

.field private left:F

.field onLongPressOverride:Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

.field onTapOverride:Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

.field parent:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

.field previousActions:I

.field previousFlags:I

.field previousLabel:Ljava/lang/String;

.field previousScrollExtentMax:F

.field previousScrollExtentMin:F

.field previousScrollPosition:F

.field previousTextSelectionBase:I

.field previousTextSelectionExtent:I

.field previousValue:Ljava/lang/String;

.field private right:F

.field scrollChildren:I

.field scrollExtentMax:F

.field scrollExtentMin:F

.field scrollIndex:I

.field scrollPosition:F

.field textDirection:Lio/flutter/view/AccessibilityBridge$TextDirection;

.field textSelectionBase:I

.field textSelectionExtent:I

.field final synthetic this$0:Lio/flutter/view/AccessibilityBridge;

.field private top:F

.field private transform:[F

.field value:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .line 963
    const-class v0, Lio/flutter/view/AccessibilityBridge;

    return-void
.end method

.method constructor <init>(Lio/flutter/view/AccessibilityBridge;)V
    .locals 0

    .line 964
    iput-object p1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->this$0:Lio/flutter/view/AccessibilityBridge;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 966
    const/4 p1, -0x1

    iput p1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    .line 984
    const/4 p1, 0x0

    iput-boolean p1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hadPreviousConfig:Z

    .line 1008
    const/4 p1, 0x1

    iput-boolean p1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->inverseTransformDirty:Z

    .line 1011
    iput-boolean p1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalGeometryDirty:Z

    .line 964
    return-void
.end method

.method static synthetic access$000(Lio/flutter/view/AccessibilityBridge$SemanticsObject;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 963
    invoke-direct {p0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->getValueLabelHint()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$102(Lio/flutter/view/AccessibilityBridge$SemanticsObject;Z)Z
    .locals 0
    .param p0, "x0"    # Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    .param p1, "x1"    # Z

    .line 963
    iput-boolean p1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalGeometryDirty:Z

    return p1
.end method

.method static synthetic access$202(Lio/flutter/view/AccessibilityBridge$SemanticsObject;Z)Z
    .locals 0
    .param p0, "x0"    # Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    .param p1, "x1"    # Z

    .line 963
    iput-boolean p1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->inverseTransformDirty:Z

    return p1
.end method

.method private ensureInverseTransform()V
    .locals 3

    .line 1167
    iget-boolean v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->inverseTransformDirty:Z

    if-nez v0, :cond_0

    .line 1168
    return-void

    .line 1170
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->inverseTransformDirty:Z

    .line 1171
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->inverseTransform:[F

    if-nez v1, :cond_1

    .line 1172
    const/16 v1, 0x10

    new-array v1, v1, [F

    iput-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->inverseTransform:[F

    .line 1174
    :cond_1
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->inverseTransform:[F

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->transform:[F

    invoke-static {v1, v0, v2, v0}, Landroid/opengl/Matrix;->invertM([FI[FI)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1175
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->inverseTransform:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([FF)V

    .line 1177
    :cond_2
    return-void
.end method

.method private getValueLabelHint()Ljava/lang/String;
    .locals 6

    .line 1330
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 1331
    .local v0, "sb":Ljava/lang/StringBuilder;
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/String;

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->value:Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v2, v1, v3

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    const/4 v4, 0x1

    aput-object v2, v1, v4

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hint:Ljava/lang/String;

    const/4 v4, 0x2

    aput-object v2, v1, v4

    .line 1332
    .local v1, "array":[Ljava/lang/String;
    array-length v2, v1

    :goto_0
    if-ge v3, v2, :cond_2

    aget-object v4, v1, v3

    .line 1333
    .local v4, "word":Ljava/lang/String;
    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v5

    if-lez v5, :cond_1

    .line 1334
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v5

    if-lez v5, :cond_0

    const-string v5, ", "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1335
    :cond_0
    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1332
    .end local v4    # "word":Ljava/lang/String;
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 1338
    :cond_2
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->length()I

    move-result v2

    if-lez v2, :cond_3

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_1

    :cond_3
    const/4 v2, 0x0

    :goto_1
    return-object v2
.end method

.method private max(FFFF)F
    .locals 1
    .param p1, "a"    # F
    .param p2, "b"    # F
    .param p3, "c"    # F
    .param p4, "d"    # F

    .line 1326
    invoke-static {p3, p4}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->max(FF)F

    move-result v0

    return v0
.end method

.method private min(FFFF)F
    .locals 1
    .param p1, "a"    # F
    .param p2, "b"    # F
    .param p3, "c"    # F
    .param p4, "d"    # F

    .line 1322
    invoke-static {p3, p4}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p2, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    invoke-static {p1, v0}, Ljava/lang/Math;->min(FF)F

    move-result v0

    return v0
.end method

.method private transformPoint([F[F[F)V
    .locals 6
    .param p1, "result"    # [F
    .param p2, "transform"    # [F
    .param p3, "point"    # [F

    .line 1313
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v0, p1

    move-object v2, p2

    move-object v4, p3

    invoke-static/range {v0 .. v5}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 1314
    const/4 v0, 0x3

    aget v1, p1, v0

    .line 1315
    .local v1, "w":F
    const/4 v2, 0x0

    aget v3, p1, v2

    div-float/2addr v3, v1

    aput v3, p1, v2

    .line 1316
    const/4 v2, 0x1

    aget v3, p1, v2

    div-float/2addr v3, v1

    aput v3, p1, v2

    .line 1317
    const/4 v2, 0x2

    aget v3, p1, v2

    div-float/2addr v3, v1

    aput v3, p1, v2

    .line 1318
    const/4 v2, 0x0

    aput v2, p1, v0

    .line 1319
    return-void
.end method


# virtual methods
.method collectRoutes(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List<",
            "Lio/flutter/view/AccessibilityBridge$SemanticsObject;",
            ">;)V"
        }
    .end annotation

    .line 1223
    .local p1, "edges":Ljava/util/List;, "Ljava/util/List<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    sget-object v0, Lio/flutter/view/AccessibilityBridge$Flag;->SCOPES_ROUTE:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {p0, v0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1224
    invoke-interface {p1, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1226
    :cond_0
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 1227
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 1228
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    invoke-virtual {v1, p1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->collectRoutes(Ljava/util/List;)V

    .line 1227
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1231
    .end local v0    # "i":I
    :cond_1
    return-void
.end method

.method didChangeLabel()Z
    .locals 3

    .line 1038
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    const/4 v1, 0x0

    if-nez v0, :cond_0

    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousLabel:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1039
    return v1

    .line 1041
    :cond_0
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousLabel:Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousLabel:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    goto :goto_0

    :cond_1
    goto :goto_1

    :cond_2
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method didScroll()Z
    .locals 2

    .line 1033
    iget v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollPosition:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousScrollPosition:F

    invoke-static {v0}, Ljava/lang/Float;->isNaN(F)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousScrollPosition:F

    iget v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollPosition:F

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method getGlobalRect()Landroid/graphics/Rect;
    .locals 1

    .line 1180
    nop

    .line 1181
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalRect:Landroid/graphics/Rect;

    return-object v0
.end method

.method getRouteName()Ljava/lang/String;
    .locals 3

    .line 1236
    sget-object v0, Lio/flutter/view/AccessibilityBridge$Flag;->NAMES_ROUTE:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {p0, v0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1237
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1238
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    return-object v0

    .line 1241
    :cond_0
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 1242
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 1243
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    invoke-virtual {v1}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->getRouteName()Ljava/lang/String;

    move-result-object v1

    .line 1244
    .local v1, "newName":Ljava/lang/String;
    if-eqz v1, :cond_1

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 1245
    return-object v1

    .line 1242
    .end local v1    # "newName":Ljava/lang/String;
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1249
    .end local v0    # "i":I
    :cond_2
    const/4 v0, 0x0

    return-object v0
.end method

.method hadAction(Lio/flutter/view/AccessibilityBridge$Action;)Z
    .locals 2
    .param p1, "action"    # Lio/flutter/view/AccessibilityBridge$Action;

    .line 1020
    iget v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousActions:I

    iget v1, p1, Lio/flutter/view/AccessibilityBridge$Action;->value:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method hadFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z
    .locals 2
    .param p1, "flag"    # Lio/flutter/view/AccessibilityBridge$Flag;

    .line 1028
    nop

    .line 1029
    iget v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousFlags:I

    iget v1, p1, Lio/flutter/view/AccessibilityBridge$Flag;->value:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method hasAction(Lio/flutter/view/AccessibilityBridge$Action;)Z
    .locals 2
    .param p1, "action"    # Lio/flutter/view/AccessibilityBridge$Action;

    .line 1016
    iget v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->actions:I

    iget v1, p1, Lio/flutter/view/AccessibilityBridge$Action;->value:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z
    .locals 2
    .param p1, "flag"    # Lio/flutter/view/AccessibilityBridge$Flag;

    .line 1024
    iget v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->flags:I

    iget v1, p1, Lio/flutter/view/AccessibilityBridge$Flag;->value:I

    and-int/2addr v0, v1

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0
.end method

.method hitTest([F)Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    .locals 12
    .param p1, "point"    # [F

    .line 1185
    const/4 v0, 0x3

    aget v0, p1, v0

    .line 1186
    .local v0, "w":F
    const/4 v1, 0x0

    aget v2, p1, v1

    div-float/2addr v2, v0

    .line 1187
    .local v2, "x":F
    const/4 v3, 0x1

    aget v3, p1, v3

    div-float/2addr v3, v0

    .line 1188
    .local v3, "y":F
    iget v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->left:F

    cmpg-float v4, v2, v4

    if-ltz v4, :cond_4

    iget v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->right:F

    cmpl-float v4, v2, v4

    if-gez v4, :cond_4

    iget v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->top:F

    cmpg-float v4, v3, v4

    if-ltz v4, :cond_4

    iget v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->bottom:F

    cmpl-float v4, v3, v4

    if-ltz v4, :cond_0

    goto :goto_2

    .line 1189
    :cond_0
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInHitTestOrder:Ljava/util/List;

    if-eqz v4, :cond_3

    .line 1190
    const/4 v4, 0x4

    new-array v4, v4, [F

    .line 1191
    .local v4, "transformedPoint":[F
    nop

    .local v1, "i":I
    :goto_0
    iget-object v5, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInHitTestOrder:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    if-ge v1, v5, :cond_3

    .line 1192
    iget-object v5, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInHitTestOrder:Ljava/util/List;

    invoke-interface {v5, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v5

    move-object v11, v5

    check-cast v11, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 1193
    .local v11, "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    sget-object v5, Lio/flutter/view/AccessibilityBridge$Flag;->IS_HIDDEN:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {v11, v5}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 1194
    goto :goto_1

    .line 1196
    :cond_1
    invoke-direct {v11}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->ensureInverseTransform()V

    .line 1197
    const/4 v6, 0x0

    iget-object v7, v11, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->inverseTransform:[F

    const/4 v8, 0x0

    const/4 v10, 0x0

    move-object v5, v4

    move-object v9, p1

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->multiplyMV([FI[FI[FI)V

    .line 1198
    invoke-virtual {v11, v4}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hitTest([F)Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    move-result-object v5

    .line 1199
    .local v5, "result":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    if-eqz v5, :cond_2

    .line 1200
    return-object v5

    .line 1191
    .end local v5    # "result":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    .end local v11    # "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    :cond_2
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1204
    .end local v1    # "i":I
    .end local v4    # "transformedPoint":[F
    :cond_3
    return-object p0

    .line 1188
    :cond_4
    :goto_2
    const/4 v1, 0x0

    return-object v1
.end method

.method isFocusable()Z
    .locals 4

    .line 1212
    sget-object v0, Lio/flutter/view/AccessibilityBridge$Flag;->SCOPES_ROUTE:Lio/flutter/view/AccessibilityBridge$Flag;

    invoke-virtual {p0, v0}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hasFlag(Lio/flutter/view/AccessibilityBridge$Flag;)Z

    move-result v0

    const/4 v1, 0x0

    if-eqz v0, :cond_0

    .line 1213
    return v1

    .line 1215
    :cond_0
    sget-object v0, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_RIGHT:Lio/flutter/view/AccessibilityBridge$Action;

    iget v0, v0, Lio/flutter/view/AccessibilityBridge$Action;->value:I

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_LEFT:Lio/flutter/view/AccessibilityBridge$Action;

    iget v2, v2, Lio/flutter/view/AccessibilityBridge$Action;->value:I

    or-int/2addr v0, v2

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_UP:Lio/flutter/view/AccessibilityBridge$Action;

    iget v2, v2, Lio/flutter/view/AccessibilityBridge$Action;->value:I

    or-int/2addr v0, v2

    sget-object v2, Lio/flutter/view/AccessibilityBridge$Action;->SCROLL_DOWN:Lio/flutter/view/AccessibilityBridge$Action;

    iget v2, v2, Lio/flutter/view/AccessibilityBridge$Action;->value:I

    or-int/2addr v0, v2

    .line 1217
    .local v0, "scrollableActions":I
    iget v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->actions:I

    xor-int/lit8 v3, v0, -0x1

    and-int/2addr v2, v3

    if-nez v2, :cond_4

    iget v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->flags:I

    if-nez v2, :cond_4

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    .line 1218
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_1
    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->value:Ljava/lang/String;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->value:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    :cond_2
    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hint:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hint:Ljava/lang/String;

    .line 1219
    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_3

    goto :goto_0

    :cond_3
    goto :goto_1

    :cond_4
    :goto_0
    const/4 v1, 0x1

    :goto_1
    return v1
.end method

.method log(Ljava/lang/String;Z)V
    .locals 3
    .param p1, "indent"    # Ljava/lang/String;
    .param p2, "recursive"    # Z

    .line 1045
    const-string v0, "FlutterView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "SemanticsObject id="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->id:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " label="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, " actions="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->actions:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, " flags="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->flags:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "  +-- textDirection="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textDirection:Lio/flutter/view/AccessibilityBridge$TextDirection;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "  +-- rect.ltrb=("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->left:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->top:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->right:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->bottom:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    const-string v2, ")\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "  +-- transform="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->transform:[F

    .line 1050
    invoke-static {v2}, Ljava/util/Arrays;->toString([F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "\n"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1045
    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1051
    iget-object v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    if-eqz v0, :cond_0

    if-eqz p2, :cond_0

    .line 1052
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1053
    .local v0, "childIndent":Ljava/lang/String;
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 1054
    .local v2, "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    invoke-virtual {v2, v0, p2}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->log(Ljava/lang/String;Z)V

    .line 1055
    .end local v2    # "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    goto :goto_0

    .line 1057
    .end local v0    # "childIndent":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method updateRecursively([FLjava/util/Set;Z)V
    .locals 16
    .param p1, "ancestorTransform"    # [F
    .param p3, "forceUpdate"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([F",
            "Ljava/util/Set<",
            "Lio/flutter/view/AccessibilityBridge$SemanticsObject;",
            ">;Z)V"
        }
    .end annotation

    .line 1254
    .local p2, "visitedObjects":Ljava/util/Set;, "Ljava/util/Set<Lio/flutter/view/AccessibilityBridge$SemanticsObject;>;"
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1256
    iget-boolean v2, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalGeometryDirty:Z

    if-eqz v2, :cond_0

    .line 1257
    const/4 v2, 0x1

    goto :goto_0

    .line 1260
    :cond_0
    move/from16 v2, p3

    .end local p3    # "forceUpdate":Z
    .local v2, "forceUpdate":Z
    :goto_0
    const/4 v3, 0x0

    if-eqz v2, :cond_3

    .line 1261
    iget-object v4, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalTransform:[F

    if-nez v4, :cond_1

    .line 1262
    const/16 v4, 0x10

    new-array v4, v4, [F

    iput-object v4, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalTransform:[F

    .line 1264
    :cond_1
    iget-object v5, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalTransform:[F

    const/4 v6, 0x0

    const/4 v8, 0x0

    iget-object v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->transform:[F

    const/4 v10, 0x0

    move-object/from16 v7, p1

    invoke-static/range {v5 .. v10}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    .line 1266
    const/4 v4, 0x4

    new-array v5, v4, [F

    .line 1267
    .local v5, "sample":[F
    const/4 v6, 0x2

    const/4 v7, 0x0

    aput v7, v5, v6

    .line 1268
    const/4 v6, 0x3

    const/high16 v7, 0x3f800000    # 1.0f

    aput v7, v5, v6

    .line 1270
    new-array v6, v4, [F

    .line 1271
    .local v6, "point1":[F
    new-array v7, v4, [F

    .line 1272
    .local v7, "point2":[F
    new-array v8, v4, [F

    .line 1273
    .local v8, "point3":[F
    new-array v4, v4, [F

    .line 1275
    .local v4, "point4":[F
    iget v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->left:F

    aput v9, v5, v3

    .line 1276
    iget v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->top:F

    const/4 v10, 0x1

    aput v9, v5, v10

    .line 1277
    iget-object v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalTransform:[F

    invoke-direct {v0, v6, v9, v5}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->transformPoint([F[F[F)V

    .line 1279
    iget v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->right:F

    aput v9, v5, v3

    .line 1280
    iget v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->top:F

    aput v9, v5, v10

    .line 1281
    iget-object v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalTransform:[F

    invoke-direct {v0, v7, v9, v5}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->transformPoint([F[F[F)V

    .line 1283
    iget v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->right:F

    aput v9, v5, v3

    .line 1284
    iget v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->bottom:F

    aput v9, v5, v10

    .line 1285
    iget-object v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalTransform:[F

    invoke-direct {v0, v8, v9, v5}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->transformPoint([F[F[F)V

    .line 1287
    iget v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->left:F

    aput v9, v5, v3

    .line 1288
    iget v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->bottom:F

    aput v9, v5, v10

    .line 1289
    iget-object v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalTransform:[F

    invoke-direct {v0, v4, v9, v5}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->transformPoint([F[F[F)V

    .line 1291
    iget-object v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalRect:Landroid/graphics/Rect;

    if-nez v9, :cond_2

    new-instance v9, Landroid/graphics/Rect;

    invoke-direct {v9}, Landroid/graphics/Rect;-><init>()V

    iput-object v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalRect:Landroid/graphics/Rect;

    .line 1293
    :cond_2
    iget-object v9, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalRect:Landroid/graphics/Rect;

    aget v11, v6, v3

    aget v12, v7, v3

    aget v13, v8, v3

    aget v14, v4, v3

    invoke-direct {v0, v11, v12, v13, v14}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->min(FFFF)F

    move-result v11

    invoke-static {v11}, Ljava/lang/Math;->round(F)I

    move-result v11

    aget v12, v6, v10

    aget v13, v7, v10

    aget v14, v8, v10

    aget v15, v4, v10

    .line 1294
    invoke-direct {v0, v12, v13, v14, v15}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->min(FFFF)F

    move-result v12

    invoke-static {v12}, Ljava/lang/Math;->round(F)I

    move-result v12

    aget v13, v6, v3

    aget v14, v7, v3

    aget v15, v8, v3

    aget v10, v4, v3

    .line 1295
    invoke-direct {v0, v13, v14, v15, v10}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->max(FFFF)F

    move-result v10

    invoke-static {v10}, Ljava/lang/Math;->round(F)I

    move-result v10

    const/4 v13, 0x1

    aget v14, v6, v13

    aget v15, v7, v13

    aget v3, v8, v13

    aget v13, v4, v13

    .line 1296
    invoke-direct {v0, v14, v15, v3, v13}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->max(FFFF)F

    move-result v3

    invoke-static {v3}, Ljava/lang/Math;->round(F)I

    move-result v3

    .line 1293
    invoke-virtual {v9, v11, v12, v10, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 1298
    const/4 v3, 0x0

    iput-boolean v3, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalGeometryDirty:Z

    .line 1301
    .end local v4    # "point4":[F
    .end local v5    # "sample":[F
    .end local v6    # "point1":[F
    .end local v7    # "point2":[F
    .end local v8    # "point3":[F
    :cond_3
    nop

    .line 1302
    nop

    .line 1304
    iget-object v4, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    if-eqz v4, :cond_4

    .line 1305
    nop

    .local v3, "i":I
    :goto_1
    iget-object v4, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v3, v4, :cond_4

    .line 1306
    iget-object v4, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    iget-object v5, v0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalTransform:[F

    invoke-virtual {v4, v5, v1, v2}, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->updateRecursively([FLjava/util/Set;Z)V

    .line 1305
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 1310
    .end local v3    # "i":I
    :cond_4
    return-void
.end method

.method updateWith(Ljava/nio/ByteBuffer;[Ljava/lang/String;)V
    .locals 8
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "strings"    # [Ljava/lang/String;

    .line 1060
    const/4 v0, 0x1

    iput-boolean v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hadPreviousConfig:Z

    .line 1061
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->value:Ljava/lang/String;

    iput-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousValue:Ljava/lang/String;

    .line 1062
    iget-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    iput-object v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousLabel:Ljava/lang/String;

    .line 1063
    iget v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->flags:I

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousFlags:I

    .line 1064
    iget v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->actions:I

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousActions:I

    .line 1065
    iget v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionBase:I

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousTextSelectionBase:I

    .line 1066
    iget v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionExtent:I

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousTextSelectionExtent:I

    .line 1067
    iget v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollPosition:F

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousScrollPosition:F

    .line 1068
    iget v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollExtentMax:F

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousScrollExtentMax:F

    .line 1069
    iget v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollExtentMin:F

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->previousScrollExtentMin:F

    .line 1071
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->flags:I

    .line 1072
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->actions:I

    .line 1073
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionBase:I

    .line 1074
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textSelectionExtent:I

    .line 1075
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollChildren:I

    .line 1076
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollIndex:I

    .line 1077
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v1

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollPosition:F

    .line 1078
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v1

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollExtentMax:F

    .line 1079
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v1

    iput v1, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->scrollExtentMin:F

    .line 1081
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 1082
    .local v1, "stringIndex":I
    const/4 v2, -0x1

    const/4 v3, 0x0

    if-ne v1, v2, :cond_0

    move-object v4, v3

    goto :goto_0

    :cond_0
    aget-object v4, p2, v1

    :goto_0
    iput-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->label:Ljava/lang/String;

    .line 1084
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 1085
    if-ne v1, v2, :cond_1

    move-object v4, v3

    goto :goto_1

    :cond_1
    aget-object v4, p2, v1

    :goto_1
    iput-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->value:Ljava/lang/String;

    .line 1087
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 1088
    if-ne v1, v2, :cond_2

    move-object v4, v3

    goto :goto_2

    :cond_2
    aget-object v4, p2, v1

    :goto_2
    iput-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->increasedValue:Ljava/lang/String;

    .line 1090
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 1091
    if-ne v1, v2, :cond_3

    move-object v4, v3

    goto :goto_3

    :cond_3
    aget-object v4, p2, v1

    :goto_3
    iput-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->decreasedValue:Ljava/lang/String;

    .line 1093
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v1

    .line 1094
    if-ne v1, v2, :cond_4

    move-object v2, v3

    goto :goto_4

    :cond_4
    aget-object v2, p2, v1

    :goto_4
    iput-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->hint:Ljava/lang/String;

    .line 1096
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v2

    invoke-static {v2}, Lio/flutter/view/AccessibilityBridge$TextDirection;->fromInt(I)Lio/flutter/view/AccessibilityBridge$TextDirection;

    move-result-object v2

    iput-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->textDirection:Lio/flutter/view/AccessibilityBridge$TextDirection;

    .line 1098
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v2

    iput v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->left:F

    .line 1099
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v2

    iput v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->top:F

    .line 1100
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v2

    iput v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->right:F

    .line 1101
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v2

    iput v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->bottom:F

    .line 1103
    iget-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->transform:[F

    const/16 v4, 0x10

    if-nez v2, :cond_5

    .line 1104
    new-array v2, v4, [F

    iput-object v2, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->transform:[F

    .line 1106
    :cond_5
    const/4 v2, 0x0

    const/4 v5, 0x0

    .local v5, "i":I
    :goto_5
    if-ge v5, v4, :cond_6

    .line 1107
    iget-object v6, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->transform:[F

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getFloat()F

    move-result v7

    aput v7, v6, v5

    .line 1106
    add-int/lit8 v5, v5, 0x1

    goto :goto_5

    .line 1109
    .end local v5    # "i":I
    :cond_6
    iput-boolean v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->inverseTransformDirty:Z

    .line 1110
    iput-boolean v0, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->globalGeometryDirty:Z

    .line 1112
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    .line 1113
    .local v0, "childCount":I
    if-nez v0, :cond_7

    .line 1114
    iput-object v3, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    .line 1115
    iput-object v3, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInHitTestOrder:Ljava/util/List;

    goto :goto_a

    .line 1117
    :cond_7
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    if-nez v4, :cond_8

    .line 1118
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    goto :goto_6

    .line 1120
    :cond_8
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 1122
    :goto_6
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_7
    if-ge v4, v0, :cond_9

    .line 1123
    iget-object v5, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->this$0:Lio/flutter/view/AccessibilityBridge;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    invoke-static {v5, v6}, Lio/flutter/view/AccessibilityBridge;->access$300(Lio/flutter/view/AccessibilityBridge;I)Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    move-result-object v5

    .line 1124
    .local v5, "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    iput-object p0, v5, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->parent:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 1125
    iget-object v6, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInTraversalOrder:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1122
    .end local v5    # "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    add-int/lit8 v4, v4, 0x1

    goto :goto_7

    .line 1128
    .end local v4    # "i":I
    :cond_9
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInHitTestOrder:Ljava/util/List;

    if-nez v4, :cond_a

    .line 1129
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInHitTestOrder:Ljava/util/List;

    goto :goto_8

    .line 1131
    :cond_a
    iget-object v4, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInHitTestOrder:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->clear()V

    .line 1133
    :goto_8
    const/4 v4, 0x0

    .restart local v4    # "i":I
    :goto_9
    if-ge v4, v0, :cond_b

    .line 1134
    iget-object v5, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->this$0:Lio/flutter/view/AccessibilityBridge;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v6

    invoke-static {v5, v6}, Lio/flutter/view/AccessibilityBridge;->access$300(Lio/flutter/view/AccessibilityBridge;I)Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    move-result-object v5

    .line 1135
    .restart local v5    # "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    iput-object p0, v5, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->parent:Lio/flutter/view/AccessibilityBridge$SemanticsObject;

    .line 1136
    iget-object v6, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->childrenInHitTestOrder:Ljava/util/List;

    invoke-interface {v6, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1133
    .end local v5    # "child":Lio/flutter/view/AccessibilityBridge$SemanticsObject;
    add-int/lit8 v4, v4, 0x1

    goto :goto_9

    .line 1139
    .end local v4    # "i":I
    :cond_b
    :goto_a
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v4

    .line 1140
    .local v4, "actionCount":I
    if-nez v4, :cond_c

    .line 1141
    iput-object v3, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->customAccessibilityActions:Ljava/util/List;

    goto :goto_e

    .line 1143
    :cond_c
    iget-object v3, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->customAccessibilityActions:Ljava/util/List;

    if-nez v3, :cond_d

    .line 1144
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3, v4}, Ljava/util/ArrayList;-><init>(I)V

    iput-object v3, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->customAccessibilityActions:Ljava/util/List;

    goto :goto_b

    .line 1147
    :cond_d
    iget-object v3, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->customAccessibilityActions:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 1149
    :goto_b
    nop

    .local v2, "i":I
    :goto_c
    if-ge v2, v4, :cond_10

    .line 1150
    iget-object v3, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->this$0:Lio/flutter/view/AccessibilityBridge;

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v5

    invoke-static {v3, v5}, Lio/flutter/view/AccessibilityBridge;->access$400(Lio/flutter/view/AccessibilityBridge;I)Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    move-result-object v3

    .line 1151
    .local v3, "action":Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    iget v5, v3, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->overrideId:I

    sget-object v6, Lio/flutter/view/AccessibilityBridge$Action;->TAP:Lio/flutter/view/AccessibilityBridge$Action;

    iget v6, v6, Lio/flutter/view/AccessibilityBridge$Action;->value:I

    if-ne v5, v6, :cond_e

    .line 1152
    iput-object v3, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->onTapOverride:Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    goto :goto_d

    .line 1153
    :cond_e
    iget v5, v3, Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;->overrideId:I

    sget-object v6, Lio/flutter/view/AccessibilityBridge$Action;->LONG_PRESS:Lio/flutter/view/AccessibilityBridge$Action;

    iget v6, v6, Lio/flutter/view/AccessibilityBridge$Action;->value:I

    if-ne v5, v6, :cond_f

    .line 1154
    iput-object v3, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->onLongPressOverride:Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;

    goto :goto_d

    .line 1158
    :cond_f
    nop

    .line 1159
    iget-object v5, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->customAccessibilityActions:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1161
    :goto_d
    iget-object v5, p0, Lio/flutter/view/AccessibilityBridge$SemanticsObject;->customAccessibilityActions:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1149
    .end local v3    # "action":Lio/flutter/view/AccessibilityBridge$CustomAccessibilityAction;
    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    .line 1164
    .end local v2    # "i":I
    :cond_10
    :goto_e
    return-void
.end method
