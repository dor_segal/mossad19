.class public final Lio/flutter/plugins/GeneratedPluginRegistrant;
.super Ljava/lang/Object;
.source "GeneratedPluginRegistrant.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .line 8
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static alreadyRegisteredWith(Lio/flutter/plugin/common/PluginRegistry;)Z
    .locals 2
    .param p0, "registry"    # Lio/flutter/plugin/common/PluginRegistry;

    .line 16
    const-class v0, Lio/flutter/plugins/GeneratedPluginRegistrant;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    .line 17
    .local v0, "key":Ljava/lang/String;
    invoke-interface {p0, v0}, Lio/flutter/plugin/common/PluginRegistry;->hasPlugin(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 18
    const/4 v1, 0x1

    return v1

    .line 20
    :cond_0
    invoke-interface {p0, v0}, Lio/flutter/plugin/common/PluginRegistry;->registrarFor(Ljava/lang/String;)Lio/flutter/plugin/common/PluginRegistry$Registrar;

    .line 21
    const/4 v1, 0x0

    return v1
.end method

.method public static registerWith(Lio/flutter/plugin/common/PluginRegistry;)V
    .locals 1
    .param p0, "registry"    # Lio/flutter/plugin/common/PluginRegistry;

    .line 10
    invoke-static {p0}, Lio/flutter/plugins/GeneratedPluginRegistrant;->alreadyRegisteredWith(Lio/flutter/plugin/common/PluginRegistry;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11
    return-void

    .line 13
    :cond_0
    return-void
.end method
